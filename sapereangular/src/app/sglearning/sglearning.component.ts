import { Component, OnInit, OnChanges, ChangeDetectorRef, ViewChild, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { ConstantsService } from '../common/services/constants.service';
import * as shape from 'd3-shape';
import * as d3 from 'd3';
import { cpuUsage } from 'process';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';

function formatDate(date) {
  var day = date.getDate();
  var month = 1+date.getMonth();
  var year = date.getFullYear();
  var result = year + "-" + format2D(month)  + "-" + format2D(day);
  return result;
}

function formatTime( date) {
  var hh = date.getHours();
  var mm = date.getMinutes();
  var result = format2D(hh)  + ":" + format2D(mm);
  //console.log("getFormatedDate result = ", result, current.getMonth());
  return result;
}

function format2D(number) {
  return ""+ ((number<10?'0':'') + number);
}

function formatTime2( date) {
  var hh = date.getHours();
  var mm = date.getMinutes();
  var ss = date.getSeconds();
  var result =  format2D(hh) + ":" + format2D(mm) + ":" + format2D(ss);
  //console.log("getFormatedDate result = ", result, current.getMonth());
  return result;
}

function precise_round(num,decimals) {
  var sign = num >= 0 ? 1 : -1;
  return (Math.round((num*Math.pow(10,decimals)) + (sign*0.001)) / Math.pow(10,decimals)).toFixed(decimals);
}


function getDefaulInitDay() {
  var defaultTime = new Date();
  return formatDate(defaultTime);
}
function getDefaultInitTime() {
  var defaultTime = new Date();
  defaultTime.setTime(defaultTime.getTime());
  return formatTime(defaultTime);
}



function getDefaultTargetDay() {
  var defaultTime = new Date();
  return formatDate(defaultTime);
}
function getDefaultTargetTime() {
  var defaultTime = new Date();
  defaultTime.setTime(defaultTime.getTime() + (1*60*60*1000));
  return formatTime(defaultTime);
}


function timeYYYYMMDDHMtoDate(datePart, timePart) {
    var result = new Date();
    var dateArray = datePart.split("-");
    var year = parseInt(dateArray[0]);
    var month = parseInt(dateArray[1]) - 1;
    var day = parseInt(dateArray[2]);
    result.setFullYear(year);
    result.setMonth(month);
    result.setDate(day);
    var timeArray = timePart.split(":");
    var hh = parseInt(timeArray[0]);
    var mm = parseInt(timeArray[1]);
    console.log("timeYYYYMMDDHMtoDate", timePart, hh, mm);
    result.setHours(hh);
    result.setMinutes(mm);
    result.setSeconds(0);
    return result;
}

function formatTimeWindow(homeTransitionMatrices) {
  var startDate = new Date(homeTransitionMatrices.timeWindow.startDate);
  var endDate =  new Date(homeTransitionMatrices.timeWindow.endDate);
  var sTimeWindow = formatTime(startDate) + "-" + formatTime(endDate);
  return sTimeWindow;
}

@Component({
  selector: 'app-sglearning',
  templateUrl: './sglearning.component.html',
  styleUrls: ['./sglearning.component.scss'],
  encapsulation: ViewEncapsulation.None
})



export class SGLearningComponent implements OnInit  {


  private margin: any = { top: 50, bottom: 50, left: 50, right: 20};

  private listHomeTransitionMatrices = null;
  private mapHomeTransitionMatrices = {};
  private homeTransitionMatrices = null;
  private variables = [];
  private listTimeWindows = [];
  private listStates = [];
  private stateNb = 0;
  private sTimeWindow = "";
  private prediction = {};
  private changeDetectorRef: ChangeDetectorRef;
  private listLocations = [];
  private filter_location = "";
  private filter_start_hour_min = "";
  private filter_start_hour_max = "";
  private prediction_location = "";

  maxDisplayTime = new Date();

  initDate = new Date();
  initDay = getDefaulInitDay();
  initTime = getDefaultInitTime();

  targetDate = new Date();
  targetDay = getDefaultTargetDay();
  targetTime = getDefaultTargetTime();

  private captionYPos = [];

  constructor(private httpClient: HttpClient,private _constant: ConstantsService, public _chartElem: ElementRef, private cd: ChangeDetectorRef) {
    this.changeDetectorRef = cd;
    //this.changeDetectorRef.detectChanges();
    this.httpClient.get(this._constant.baseAppUrl+'energy/getLocations').
      subscribe((result :any[])=> {
        this.listLocations = result;
        console.log("this.listLocations = ", this.listLocations);
        if(this.listLocations.length>0) {
          var defaultLocation = (this.listLocations[0]).value;
          console.log("defaultLocation", defaultLocation);
          if(this.filter_location=="") {
            // Set location by default
            this.filter_location = defaultLocation;
          }
          if(this.prediction_location=="") {
             // Set location by default
            this.prediction_location = defaultLocation;
          }
        }
      });
    this.refreshMatrices();
   }

  selectNormalizedMatrixRow(variableName, timeWindow, rowId) {
    //console.log("selectNormalizedMatrixRow begin", variableName, timeWindow, rowId);
    var subMap = this.mapHomeTransitionMatrices[timeWindow];
    var matrix = subMap.mapNormalizedMatrices[variableName];
    //console.log("selectNormalizedMatrixRow", matrix, matrix.array);
    return matrix.array[rowId];
  }

  selectAllObsMatrixRow(variableName, timeWindow, rowId) {
    //console.log("selectNormalizedMatrixRow begin", variableName, timeWindow, rowId);
    var subMap = this.mapHomeTransitionMatrices[timeWindow];
    var matrix = subMap.mapAllObsMatrices[variableName];
    //console.log("selectNormalizedMatrixRow", matrix, matrix.array);
    return matrix.array[rowId];
  }

  selectAllObsMatrixRowSum(variableName, timeWindow, rowId) {
    //console.log("selectNormalizedMatrixRow", matrix, matrix.array);
    var row = this.selectAllObsMatrixRow(variableName, timeWindow, rowId);
    var sum=0;
    for(var idx in row) {
      sum+=row[idx];
    }
    return sum;
  }

  selectAllObsMatrixCell(variableName, timeWindow, rowId, columnId) {
    //console.log("selectNormalizedMatrixRow begin", variableName, timeWindow, rowId);
    var subMap = this.mapHomeTransitionMatrices[timeWindow];
    var matrix = subMap.mapAllObsMatrices[variableName];
    //console.log("selectAllObsMatrixCell", matrix, matrix.array, matrix.array[rowId]);
    return matrix.array[rowId][columnId];
  }

  getNormalizedMatrixRowClass(variableName, timeWindow, rowId) {
    var row =  this.selectNormalizedMatrixRow(variableName, timeWindow, rowId);
    var sum=0;
    for(var idx in row) {
      sum+=row[idx];
    }
    if(sum==0) {
      return "warning_high";
    }
    return "";
  }

  getNbOfObservations(variableName, timeWindow) {
    var subMap = this.mapHomeTransitionMatrices[timeWindow];
    return subMap.mapNbOfObservations[variableName];
  }

  getNbOfIterations(variableName, timeWindow) {
    var subMap = this.mapHomeTransitionMatrices[timeWindow];
    //console.log("getNbOfIterations", subMap);
    if(subMap) {
      return subMap.mapNbOfIterations[variableName];
    } else {
      return 0;
    }
  }

  refreshMatrices(){
    let filterParams = new HttpParams()
      .set('location', ""+this.filter_location)
      .set('startHourMin', ""+this.filter_start_hour_min)
      .set('startHourMax', ""+this.filter_start_hour_max)
    ;
    console.log("refreshMatrices : filterParams2 = ", filterParams);
    this.httpClient.get(this._constant.baseAppUrl+'energy/allHomeTransitionMatrices', { params: filterParams }).
      subscribe((res :any[])=> {
        this.listHomeTransitionMatrices =  res;
        this.homeTransitionMatrices=null
        this.listTimeWindows = [];
        this.listStates = [];
        //this.changeDetectorRef = cd;
        this.changeDetectorRef.detectChanges();
        console.log("change detector", this.changeDetectorRef);
        console.log(this.listHomeTransitionMatrices);
        for(var idx in this.listHomeTransitionMatrices) {
            var homeTransitionMatrices = this.listHomeTransitionMatrices[idx];
            if(this.homeTransitionMatrices==null) {
              this.homeTransitionMatrices = homeTransitionMatrices;
              console.log("homeTransitionMatrices", this.homeTransitionMatrices);
              this.variables = homeTransitionMatrices.variables;
              console.log("homeTransitionMatrices.statesList", homeTransitionMatrices.statesList);
              for(var idxState in homeTransitionMatrices.statesList) {
                var state = homeTransitionMatrices.statesList[idxState];
                this.listStates.push(state.label);
              }
            }
            var sTimeWindow = formatTimeWindow(homeTransitionMatrices);
            this.sTimeWindow = sTimeWindow;
            this.listTimeWindows.push(sTimeWindow);
            this.mapHomeTransitionMatrices[sTimeWindow] = homeTransitionMatrices;
            /*
            for (const homeTransitionMatrices in this.homeTransitionMatrices.mapMatrices) {
              //console.log("homeTransitionMatrices", this.homeTransitionMatrices);
              for (const variableName in this.homeTransitionMatrices.mapMatrices) {
                //console.log("variableName",variableName, "matrix", this.homeTransitionMatrices.mapMatrices[variableName]);
              }
            }*/
        }
        this.stateNb = this.listStates.length;
        console.log("variables", this.variables);
        console.log("listStates", this.listStates, this.stateNb);
        console.log("listTimeWindows", this.listTimeWindows);
        console.log("mapHomeTransitionMatrices", this.mapHomeTransitionMatrices);
        var test = this.getNbOfIterations("consumed", "09:00-10:00");
        console.log("getNbOfIterations", test);
        var test2 = this.selectNormalizedMatrixRow("consumed", "09:00-10:00", 1);
        console.log("selectNormalizedMatrixRow", test2);
        this.changeDetectorRef.detectChanges();
      });
  }

  getPrediction(){
    this.initDate = timeYYYYMMDDHMtoDate(this.initDay, this.initTime);
    this.targetDate = timeYYYYMMDDHMtoDate(this.targetDay, this.targetTime);
    console.log("getPRedicton targetTime = ", this.targetTime, " targetDate = ", this.targetDate );
    this.httpClient.post(this._constant.baseAppUrl+'energy/getPrediction',
          { "initDate":this.initDate, "targetDate":this.targetDate, "location":this.prediction_location}
          , { responseType: 'json' }).
    subscribe(res => {
      this.prediction = res;
      console.log("getPrediction : this.prediction = ", this.prediction);
      console.log("before changeDetectorRef");
      //this.reload();
      if(this.prediction.hasOwnProperty('mapLastResults')) {
        var mapLastResults = this.prediction['mapLastResults'];
        console.log("mapLastResults", mapLastResults);
        /*
        for(var variable in mapLastResults) {
          var result =  mapLastResults[variable];
          var litProb = result['stateProbabilities'];
          console.log(variable,litProb);
        }*/
      }
      this.changeDetectorRef.detectChanges();

    })
  }

formatTimeSlot(timeSlot) {
  var date1 = new Date(timeSlot.startDate);
  var date2 = new Date(timeSlot.endDate);
  //console.log("formatTimeSlot", sdate1, sdate2);
  var day1 = formatDate(date1);
  var time1 = formatTime(date1);
  var day2 = formatDate(date2);
  var time2 = formatTime(date2);
  var markovWindow = "  (MTW:" + timeSlot.markovTimeWindow.id+")";
  //console.log("formatTimeSlot", day1, day2);
  if(day1 == day2)   {
    return day1 + "  " +  time1 + "-" + time2 + markovWindow;
  } else {
    return day1 + "  " +  time1 + "-" + day2 + " " + time2 + markovWindow;
  }
}

formatDateTime(sdate) {
    var date = new Date(sdate);
    return formatDate(date) + "  " + formatTime(date);
  }

  fnum2(num) {
    if(num==0) {
      return "";
    } else {
      return precise_round(num,2);
    }
  }

  fnum3(num) {
    if(num==0) {
      return "";
    } else {
      return precise_round(num,3);
    }
  }

  reload() {
    console.log("--- refresh page");
    location.reload()
  }


  ngOnInit() {
  }

  ngOnChanges() {
    console.log("ngOnChanges");
  }

  displayTimeSteps() {
    var spanElement = document.getElementById('ngc_time_steps');
    var toDisplay = (spanElement.className=='display_none');
    spanElement.className = toDisplay? 'display_yes' : 'display_none';
  }

  greyLevel(pValue, rowIdx, colIdx){
    if(pValue>0) {
      if(rowIdx>=0 && rowIdx==colIdx && pValue>0.99) {
        return "p_stationary";
      }
      var val2 = 10*pValue;
      var pDec1 = Math.floor(10*pValue);
      //console.log("greyLevel", pValue,val2, pDec1);
      return "p_"+pDec1;
    }
    return "";
  }
}
