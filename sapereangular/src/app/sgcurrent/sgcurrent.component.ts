import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { ConstantsService } from '../common/services/constants.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { C } from 'angular-bootstrap-md/lib/free/utils/keyboard-navigation';
import { Observable, interval, Subscription, timer } from 'rxjs';

function formatTime( date) {
  var hh = date.getHours();
  var mm = date.getMinutes();
  var result = "" + (hh<0?'0':'') + hh + ":" + (mm<10?'0':'') + mm ;
  //console.log("getFormatedDate result = ", result, current.getMonth());
  return result;
}

function format2D(number) {
  return ""+ ((number<10?'0':'') + number);
}

function formatTime2( date) {
  var hh = date.getHours();
  var mm = date.getMinutes();
  var ss = date.getSeconds();
  var result =  format2D(hh) + ":" + format2D(mm) + ":" + format2D(ss);
  //console.log("getFormatedDate result = ", result, current.getMonth());
  return result;
}

function precise_round(num,decimals) {
  var sign = num >= 0 ? 1 : -1;
  return (Math.round((num*Math.pow(10,decimals)) + (sign*0.001)) / Math.pow(10,decimals)).toFixed(decimals);
}

function getDefaultTime() {
  var current = new Date();
  return formatTime(current);
}

function getDefaultTime2(shiftMinutes) {
  var current = new Date();
  current.setTime(current.getTime() + (shiftMinutes*60*1000));
  return formatTime(current);
}



function timeHMtoDate(beginTime) {
    var result = new Date();
    var time = beginTime.split(":");
    var hh = parseInt(time[0]);
    var mm = parseInt(time[1]);
    console.log("setBeginDate", time, hh, mm);
    result.setHours(hh);
    result.setMinutes(mm);
    result.setSeconds(0);
    return result;
}



@Component({
  selector: 'app-energy',
  templateUrl: './sgcurrent.component.html',
  styleUrls: ['./sgcurrent.component.scss']
})
export class SGCurrentComponent implements OnInit {
  private subscription: Subscription;
  everySecond: Observable<number> = timer(0, 1000);

  activateAutoRefresh = false;

  homeContent = {};
  homeTotalHistory = [];

  // Filters
  filter_consumerDeviceCategories = [];
  filter_producerDeviceCategories = [];
  filter_hideExpiredAgents = "NO";
  // Form of consumer creation
  c_beginDate = new Date();
  c_beginTime = getDefaultTime();
  c_endDate = new Date();
  c_endTime = getDefaultTime();
  c_power = 0;
  c_duration = 0;
  c_delayToleranceMinutes = 0;
  c_priorityLevel = "Low";
  c_deviceName = "";
  c_deviceCategory = "";
  //listPriorityLevel = [];

  // Form of producer creation
  p_beginDate = new Date();
  p_beginTime = getDefaultTime();
  p_endDate = new Date();
  p_endTime = getDefaultTime();
  p_power = 0;
  p_duration = 0;
  p_deviceName = "";
  p_deviceCategory = "";

  tab_beginTime = {};
  tab_endTime = {};
  tab_duration = {};
  tab_power = {};
  tab_deviceName = {};
  tab_delayToleranceRatio = {};

  listPriorityLevel = [];
  listDeviceCategoryProducer = [];
  listDeviceCategoryConsumer = [];
  listYesNo = [];
  maxDisplayTime = new Date();

  targetAction = ""; // restart agent or modify agent

  constructor(private httpClient: HttpClient
      ,private _constant: ConstantsService
     // ,private updateSubscription: Subscription

      ) {
    this.refreshHomeContent();
  }

  refreshHomeContent() {
    console.log("refreshHomeContent : filter_hideExpiredAgents = ", this.filter_hideExpiredAgents);
    let filterParams = new HttpParams()
      .set('consumerDeviceCategories', ""+this.filter_consumerDeviceCategories)
      .set('producerDeviceCategories', ""+this.filter_producerDeviceCategories)
      .set('hideExpiredAgents', (this.filter_hideExpiredAgents=='YES')? 'true' : 'false');
    ;
    console.log("refreshHomeContent filterParams = ", filterParams);
    this.httpClient.get(this._constant.baseAppUrl+'energy/homeContent', { params: filterParams }).
      subscribe((res :any[])=> {
        this.homeContent=res;
        this.listYesNo = this.homeContent['listYesNo'];
        this.listPriorityLevel = this.homeContent['listPriorityLevel'];
        this.listDeviceCategoryProducer = this.homeContent['listDeviceCategoryProducer'];
        this.listDeviceCategoryConsumer = this.homeContent['listDeviceCategoryConsumer'];
        console.log('listDeviceCategoryProducer:', this.listDeviceCategoryProducer);
        console.log(this._constant.baseAppUrl+'homeContent');
        console.log("this.httpClient.get : homeContent = ", this.homeContent, this.homeContent['consumers']);
        for (const consumer of this.homeContent['consumers']) {
            var agent = consumer.agentName;
            //console.log("consumer", consumer, agent);
            this.tab_beginTime[agent] = getDefaultTime();
            this.tab_endTime[agent] = getDefaultTime2(60);
            this.tab_power[agent] = consumer.power > 0 ? consumer.power : consumer.disabledPower;
            this.tab_duration[agent] = 0;
            this.tab_delayToleranceRatio[agent] = consumer.delayToleranceRatio;
            // console.log("agent.delayToleranceRatio", consumer, consumer.delayToleranceRatio);
        }
        for (const producer of this.homeContent['producers']) {
            var agent = producer.agentName;
            //console.log("producer", producer, agent);
            this.tab_beginTime[agent] = getDefaultTime();
            this.tab_endTime[agent] = getDefaultTime2(60);
            this.tab_power[agent] = producer.power > 0 ? producer.power : producer.disabledPower;
            this.tab_duration[agent] = 0;
      }
      this.maxDisplayTime.setHours(0);
      this.maxDisplayTime.setMinutes(0);
      this.maxDisplayTime.setSeconds(0);
      this.maxDisplayTime.setMilliseconds(0);
      this.maxDisplayTime.setTime(this.maxDisplayTime.getTime() + 60 * 24*60 * 1000 );
      console.log("maxDisplayTime", this.maxDisplayTime);
    });
  }


  disaplyTime(dateStr) {
    var date = new Date(dateStr);
    //console.log("disaplyHHMM", dateStr, date);
    if(date > this.maxDisplayTime) {
      return "";
    }
  return formatTime2(date);
}

  getClassTRagent(agent) {
    if(agent.hasExpired) {
      return 'tr_expired';
    } else if(agent.isInSpace && agent.agentType=='Consumer' && !agent.isContractAgentInSpace ) {
      return 'tr_contract_agent_not_inspace';
    } else if(!agent.isInSpace) {
      return 'tr_not_inspace';
    } else if(agent.isDisabled) {
      return 'tr_disabled';
    } else {
      return '';
      //return 'tr_ok';
    }
    //class='expired_{{consumer.hasExpired}}'
  }

  getAlertNotInSapce(agent) {
    if(agent.hasExpired) {
      return "";
    } else if(agent.isInSpace){
        console.log("agent.agentType = ", agent.agentType);
        if(agent.agentType=='Consumer' &&  !agent.isContractAgentInSpace) {
          return " [!!CONTRACT AGENT NOT IN SPACE!!]"
        } else {
          return "";
        }
    } else {
      return " [!!NOT IN SPACE!!]"
    }
  }

  getTxtClassConsumer(consumer) {
    if(consumer.hasExpired || consumer.isDisabled)  {
      return "";
    } else if(consumer.missingPower > 0.001) {
        return 'txt_warning_high';
    } else {
      return 'txt_ok';
    }
  }

  getClassMissing(consumer, available) {
    var missingPower = consumer.missingPower
    if(missingPower <= 0.001) {
      return '';
    } else if(missingPower <available ) {
      if(missingPower<0.001) {
        console.log("getClassMissing", missingPower, available);
      }
      var warningDurationSec = consumer.warningDurationSec;
      console.log("getClassMissing warningDurationSec = ", warningDurationSec);
      if(warningDurationSec>=40) {
        return "warning_duration_catastrophic";
      } else if(warningDurationSec>=20) {
        return "warning_duration_veryhigh";
      } else if(warningDurationSec>=10) {
        return "warning_duration_high";
      } else if(warningDurationSec>=5) {
        return "warning_duration_medium";
      } else {
        return "warning_duration_weak";
      }
    } else {
      return '';
    }
  }

  getOffersClass(consumer) {
      if(consumer.offersTotal > 0
         && (consumer.offersTotal >= consumer.missingPower - 0.0001)) {
          return "offers_ok";
      }
      return "";
  }

  getClassMissingRequests( homeTotal) {
    return this.getClassMissing(homeTotal.minMissingRequests, homeTotal.available);
  }


  getClassEndDate(sDate) {
    var date = new Date(sDate);
    //console.log("getClassEndDate", date);
    var current = (new Date()).getTime();
    var time = date.getTime();
    var remain = time - current;
    if(remain<0) {
      return '';
    } else if(remain < 1000*60) {
      return "txt_warning_high";
    } else if(remain < 5*1000*60) {
      return "txt_warning_medium";
    } else {
      return "";
    }
  }

  fnum2(num) {
    if (Math.abs(num) <= 0.0001) {
      return "";
    } else {
      var rounded =  precise_round(num,2);
      if(rounded.length>6) {
        var intPart = rounded.substring(0,  rounded.indexOf("."));
        var floatPart = rounded.substring( rounded.indexOf("."));
        //console.log("fnum2", rounded, rounded.indexOf("."), intPart);
        if(intPart.length>3) {
          var bigPart = intPart.substring(0,intPart.length-3);
          var smallPart = intPart.substring(intPart.length-3);
          //console.log(rounded,intPart, bigPart,  smallPart, floatPart );
          return bigPart + " " + smallPart + floatPart;
        }
      }
      return rounded;
      //return (precise_round(num,2)).toLocaleString('en');
    }
  }

  addConsumer(){
    this.c_beginDate = timeHMtoDate(this.c_beginTime);
    this.c_endDate = timeHMtoDate(this.c_endTime);
    console.log("addConsumer", this.c_beginDate ,"c_priorityLevel", this.c_priorityLevel);
    this.httpClient.post(this._constant.baseAppUrl+'energy/addAgent',
          { "agentType":"Consumer", "beginDate":this.c_beginDate, "endDate":this.c_endDate, "priorityLevel":this.c_priorityLevel
            , "deviceName":this.c_deviceName, "deviceCategoryCode":this.c_deviceCategory
            , "power":this.c_power, "delayToleranceMinutes": this.c_delayToleranceMinutes, "delayToleranceRatio" : 0 }
           , { responseType: 'text' }).
    subscribe(res => {
      console.log("addConsumer : result = ", res);
      this.reload();
    })
  }


  addProducer() {
    this.p_beginDate = timeHMtoDate(this.p_beginTime);
    this.p_endDate = timeHMtoDate(this.p_endTime);
    console.log("addProducer", this.p_beginTime, this.p_beginDate );
    if(this.p_power>0 && this.p_endTime !='') {
      this.httpClient.post(this._constant.baseAppUrl+'energy/addAgent',
      { "agentType":"Producer", "beginDate":this.p_beginDate, "endDate":this.p_endDate, "power":this.p_power
          , "deviceName":this.p_deviceName, "deviceCategoryCode":this.p_deviceCategory }
      , { responseType: 'text' }).
      subscribe(res => {
        console.log("addProducer : res = ", res);
        this.reload();
      })
    }
  }


reload() {
    console.log("--- refresh page");
    location.reload()
}



modify_agent(agent, _targetAction) {
  this.targetAction = _targetAction;
  var agentName = agent.agentName;
  console.log("modify_agent", agent, agentName, " targetAction = ", this.targetAction);
  var oBeginTime = document.getElementById( "beginTime_" + agentName);
  console.log("modify_agent : oBeginTime", oBeginTime);
  oBeginTime.classList.remove("hide");
  var oEndTime = document.getElementById( "endTime_" + agentName);
  oEndTime.classList.remove("hide");
  var oDuration = document.getElementById( "duration_" + agentName);
  //oDuration.classList.remove("hide");
  var oPower = document.getElementById( "power_" + agentName);
  console.log("modify_agent : oPower = ", oPower);
  oPower.classList.remove("hide");
  var oToleance =  document.getElementById( "delayToleranceRatio_" + agentName);
  if(oToleance!=null) {
    oToleance.classList.remove("hide");
  }
  var oTxtBeginTime = document.getElementById( "txt_beginTime_" + agentName);
  oTxtBeginTime.innerHTML="";
  var oTxtEndTime = document.getElementById( "txt_endTime_" + agentName);
  oTxtEndTime.innerHTML="";
  var oTxtPower = document.getElementById( "txt_power_" + agentName);
  oTxtPower.innerHTML="";
  var oTxtTolerance = document.getElementById( "txt_delayToleranceRatio_" + agentName);
  if(oTxtTolerance!=null) {
    oTxtTolerance.innerHTML="";
  }


  // Buttons
  var oRestart = document.getElementById("restart_"+ agentName );
  oRestart.classList.add("hide");
  var oModify = document.getElementById("modify_"+ agentName );
  if(oModify!=null) {
    oModify.classList.add("hide");
  }
  var oStop = document.getElementById("stop_"+ agentName );
  oStop.classList.add("hide");
  var oSave = document.getElementById("save_"+ agentName ); 
  oSave.classList.remove("hide");



}


save_agent(agent) {
  var agentName = agent.agentName;
  console.log("save_agent", agent, agentName);
  var oBeginTime = document.getElementById( "beginTime_" + agentName);
  oBeginTime.classList.add("hide");
  var oEndTime = document.getElementById( "endTime_" + agentName);
  oEndTime.classList.add("hide");
  var oDuration = document.getElementById( "duration_" + agentName);
  if(oDuration!=null) {
    oDuration.classList.add("hide");
  }
  var oPower = document.getElementById( "power_" + agentName);
  oPower.classList.add("hide");
  var oToleance =  document.getElementById( "delayToleranceRatio_" + agentName);
  if(oToleance!=null) {
    oToleance.classList.add("hide");
  }
  // Buttons
  var oRestart = document.getElementById("restart_"+ agentName );
  oRestart.classList.remove("hide");
  var oModify = document.getElementById("modify_"+ agentName );
  oModify.classList.remove("hide");
  var oSave = document.getElementById("save_"+ agentName );
  oSave.classList.add("hide");

  agent.beginDate = timeHMtoDate(this.tab_beginTime[agentName]);
  agent.endDate = timeHMtoDate(this.tab_endTime[agentName]);
  //console.log("modify_agent beginDate", agent.beginDate, "endDate",  agent.endDate);
  agent.power = this.tab_power[agentName];
  console.log("save_agent : agent.power = ", agent.power);
  agent.delayToleranceRatio = this.tab_delayToleranceRatio[agentName];
  if(agent.delayToleranceRatio==null) {
    agent.delayToleranceRatio = 0;
  }

  if(agent.power>0)  {
    console.log("save_agent : targetAction = ", this.targetAction);
    if(this.targetAction == 'restartAgent') {
      this.httpClient.post(this._constant.baseAppUrl+'energy/restartAgent', agent , { responseType: 'text' }).
        subscribe(res => {
          console.log("restartAgent : result = ", res);
          this.reload();
        });
      } else if(this.targetAction == 'modifyAgent') {
        this.httpClient.post(this._constant.baseAppUrl+'energy/modifyAgent', agent , { responseType: 'text' }).
        subscribe(res => {
          console.log("modifyAgent : result = ", res);
          this.reload();
        });
      }
    }
}


applyFilter() {
  //this.reload();
  this.refreshHomeContent();
}


stop_agent(agent) {
  var agentName = agent.agentName;
  console.log("stop_agent", agent, agentName);
  this.httpClient.post(this._constant.baseAppUrl+'energy/stopAgent', agent , { responseType: 'text' }).
    subscribe(res => {
      console.log("stop_agent : result = ", res);
      this.reload();
    });
}


  ngOnInit() {
    console.log("ngOnInit : subscription  = ", this.subscription);
    if(this.activateAutoRefresh) {
      this.subscription = this.everySecond.subscribe((seconds) => {
        this.refreshHomeContent();
      })
    }
  }

  set_c_endTime(event) {
    console.log("set_c_endTime", event, event.target.power);
    this.c_beginDate = timeHMtoDate(this.c_beginTime);
    this.c_endDate = this.c_beginDate;
    this.c_endDate.setTime(this.c_beginDate.getTime() + 60 * this.c_duration * 1000 );
    this.c_endTime = formatTime(this.c_endDate);
    this.c_delayToleranceMinutes = this.c_duration;
    console.log("set_c_endTime", this.c_beginDate.getTime(), this.c_beginTime, this.c_duration, this.c_endTime);
  }

  set_p_endTime(event) {
    console.log("set_p_endTime", event, event.target.power);
    this.p_beginDate = timeHMtoDate(this.p_beginTime);
    this.p_endDate = this.c_beginDate;
    this.p_endDate.setTime(this.p_beginDate.getTime() + 60 * this.p_duration * 1000 );
    this.p_endTime = formatTime(this.p_endDate);
    console.log("set_p_endTime", this.c_beginDate.getTime(), this.p_beginTime, this.p_duration, this.p_endTime);
  }


  toogleDisplay(spanId) {
    console.log("toogleDisplay", spanId);
    var divObj = document.getElementById(spanId);
    //divObj.style.display='yes';
    if(divObj.className=='display_none') {
      divObj.className  = 'display_yes';
    } else {
      divObj.className  = 'display_none';
    }
  }
}
