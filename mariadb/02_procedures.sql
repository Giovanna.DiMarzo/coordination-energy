DELIMITER �


DROP FUNCTION IF EXISTS GET_EFFECTIVE_END_DATE
�
CREATE FUNCTION GET_EFFECTIVE_END_DATE(p_id_event INT) RETURNS datetime
BEGIN
	DECLARE vResult datetime;
	DECLARE vInterruption datetime;
	SET vResult = (SELECT
		IF(event.cancel_date IS NULL OR event.cancel_date > event.expiry_date  , event.expiry_date, event.cancel_date)
		FROM event WHERE event.ID = p_id_event)
	;
	SET vInterruption = (SELECT interruption.begin_date
		FROM event AS interruption WHERE interruption.id_origin = p_id_event and interruption.is_cancel
		ORDER BY interruption.begin_date  LIMIT 0,1)
	;
	IF NOT(vInterruption IS NULL) THEN
		SET vResult = LEAST(vResult , vInterruption);
	END IF
	;
	RETURN vResult
	;
END
�

DROP FUNCTION IF EXISTS GET_ITERATION_ID
�
CREATE FUNCTION GET_ITERATION_ID(p_id_transition_matrix INT, p_date VARCHAR(100) ) RETURNS INT UNSIGNED
BEGIN
	DECLARE vResult INT(11) UNSIGNED;
	SET vResult = (SELECT id FROM transition_matrix_iteration WHERE
		 id_transition_matrix=p_id_transition_matrix
		  AND begin_date<=p_date AND end_date > p_date
		LIMIT 0,1)
	;
	RETURN vResult
	;
END
�



DROP FUNCTION IF EXISTS COMPUTE_OBS_NB_FROM_ITERATIONS
�
CREATE FUNCTION COMPUTE_OBS_NB_FROM_ITERATIONS(p_id_transition_matrix INT, p_itnumber_min INT, p_itnumber_max INT, p_row_idx TINYINT, p_column_idx TINYINT) RETURNS INT
BEGIN
	DECLARE vResult INT(11);
	SET vResult = (SELECT IFNULL(SUM(obs_number),0)
			FROM transition_matrix_iteration AS it
			JOIN transition_matrix_cell_iteration AS it_cell ON it_cell.id_transition_matrix_iteration = it.id
			WHERE it.id_transition_matrix = p_id_transition_matrix
				AND it.number >=p_itnumber_min
				AND it.number <=p_itnumber_max
				AND it_cell.row_idx  = p_row_idx
				AND it_cell.column_idx  = p_column_idx
				AND it.begin_date >= '2021-04-20'
			);
	RETURN vResult
	;
END
�



SELECT event.*
  ,GET_EFFECTIVE_END_DATE(Event.ID) AS effective_end_date
  FROM event
  WHERE NOT event.is_cancel

�




DROP FUNCTION IF EXISTS COMPUTE_WARNING_SUM
�
CREATE FUNCTION COMPUTE_WARNING_SUM(p_id_histo INT) RETURNS DECIMAL(15,3)
BEGIN
	DECLARE vAvailable 		DECIMAL(15,3);
	DECLARE vWarningSum  	DECIMAL(15,3);
	DECLARE vWarningPower  	DECIMAL(15,3);
	DECLARE done INT DEFAULT false;
	DECLARE cursorWarningReq CURSOR FOR
		SELECT power
		FROM TmpRequestContract
		WHERE id_histo = p_id_histo AND power <= total_available AND warning_duration > 0
		ORDER BY 1*warning_duration DESC ;
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = true
   ;
   SET vAvailable = (SELECT IFNULL(total_available,0)
	FROM history
    WHERE id=p_id_histo AND total_missing > 0 and total_available > 0
			AND EXISTS (SELECT 1 FROM TmpRequestContract AS ReqCtr WHERE ReqCtr.id_histo = history.id
			AND ReqCtr.power < ReqCtr.total_available )
	  )
    ;
	SET vWarningSum = 0
	;
	IF(vAvailable > 0 ) THEN
		OPEN cursorWarningReq
		;
		FETCH cursorWarningReq INTO vWarningPower
		;
		WHILE(NOT done) DO
	        IF(vWarningSum + vWarningPower <= vAvailable) THEN
			    SET vWarningSum = vWarningSum + vWarningPower
			    ;
			END IF
			;
	        FETCH cursorWarningReq INTO vWarningPower
	        ;
	    END WHILE
	    ;
		CLOSE cursorWarningReq
		;
	END IF
	;
	RETURN vWarningSum
	;
END
�


DROP FUNCTION IF EXISTS GET_FIRST_WARNING
�
CREATE FUNCTION GET_FIRST_WARNING(p_id_histo INT, p_id_request INT) RETURNS INT UNSIGNED
BEGIN
	DECLARE vCurrent INT(11) UNSIGNED;
	DECLARE vResult INT(11) UNSIGNED;
	DECLARE vPrevious INT(11) UNSIGNED;
	DECLARE vLoopIdx  INT(11) UNSIGNED;
	DECLARE vRecCtrId INT(11) UNSIGNED;

	SET vRecCtrId = (SELECT MAX(ReqCtr.id)
				FROM TmpRequestContract AS ReqCtr
				WHERE ReqCtr.id_request = p_id_request AND ReqCtr.id_histo = p_id_histo  AND ReqCtr.Warning
			)
		;
	IF(vRecCtrId is NULL) THEN
		RETURN vRecCtrId;
	ELSE
		SELECT id_histo, id_histo, id_last into vResult,vCurrent,vPrevious
				FROM TmpRequestContract WHERE id=vRecCtrId
		;
	END IF
	;
	SET vLoopIdx = 0
	;
	WHILE((NOT vRecCtrId IS NULL) AND vLoopIdx < 100) DO
		SET vCurrent = vPrevious
		;
		SET vRecCtrId = (SELECT MAX(ReqCtr.id)
				FROM TmpRequestContract AS ReqCtr
				WHERE ReqCtr.id_request = p_id_request AND ReqCtr.id_histo = vCurrent  AND ReqCtr.Warning
			)
		;
		IF(NOT vRecCtrId IS NULL) THEN
			SELECT id_histo, id_histo, id_last INTO vResult,vCurrent,vPrevious
				FROM TmpRequestContract WHERE id=vRecCtrId
			;
		END IF
		;
		SET vLoopIdx=vLoopIdx+1
		;
	END WHILE
	;
	RETURN vResult
	;
END

�
DROP FUNCTION IF EXISTS GET_DATE_FIRST_WARNING
�
CREATE FUNCTION GET_DATE_FIRST_WARNING(p_id_histo INT, p_id_request INT) RETURNS DATETIME
BEGIN
	DECLARE vResult DATETIME;
	DECLARE vDate  DATETIME;
	DECLARE vLastDateNotWarning DATETIME;
	SET vResult = (SELECT MAX(ReqCtr.date)
				FROM TmpRequestContract AS ReqCtr
				WHERE ReqCtr.id_request = p_id_request AND ReqCtr.id_histo = p_id_histo  AND ReqCtr.Warning
			)
		;
	IF(NOT vResult is NULL) then
		SET vDate = (SELECT date FROM history WHERE id =  p_id_histo)
		;
		SET vLastDateNotWarning = NULL
		;
		SET vLastDateNotWarning = (SELECT ReqCtr.date
				FROM TmpRequestContract AS ReqCtr
				WHERE ReqCtr.id_request = p_id_request
					AND ReqCtr.date < vDate
					AND NOT ReqCtr.Warning
				ORDER BY ReqCtr.date DESC LIMIT 0,1)
		;
		SET vResult =  (SELECT ReqCtr.date
				FROM TmpRequestContract AS ReqCtr
				WHERE ReqCtr.id_request = p_id_request
					AND ReqCtr.date > IFNULL(vLastDateNotWarning,'2000-01-01')
					AND ReqCtr.Warning
					ORDER BY ReqCtr.date LIMIT 0,1)
		;
	END IF
	;
	RETURN vResult
	;
END



�
DROP PROCEDURE IF EXISTS UPDATE_TRANSITION_MATRIX_CELL
�
DROP PROCEDURE IF EXISTS REFRESH_TRANSITION_MATRIX_CELL
�
CREATE PROCEDURE REFRESH_TRANSITION_MATRIX_CELL(IN p_location VARCHAR(32) , p_scenario VARCHAR(32), p_max_iteration_nb INT)
BEGIN
	DROP TEMPORARY  TABLE IF EXISTS TmpComputeObsNb
	;
	CREATE temporary TABLE TmpComputeObsNb AS
		SELECT tr_mtx.*
		 ,cell.*
 		 ,COMPUTE_OBS_NB_FROM_ITERATIONS(tr_mtx.id
			, if(tr_mtx.iteration_number>=p_max_iteration_nb  ,tr_mtx.iteration_number - p_max_iteration_nb ,0)
			 , tr_mtx.iteration_number, cell.row_idx , cell.column_idx) AS new_obs_number
		FROM transition_matrix AS tr_mtx
		JOIN
			(SELECT id_transition_matrix , row_idx , column_idx
				FROM transition_matrix_cell_iteration
				WHERE obs_number > 0
				GROUP BY id_transition_matrix , row_idx , column_idx
			) AS cell ON cell.id_transition_matrix  = tr_mtx.id
		WHERE tr_mtx.location = p_location AND tr_mtx.scenario = p_scenario
	;
	-- SELECT FROM transition_matrix_cell_iteration
	--
	-- DELETE transition_matrix_cell FROM transition_matrix_cell
	-- ;
	INSERT INTO transition_matrix_cell(id_transition_matrix ,row_idx,column_idx,obs_number)
		SELECT id_transition_matrix ,row_idx,column_idx,new_obs_number
		FROM TmpComputeObsNb WHERE new_obs_number > 0
		ON DUPLICATE KEY UPDATE obs_number = TmpComputeObsNb.new_obs_number
	;
 END



�
DROP PROCEDURE IF EXISTS compute_missing_request
�
DROP PROCEDURE IF EXISTS compute_missing_request2
�
CREATE PROCEDURE compute_missing_request2(IN p_date_min DATETIME, p_location VARCHAR(32))
BEGIN
	-- Correct link_history_active_event
	UPDATE link_history_active_event SET id_contract_evt = null  WHERE NOT id_contract_evt IS NULL AND EXISTS (
		SELECT 1 FROM event WHERE event.id = link_history_active_event.id_contract_evt
			AND  event.cancel_date <= link_history_active_event.`date`
		)
    ;
	DROP TABLE IF EXISTS TmpRequestContract
	;
	CREATE TEMPORARY TABLE TmpRequestContract(
			`id` 					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT
		    ,id_histo_last_sreq 	INT(11) UNSIGNED
			,id_histo_first_warning INT(11) UNSIGNED
			,date_last_sreq 		DATETIME
			,date_first_warning 	DATETIME
			,warning 				BIT (1)  NOT NULL DEFAULT b'0'
			,warning_duration 		INT (11) NOT NULL DEFAULT 0
			,id_contract_evt  		INT (11) UNSIGNED NULL
			,durationSec 			INT (11) UNSIGNED
			,contractNb  			INT (11) UNSIGNED
			,contract 				TEXT NULL
			,primary KEY(id)
			) AS
		SELECT histo_req.type
			,histo_req.agent
			,req.begin_date
			,req.expiry_date
			,req.power
			,CONCAT(req.agent, '(',  req.power, ')'  ) AS Label
			-- ,GET_EFFECTIVE_END_DATE(histo_req.id_event)
			,histo_req.id_event AS id_request
			,history.ID as id_histo
			,0 AS id_histo_last_sreq
			,0 AS id_histo_first_warning
			,id_contract_evt IS NULL AND req.power>0 AND req.power < history.total_available AS warning
			,0 AS warning_duration
			,NULL AS date_last_sreq
			,NULL AS date_first_warning
			,history.date
			,history.id_last
			,history.total_available
			,IF(id_contract_evt IS NULL, 0, 1) as contractNb
			,histo_req.id_contract_evt
			-- ,0 as OLD_durationSec
			-- ,NULL as contract
			,(SELECT GROUP_CONCAT(producer.agent_name)
				FROM  link_event_agent AS producer
				WHERE producer.id_event = histo_req.id_contract_evt AND producer.agent_type='Producer'
			) as contract
			,(SELECT IFNULL(UNIX_TIMESTAMP(GET_EFFECTIVE_END_DATE(contract.ID)) - UNIX_TIMESTAMP(contract.begin_date),0)
				FROM event AS contract
				WHERE contract.id = histo_req.id_contract_evt ) AS durationSec
		FROM link_history_active_event AS histo_req
		JOIN history ON history.id = histo_req.id_history
		JOIN event AS req ON req.id = histo_req.id_event
		WHERE histo_req.is_request AND histo_req.location = p_location
		ORDER BY histo_req.agent, histo_req.date
	;
	/*
	UPDATE TmpRequestContract SET contract = (SELECT GROUP_CONCAT(producer.agent_name)
			FROM  link_event_agent AS producer
			WHERE producer.id_event = TmpRequestContract.id_contract_evt AND producer.agent_type='Producer'
			)
		WHERE NOT id_contract_evt IS NULL
	;
	UPDATE TmpRequestContract
		JOIN event AS contract ON contract.id = TmpRequestContract.id_contract_evt
		SET TmpRequestContract.durationSec = UNIX_TIMESTAMP(GET_EFFECTIVE_END_DATE(contract.ID)) - UNIX_TIMESTAMP(contract.begin_date)
		WHERE NOT id_contract_evt IS NULL
	;
	*/
	ALTER TABLE TmpRequestContract ADD KEY(id_request, id_histo)
	;
	-- ALTER TABLE TmpRequestContract ADD KEY(id_histo)
	-- ;
	-- ALTER TABLE TmpRequestContract ADD KEY(id_request, warning)
	-- ;
	UPDATE TmpRequestContract SET date_first_warning  = GET_DATE_FIRST_WARNING(id_histo, id_request) WHERE Warning
	;
	UPDATE TmpRequestContract SET warning_duration = UNIX_TIMESTAMP(date) - UNIX_TIMESTAMP(IFNULL(date_first_warning, begin_date)) WHERE warning
	;
	DROP TABLE IF EXISTS TmpUnsatisfiedRequest
	;
	CREATE TEMPORARY TABLE TmpUnsatisfiedRequest AS
		SELECT id_request
			,agent as consumer
			,power
			,total_available
			,begin_date
			,date_first_warning
			,date AS history_date
			,id_histo
			,label
			,warning
			,warning_duration
			,CONCAT(label, IF(warning, CONCAT(':',warning_duration), '') ) AS label2
		FROM TmpRequestContract
		WHERE TmpRequestContract.id_contract_evt is null AND power>0
	;
	ALTER TABLE TmpUnsatisfiedRequest add KEY(id_request,history_date)
	;
END





�
DROP PROCEDURE IF EXISTS CORRECT_OBS_NUMBERS
�
CREATE PROCEDURE CORRECT_OBS_NUMBERS(IN p_location ENUM('', 'Home', 'Neighborhood') , p_scenario VARCHAR(32), p_start_hour INT, p_variable_name VARCHAR(32), p_row_idx TINYINT , p_column_idx TINYINT , p_obs_number INT)
BEGIN
	DECLARE v_id_time_window INT(11);
	DECLARE v_id_transition_matrix INT(11);
	DECLARE v_id_iteration INT(11);
	SET v_id_time_window=(SELECT id from time_window where start_hour=p_start_hour)
	;
	SET v_id_transition_matrix = (SELECT id FROM transition_matrix WHERE location = p_location AND scenario = p_scenario AND variable_name = p_variable_name and id_time_window = v_id_time_window LIMIT 0,1)
	;
	SET v_id_iteration = (SELECT id FROM transition_matrix_iteration where id_transition_matrix = v_id_transition_matrix ORDER BY `number`  DESC limit 0,1)
	;
	INSERT INTO transition_matrix_cell_iteration(id_transition_matrix_iteration, id_transition_matrix, row_idx ,column_idx, obs_number) values
		 (v_id_iteration,v_id_transition_matrix,p_row_idx,p_column_idx,p_obs_number)
		ON DUPLICATE KEY UPDATE obs_number = p_obs_number
	;
  -- CALL REFRESH_TRANSITION_MATRIX_CELL(3)
END



�
GRANT EXECUTE ON PROCEDURE compute_missing_request2  TO 'learning_agent'@'%'
�
GRANT EXECUTE ON FUNCTION GET_EFFECTIVE_END_DATE TO 'learning_agent'@'%'
�
GRANT EXECUTE ON FUNCTION GET_FIRST_WARNING TO 'learning_agent'@'%'
�
GRANT EXECUTE ON FUNCTION GET_DATE_FIRST_WARNING TO 'learning_agent'@'%'
�
GRANT EXECUTE ON PROCEDURE CORRECT_OBS_NUMBERS TO 'learning_agent'@'%'



DELIMITER �

CALL compute_missing_request2('2021-01-01', '192.168.1.79:10001')
�
SELECT TRC.*
	-- ,GET_FIRST_WARNING(TRC.id_histo, TRC.id_request) as test
	FROM TmpRequestContract as TRC where Warning
