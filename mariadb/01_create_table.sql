DELIMITER �

-- energy.history definition
CREATE TABLE `history` (
  `id` 					INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` 				DATETIME NOT NULL DEFAULT current_timestamp(),
  `id_session` 			VARCHAR(32) NOT NULL,
  `learning_agent` 		VARCHAR(100) NOT NULL,
  `location` 		VARCHAR(32) NOT NULL DEFAULT '',
  `distance` 			TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
  `id_last` 			INT(10) unsigned DEFAULT NULL,
  `id_next` 			INT(10) unsigned DEFAULT NULL,
  `total_produced` 		DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total produced (KWH)',
  `total_provided` 		DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total provided (KWH)',
  `total_requested` 	DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total needed for consumption (KWH)',
  `total_consumed` 		DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total consumed (KWH)',
  `total_available` 	DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total produced - total consumed (KWH)',
  `total_missing` 		DECIMAL(15,3) NOT NULL DEFAULT 0.00 COMMENT 'total requested and not provided (KWH)',
  -- Markov states
  `state_idx_produced` 	TINYINT UNSIGNED NULL,
  `state_idx_requested` TINYINT UNSIGNED NULL,
  `state_idx_consumed` 	TINYINT UNSIGNED NULL,
  `state_idx_available` TINYINT UNSIGNED NULL,
  `state_idx_missing` 	TINYINT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unicity_date_loc` (`date`, `location`),
  CONSTRAINT `link_id_last` FOREIGN KEY (`id_last`) REFERENCES `history` (`id`),
  CONSTRAINT `link_id_next` FOREIGN KEY (`id_next`) REFERENCES `history` (`id`),
  KEY _id_session(`id_session`),
  KEY idx_date (date)
) ENGINE=InnoDB AUTO_INCREMENT=9298 DEFAULT CHARSET=utf8 COMMENT='History of energy production/consumption in a house'
�

-- energy.event definition
CREATE TABLE `event` (
  `id` 					INT(11) NOT NULL AUTO_INCREMENT,
  `creation_time` 		DATETIME DEFAULT current_timestamp(),
  `id_session` 			VARCHAR(32) NOT NULL,
  `id_histo` 			INT(10) unsigned NULL,
  `type` 				ENUM('','PRODUCTION','REQUEST','CONTRACT'
					  	,'PRODUCTION_STOP','REQUEST_STOP','CONTRACT_STOP'
					  	,'PRODUCTION_EXPIRY','REQUEST_EXPIRY','CONTRACT_EXPIRY'
					  	,'PRODUCTION_UPDATE', 'REQUEST_UPDATE', 'CONTRACT_UPDATE') DEFAULT NULL,
  `warning_type` 		VARCHAR(32) NOT NULL DEFAULT '',
  `agent` 				VARCHAR(100) NOT NULL,
  `location` 			VARCHAR(32) NOT NULL DEFAULT '',
  `distance` 			TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
  `device_name` 		VARCHAR(100) NOT NULL DEFAULT '',
  `device_category` 	VARCHAR(100) NOT NULL DEFAULT '',
  `begin_date` 			DATETIME NOT NULL DEFAULT current_timestamp(),
  `expiry_date` 		DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
  `cancel_date` 		DATETIME DEFAULT NULL,
  `duration` 			DECIMAL(15,3) NOT NULL DEFAULT 0.00,
  `power` 				DECIMAL(15,3) NOT NULL,
  `id_origin` 			INT(11) DEFAULT NULL,
  `is_cancel` 			BIT(1) NOT NULL DEFAULT b'0',
  `is_ending` 			BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  CONSTRAINT `link_id_histo` FOREIGN KEY (`id_histo`) REFERENCES `history` (`id`),
  UNIQUE KEY `unicity_1` (`begin_date`,`type`,`agent`),
  KEY idx_begin_date(begin_date),
  KEY idx_expiry_date(expiry_date),
  KEY idx_cancel_date(cancel_date),
  KEY idx_agent(agent),
  KEY id_origin(id_origin)
) ENGINE=InnoDB AUTO_INCREMENT=9637 DEFAULT CHARSET=utf8
�





-- energy.link_event_agent definition

CREATE TABLE `link_event_agent` (
  `id` 				INT(11) NOT NULL AUTO_INCREMENT,
  `id_event` 		INT(11) DEFAULT NULL,
  `agent_type` 		VARCHAR(100) NOT NULL,
  `agent_name` 		VARCHAR(100) NOT NULL,
  `agent_location` 	VARCHAR(32) NOT NULL DEFAULT '',
  `power` 			DECIMAL(15,3) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`),
  KEY `event_id` (`id_event`),
  CONSTRAINT `link_event_agent_ibfk_1` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=746 DEFAULT CHARSET=utf8


�
CREATE TABLE `link_history_active_event` (
  `id` 				INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_history`		INT(10) unsigned  NULL,
  `id_event` 		INT(11) DEFAULT NULL,
  `date` 			DATETIME NOT NULL DEFAULT current_timestamp(),
  `type` 			VARCHAR(100) NOT NULL,
  `agent` 			VARCHAR(100) NOT NULL,
  `location` 		VARCHAR(32) NOT NULL DEFAULT '',
  `power` 			DECIMAL(15,3) NOT NULL,
  `is_request` 		BIT(1) NOT NULL DEFAULT b'0',
  `is_producer` 	BIT(1) NOT NULL DEFAULT b'0',
  `is_contract` 	BIT(1) NOT NULL DEFAULT b'0',
  `id_contract_evt` INT(11) NULL,
   PRIMARY KEY (`id`)
  ,CONSTRAINT `history_active_evt_1` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`)
  ,CONSTRAINT `history_active_evt_2` FOREIGN KEY (`id_history`) REFERENCES `history` (`id`)
  ,CONSTRAINT `history_active_evt_3` FOREIGN KEY (`id_contract_evt`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8



�
CREATE TABLE `single_offer` (
  `id` 					INT(11) NOT NULL AUTO_INCREMENT,
  `id_session` 			VARCHAR(32) NOT NULL,
  `creation_time` 		DATETIME DEFAULT current_timestamp(),
  `deadline` 			DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
  `producer_agent` 		VARCHAR(100) NOT NULL,
  `consumer_agent` 		VARCHAR(100) NOT NULL,
  `production_event_id` INT(11)  NULL,
  `request_event_id` 	INT(11) NULL,
  `contract_event_id` 	INT(11) NULL,
  `power` 				DECIMAL(15,3) NOT NULL,
  `acquitted`  			BIT(1) NOT NULL DEFAULT b'0',
  `used`  				BIT(1) NOT NULL DEFAULT b'0',
  `used_time` 			DATETIME NULL,
  `accepted`  			BIT(1) NOT NULL DEFAULT b'0',
  `acceptance_time` 	DATETIME NULL,
   `log` 				TEXT NOT NULL DEFAULT '',
   `log2` 				TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  CONSTRAINT `link_contract_event_id` FOREIGN KEY (`contract_event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `link_production_event_id` FOREIGN KEY (`production_event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `link_request_event_id` FOREIGN KEY (`request_event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8






�
CREATE TABLE time_window (
	id				INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	days_of_week  	VARCHAR(32) DEFAULT '',
	start_hour		TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
	start_minute	TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
	end_hour		TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
	end_minute		TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
�
CREATE TABLE transition_matrix (
	`id` 					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_time_window`		INT(11) UNSIGNED NOT NULL,
	`variable_name`			VARCHAR(100) NOT NULL,
	`location` 				VARCHAR(32) NOT NULL DEFAULT '',
	`scenario`				VARCHAR(32) NOT NULL DEFAULT '',
	`iteration_number`		INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`learning_agent`		VARCHAR(100) NOT NULL,
	`last_update`			DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
	PRIMARY KEY (`id`),
	UNIQUE KEY `unicity_trmatrix` (`id_time_window`, `variable_name`, `location`, `scenario`),
	CONSTRAINT `time_window_fk` FOREIGN KEY (`id_time_window`) REFERENCES `time_window` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


�
CREATE TABLE transition_matrix_cell (
	id_transition_matrix 	INT(11) UNSIGNED NOT NULL,
	row_idx					TINYINT UNSIGNED NOT NULL,
	column_idx				TINYINT UNSIGNED NOT NULL,
	obs_number				INT(11) UNSIGNED NOT NULL DEFAULT 0.0,
	value					DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	rowsum					INT(11) NOT NULL DEFAULT 0.0,
	CONSTRAINT `link_transition_matrix_fk` FOREIGN KEY (`id_transition_matrix`) REFERENCES `transition_matrix` (`id`),
	UNIQUE KEY `unicity_1` (`id_transition_matrix`, `row_idx`,  `column_idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


�
CREATE TABLE transition_matrix_iteration (
	`id` 						INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`creation_time` 			DATETIME DEFAULT current_timestamp(),
	`id_time_window`			INT(11) UNSIGNED NOT NULL,
	`id_transition_matrix` 		INT(11) UNSIGNED NOT NULL,
	`number` 					INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`begin_date`				DATETIME,
	`end_date`					DATETIME,
	`last_update`				DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
	PRIMARY KEY (`id`),
	UNIQUE KEY `unicity_1` (`id_transition_matrix`, `end_date`),
	CONSTRAINT `link_transition_matrix_fk2` FOREIGN KEY (`id_transition_matrix`) REFERENCES `transition_matrix` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


�
CREATE TABLE transition_matrix_cell_iteration (
	`id_transition_matrix_iteration` 	INT(11) UNSIGNED NOT NULL,
    `id_transition_matrix`	 			INT(11) UNSIGNED NOT NULL,
	`row_idx`							TINYINT UNSIGNED NOT NULL,
	`column_idx`						TINYINT UNSIGNED NOT NULL,
	`obs_number`						INT(11) NOT NULL DEFAULT 0.0,
	CONSTRAINT `fk_transition_matrix_iteration` FOREIGN KEY (`id_transition_matrix_iteration`) REFERENCES `transition_matrix_iteration` (`id`),
	CONSTRAINT `fk3_transition_matrix` FOREIGN KEY (`id_transition_matrix`) REFERENCES `transition_matrix` (`id`),
	UNIQUE KEY `unicity_1_tmcellit` (`id_transition_matrix_iteration`, `row_idx`,  `column_idx`)
)

�
CREATE TABLE prediction (
	`id` 					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`creation_date` 		DATETIME NOT NULL DEFAULT current_timestamp(),
	-- `id_time_window`		INT(11) UNSIGNED NOT NULL,
	`variable_name`			VARCHAR(100) NOT NULL,
	`location` 				VARCHAR(32) NOT NULL DEFAULT '',
	`scenario`				VARCHAR(32) NOT NULL DEFAULT '',
	`initial_date`			DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
	`target_date`			DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
	`random_state_idx`		TINYINT UNSIGNED NOT NULL,
	`random_state_name`		VARCHAR(32) DEFAULT '',
	`learning_window`		INT(11) UNSIGNED NOT NULL,
	`horizon_minutes`		INT(11) unsigned NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


�
CREATE TABLE prediction_item (
	id_prediction			INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	value					DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	state_idx				TINYINT UNSIGNED NOT NULL,
	state_name				VARCHAR(32) DEFAULT '',
	CONSTRAINT `fk3_prediction` FOREIGN KEY (`id_prediction`) REFERENCES `prediction` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

�
CREATE TABLE state_history (
	`id` 					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`creation_date` 		DATETIME NOT NULL DEFAULT current_timestamp(),
	`variable_name`			VARCHAR(100) NOT NULL,
	`location` 				VARCHAR(32) NOT NULL DEFAULT '',
	`scenario`				VARCHAR(32) NOT NULL DEFAULT '',
	`date`					DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
	`date_next`				DATETIME  NULL,
	`value`					DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	`state_idx`				TINYINT UNSIGNED NOT NULL,
	`state_name`			VARCHAR(32) DEFAULT '',
	 PRIMARY KEY (`id`),
	 UNIQUE KEY `unicity_date_variable_name` (`date`, `variable_name`, `location`, `scenario`),
	 KEY(date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8









�
CREATE TABLE device (
	id					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	name				VARCHAR(64) DEFAULT '',
	category 			ENUM ('UNKNOWN','WATER_HEATING', 'HEATING', 'COOKING', 'SHOWERS', 'WASHING_DRYING', 'LIGHTING'
						, 'AUDIOVISUAL', 'COLD_APPLIANCES', 'ICT', 'OTHER'
						, 'WIND_ENG', 'SOLOR_ENG', 'EXTERNAL_ENG', 'BIOMASS_ENG', 'HYDRO_ENG')
						 DEFAULT 'UNKNOWN',
	power_min			DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	power_max			DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	avg_duration		DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	is_producer			BIT(1) NOT NULL DEFAULT b'0',
	priority_level		TINYINT UNSIGNED NOT NULL DEFAULT 0.0,
	PRIMARY KEY (`id`),
	UNIQUE KEY `unicity_device_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


�
CREATE TABLE device_statistic(
	`device_category`	ENUM('UNKNOWN','WATER_HEATING', 'HEATING', 'COOKING', 'SHOWERS', 'WASHING_DRYING', 'LIGHTING'
						, 'AUDIOVISUAL', 'COLD_APPLIANCES', 'ICT', 'OTHER'
						, 'WIND_ENG', 'SOLOR_ENG', 'EXTERNAL_ENG', 'BIOMASS_ENG', 'HYDRO_ENG')
			 			DEFAULT 'UNKNOWN',
	`start_hour`		TINYINT UNSIGNED NOT NULL DEFAULT 0,
	`end_hour`			TINYINT UNSIGNED NOT NULL DEFAULT 0,
	`power`				DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	PRIMARY KEY (`device_category`, `start_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8



�
CREATE TABLE simulator_log(
	`id` 					INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_session` 			VARCHAR(32) NOT NULL,
	`creation_time` 		DATETIME DEFAULT current_timestamp(),
	`device_category`		ENUM ('UNKNOWN','WATER_HEATING', 'HEATING', 'COOKING', 'SHOWERS', 'WASHING_DRYING', 'LIGHTING'
							,'AUDIOVISUAL', 'COLD_APPLIANCES', 'ICT', 'OTHER'
							,'WIND_ENG', 'SOLOR_ENG', 'EXTERNAL_ENG', 'BIOMASS_ENG', 'HYDRO_ENG')
			 				DEFAULT 'UNKNOWN',
	`loop_Number` 			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`power_target`			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`power_target_min`	 	DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	`power_target_max` 		DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	`power`					DECIMAL(10,5) NOT NULL DEFAULT 0.0,
	`is_reached`			BIT(1) NOT NULL DEFAULT b'0',
	`nb_started`			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`nb_modified`			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`nb_stopped`			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`nb_devices`			INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`target_device_combination_found`	BIT(1) NOT NULL DEFAULT b'0',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
