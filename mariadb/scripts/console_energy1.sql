
select  transition_matrix_cell_iteration btmci 

alter table prediction  add  `creation_date` 		DATETIME NOT NULL DEFAULT current_timestamp() after id

alter table prediction drop column horizonMinutes


update transition_matrix  set scenario  = REPLACE(scenario, 'HomeSimulator1', 'HomeSimulator')
SELECT * FROM transition_matrix_cell_iteration
	join transition_matrix_iteration tmi on tmi.id = transition_matrix_cell_iteration.id_transition_matrix_iteration 
	join transition_matrix tm  on tm.id = transition_matrix_cell_iteration.id_transition_matrix 
	where tm.scenario = 'HomeSimulator'


alter table history add   `localization` 		VARCHAR(32) NOT NULL DEFAULT '' after agent_url;
alter table history add     `distance` 			TINYINT UNSIGNED NOT NULL DEFAULT 0.0 after localization;

select * from history

select * from link_history_active_event

select * from event e where  not localization = '192.168.1.79:10001'

update transition_matrix  set scenario  = REPLACE(scenario, 'HomeSimulator1', 'HomeSimulator')


select * from transition_matrix where scenario = 'HomeSimulator'


select * from event e  where agent like '192.168.%'


alter table history drop constraint unicity_date
alter table history add constraint UNIQUE KEY `unicity_date_loc` (`date`, `localization`)
alter table history drop column agent_url

alter table  link_history_active_event add   `locatin` 	VARCHAR(32) NOT NULL DEFAULT '' after agent


alter table history add `total_provided` DECIMAL(15,2) NOT NULL DEFAULT 0.00 COMMENT 'total provided (KWH)' after total_produced


select * from link_event_agent lea where agent_name = 'Prod_N1_1'

select prod.* 
	,(select SUM(link_event_agent.power) 
			from link_event_agent where link_event_agent.agent_name = prod.agent 
	) as provided
	from event as prod
	-- left join link_event_agent on link_event_agent.agent_name = prod.agent 
	where	prod.type='PRODUCTION' and prod.distance = 0


select * from transition_matrix
	
alter table transition_matrix CHANGE  `scope` `location` VARCHAR(32) NOT NULL DEFAULT '';

alter table prediction CHANGE  `scope` `location` VARCHAR(32) NOT NULL DEFAULT '';

alter table state_history CHANGE  `scope` `location` VARCHAR(32) NOT NULL DEFAULT '';

update transition_matrix set  location = '192.168.1.79:10001';
update prediction set  location = '192.168.1.79:10001';
update state_history set  location = '192.168.1.79:10001';

select * from prediction p2  where initial_date >='2021-09-15'







select * from transition_matrix_cell_iteration tmci
 join transition_matrix tm  on tm.id = tmci.id_transition_matrix 
 join transition_matrix_iteration tmi  on tmi.id = tmci.id_transition_matrix_iteration 
where creation_time >='2021-09-16' and tm.location = '192.168.1.79:10001'

select * from transition_matrix_cell tmc
 join transition_matrix tm  on tm.id = tmc.id_transition_matrix 
where  tm.location = '192.168.1.79:10001'


CALL REFRESH_TRANSITION_MATRIX_CELL('192.168.1.79:10001', 'test', 100)


DELIMITER �

DROP TEMPORARY TABLE IF EXISTS TmpTrMatrix
�
CREATE TEMPORARY TABLE TmpTrMatrix AS
SELECT transition_matrix.id
, transition_matrix.variable_name
, transition_matrix.location
, transition_matrix.scenario
, transition_matrix.id_time_window
, transition_matrix.iteration_number
, GET_ITERATION_ID(transition_matrix.id, '2021-09-16 10:54:06' )  AS id_transition_matrix_iteration
 FROM transition_matrix 
 WHERE transition_matrix.id_time_window IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)
 	AND variable_name IN ('requested','produced','consumed','available','missing')
 	AND location = '192.168.1.79:10001'
 	AND scenario = 'HomeSimulator1'
�
SELECT TmpTrMatrix.*
	,(TmpTrMatrix.id_transition_matrix_iteration IS NULL) AS IsNewIteration
	,cell.*
	,IFNULL(cellIt.obs_number,0) AS obs_number_iter
 FROM TmpTrMatrix 
 JOIN transition_matrix_cell AS cell ON cell.id_transition_matrix = TmpTrMatrix.id
 LEFT JOIN transition_matrix_cell_iteration  AS cellIt ON cellIt.id_transition_matrix_iteration = TmpTrMatrix.id_transition_matrix_iteration
 			AND cellIt.row_idx = cell.row_idx AND cellIt.column_idx = cell.column_idx
 WHERE 1
 ORDER BY TmpTrMatrix.ID
 
 
 
 
 
 
 
 
 select * from prediction where initial_date >= '2021-09-17 15:00' and horizon_minutes < 60
 
 
 SELECT horizon_minutes AS horizon
	,variable_name , count(*)
	,location
	,scenario
 	,SUM(is_ok) AS nb_ok
 	,SUM(is_ok) / SUM(1) AS rate_ok
 	from (
		 select p.* 
	 		,sh.`date`	 		
	 		,sh.state_idx 
	 		, sh.state_name 	 	
	 		, (p.random_state_idx = sh.state_idx) as is_ok
	 		, ABS(CONVERT(p.random_state_idx , signed)   -  CONVERT(sh.state_idx, signed) ) as delta_abs
	 		, (CONVERT(p.random_state_idx , signed)   -  CONVERT(sh.state_idx, signed) ) as delta
	 	FROM prediction p
	 	JOIN state_history AS sh ON sh.date<=p.target_date  AND sh.date_next > p.target_date
	 	 		AND sh.variable_name  = p.variable_name
	 	 		AND sh.location  = p.location AND sh.scenario  = p.scenario
	 	WHERE p.initial_date >='2021-09-16 00:00'  AND p.horizon_minutes = 5
 	) AS result
 	GROUP BY variable_name , horizon
 
 
 select  h.*, abs(total_consumed - total_provided) as delta from history h where abs(total_consumed - total_provided) >= 0.01 
 
 
select * from link_event_agent lea where id_event = '428277' -- //agent_name = 'Consumer_N1_27'
 
 SELECT ctr.*  ,(SELECT IFNULL(sum(link2.power),0) FROM link_event_agent AS link2 		WHERE link2.id_event = ctr.id_contract_evt and link2.agent_type = 'Producer') AS provided FROM TmpEvent AS ctr  WHERE ctr.is_selected_local AND ctr.is_contract
 
 
DELIMITER �
DROP TEMPORARY TABLE IF EXISTS TmpEvent
�
CREATE TEMPORARY TABLE TmpEvent(
	 effective_end_date 	DATETIME
	,interruption_date 		DATETIME
	,is_selected 			BIT(1) NOT NULL DEFAULT b'0'
	,is_selected_local 		BIT(1) NOT NULL DEFAULT b'0'
	,id_contract_evt 		INT(11) NULL
	,provided 				DECIMAL(15,2) NOT NULL DEFAULT 0.0
	) AS
	SELECT ID, begin_date
		,expiry_date 		AS effective_end_date
		,type,agent,location,power,distance
		,type IN ('REQUEST', 'REQUEST_UPDATE') 			AS is_request
		,type IN ('PRODUCTION', 'PRODUCTION_UPDATE') 	AS is_producer
		,type IN ('CONTRACT', 'CONTRACT_UPDATE') 		AS is_contract
		,0 					AS is_selected
		,0 					AS is_selected_local
		,NULL 				AS id_contract_evt
		,0.0 				AS provided
 		,event.distance=0 	AS is_local
	FROM event
	WHERE NOT event.is_ending AND IFNULL(event.cancel_date,'3000-01-01') > '2021-09-20 09:29:38'
�
UPDATE TmpEvent SET interruption_date = (SELECT interruption.begin_date
		FROM event AS interruption WHERE interruption.id_origin = TmpEvent.id AND interruption.is_cancel
		ORDER BY interruption.begin_date  LIMIT 0,1)
�
	UPDATE TmpEvent SET effective_end_date = LEAST(effective_end_date , interruption_date) WHERE NOT interruption_date IS NULL
�
	UPDATE TmpEvent SET is_selected = 1 WHERE begin_date<='2021-09-20 09:29:38' AND effective_end_date > '2021-09-20 09:29:38'
�
	UPDATE TmpEvent SET is_selected_local = is_selected AND is_local
�
DROP TEMPORARY TABLE IF EXISTS TmpContractEvent
�
CREATE TEMPORARY TABLE TmpContractEvent AS
 	SELECT TmpEvent.id, consumer.agent_name AS consumer
 	FROM TmpEvent 
 	JOIN link_event_agent AS consumer ON consumer.id_event = TmpEvent.id AND consumer.agent_type='Consumer'
  	WHERE is_selected AND is_contract
�
ALTER TABLE TmpContractEvent ADD KEY (consumer)
�
UPDATE TmpEvent 
	JOIN TmpContractEvent ON TmpContractEvent.consumer = TmpEvent.agent
	SET TmpEvent.id_contract_evt = TmpContractEvent.id 
	WHERE TmpEvent.is_selected AND TmpEvent.is_request
�
UPDATE TmpEvent SET provided = (SELECT IFNULL(SUM(lea.power),0) 
   		FROM link_event_agent AS lea 
    	JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
		WHERE lea.agent_name = TmpEvent.agent)
	WHERE TmpEvent.is_selected_local AND TmpEvent.is_producer
�
SELECT * from TmpEvent where is_selected_local

select * from link_event_agent 
	where id_event=427788
	
	
	select id_event, agent_type , group_concat(agent_name), SUM(power) from link_event_agent group by id_event , agent_type 

	SELECT FOO.* from (
		select id_event, agent_type , agent_name, power as consumed
		, (select sum(link2.power) from link_event_agent as link2 where link2.id_event = link_event_agent.id_event and link2.agent_type = 'Producer') as provided
		from link_event_agent 
		where agent_type = 'Consumer'
		) as FOO 
		WHERE not consumed = provided 
		
		SELECT ctr.*  
			,(SELECT sum(link2.power) FROM link_event_agent AS link2 		
			 WHERE link2.id_event = ctr.id_event and link2.agent_type = 'Producer') AS provided 
		FROM TmpEvent AS ctr  WHERE ctr.is_selected_local AND ctr.is_contract
	
agent_name='Contract_N1_22'

SELECT IFNULL(SUM(lea.power),0) AS provided FROM link_event_agent AS lea WHERE lea.id_event = '428441' AND lea.agent_type = 'Producer' 


select * from event e 


SELECT '2021-09-20 09:29:38' AS date 
,IFNULL(SUM(TmpEvent.power),0) AS sum_all
,IFNULL(SUM(IF(TmpEvent.is_request, TmpEvent.power,0.0)),0) AS total_requested
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.power,0.0)),0) AS total_produced
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.provided,0.0)),0) AS total_provided
,IFNULL(SUM(IF(TmpEvent.is_contract, TmpEvent.power,0.0)),0) AS total_consumed
	 FROM TmpEvent WHERE is_selected_local
 
	 
	 select * from event e where agent = 'Contract_N1_93'

	  select * from event e where agent = 'Consumer_N1_93'
	 
	 
 select * from single_offer so  where not power = round(power,2) 
select * from event e  where not power = round(power,2) 
 
select h2.*,  abs(total_provided - total_consumed) as gap
  from history h2
	where abs( total_provided - total_consumed) > 0.99 

	
	select * from history h 
	
	-- 430231
	select * from event e  where agent = 'Consumer_N1_59'
	select * from event e  where agent = 'Contract_N1_59'

	
select * from event e  where distance >0 


select * from event e  where `type` = 'CONTRACT_STOP'


select *  from link_event_agent




DELIMITER �
DROP TEMPORARY TABLE IF EXISTS TmpEvent
�
CREATE TEMPORARY TABLE TmpEvent(
	 effective_end_date 	DATETIME
	,interruption_date 		DATETIME
	,is_selected 			BIT(1) NOT NULL DEFAULT b'0'
	,is_selected_location	BIT(1) NOT NULL DEFAULT b'0'
	,id_contract_evt 		INT(11) NULL
	,power					DECIMAL(15,3) NOT NULL DEFAULT 0.0
	,provided 				DECIMAL(15,3) NOT NULL DEFAULT 0.0
	,provided2				TEXT NULL
	) AS
	SELECT ID, begin_date
		,expiry_date 		AS effective_end_date
		,type,agent,location,power,distance
		,type IN ('REQUEST', 'REQUEST_UPDATE') 			AS is_request
		,type IN ('PRODUCTION', 'PRODUCTION_UPDATE') 	AS is_producer
		,type IN ('CONTRACT', 'CONTRACT_UPDATE') 		AS is_contract
		,0 					AS is_selected
		,0 					AS is_selected_location
		,NULL 				AS id_contract_evt
		,0.0 				AS provided
 		,location='192.168.1.79:10001' 	AS is_location_ok
	FROM event
	WHERE NOT event.is_ending AND IFNULL(event.cancel_date,'3000-01-01') > '2021-09-20 19:11:43'
		-- and (event.`type`IN ('PRODUCTION', 'PRODUCTION_UPDATE') or event.agent like '%93')
	    --  and (NOT event.`type`IN ('PRODUCTION', 'PRODUCTION_UPDATE') or event.agent IN ('Prod_N1_3', 'Prod_N1_5'))
�
UPDATE TmpEvent SET interruption_date = (SELECT interruption.begin_date
		FROM event AS interruption WHERE interruption.id_origin = TmpEvent.id AND interruption.is_cancel
		ORDER BY interruption.begin_date  LIMIT 0,1)
�
	UPDATE TmpEvent SET effective_end_date = LEAST(effective_end_date , interruption_date) WHERE NOT interruption_date IS NULL
�
	UPDATE TmpEvent SET is_selected = 1 WHERE begin_date<='2021-09-20 19:11:43' AND effective_end_date > '2021-09-20 19:11:43'
�
	UPDATE TmpEvent SET is_selected_location = is_selected AND is_location_ok
�
DROP TEMPORARY TABLE IF EXISTS TmpRequestEvent
�
CREATE TEMPORARY TABLE TmpRequestEvent AS
  SELECT TmpEvent.id, TmpEvent.agent AS consumer, power, is_location_ok
	  FROM TmpEvent 
  WHERE is_selected AND is_request
�
ALTER TABLE TmpRequestEvent ADD KEY (consumer)
�
DROP TEMPORARY TABLE IF EXISTS TmpContractEvent
�
CREATE TEMPORARY TABLE TmpContractEvent AS
 	SELECT TmpEvent.id, consumer.agent_name AS consumer, TmpEvent.is_location_ok
 	FROM TmpEvent 
 	JOIN link_event_agent AS consumer ON consumer.id_event = TmpEvent.id AND consumer.agent_type='Consumer'
    JOIN TmpRequestEvent ON TmpRequestEvent.consumer = consumer.agent_name  
  	WHERE is_selected AND is_contract
�
ALTER TABLE TmpContractEvent ADD KEY (consumer)
�
UPDATE TmpEvent 
	JOIN TmpContractEvent ON TmpContractEvent.consumer = TmpEvent.agent
	SET TmpEvent.id_contract_evt = TmpContractEvent.id 
	WHERE TmpEvent.is_selected AND TmpEvent.is_request
�
UPDATE TmpEvent SET provided = (SELECT IFNULL(SUM(lea.power),0) 
   		FROM link_event_agent AS lea 
    	JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
		WHERE lea.agent_name = TmpEvent.agent and TmpContractEvent.consumer like '%93')
	WHERE TmpEvent.is_selected_location AND TmpEvent.is_producer
�
UPDATE TmpEvent SET provided2 = (SELECT IFNULL(group_concat(TmpContractEvent.consumer),'') 
   		FROM link_event_agent AS lea 
    	JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
		WHERE lea.agent_name = TmpEvent.agent)
	WHERE TmpEvent.is_selected_location AND TmpEvent.is_producer
�
SELECT '2021-09-20 19:11:43' AS date 
,IFNULL(SUM(TmpEvent.power),0) AS sum_all
,IFNULL(SUM(IF(TmpEvent.is_request, TmpEvent.power,0.0)),0) AS total_requested
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.power,0.0)),0) AS total_produced
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.provided,0.0)),0) AS total_provided
,IFNULL(SUM(IF(TmpEvent.is_contract and not TmpEvent.agent like '%93', TmpEvent.power,0.0)),0) AS total_consumed
	 FROM TmpEvent WHERE is_selected_location

	 
	 
	 
	 
select * from TmpEvent where provided2 like '%93%'
	
	
-- select * FROM link_event_agent
�
SELECT '2021-09-20 19:11:43' AS date 
,IFNULL(SUM(TmpEvent.power),0) AS sum_all
,IFNULL(SUM(IF(TmpEvent.is_request, TmpEvent.power,0.0)),0) AS total_requested
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.power,0.0)),0) AS total_produced
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.provided,0.0)),0) AS total_provided
,IFNULL(SUM(IF(TmpEvent.is_contract, TmpEvent.power,0.0)),0) AS total_consumed
	 FROM TmpEvent WHERE is_selected_location

	 
	 
	 
	 
	 
	 
	 
	 
	 
SELECT TmpEvent.*
 , (SELECT IFNULL(SUM(lea.power),0) 
			FROM link_event_agent AS lea
			JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
			WHERE lea.agent_name = TmpEvent.agent) as Test_provided
	FROM TmpEvent 
	-- JOIN TmpContractEvent ON TmpContractEvent.consumer = TmpEvent.agent
	WHERE  1
	
	
	
	
 * 
 * 
 * 
select * FROM link_event_agent AS lea 
			-- JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
			WHERE lea.id_event = '431038'
*/


select * from TmpContractEvent where consumer LIKE '%N1_93'














�
UPDATE TmpEvent 
	JOIN TmpContractEvent ON TmpContractEvent.consumer = TmpEvent.agent
	SET TmpEvent.id_contract_evt = TmpContractEvent.id 
	WHERE TmpEvent.is_selected AND TmpEvent.is_request
�
UPDATE TmpEvent SET provided = (SELECT IFNULL(SUM(lea.power),0) 
   		FROM link_event_agent AS lea 
    	JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event
		WHERE lea.agent_name = TmpEvent.agent)
	WHERE TmpEvent.is_selected_location AND TmpEvent.is_producer
�
SELECT '2021-09-20 17:47:27' AS date 
,IFNULL(SUM(TmpEvent.power),0) AS sum_all
,IFNULL(SUM(IF(TmpEvent.is_request, TmpEvent.power,0.0)),0) AS total_requested
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.power,0.0)),0) AS total_produced
,IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.provided,0.0)),0) AS total_provided
,IFNULL(SUM(IF(TmpEvent.is_contract, TmpEvent.power,0.0)),0) AS total_consumed
	 FROM TmpEvent WHERE is_selected_location