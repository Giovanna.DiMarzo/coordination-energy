package com.energy.util;

public class PermissionException extends Exception {
	private static final long serialVersionUID = 1521L;

	public PermissionException(String message) {
		super(message);
	}
}
