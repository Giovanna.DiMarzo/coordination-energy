package com.energy.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.energy.markov.HomeMarkovStates;
import com.energy.markov.HomeTransitionMatrices;
import com.energy.markov.MarkovState;
import com.energy.markov.MarkovTimeWindow;
import com.energy.model.AgentType;
import com.energy.model.Contract;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.EnergyRequest;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.ExtendedEvent;
import com.energy.model.ExtendedHomeTotal;
import com.energy.model.GlobalOffer;
import com.energy.model.HomeTotal;
import com.energy.model.PriorityLevel;
import com.energy.model.SimulatorLog;
import com.energy.model.SingleOffer;
import com.energy.model.WarningType;
import com.sapereapi.model.PredictionData;
import com.sapereapi.model.PredictionResult;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;

import Jama.Matrix;
import eu.sapere.middleware.node.NodeManager;

public class EnergyDbHelper {
	private static DBConnection dbConnection = null;
	private static EnergyDbHelper instance = null;
	private static String sessionId = SapereUtil.generateSessionId();
	public final static String CR = System.getProperty("line.separator");	// Cariage return
	private static int debugLevel = 0;
	private static SapereLogger logger = SapereLogger.getInstance();

	public static EnergyDbHelper getInstance() {
		if (instance == null) {
			instance = new EnergyDbHelper();
		}
		return instance;
	}

	public EnergyDbHelper() {
		// initialise db connection
		dbConnection = DBConnection.getInstance();
	}

	public static String getSessionId() {
		return sessionId;
	}


	public static DBConnection getDbConnection() {
		return dbConnection;
	}

	public static Event registerEvent(Event event) throws DoublonException {
		return registerEvent(event, null);
	}

	private static Event auxGetEvent(Map<String, Object> row) {
		String agent = ""+ row.get("agent");
		String location = ""+ row.get("location");
		String typeLabel = "" + row.get("type");
		EventType type = EventType.getByLabel(typeLabel);
		String categoryLabel = "" + row.get("device_category");
		DeviceCategory category = DeviceCategory.getByName(categoryLabel);
		Event result= new Event(type, agent, location, SapereUtil.getFloatValue(row, "power"), (Date) row.get("begin_date") , (Date) row.get("expiry_date"), ""+row.get("device_name"), category);
		result.setId(SapereUtil.getLongValue(row, "id"));
		result.setHistoId(SapereUtil.getLongValue(row, "id_histo"));
		if(row.get("warning_type")!=null) {
			result.setWarningType(WarningType.getByLabel(""+row.get("warning_type")));
		}
		return result;
	}

	public static List<ExtendedEvent> retrieveLastSessionEvents() {
		List<ExtendedEvent> result = new ArrayList<ExtendedEvent>();
		Map<String, Object> row = DBConnection.executeSingleRowSelect("SELECT id_session FROM event ORDER BY ID DESC LIMIT 0,1)");
		if(row!=null) {
			String sessionId = ""+row.get("id_session");
			return retrieveSessionEvents(sessionId, true);
		}
		return result;
	}

	public static List<ExtendedEvent> retrieveCurrentSessionEvents() {
		return retrieveSessionEvents(sessionId, true);
	}

	public static List<ExtendedEvent> retrieveSessionEvents(String sessionId, boolean onlyActive) {
		List<ExtendedEvent> result = new ArrayList<ExtendedEvent>();
		StringBuffer query = new StringBuffer("");
		query.append(CR).append("SELECT event.*")
		.append(CR).append(" ,GET_EFFECTIVE_END_DATE(event.id) as effective_end_date")
		.append(CR).append(" ,IF(event.Type IN ('CONTRACT', 'CONTRACT_UPDATE') ")
		.append(CR).append( "	,(SELECT consumer.agent_name FROM link_event_agent AS consumer ")
		.append(CR).append( "			WHERE consumer.id_event = event.id AND consumer.agent_type = 'Consumer' LIMIT 0,1)")
		.append(CR).append( "	, NULL) AS linked_consumer")
		.append(CR).append(" ,IF(event.Type IN ('CONTRACT', 'CONTRACT_UPDATE') ")
		.append(CR).append( "	,(SELECT consumer.agent_location FROM link_event_agent AS consumer ")
		.append(CR).append( "			WHERE consumer.id_event = event.id AND consumer.agent_type = 'Consumer' LIMIT 0,1)")
		.append(CR).append( "	, NULL) AS linked_consumer_location")
		.append(CR).append(" FROM event")
		.append(CR).append(" WHERE event.id_session = ").append(addQuotes(sessionId));
		if(onlyActive) {
			query.append(" AND GET_EFFECTIVE_END_DATE(event.id) > NOW()");
		}
		query.append(CR).append(" ORDER BY event.type, 1*event.agent, event.creation_time ")
		;
		List<Map<String, Object>> rows = DBConnection.executeSelect(query.toString());
		for(Map<String, Object> row: rows) {
			Event nextEvent = auxGetEvent(row);
			ExtendedEvent nextEvent2 = new ExtendedEvent( nextEvent.getType(),  nextEvent.getIssuer(), nextEvent.getIssuerLocation(), nextEvent.getPower()
					, nextEvent.getBeginDate(), nextEvent.getEndDate(), nextEvent.getDeviceName(), nextEvent.getDeviceCategory());
			nextEvent2.setEffectiveEndDate((Date) row.get("effective_end_date"));
			nextEvent2.setHistoId(nextEvent.getHistoId());
			nextEvent2.setWarningType(nextEvent.getWarningType());
			if(row.get("linked_consumer")!=null) {
				nextEvent2.setLinkedConsumer(""+row.get("linked_consumer"));
			}
			if(row.get("linked_consumer_location")!=null) {
				nextEvent2.setLinkedConsumerLocation(""+row.get("linked_consumer_location"));
			}
			result.add(nextEvent2);
		}
		return result;
	}

	public static List<MarkovTimeWindow> retrieveTimeWindows() {
		List<MarkovTimeWindow> result = new ArrayList<MarkovTimeWindow>();
		List<Map<String, Object>> rows = DBConnection.executeSelect("SELECT * FROM time_window");
		for(Map<String, Object> row : rows) {
			String sDaysOfWeek = "" + row.get("days_of_week");
			String[] tabDaysOfWeek = sDaysOfWeek.split(",");
			Set<Integer> daysOfWeek = new HashSet<Integer>();
			for(String nextDayOfWeek : tabDaysOfWeek) {
				daysOfWeek.add(new Integer(nextDayOfWeek));
			}
			int id = SapereUtil.getIntValue(row, "id");
			int startHour = SapereUtil.getIntValue(row, "start_hour");
			int startMinute = SapereUtil.getIntValue(row, "start_minute");
			int endHour = SapereUtil.getIntValue(row, "end_hour");
			int endMinute = SapereUtil.getIntValue(row, "end_minute");
			MarkovTimeWindow timeWindow = new MarkovTimeWindow(id, daysOfWeek, startHour, startMinute, endHour, endMinute);
			result.add(timeWindow);
		}
		return result;
	}
	public static Event retrieveEventById(Long id) {
		List<Map<String, Object>> rows = DBConnection.executeSelect("SELECT * FROM event WHERE id= " + id);
		if (rows.size() > 0) {
			Map<String, Object> row = rows.get(0);
			Event result= auxGetEvent(row);
			return result;
		}
		return null;
	}

	public static Event retrieveEvent(EventType evtType, String agentName, Date beginDate) {
		String sBeginDate = SapereUtil.format_sql.format(beginDate);
		List<Map<String, Object>> rows = DBConnection.executeSelect("SELECT * FROM event WHERE "
				+ " begin_date = '" + sBeginDate + "'"
				+ " AND type = '"	+ evtType+ "' "
				+ " AND agent = '" + agentName + "'");
		if (rows.size() > 0) {
			Map<String, Object> row = rows.get(0);
			Event result= auxGetEvent(row);
			Object oIdOrigin = row.get("id_origin");
			Long originId = null;
			if(oIdOrigin instanceof Long) {
				originId = (Long) oIdOrigin;
			} else if (oIdOrigin instanceof Integer) {
				originId = new Long((Integer) oIdOrigin);
			}
			if(originId!=null) {
				Event originEvent = retrieveEventById(originId);
				result.setOriginEvent(originEvent);
			}
			return result;
		}
		return null;
	}

	public static Event registerEvent2(Event event) {
		return registerEvent2(event, null);
	}

	public static Event registerEvent2(Event event, Contract contract) {
		Event result = null;
		try {
			result = EnergyDbHelper.registerEvent(event, contract);
		} catch (DoublonException e) {
			logger.error(e);
			result =  retrieveEvent(event.getType(), event.getIssuer(), event.getBeginDate());
		}
		return result;
	}

	public static Event registerEvent(Event event, Contract contract) throws DoublonException {
		String sBeginDate = SapereUtil.format_sql.format(event.getBeginDate());
		String sExpiryDate = SapereUtil.format_sql.format(event.getEndDate());
		if(event.getIssuerLocation() == null || "".equals(event.getIssuerLocation())) {
			throw new RuntimeException("registerEvent : location is not set on event " + event.toString());
		}
		// Check duplicate event
		Event doublonCheck = retrieveEvent(event.getType(), event.getIssuer(), event.getBeginDate());
		if(doublonCheck!=null) {
			throw new DoublonException("Event doublon : The client program tries to insert the following event twice : " + event.toString());
		}
		StringBuffer query = new StringBuffer("INSERT INTO event SET ");
		query.append("begin_date = ").append(addQuotes(sBeginDate))
				.append(", expiry_date = ").append(addQuotes(sExpiryDate))
				.append(", id_session = ").append(addQuotes(sessionId))
				.append(", type = ").append(addQuotes(""+event.getType()))
				.append(", warning_type = ").append(event.getWarningType()==null? "''" : addQuotes(event.getWarningType().getLabel()))
				.append(", power = ").append(addQuotes(""+event.getPower()))
				.append(", agent = ").append(addQuotes(event.getIssuer()))
				.append(", location = ").append(addQuotes(event.getIssuerLocation()))
				.append(", distance = ").append(addQuotes(event.getIssuerDistance()))
				.append(", device_name = ").append(addQuotes(event.getDeviceName()))
				.append(", device_category = ").append(addQuotes(event.getDeviceCategory().getLabel()))
				.append(", is_cancel = ").append(event.getType().getIsCancel() ? "1" : "0")
				.append(", is_ending = ").append(event.getType().getIsEnding() ? "1" : "0")
				.append(", id_origin = ").append(event.getOriginEvent() == null ? "NULL" : event.getOriginEvent().getId())
		;
		Long eventId = DBConnection.execUpdate(query.toString());
		Event result = event;
		result.setId(eventId);
		if(contract!=null) {
			if (EventType.CONTRACT.equals(event.getType()) || EventType.CONTRACT_UPDATE.equals(event.getType())) {
				registerLinksEventAgent(eventId, contract);
			}
		}
		DBConnection.execUpdate("UPDATE event SET cancel_date = " + addQuotes(sBeginDate) + " WHERE "
				+ " agent = " + addQuotes(event.getIssuer()) + " AND begin_date < " + addQuotes(sBeginDate)
				+ " AND cancel_date IS NULL");
		return result;
	}

	public static void registerLinksEventAgent(Long eventId, Contract contract) {
		// Consumer agent
		Float globalPower = contract.computePower();
		StringBuffer query = new StringBuffer("INSERT INTO link_event_agent SET ");
		query.append("id_event = '").append(eventId).append("'")
				.append(", agent_type = ").append(addQuotes(AgentType.CONSUMER.getLabel()))
				.append(", agent_name = ").append(addQuotes(contract.getConsumerAgent()))
				.append(", agent_location = ").append(addQuotes(contract.getConsumerLocation()))
				.append(", power = '").append(globalPower).append("'")
				;
		DBConnection.execUpdate(query.toString());
		// Contract agent
		query = new StringBuffer("INSERT INTO link_event_agent SET ");
		query.append("id_event = '").append(eventId).append("'")
				.append(", agent_type = ").append(addQuotes(AgentType.CONTRACT.getLabel()))
				.append(", agent_name = ").append(addQuotes(contract.getContractAgent()))
				.append(", agent_location = ").append(addQuotes(contract.getConsumerLocation()))
				.append(", power = '").append(globalPower).append("'")
			;
		DBConnection.execUpdate(query.toString());
		// Producer agents
		for (String producer : contract.getProducerAgents()) {
			query = new StringBuffer("INSERT INTO link_event_agent SET ");
			Float power = contract.getPowerFromAgent(producer);
			String location = contract.getLocationFromAgent(producer);
			query.append("id_event = '").append(eventId).append("'")
					.append(", agent_type = ").append(addQuotes(AgentType.PRODUCER.getLabel()))
					.append(", agent_name = ").append(addQuotes(producer))
					.append(", agent_location = ").append(addQuotes(location))
					.append(", power = '").append(power).append("'");
			DBConnection.execUpdate(query.toString());
		}
		// Check gap
		if(eventId>0) {
			Map<String, Object> rowCheckGap = DBConnection.executeSingleRowSelect("SELECT IFNULL(SUM(lea.power),0) AS provided FROM link_event_agent AS lea WHERE lea.id_event = " + addQuotes(eventId)
				+ " AND lea.agent_type = 'Producer'");
			float provided = SapereUtil.getFloatValue(rowCheckGap, "provided");
			if(Math.abs(globalPower - provided) > 0.0001) {
				float contractGap = contract.computeGap();
				logger.warning("Gap found in new contract " + rowCheckGap + " contractGap = " + contractGap);
			}
		}
	}

	public static HomeTotal generateHomeTotal(Date computeDate, Long idLast, Event linkedEvent, String url, String agentName, String location) {
		int distance = Sapere.getInstance().getDistance(location);
		//boolean local = (distance==0);
		HomeTotal homeTotal = new HomeTotal();
		homeTotal.setDate(computeDate);
		homeTotal.setLocation(location);
		homeTotal.setDistance(distance);
		//homeTotal.setIdLast(idLast);
		homeTotal.setAgentName(agentName);
		String sComputeDate = SapereUtil.format_sql.format(computeDate);
		String quotedComputeDate = addQuotes(sComputeDate);
		//String distanceFilter = local ? "event.distance=0" : "event.distance > 0";
		String query = "DROP TEMPORARY TABLE IF EXISTS TmpEvent"
				+ CR + "§"
				+ CR + "CREATE TEMPORARY TABLE TmpEvent("
				+ CR + "	 effective_end_date 	DATETIME"
				+ CR + "	,interruption_date 		DATETIME"
				+ CR + "	,is_selected 			BIT(1) NOT NULL DEFAULT b'0'"
				+ CR + "	,is_selected_location	BIT(1) NOT NULL DEFAULT b'0'"
				+ CR + "	,id_contract_evt 		INT(11) NULL"
				+ CR + "	,power					DECIMAL(15,3) NOT NULL DEFAULT 0.0"
				+ CR + "	,provided 				DECIMAL(15,3) NOT NULL DEFAULT 0.0"
				+ CR + "	) AS"
				+ CR + "	SELECT ID, begin_date"
				+ CR + "		,expiry_date 		AS effective_end_date"
				+ CR + "		,type,agent,location,power,distance"
				+ CR + "		,type IN ('REQUEST', 'REQUEST_UPDATE') 			AS is_request"
				+ CR + "		,type IN ('PRODUCTION', 'PRODUCTION_UPDATE') 	AS is_producer"
				+ CR + "		,type IN ('CONTRACT', 'CONTRACT_UPDATE') 		AS is_contract"
				+ CR + "		,0 					AS is_selected"
				+ CR + "		,0 					AS is_selected_location"
				+ CR + "		,NULL 				AS id_contract_evt"
				+ CR + "		,0.0 				AS provided"
				+ CR + " 		,location="+addQuotes(location) +" 	AS is_location_ok"
				+ CR + "	FROM event"
				+ CR + "	WHERE NOT event.is_ending AND IFNULL(event.cancel_date,'3000-01-01') > " + quotedComputeDate
				+ CR + "§"
				+ CR + "UPDATE TmpEvent SET interruption_date = (SELECT interruption.begin_date"
				+ CR + "		FROM event AS interruption WHERE interruption.id_origin = TmpEvent.id AND interruption.is_cancel"
				+ CR + "		ORDER BY interruption.begin_date  LIMIT 0,1)"
				+ CR + "§"
				+ CR +"	UPDATE TmpEvent SET effective_end_date = LEAST(effective_end_date , interruption_date) WHERE NOT interruption_date IS NULL"
				+ CR + "§"
				+ CR +"	UPDATE TmpEvent SET is_selected = 1 WHERE begin_date<=" + quotedComputeDate + " AND effective_end_date > "+ quotedComputeDate
				+ CR + "§"
				+ CR +"	UPDATE TmpEvent SET is_selected_location = is_selected AND is_location_ok"
				+ CR + "§"
				+ CR + "DROP TEMPORARY TABLE IF EXISTS TmpRequestEvent"
				+ CR + "§"
				+ CR + "CREATE TEMPORARY TABLE TmpRequestEvent AS"
				+ CR + "  SELECT TmpEvent.id, TmpEvent.agent AS consumer, power, is_location_ok"
				+ CR +"	  FROM TmpEvent "
				+ CR + "  WHERE is_selected AND is_request"
				+ CR + "§"
				+ CR + "ALTER TABLE TmpRequestEvent ADD KEY (consumer)"
				+ CR + "§"
				+ CR + "DROP TEMPORARY TABLE IF EXISTS TmpContractEvent"
				+ CR + "§"
				+ CR + "CREATE TEMPORARY TABLE TmpContractEvent AS"
				+ CR + " 	SELECT TmpEvent.id, consumer.agent_name AS consumer, TmpEvent.is_location_ok"
				+ CR + " 	FROM TmpEvent "
				+ CR + " 	JOIN link_event_agent AS consumer ON consumer.id_event = TmpEvent.id AND consumer.agent_type='Consumer'"
				+ CR + "    JOIN TmpRequestEvent ON TmpRequestEvent.consumer = consumer.agent_name "
				+ CR+ "  	WHERE is_selected AND is_contract"
				+ CR + "§"
				+ CR + "UPDATE TmpEvent "
				+ CR + "	LEFT JOIN TmpContractEvent on TmpContractEvent.id = tmpevent.ID"
				+ CR + "	SET TmpEvent.is_selected_location = 0, TmpEvent.is_selected = 0"
				+ CR + "	WHERE tmpevent.is_selected AND is_contract  and TmpContractEvent.id is NULL"
				+ CR + "§"
				+ CR + "ALTER TABLE TmpContractEvent ADD KEY (consumer)"
				+ CR + "§"
				+ CR + "UPDATE TmpEvent "
				+ CR + "	JOIN TmpContractEvent ON TmpContractEvent.consumer = TmpEvent.agent"
				+ CR + "	SET TmpEvent.id_contract_evt = TmpContractEvent.id "
				+ CR + "	WHERE TmpEvent.is_selected AND TmpEvent.is_request"
				+ CR + "§"
				+ CR + "UPDATE TmpEvent SET provided = (SELECT IFNULL(SUM(lea.power),0) "
				+ CR + "   		FROM link_event_agent AS lea "
				+ CR + "    	JOIN TmpContractEvent ON TmpContractEvent.id = lea.id_event"
				+ CR + "		WHERE lea.agent_name = TmpEvent.agent)"
				+ CR + "	WHERE TmpEvent.is_selected_location AND TmpEvent.is_producer"
				+ CR + "§"
				+ CR + "SELECT " + quotedComputeDate + " AS date "
				+ CR +",IFNULL(SUM(TmpEvent.power),0) AS sum_all"
				+ CR +",IFNULL(SUM(IF(TmpEvent.is_request, TmpEvent.power,0.0)),0) AS total_requested"
				+ CR +",IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.power,0.0)),0) AS total_produced"
				+ CR +",IFNULL(SUM(IF(TmpEvent.is_producer, TmpEvent.provided,0.0)),0) AS total_provided"
				+ CR +",IFNULL(SUM(IF(TmpEvent.is_contract, TmpEvent.power,0.0)),0) AS total_consumed"
				+ CR +"	 FROM TmpEvent WHERE is_selected_location"
				;
		List<Map<String, Object>> sqlResult = DBConnection.executeSelect(query.toString());
		if (sqlResult.size() > 0) {
			Map<String, Object> row = sqlResult.get(0);
			float requested = SapereUtil.getFloatValue(row, "total_requested");
			float produced = SapereUtil.getFloatValue(row, "total_produced");
			float provided = SapereUtil.getFloatValue(row, "total_provided");
			float consumed = SapereUtil.getFloatValue(row, "total_consumed");
			if(linkedEvent!=null && EventType.REQUEST_EXPIRY.equals(linkedEvent.getType())) {
				logger.info("Request expiry");
			}
			boolean checkGaps = false;
			if(checkGaps) {
				logger.warning("generateHomeTotal step 12345 : consumed = " + consumed + ", provided = " + provided);
				if(consumed > requested + 0.01) {
					logger.warning("Consumed greatedthen requested : consumed = " + consumed + ", requested = " + requested );
					String testGap1 = "SELECT ctr.*,IFNULL(TmpRequestEvent.power,0) AS requested "
							+ " FROM TmpContractEvent "
							+ " LEFT JOIN TmpRequestEvent ON TmpRequestEvent.consumer = TmpContractEvent.consumer "
							+ " WHERE TmpContractEvent.is_selected AND is_location_ok";
					List<Map<String, Object>> rowsTestGap = DBConnection.executeSelect(testGap1);
					float totalConsumed = 0;
					float totalRequested = 0;
					for(Map<String, Object> nextRow : rowsTestGap) {
						float nextConsumed = SapereUtil.getFloatValue(nextRow, "power");
						float nextRequested = SapereUtil.getFloatValue(nextRow, "requested");
						if(nextConsumed  > nextRequested ) {
							logger.info("nextRow = " + nextRow);
							logger.info("Gap found for contract " + nextRow.get("agent") + " conumed = " + nextConsumed + ", nextRequested = "+ nextRequested);
						}
						totalConsumed+=nextConsumed;
						totalRequested+=nextRequested;
					}
					logger.info("totalConsumed = " + totalConsumed + ", totalProvided = " + totalRequested);
				}
			}
			// Consumption cannot be greater than request
			if(consumed > requested) {
				consumed = requested;
			}
			// Provided cannot be greater than produced
			if(provided > produced) {
				provided = produced;
			}
			if(checkGaps) {
				if(Math.abs(consumed - provided) >= 0.99 ) {
					logger.warning("Gap between provided and consumed : " + Math.abs(consumed - provided));
					String testGap1 = "SELECT ctr.* "
							+ " ,(SELECT IFNULL(sum(link2.power),0) FROM link_event_agent AS link2 "
							+ "		WHERE link2.id_event = ctr.id and link2.agent_type = 'Producer') AS provided"
							+ " FROM TmpEvent AS ctr "
							+ " WHERE ctr.is_selected_location AND ctr.is_contract";
					List<Map<String, Object>> rowsTestGap = DBConnection.executeSelect(testGap1);
					float totalConsumed = 0;
					float totalProvided = 0;
					for(Map<String, Object> nextRow : rowsTestGap) {
						float nextConsumed = SapereUtil.getFloatValue(nextRow, "power");
						float nextProvided = SapereUtil.getFloatValue(nextRow, "provided");
						if(Math.abs(nextConsumed - nextProvided) > 0.0001 ) {
							logger.info("nextRow = " + nextRow);
							logger.info("Gap found for contract " + nextRow.get("agent") + " conumed = " + nextConsumed + ", nextProvided = "+ nextProvided);
						}
						totalConsumed+=nextConsumed;
						totalProvided+=nextProvided;
					}
					logger.info("totalConsumed = " + totalConsumed + ", totalProvided = " + totalProvided);
				}
			}
			homeTotal.setRequested(requested);
			homeTotal.setProduced(produced);
			homeTotal.setConsumed(consumed);
			homeTotal.setProvided(provided);
			homeTotal.setAvailable(produced - provided);
			homeTotal.setMissing(requested - consumed);
		}
		if(homeTotal.hasActivity()) {
			Long id = registerHomeTotal(homeTotal, linkedEvent);
			homeTotal.setId(id);
			Date timeBefore = new Date();
			String queryInsertLinkeHistoEvent = "DELETE FROM link_history_active_event WHERE id_history = @new_id_histo"
					+ CR + "§"
					+ "INSERT INTO link_history_active_event(id_history,id_event,date,type,agent,location,power,is_request,is_producer,is_contract,id_contract_evt)"
					+ CR + "SELECT @new_id_histo, id ," + quotedComputeDate  + ",type,agent,location,power,is_request,is_producer,is_contract,id_contract_evt"
					+ CR + " FROM TmpEvent"
					+ CR + " WHERE TmpEvent.is_selected_location"
			;
			DBConnection.execUpdate(queryInsertLinkeHistoEvent);
			long duration = new Date().getTime() - timeBefore.getTime();
			if(duration>50) {
				logger.info("queryInsertLinkeHistoEvent duration (MS) : " + duration);
			}
		}
		return homeTotal;
	}

	public static void resetSimulatorLogs() {
		logger.info("resetSimulatorLogs ");
		DBConnection.execUpdate("DELETE simulator_log FROM simulator_log");
	}

	public static void registerSimulatorLog(SimulatorLog simulatorLog) {
		logger.info("registerSimulatorLog " + simulatorLog);
		StringBuffer query = new StringBuffer();
		query.append("INSERT INTO simulator_log SET ")
			.append(CR).append(" id_session = ").append(addQuotes(sessionId))
			.append(CR).append(",device_category = ").append(addQuotes(simulatorLog.getDeviceCategoryCode()))
			.append(CR).append(",loop_Number = ").append(addQuotes(""+simulatorLog.getLoopNumber()))
			.append(CR).append(",power_target = ").append(addQuotes(""+simulatorLog.getPowerTarget()))
			.append(CR).append(",power_target_min = ").append(addQuotes(""+""+simulatorLog.getPowerTargetMin()))
			.append(CR).append(",power_target_max = ").append(addQuotes(""+simulatorLog.getPowerTargetMax()))
			.append(CR).append(",power = ").append(addQuotes(""+simulatorLog.getPower()))
			.append(CR).append(",is_reached = ").append(simulatorLog.isReached()?1:0)
			.append(CR).append(",nb_started = ").append(addQuotes(""+simulatorLog.getNbStarted()))
			.append(CR).append(",nb_modified = ").append(addQuotes(""+simulatorLog.getNbModified()))
			.append(CR).append(",nb_stopped = ").append(addQuotes(""+simulatorLog.getNbStopped()))
			.append(CR).append(",nb_devices = ").append(addQuotes(""+simulatorLog.getNbDevices()))
			.append(CR).append(",target_device_combination_found = ").append(simulatorLog.isTargetDeviceCombinationFound())
		;
		DBConnection.execUpdate(query.toString());
	}

	private static long registerHomeTotal(HomeTotal homeTotal, Event linkedEvent) {
		StringBuffer query = new StringBuffer();
		String location = homeTotal.getLocation();
		String location2 = addQuotes(location);
		String sHistoryDate = linkedEvent == null ? "CURRENT_TIMESTAMP()": "'" + SapereUtil.format_sql.format(linkedEvent.getBeginDate()) + "'";
		query.append("SET @histo_date = ").append(sHistoryDate)
			.append(CR).append("§")
			.append(CR).append("SET @id_last = (SELECT ID FROM history WHERE date < @histo_date AND location = " + location2 + " ORDER BY date DESC LIMIT 0,1)")
			.append(CR).append("§");
		query.append("INSERT INTO history SET ")
			.append(" date = @histo_date")
			.append(", id_last = @id_last")
			.append(", learning_agent = ").append(addQuotes(homeTotal.getAgentName()))
			.append(", location = ").append(addQuotes(homeTotal.getLocation()))
			.append(", distance = ").append(addQuotes(homeTotal.getDistance()))
			.append(", id_session = ").append(addQuotes(sessionId))
			.append(", total_requested = ").append(addQuotes(""+homeTotal.getRequested()))
			.append(", total_consumed = ").append(addQuotes(""+homeTotal.getConsumed()))
			.append(", total_produced = ").append(addQuotes(""+homeTotal.getProduced()))
			.append(", total_provided = ").append(addQuotes(""+homeTotal.getProvided()))
			.append(", total_available = ").append(addQuotes(""+homeTotal.getAvailable()))
			.append(", total_missing = ").append(addQuotes(""+homeTotal.getMissing()))
			.append(CR).append(" ON DUPLICATE KEY UPDATE")
			.append(CR).append("  total_requested = ").append(addQuotes(""+homeTotal.getRequested()))
			.append(CR).append(", total_consumed = ").append(addQuotes(""+homeTotal.getConsumed()))
			.append(CR).append(", total_produced = ").append(addQuotes(""+homeTotal.getProduced()))
			.append(CR).append(", total_provided = ").append(addQuotes(""+homeTotal.getProvided()))
			.append(CR).append(", total_available = ").append(addQuotes(""+homeTotal.getAvailable()))
			.append(CR).append(", total_missing = ").append(addQuotes(""+homeTotal.getMissing()))
			.append(CR).append(", id_last = @id_last")
		;
		query.append(CR).append("§")
				.append(CR).append("SET @new_id_histo = (SELECT MAX(ID) FROM history WHERE date = @histo_date AND location = " + location2 + ")")
		;
		if(linkedEvent!=null) {
			query.append(CR).append("§");
			query.append("UPDATE event SET id_histo=@new_id_histo WHERE ID = '").append(linkedEvent.getId()).append("'");
		}
		// Correction of history.id_next
		query.append(CR).append("§")
			 .append(CR).append("SET @date_last = (SELECT date FROM history WHERE id=@id_last)")
		;
		query.append(CR).append("§");
		query.append("UPDATE history h  SET id_next = (SELECT h2.ID FROM history h2 WHERE h2.date > h.date AND h2.location = h.location ORDER BY h2.date LIMIT 0,1)"
						+ " WHERE h.date >= @date_last AND location = " + location2 + " AND id_session = " + addQuotes(sessionId)
								+ " AND IFNULL(id_next,0) <> (SELECT h2.ID FROM history h2 WHERE h2.date > h.date AND h2.location = h.location ORDER BY h2.date LIMIT 0,1)");
		query.append(CR).append("§");
		query.append("UPDATE history h  SET id_last = (SELECT h2.ID FROM history h2 WHERE h2.date < h.date AND h2.location = h.location ORDER BY h2.date DESC LIMIT 0,1)"
						+ " WHERE h.date > @histo_date AND location = " + location2 + " AND id_session = " + addQuotes(sessionId)
								+ " AND IFNULL(id_last,0) <> (SELECT h2.ID from history h2 WHERE h2.date < h.date AND h2.location = h.location ORDER BY h2.date DESC LIMIT 0,1)");
		DBConnection.execUpdate(query.toString());
		Map<String, Object> row = DBConnection.executeSingleRowSelect("SELECT @new_id_histo AS id");
		Long id = SapereUtil.getLongValue(row, "id");
		return id;
	}

	public static void cleanHistoryDB() {
		DBConnection.execUpdate("DELETE FROM single_offer");
		DBConnection.execUpdate("DELETE FROM link_event_agent");
		DBConnection.execUpdate("DELETE FROM link_history_active_event");
		DBConnection.execUpdate("DELETE FROM event");
		DBConnection.execUpdate("UPDATE history SET id_last=NULL, id_next=NULL");
		DBConnection.execUpdate("DELETE FROM history");
	}

	public static long registerSingleOffer(SingleOffer offer, Event productionEvent) {
		String prodEvtId = (productionEvent==null)? "NULL" : ""+productionEvent.getId();
		String sExpiryDate = SapereUtil.format_sql.format(offer.getDeadline());
		String reqEvtId = (offer.getRequest()==null || offer.getRequest().getEventId()==null) ? "NULL" : "" + offer.getRequest().getEventId();
		StringBuffer query = new StringBuffer("INSERT INTO single_offer SET ");
		query.append("deadline = ").append(addQuotes(sExpiryDate))
			.append(" ,id_session = ").append(addQuotes(sessionId))
			.append(" ,producer_agent = ").append(addQuotes(offer.getProducerAgent()))
			.append(" ,consumer_agent = ").append(addQuotes(offer.getConsumerAgent()))
			.append(" ,power = '").append(offer.getPower()).append("'")
			.append(" ,production_event_id = ").append(prodEvtId)
			.append(" ,request_event_id =  ").append(reqEvtId)
			.append(" ,log = ").append(addQuotes(offer.getLog()))
			;
		;
		return DBConnection.execUpdate(query.toString());
	}

	public static long setSingleOfferAcquitted(SingleOffer offer,  Event requestEvent, boolean used) {
		if(offer!=null) {
			//String requestEvtId = (requestEvent==null)? "NULL" : ""+requestEvent.getId();
			StringBuffer query = new StringBuffer("UPDATE single_offer SET ");
			//query.append("request_event_id = ").append(requestEvtId)
			query.append(" acquitted=1")
				.append(" ,used=").append(used? "1":"0")
				.append(" ,used_time=").append(used? "NOW()":"NULL")
				.append(" WHERE id=").append(offer.getId());
			return DBConnection.execUpdate(query.toString());
		}
		return 0;
	}

	public static long setSingleOfferAccepted(GlobalOffer globalOffer) {
		String offerids = globalOffer.getSingleOffersIdsStr();
		if(offerids.length()>0) {
			StringBuffer query = new StringBuffer("UPDATE single_offer SET accepted=1, acceptance_time = NOW()")
				.append(" WHERE id IN (").append(offerids).append(")");
			return DBConnection.execUpdate(query.toString());
		}
		return 0;
	}


	public static HomeTotal retrieveLastHomeTotal() {
		List<Map<String, Object>> sqlResult = DBConnection.executeSelect("SELECT * FROM history ORDER BY history.date DESC LIMIT 0,1");
		if(sqlResult.size()>0) {
			Map<String, Object> row = sqlResult.get(0);
			HomeTotal total = new HomeTotal();
			BigDecimal bdTotalConsumed = (BigDecimal) row.get("total_consumed");
			BigDecimal bdTotalProduced = (BigDecimal) row.get("total_produced");
			BigDecimal bdTotalRequested = (BigDecimal) row.get("total_requested");
			BigDecimal bdTotalAvailable = (BigDecimal) row.get("total_available");
			BigDecimal bdTotalMissing = (BigDecimal) row.get("total_missing");
			total.setId((Long) row.get("id"));
			total.setConsumed(bdTotalConsumed.floatValue());
			total.setProduced(bdTotalProduced.floatValue());
			total.setRequested(bdTotalRequested.floatValue());
			total.setAvailable(bdTotalAvailable.floatValue());
			total.setMissing(bdTotalMissing.floatValue());
			total.setDate((Date) row.get("date"));
			total.setAgentName("" + row.get("learning_agent"));
			return total;
		}
		return null;
	}

	public List<ExtendedHomeTotal> retrieveHomeTotalHistory() {
		//correctHisto();
		String location = NodeManager.getLocation();
		List<ExtendedHomeTotal> result = new ArrayList<ExtendedHomeTotal>();
		StringBuffer query = new StringBuffer(""
				// + "DELIMITER §"
				+ "CALL compute_missing_request2(CURDATE(), " + addQuotes(location) + ")"
				+ CR + "§"
				+ CR + "SELECT history.id, history.id_last, history.id_next, history.date, history.total_produced, history.total_requested, history.total_consumed "
				+ CR + ",history.total_available, history.total_missing, history.total_provided, history.location, history.distance"
				+ CR + ",IFNULL(hNext.date,NOW()) AS date_next"
				+ CR + ",(SELECT Count(*) FROM single_offer AS sg WHERE sg.creation_time >= history.date AND sg.creation_time < IFNULL(hNext.date, DATE_ADD(NOW(), interval 1 DAY))  ) AS nb_offers"
				+ CR + ",(SELECT Count(*) FROM TmpUnsatisfiedRequest WHERE TmpUnsatisfiedRequest.id_histo = history.ID) AS nb_missing_request"
				+ CR + ",(SELECT IFNULL(SUM(TmpUnsatisfiedRequest.power),0) FROM TmpUnsatisfiedRequest WHERE TmpUnsatisfiedRequest.id_histo = history.ID) AS sum_missing_requests"
				+ CR + ",(SELECT IFNULL(MIN(TmpUnsatisfiedRequest.power),0) FROM TmpUnsatisfiedRequest WHERE TmpUnsatisfiedRequest.id_histo = history.ID) AS min_missing_request"
				+ CR + ",(SELECT IFNULL(GROUP_CONCAT(UnReq.Label SEPARATOR  ', '),'') "
				+ CR + "			FROM TmpUnsatisfiedRequest AS UnReq WHERE UnReq.id_histo = history.ID) AS missing_requests_consumers"
				+ CR + ",(SELECT IFNULL(GROUP_CONCAT(UnReq.Label2 SEPARATOR  ', '),'') "
				+ CR + "			FROM TmpUnsatisfiedRequest AS UnReq WHERE UnReq.id_histo = history.ID AND UnReq.warning) AS warning_requests"
				+ CR + ",(SELECT IFNULL(MAX(UnReq.warning_duration),0) "
				+ CR + "			FROM TmpUnsatisfiedRequest AS UnReq WHERE UnReq.id_histo = history.ID AND UnReq.warning) AS max_warning_duration"
				+ CR + ",(SELECT IFNULL(SUM(UnReq.power),0) "
				+ CR + "			FROM TmpUnsatisfiedRequest AS UnReq WHERE UnReq.id_histo = history.ID AND UnReq.warning) AS sum_warning_power2"
				+ CR + ",COMPUTE_WARNING_SUM(history.id) AS sum_warning_power"
				+ CR + " FROM history "
				+ CR + " LEFT JOIN history AS hNext ON hNext.id = history.id_next "
				+ CR + " WHERE history.id_session =  " + addQuotes(sessionId) + " AND history.location =" + 	addQuotes(location)
				+ CR + " ORDER BY history.date, history.ID ");
		Date dateMin = null;
		Date dateMax = null;
		List<Map<String, Object>> sqlResult = DBConnection.executeSelect(query.toString());
		for (Map<String, Object> nextRow : sqlResult) {
			ExtendedHomeTotal nextTotal = new ExtendedHomeTotal();
			Date nextDate = (Date) nextRow.get("date");
			if(dateMin==null) {
				dateMin = nextDate;
			}
			dateMax = nextDate;
			nextTotal.setAgentName("" + nextRow.get("learning_agent"));
			nextTotal.setDate((Date) nextRow.get("date"));
			nextTotal.setLocation("" + nextRow.get("location"));
			nextTotal.setDistance(SapereUtil.getIntValue(nextRow, "distance"));
			nextTotal.setConsumed(SapereUtil.getFloatValue(nextRow, "total_consumed"));
			nextTotal.setProduced(SapereUtil.getFloatValue(nextRow, "total_produced"));
			nextTotal.setProvided(SapereUtil.getFloatValue(nextRow, "total_provided"));
			nextTotal.setRequested(SapereUtil.getFloatValue(nextRow, "total_requested"));
			nextTotal.setAvailable(SapereUtil.getFloatValue(nextRow, "total_available"));
			nextTotal.setMissing(SapereUtil.getFloatValue(nextRow, "total_missing"));
			nextTotal.setSumWarningPower(SapereUtil.getFloatValue(nextRow, "sum_warning_power"));
			nextTotal.setId((Long) nextRow.get("id"));
			nextTotal.setIdLast((Long) nextRow.get("id_last"));
			nextTotal.setIdNext((Long) nextRow.get("id_next"));
			if(nextRow.get("date_next") != null) {
				nextTotal.setDateNext((Date) nextRow.get("date_next"));
			}
			nextTotal.setNbOffers((Long) nextRow.get("nb_offers")) ;
			if(nextTotal.getContractDoublons()!=null && nextTotal.getContractDoublons().length()>0) {
				logger.warning("DEBUG ContractDoublons : " + nextTotal.getContractDoublons());
			}
			nextTotal.setConsumersMissingRequests("" + nextRow.get("missing_requests_consumers"));
			nextTotal.setConsumersWarningRequests("" + nextRow.get("warning_requests"));
			BigDecimal bdMinMinMissigRequests = (BigDecimal) nextRow.get("min_missing_request");
			if (bdMinMinMissigRequests != null) {
				nextTotal.setMinMissingRequests(bdMinMinMissigRequests.floatValue());
			}
			Integer maxWarningDuration = (Integer) nextRow.get("max_warning_duration");
			if(maxWarningDuration!=null) {
				nextTotal.setMaxWarningDuration(maxWarningDuration.floatValue());
			}
			nextTotal.setNbMissingRequests((Long) nextRow.get("nb_missing_request"));
			result.add(nextTotal);
		}
		// Retrieve events
		List<ExtendedEvent> listEvents = retrieveSessionEvents(sessionId, false);
		// Sort events by histoId
		Map<Long, List<Event>> mapHistoEvents = new HashMap<Long, List<Event>>();
		for(ExtendedEvent nextEvent : listEvents) {
			Long histoId = nextEvent.getHistoId();
			if(!mapHistoEvents.containsKey(histoId)) {
				mapHistoEvents.put(histoId, new ArrayList<Event>());
			}
			(mapHistoEvents.get(histoId)).add(nextEvent);
		}

		// Retrieve offers
		List<SingleOffer> listOffers = retrieveOffers(dateMin, dateMax, null, null, null);
		// Sort offers by histoId
		Map<Long, List<SingleOffer>> mapHistoOffers = new HashMap<Long, List<SingleOffer>>();
		for(SingleOffer offer : listOffers) {
			Long histoId = offer.getHistoId();
			if(!mapHistoOffers.containsKey(histoId)) {
				mapHistoOffers.put(histoId, new ArrayList<SingleOffer>());
			}
			(mapHistoOffers.get(histoId)).add(offer);
		}
		for(ExtendedHomeTotal nextTotal : result) {
			Long histoId = nextTotal.getId();
			// Add events
			if(mapHistoEvents.containsKey(histoId)) {
				nextTotal.setLinkedEvents(mapHistoEvents.get(histoId));
			}
			// Add offers
			if(mapHistoOffers.containsKey(histoId)) {
				//List<SingleOffer> offers = mapHistoOffers.get(histoId);
				nextTotal.setOffers(mapHistoOffers.get(histoId));
			}
		}
		return result;
	}

	public static String addQuotes(String str) {
		StringBuffer quoted = new StringBuffer();
		quoted.append("'").append(str).append("'");
		return quoted.toString();
	}

	public static String addQuotes(Integer number) {
		StringBuffer quoted = new StringBuffer();
		quoted.append("'").append(number).append("'");
		return quoted.toString();
	}

	public static String addQuotes(Long number) {
		StringBuffer quoted = new StringBuffer();
		quoted.append("'").append(number).append("'");
		return quoted.toString();
	}

	public static String addQuotes(Float number) {
		StringBuffer quoted = new StringBuffer();
		quoted.append("'").append(number).append("'");
		return quoted.toString();
	}

	public static void correctHisto() {
		// Correction of history.id_last
		DBConnection.execUpdate("UPDATE history h  SET id_last = (SELECT h2.ID FROM history h2 WHERE h2.date < h.date AND h2.location = h.location ORDER BY h2.date DESC LIMIT 0,1)"
			+ " WHERE id_session=" + addQuotes(sessionId) + " AND id_last <> (SELECT h2.ID FROM history h2 WHERE h2.date < h.date AND h2.location = h.location ORDER BY h2.date DESC LIMIT 0,1)");
		// Correction of history.id_next
		DBConnection.execUpdate("UPDATE history h  SET id_next = (SELECT h2.ID FROM history h2 WHERE h2.date > h.date AND h2.location = h.location ORDER BY h2.date LIMIT 0,1)"
				+ " WHERE id_session=" + addQuotes(sessionId) + " AND IFNULL(id_next,0) <> (SELECT h2.ID FROM history h2 WHERE h2.date > h.date AND h2.location = h.location ORDER BY h2.date LIMIT 0,1)");

	}

	public static boolean isOfferAcuitted(Long id) {
		String sId = ""+id;
		Map<String, Object> row = DBConnection.executeSingleRowSelect("SELECT id,acquitted FROM single_offer WHERE id = " + addQuotes(sId));
		if(row!=null && row.get("acquitted") instanceof Boolean)  {
			Boolean acquitted = (Boolean) row.get("acquitted");
			return acquitted;
		}
		return false;
	}

	public static void addLogOnOffer(Long offerId, String textToAdd) {
		String sId = ""+offerId;
		String query = "UPDATE single_offer SET log2 = CONCAT(log2, ' ', " + addQuotes(textToAdd) + ") WHERE id = "+ addQuotes(sId);
		DBConnection.execUpdate(query);
	}

	public static List<SingleOffer> retrieveOffers(Date dateMin, Date dateMax, String consumerFilter, String producerFilter, String addFilter) {
		List<SingleOffer> result = new ArrayList<SingleOffer>();
		if(dateMin==null) {
			return result;
		}
		String sDateMin = SapereUtil.format_sql.format(dateMin);
		String sDateMax = SapereUtil.format_sql.format(dateMax);
		String sConsumerFilter = consumerFilter==null? "1": "single_offer.consumer_agent = " +  addQuotes(consumerFilter) + "";
		String sProducerFilter = producerFilter==null? "1": "single_offer.producer_agent = " +  addQuotes(producerFilter) + "";
		String sAaddedFilter = (addFilter==null? "1" : addFilter);
		StringBuffer query = new StringBuffer();
		query.append( "SELECT single_offer.* ")
			 .append(CR).append(" ,prodEvent.begin_date AS prod_begin_date")
			 .append(CR).append(" ,prodEvent.expiry_date AS prod_expiry_date")
			 .append(CR).append(" ,prodEvent.power AS prod_power")
			 .append(CR).append(" ,prodEvent.device_name AS prod_device_name")
			 .append(CR).append(" ,prodEvent.device_category AS prod_device_category")
			 .append(CR).append(" ,requestEvent.begin_date AS req_begin_date")
			 .append(CR).append(" ,requestEvent.expiry_date AS  req_expiry_date")
			 .append(CR).append(" ,requestEvent.agent AS req_agent")
			 .append(CR).append(" ,requestEvent.power AS req_power")
			 .append(CR).append(" ,requestEvent.device_name AS  req_device_name")
			 .append(CR).append(" ,requestEvent.device_category AS  req_device_category")
			 .append(CR).append(" ,(select h.ID from history h where h.date <= single_offer.creation_time order by h.date desc limit 0,1) as id_histo")
			 .append(CR).append(" FROM single_offer ")
			 .append(CR).append(" LEFT JOIN event AS prodEvent ON prodEvent.id = single_offer.production_event_id")
			 .append(CR).append(" LEFT JOIN event AS requestEvent ON requestEvent.id = single_offer.request_event_id")
			 .append(CR).append(" WHERE single_offer.Creation_time >= ").append(addQuotes(sDateMin))
			 .append(CR).append("    AND  single_offer.Creation_time < ") .append(addQuotes(sDateMax))
			 .append(CR).append("    AND ").append(sConsumerFilter).append(" AND ").append(sProducerFilter)
			 .append(CR).append("    AND ").append(sAaddedFilter)
			 .append(CR).append(" ORDER BY single_offer.creation_time, single_offer.ID ");
		List<Map<String, Object>> rows = DBConnection.executeSelect(query.toString());
		for(Map<String, Object> row : rows) {
			EnergyRequest request  = null;
			Boolean accepted = (Boolean) row.get("accepted");
			Boolean used = (Boolean) row.get("used");
			Boolean acquitted = (Boolean) row.get("acquitted");
			BigDecimal bdReqPower =  (BigDecimal) (row.get("req_power"));
			if(bdReqPower!=null) {
				Date reqBeginDate =   (Date) row.get("req_begin_date");
				Date reqExpiryDate = (Date) row.get("req_expiry_date");
				String sDeviceCat = "" + row.get("req_device_category");
				DeviceCategory deviceCategory = DeviceCategory.getByName(sDeviceCat);
				Float tolerance = SapereUtil.computeDurationMinutes(reqBeginDate, reqExpiryDate);
				request = new EnergyRequest("" + row.get("req_agent"), NodeManager.getLocation(), bdReqPower.floatValue()
						, reqBeginDate, reqExpiryDate , tolerance, PriorityLevel.LOW, ""+ row.get("req_device_name"), deviceCategory);
			}
			EnergySupply supply = null;
			BigDecimal bdPower = (BigDecimal) (row.get("power"));
			if(bdPower!=null) {
				String producerAgent = "" + row.get("producer_agent");
				String sDeviceCat = "" + row.get("prod_device_category");
				DeviceCategory deviceCategory = DeviceCategory.getByName(sDeviceCat);
				supply = new EnergySupply(producerAgent, NodeManager.getLocation(), bdPower.floatValue(), (Date) row.get("prod_begin_date") , (Date) row.get("prod_expiry_date"), ""+row.get("prod_device_name"), deviceCategory);
				SingleOffer nextOffer = new SingleOffer(producerAgent, supply, 0, request);
				nextOffer.setDeadline((Date) row.get("deadline"));
				if( row.get("used_time")!=null) {
					nextOffer.setUsedTime((Date) row.get("used_time"));
				}
				if( row.get("used_time")!=null) {
					nextOffer.setUsedTime((Date) row.get("used_time"));
				}
				if( row.get("acceptance_time")!=null) {
					nextOffer.setAcceptanceTime((Date) row.get("acceptance_time"));
				}
				nextOffer.setCreationTime((Date) row.get("creation_time"));
				nextOffer.setUsed(used);
				nextOffer.setLog("" + row.get("log"));
				nextOffer.setHistoId((Long) row.get("id_histo"));
				nextOffer.setAcquitted(acquitted);
				nextOffer.setAccepted(accepted);
				if(row.get("id") instanceof Integer) {
					Integer integer = (Integer) row.get("id");
					nextOffer.setId(integer.longValue());
				} else if(row.get("id") instanceof Long) {
					nextOffer.setId((Long) row.get("id"));
				}
				result.add(nextOffer);
			}
		}
		return result;
	}

	public static Map<Integer,HomeTransitionMatrices> loadListHomeTransitionMatrice(String learningAgentName
			, String[] _variables
			//, TransitionMatrixScope scope
			, String location
			, String scenario
			, List<MarkovTimeWindow> listTimeWindows
			, Date currentDate) {
		logger.info("loadListHomeTransitionMatrice : begin");
		Map<Integer,HomeTransitionMatrices> mapTransitionMatrices = new HashMap<Integer,HomeTransitionMatrices>();
		List<String> idsTimeWindows = new ArrayList<String>();
		for(MarkovTimeWindow timeWindow : listTimeWindows) {
			//MarkovTimeWindow timeWindow = timeSlot.getMarkovTimeWindow();
			HomeTransitionMatrices homeTransitionMatrices = new HomeTransitionMatrices(learningAgentName, _variables, location, scenario, timeWindow);
			mapTransitionMatrices.put(timeWindow.getId(), homeTransitionMatrices);
			idsTimeWindows.add(""+timeWindow.getId());
		}
		String sCurrentDate = SapereUtil.format_sql.format(currentDate);
		String sIdsTimeWindows = SapereUtil.implode(idsTimeWindows, ",");
		String sVariableNames = "'"+ SapereUtil.implode(Arrays.asList(_variables), "','") + "'";
		StringBuffer loadQuery1 = new StringBuffer();
		loadQuery1.append(CR).append( "DROP TEMPORARY TABLE IF EXISTS TmpTrMatrix")
			.append(CR).append("§");
		loadQuery1.append(CR).append( "CREATE TEMPORARY TABLE TmpTrMatrix AS")
			.append(CR).append( "SELECT transition_matrix.id")
			.append(CR).append(", transition_matrix.variable_name")
			.append(CR).append(", transition_matrix.location")
			.append(CR).append(", transition_matrix.scenario")
			.append(CR).append(", transition_matrix.id_time_window")
			.append(CR).append(", transition_matrix.iteration_number")
			.append(CR).append(", GET_ITERATION_ID(transition_matrix.id, ").append(addQuotes(sCurrentDate)).append(" )  AS id_transition_matrix_iteration")
			.append(CR).append(" FROM transition_matrix ")
			//.append(CR).append(" LEFT JOIN transition_matrix_iteration ON transition_matrix_iteration.ID = GET_ITERATION_ID(transition_matrix.id, ").append(addQuotes(sCurrentDate)).append(" ) ")
			.append(CR).append(" WHERE transition_matrix.id_time_window IN (").append(sIdsTimeWindows).append(")")
			.append(CR).append(" 	AND variable_name IN (").append(sVariableNames).append(")")
			.append(CR).append(" 	AND location = ").append(addQuotes(location))
			.append(CR).append(" 	AND scenario = ").append(addQuotes(scenario))
		;
		loadQuery1.append(CR).append("§")
				.append(CR).append( "SELECT TmpTrMatrix.*")
				.append(CR).append("	,(TmpTrMatrix.id_transition_matrix_iteration IS NULL) AS IsNewIteration")
				.append(CR).append("	,cell.*")
				.append(CR).append("	,IFNULL(cellIt.obs_number,0) AS obs_number_iter")
				.append(CR).append(" FROM TmpTrMatrix ")
				.append(CR).append(" JOIN transition_matrix_cell AS cell ON cell.id_transition_matrix = TmpTrMatrix.id")
				.append(CR).append(" LEFT JOIN transition_matrix_cell_iteration  AS cellIt ON cellIt.id_transition_matrix_iteration = TmpTrMatrix.id_transition_matrix_iteration")
				.append(CR).append(" 			AND cellIt.row_idx = cell.row_idx AND cellIt.column_idx = cell.column_idx")
				.append(CR).append(" WHERE 1")
				.append(CR).append(" ORDER BY TmpTrMatrix.ID");
		//DBConnection.getInstance().setDebugLevel(1);
		Date before = new Date();
		List<Map<String, Object>> rows = DBConnection.executeSelect(loadQuery1.toString());
		Date after = new Date();
		long loadTime = after.getTime() - before.getTime();
		logger.info("loadListHomeTransitionMatrice : request time = " + loadTime);
		for(Map<String, Object> row : rows) {
			String variable = "" + row.get("variable_name");
			Long timeWindowId1 = (Long) row.get("id_time_window");
			Long iterationNumber = (Long) row.get("iteration_number");
			Integer timeWindowId = new Integer(timeWindowId1.intValue());
			Integer rowIdx = (Integer) row.get("row_idx");
			//Integer newTransition = (Integer) row.get("IsNewIteration");
			Integer columnIdx = (Integer) row.get("column_idx");
			Object oNumberOfObservation = row.get("obs_number");
			if(oNumberOfObservation != null) {
				double nbOfObs = SapereUtil.getDoubleValue(row, "obs_number");
				double nbOfObsIteration = SapereUtil.getDoubleValue(row, "obs_number_iter");
				if(nbOfObsIteration>0 && debugLevel> 0) {
					logger.info("loadListHomeTransitionMatrice : " + variable + " " + timeWindowId1 + " " + nbOfObs + " " + nbOfObsIteration);
				}
				HomeTransitionMatrices homeTransitionMatrices = mapTransitionMatrices.get(timeWindowId);
				homeTransitionMatrices.setValue(variable, rowIdx, columnIdx, nbOfObsIteration, nbOfObs);
				homeTransitionMatrices.getMapNbOfIterations().put(variable, iterationNumber.intValue());
			}
		}
		logger.info("loadListHomeTransitionMatrice : end");
		return mapTransitionMatrices;
	}

	public static HomeTransitionMatrices loadHomeTransitionMatrices(String learningAgentName, String[] _variables, String location /*TransitionMatrixScope scope*/, String scenario, MarkovTimeWindow timeWindow, Date loadDate) {
		 List<MarkovTimeWindow> listTimeWindows = new ArrayList<MarkovTimeWindow>();
		 listTimeWindows.add(timeWindow);
		 Map<Integer,HomeTransitionMatrices> mapResult = loadListHomeTransitionMatrice(learningAgentName, _variables, location, scenario, listTimeWindows, loadDate);
		 if(mapResult.size()>0) {
			 for(HomeTransitionMatrices transitionMatrices : mapResult.values()) {
				 // Return the first row
				 return transitionMatrices;
			 }
		 }
		 //MarkovTimeWindow timeWindow = timeSlot.getMarkovTimeWindow();
		 HomeTransitionMatrices homeTransitionMatrices = new HomeTransitionMatrices(learningAgentName, _variables, location, scenario,timeWindow);
		 homeTransitionMatrices.reset();
		 return homeTransitionMatrices;
	}

	public static void setHistoryStates(HomeTotal homeTotal, HomeMarkovStates homeMarkovStates, String scenario) {
		Long historyId = homeTotal.getId();
		if(!homeTotal.hasActivity()) {
			return;
		}
		String location = homeTotal.getLocation();
		Map<String, MarkovState> mapStates = homeMarkovStates.getMapStates();
		Map<String, Float> mapValues = homeMarkovStates.getMapValues();
		StringBuffer query1 = new StringBuffer("UPDATE history SET ");
		StringBuffer query2 = new StringBuffer();
		String sqlDate = SapereUtil.format_sql.format(homeTotal.getDate());
		query2.append("SET @date_last = (SELECT MAX(date) FROM state_history WHERE location = " + addQuotes(location)
			+ " AND scenario = " + addQuotes(scenario) + " AND date < " + addQuotes(sqlDate) + ")");
		query2.append(CR).append("§");
		query2.append(CR).append("INSERT INTO state_history(date,variable_name,location,scenario,state_idx,value) VALUES ");
		boolean stateAdded = false;
		for(String variable : homeMarkovStates.getVariables()) {
			if(mapStates.containsKey(variable)) {
				MarkovState state = mapStates.get(variable);
				float value = -1;
				if(mapValues.containsKey(variable)) {
					value = mapValues.get(variable);
				}
				int stateIdx = state.getId() -1;
				if(stateAdded) {
					query1.append(", ");
					query2.append(", ");
				}
				query1.append("state_idx_").append(variable).append(" = ").append(stateIdx);
				query2.append("(").append(addQuotes(sqlDate)).append(",").append(addQuotes(variable))
					.append(",").append(addQuotes(location)).append(",").append(addQuotes(scenario))
					.append(",").append(stateIdx).append(",").append(addQuotes(value)).append(")");
				stateAdded = true;
			}
		}
		query2.append(CR).append("§");
		query2.append(CR).append("UPDATE state_history SET state_name = CONCAT('S', (state_idx+1)) WHERE state_history.date >= @date_last AND state_name='' ");
		query2.append(CR).append("§");
		query2.append(CR).append("UPDATE state_history SET ")
				.append(CR).append("date_next = (SELECT next_sh.date FROM state_history as next_sh")
				.append(CR).append("     WHERE next_sh.date > state_history.date AND next_sh.variable_name  = state_history.variable_name")
				.append(CR).append("    	AND next_sh.location  = state_history.location AND next_sh.scenario  = state_history.scenario")
				.append(CR).append(" 	ORDER BY next_sh.date LIMIT 0,1)")
				.append(CR).append(" WHERE state_history.date >= DATE_ADD(@date_last, INTERVAL -10 MINUTE) AND date < " + addQuotes(sqlDate));
		query1.append(" WHERE id = ").append(historyId);
		if(stateAdded) {
			//DBConnection.execUpdate(query1.toString());
			DBConnection.execUpdate(query2.toString());
		}
	}

	public static void saveHomeTransitionMatrices(HomeTransitionMatrices homeTransitionMatrices, int learningWindow) {
		StringBuffer query = new StringBuffer("");
		MarkovTimeWindow timeWindow = homeTransitionMatrices.getTimeWindow();
		Integer timeWindowId = homeTransitionMatrices.getTimeWindowId();
		//TransitionMatrixScope scope = homeTransitionMatrices.getScope();
		String location = homeTransitionMatrices.getLocation();
		String scenario = homeTransitionMatrices.getScenario();
		if(timeWindowId>0) {
			boolean refreshTransitionMatrixCells = false;
			for(String variable : homeTransitionMatrices.getMapAllObsMatrices().keySet()) {
				query = new StringBuffer("");
				query.append("INSERT INTO transition_matrix SET ")
				.append(CR).append(" id_time_window = " ).append(addQuotes(timeWindowId))
				.append(CR).append(",variable_name = ").append(addQuotes(variable))
				.append(CR).append(",location = ").append(addQuotes(location))
				.append(CR).append(",scenario = ").append(addQuotes(scenario))
				.append(CR).append(",learning_agent = ").append(addQuotes(homeTransitionMatrices.getLearningAgentName()))
				.append(CR).append(",last_update = current_timestamp()")
				.append(CR).append(" ON DUPLICATE KEY UPDATE last_update = current_timestamp()")
				;
				// Retrieve ID of transition matrix
				query.append(CR).append(" §")
					.append(CR).append("SET @id_transition_matrix = (SELECT ID FROM transition_matrix WHERE id_time_window=").append(addQuotes(timeWindowId))
					.append(" AND variable_name = ").append(addQuotes(variable))
					.append(" AND location = ").append(addQuotes(location))
					.append(" AND scenario = ").append(addQuotes(scenario))
					.append(")");

				// Retrieve last iteration ID
				query.append(CR).append(" §")
					.append(CR).append("SET @last_id_iteration = (SELECT IFNULL(MAX(ID),0) FROM transition_matrix_iteration WHERE id_transition_matrix = @id_transition_matrix)");

				// Save iteration
				String startDate = SapereUtil.format_sql.format(timeWindow.getStartDate());
				String endDate = SapereUtil.format_sql.format(timeWindow.getEndDate());
				query.append(CR).append(" §")
				.append(CR).append("INSERT INTO transition_matrix_iteration SET ")
				.append(CR).append("    id_time_window = " ).append(addQuotes(timeWindowId))
				.append(CR).append("   ,id_transition_matrix = @id_transition_matrix")
				.append(CR).append("   ,begin_date = ").append(addQuotes(startDate))
				.append(CR).append("   ,end_date = ").append(addQuotes(endDate))
				.append(CR).append("   ,last_update = current_timestamp()")
				.append(CR).append("   ,number = 1+ (SELECT IFNULL(MAX(number),0) FROM transition_matrix_iteration AS it ")
				.append(CR).append("       WHERE it.id_transition_matrix = @id_transition_matrix)")
				.append(CR).append(" ON DUPLICATE KEY UPDATE last_update = current_timestamp()")
				.append(CR).append(" §")
				.append(CR).append(" SET @IsNewIteration = LAST_INSERT_ID() > @last_id_iteration")
				;
				// Update number of iteration
				query.append(CR).append(" §")
					.append(CR).append("SET @id_iteration=(SELECT ID FROM transition_matrix_iteration WHERE id_transition_matrix = @id_transition_matrix")
					.append(CR).append(" AND end_date = ").append(addQuotes(endDate)).append(")")
				;
				query.append(CR).append(" §")
					.append(CR).append(" UPDATE transition_matrix SET iteration_number = " )
					.append(CR).append("     (SELECT IFNULL(MAX(it.number),0) FROM transition_matrix_iteration AS it WHERE it.id_transition_matrix = transition_matrix.ID) ")
					.append(CR).append("     WHERE transition_matrix.ID =  @id_transition_matrix")
				;
				// Save matrix cells
				Matrix normalizedMatrix = homeTransitionMatrices.getNormalizedMatrix(variable);
				Matrix iterObsMatrix = homeTransitionMatrices.getIterObsMatrice(variable);
				if(iterObsMatrix!=null) {
					List<String> iterationCells = new ArrayList<String>();
					for(int rowIdx = 0; rowIdx < iterObsMatrix.getRowDimension(); rowIdx++) {
						for(int columnIdx = 0; columnIdx < normalizedMatrix.getColumnDimension(); columnIdx++) {
							double iterObsNb = iterObsMatrix.get(rowIdx, columnIdx);
							if(iterObsNb!=Double.NaN && iterObsNb!=0) {
								StringBuffer buffCellIteration = new StringBuffer();
								buffCellIteration.append("(@id_iteration,@id_transition_matrix,").append(rowIdx).append(",").append(columnIdx).append(",").append(iterObsNb).append(")");
								iterationCells.add(buffCellIteration.toString());
							}
						}
					}
					if(iterationCells.size()>0) {
						query.append(CR).append(" §")
							.append(CR).append("DELETE transition_matrix_cell_iteration FROM transition_matrix_cell_iteration WHERE id_transition_matrix_iteration = @id_iteration");
						query.append(CR).append(" §")
							 .append(CR).append("INSERT INTO transition_matrix_cell_iteration(id_transition_matrix_iteration,id_transition_matrix ,row_idx,column_idx,obs_number) VALUES ")
						;
						query.append(SapereUtil.implode(iterationCells, CR+","));
						refreshTransitionMatrixCells = true;
					}
				}
				DBConnection.execUpdate(query.toString());
			} // end for
			if(refreshTransitionMatrixCells) {
				refreshTransitionMatrixCell(location,scenario,learningWindow);
			}
		}
	}

	public static void refreshTransitionMatrixCell(String location, String scenario, int learningWindow) {
		StringBuffer query = new StringBuffer();
		query.append("CALL REFRESH_TRANSITION_MATRIX_CELL(").append(addQuotes(location))
			.append(",").append(addQuotes(scenario))
			.append(",").append(learningWindow).append(")");
		DBConnection.execUpdate(query.toString());
	}

	public static void savePredictionResult(PredictionData prediction) {
		Date initialDate = prediction.getInitialDate();
		for(Date targetDate : prediction.getTargetDates()) {
			for(String variableName : prediction.getVariables()) {
				if(prediction.hasResult(variableName, targetDate)) {
					PredictionResult predictionResult = prediction.getResult(variableName, targetDate);
					StringBuffer sqlInsertPrediction = new StringBuffer();
					int horizonMinutes = Math.round(predictionResult.getTimeHorizonMinutes());
					sqlInsertPrediction.append("INSERT INTO prediction SET variable_name=").append(addQuotes(variableName))
						.append(", location =").append(addQuotes(prediction.getLocation()))
						.append(", scenario =").append(addQuotes(prediction.getScenario()))
						.append(", initial_date=").append(addQuotes(SapereUtil.format_sql.format(initialDate)))
						.append(", target_date=").append(addQuotes(SapereUtil.format_sql.format(targetDate)))
						.append(", horizon_minutes=").append(horizonMinutes)
						.append(", learning_window=").append(prediction.getLearningWindow())
					;
					if(predictionResult.getRadomTargetState() !=null) {
						MarkovState randomState = predictionResult.getRadomTargetState();
						int stateIdx = randomState.getId()-1;
						String stateName = "S"+randomState.getId();
						sqlInsertPrediction.append(", random_state_idx=").append(stateIdx)
											.append(", random_state_name=").append(addQuotes(stateName));
					}
					double predictionId = DBConnection.execUpdate(sqlInsertPrediction.toString());
					StringBuffer sqlInsertItems = new StringBuffer("INSERT INTO prediction_item(id_prediction,state_idx,state_name,value) VALUES ");
					List<Double> content = predictionResult.getStateProbabilities();
					int stateIndex = 0;
					for(Double nextValue : content) {
						String stateName = "S"+(1+stateIndex);
						if(stateIndex>0) {
							sqlInsertItems.append(",");
						}
						sqlInsertItems.append("(").append(predictionId).append(",").append(stateIndex).append(",").append(addQuotes(stateName))
							.append(",").append(nextValue).append(")");
						stateIndex++;
					}
					DBConnection.execUpdate(sqlInsertItems.toString());
				}
			}
		}
	}

	public static List<Device> retrieveHomeDevices(double powerCoeffProducer, double powerCoeffConsumer) {
		List<Device> result = new ArrayList<>();
		List<Map<String, Object>> rows = DBConnection.executeSelect("SELECT * FROM device");
		for(Map<String, Object> row: rows) {
			Long id = SapereUtil.getLongValue(row, "id");
			Boolean isProducer = (Boolean) row.get("is_producer");
			Float powerCoeff = (float) (isProducer.booleanValue() ?  powerCoeffProducer : powerCoeffConsumer);
			Float powerMin = powerCoeff*SapereUtil.getFloatValue(row, "power_min");
			Float powerMax = powerCoeff*SapereUtil.getFloatValue(row, "power_max");
			Float avergaeDuration =  SapereUtil.getFloatValue(row, "avg_duration");
			//(Long id, String name, float powerMin, float powerMax, float averageDurationMinutes)
			Device nextDevice = new Device(id, ""+row.get("name"), ""+row.get("category"), powerMin, powerMax, avergaeDuration);
			nextDevice.setPriorityLevel(SapereUtil.getIntValue(row, "priority_level"));
			nextDevice.setProducer(isProducer);
			result.add(nextDevice);
		}
		return result;
	}

	public static Map<String, Double> retrieveDeviceStatistics(double powerCoeffConsumer, double powerCoeffProducer, List<DeviceCategory> categoryFilter, int hourOfDay) {
		Map<String, Double> result = new HashMap<String, Double>();
		StringBuffer sCategoryFilter = new StringBuffer("1");
		if(categoryFilter!=null && categoryFilter.size()>0) {
			String sep="";
			sCategoryFilter = new StringBuffer("device_category IN (");
			for(DeviceCategory category : categoryFilter) {
				sCategoryFilter.append(sep).append("'").append(category).append("'");
				sep=",";
			}
			sCategoryFilter.append(")");
		}
		List<Map<String, Object>> rows = DBConnection.executeSelect("SELECT * FROM device_statistic WHERE start_hour = " + hourOfDay + " AND " + sCategoryFilter.toString());
		for(Map<String, Object> row: rows) {
			String category = "" + row.get("device_category");
			boolean isProducer = category.endsWith("_ENG");
			double powerCoeff = isProducer? powerCoeffProducer : powerCoeffConsumer;
			double power = powerCoeff*SapereUtil.getDoubleValue(row, "power");
			result.put(category, power);
		}
		return result;
	}
}
