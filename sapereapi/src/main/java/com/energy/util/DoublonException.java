package com.energy.util;

public class DoublonException extends Exception {
	private static final long serialVersionUID = 1779L;

	public DoublonException(String message) {
		super(message);
	}
}
