package com.energy.util;

import java.io.IOException;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import com.sapereapi.model.SapereUtil;

public class SapereLogger {
	private static Logger logger = null;
	private static FileHandler fileHandler = null;
	private static String logFilename = null;
	public static final String FILE_SEP = System.getProperty("file.separator");
	public static final String CR = System.getProperty("line.separator");

	static {
		// System.setProperty("java.util.logging.SimpleFormatter.format", format3);
		logger = Logger.getLogger(SapereLogger.class.getName());
	}

	/** Instance unique pré-initialisée */
	private static SapereLogger instance = new SapereLogger();

	private void initFileHandlers() {
		String sDay = SapereUtil.format_sessionid.format(new Date());
		logFilename = "log" + FILE_SEP + "sapere." + sDay + ".log";
		try {
			fileHandler = new FileHandler(logFilename);
			fileHandler.setFormatter(new SapereLogFormatter());
			logger.addHandler(fileHandler);
			// logger.addHandler(new ConsoleLoggerBase());
			// the following statement is used to log any messages
			logger.info("--------------------------- First log -------------------------");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private SapereLogger() {
		super();
		// TODO Auto-generated constructor stub
		initFileHandlers();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static SapereLogger getInstance() {
		return instance;
	}

	public void info(String message) {
		logger.info(message);
	}

	public void warning(String message) {
		logger.warning(message);
	}

	public void error(String message) {
		logger.severe(message);
	}

	public void error(Throwable t) {
		logger.severe(t + " " + t.getMessage() + CR + t.getCause() + CR + SapereUtil.stackTraceToString(t));
	}

}
