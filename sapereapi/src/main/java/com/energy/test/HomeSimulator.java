package com.energy.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.energy.model.AgentType;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.HomeTotal;
import com.energy.model.PriorityLevel;
import com.energy.model.SimulatorLog;
import com.sapereapi.model.AgentForm;
import com.sapereapi.model.HomeContent;
import com.sapereapi.model.InitializationForm;
import com.sapereapi.model.SapereUtil;

public class HomeSimulator extends TestSimulator {

	static Map<String, Double> deviceStatistics = new HashMap<String, Double>();
	static int loopCounter = 0;
	final static String CR = System.getProperty("line.separator");
	static SimulatorLog simulatorLog = null;
	// Define device category filter
	static DeviceCategory[] categoryFilter = new DeviceCategory[] {};

	static List<String> updatedDevices = new ArrayList<String>();
	static List<String> stoppedDevices = new ArrayList<String>();
	static List<String> startedDevices = new ArrayList<String>();
	static HomeContent partialHomeContent = null;
	static Integer forcedHourOfDay = null;

	private static AgentForm generateAgentForm(Device aDevice, double targetPower, AgentForm existingForm) {
		float duration = new Float(24 * 60 * 365);
		if (aDevice.getAverageDurationMinutes() > 0) {
			duration = new Float(aDevice.getAverageDurationMinutes() * (1 + 0.10 * random.nextGaussian()));
		}
		PriorityLevel priority = PriorityLevel.LOW;
		if (aDevice.getPriorityLevel() > 1) {
			priority = PriorityLevel.HIGH;
		}
		float power = aDevice.getPowerMin();
		if (targetPower > 0 && targetPower >= aDevice.getPowerMin() && targetPower <= aDevice.getPowerMax()) {
			power = (float) targetPower;
		} else {
			double powerRandom = Math.random();
			power = aDevice.getPowerMin() + (float) (powerRandom * (aDevice.getPowerMax() - aDevice.getPowerMin()));
		}
		if (!aDevice.isProducer()) {
			// power = (float) 0.1 * power;
		}
		Date current = new Date();
		AgentType agentType = aDevice.isProducer() ? AgentType.PRODUCER : AgentType.CONSUMER;
		DeviceCategory deviceCategory = DeviceCategory.getByName(aDevice.getCategory());
		// DeviceCategory test = DeviceCategory.getByName("WASHING_DRYING");
		Date endDate = SapereUtil.shiftDateMinutes(current, duration);
		AgentForm result = new AgentForm(agentType, "", aDevice.getName(), deviceCategory, power, current, endDate,
				priority, duration);
		if (existingForm != null) {
			result.setAgentName(existingForm.getAgentName());
			result.setId(existingForm.getId());
		}
		return result;
	}

	private static float computeCurrentCategoryPower(String deviceCategory, boolean isProducer) {
		partialHomeContent = getPartialHomeContent(deviceCategory, isProducer);
		return aux_computeCurrentCategoryPower(partialHomeContent, deviceCategory, isProducer);
	}

	private static float aux_computeCurrentCategoryPower(HomeContent homeContent, String deviceCategory, boolean isProducer) {
		if(homeContent==null) {
			logger.warning("Loop #" + loopCounter + " " + deviceCategory + " computeCurrentCategoryPower : homeContent is null");
			return 0;
		}
		HomeTotal homeTotal = homeContent.getPartialTotal(deviceCategory);
		if(homeTotal==null) {
			return 0;
		}
		return isProducer? homeTotal.getProduced() : homeTotal.getRequested();
	}

	private static boolean isMarginSufficient(List<Device> devices, double powerTarget) {
		return getPowerMin(devices) <= powerTarget && getPowerMax(devices) >= powerTarget;
	}

	public static void adjustRunningDevices(boolean isProducer, String category, double powerTarget, List<String> excludedDevices) {
		if(debugLevel>0) {
			logger.info("Loop #" + loopCounter + " " + category + "  adjustExistingPowers begin");
		}
		AgentForm agentForm = null;
		//HomeTotal currentTotal = getCurrentTotal(homeContent, category);
		//List<Device> result = new ArrayList<Device>();
		partialHomeContent = getPartialHomeContent(category, isProducer);
		double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
		double powerTargetMin = powerTarget * 0.99;
		double powerTargetMax = powerTarget * 1.01;
		double toCorrect = powerTarget - currentPower;
		List<Device> devices = getDevices(partialHomeContent, isProducer, category, Device.STATUS_RUNNING, 0, powerTargetMax, excludedDevices);
		Collections.shuffle(devices);
		List<Device> chosenDevices = new ArrayList<>();
		if(!isMarginSufficient(devices, powerTarget)) {
			logger.info(loopCounter + " " + category + " : adjustExistingPowers : not enough margin left to reach target "
					+ SapereUtil.df.format(powerTarget)
					+ " from " + currentPower);
			logger.info(" ( Existing devices : " + devices + " )" );
			chosenDevices = devices;
		} else {
			for(Device nextDevice : devices) {
				if(!isMarginSufficient(chosenDevices, powerTarget)) {
					chosenDevices.add(nextDevice);
				} else {
					break;
				}
			}
		}
		for (Device nextDevice : chosenDevices) {
			if (currentPower < powerTargetMin  || currentPower > powerTargetMax) {
				toCorrect = powerTarget - currentPower;
				if (toCorrect != 0) {
					agentForm = partialHomeContent.getAgentByDeviceName(nextDevice.getName());
					if(agentForm != null && aux_isRuinning(nextDevice, partialHomeContent.getMapRunningAgents())) {
					//if(agentForm != null && agentForm.getIsSatisfied() != null && agentForm.getIsSatisfied()) {
						double powerToSet = agentForm.getPower() + toCorrect;
						if (powerToSet > nextDevice.getPowerMax()) {
							powerToSet = nextDevice.getPowerMax();
						}
						if (powerToSet < nextDevice.getPowerMin()) {
							powerToSet = nextDevice.getPowerMin();
						}
						if (Math.abs(powerToSet - agentForm.getPower()) >= 0.001) {
							double lastTotal = aux_computeCurrentCategoryPower(partialHomeContent,category, isProducer);
							// Modify agent
							float powerBefore = agentForm.getPower();
							agentForm = generateAgentForm(nextDevice, powerToSet, agentForm);
							logger.info("Loop #" + loopCounter + " " + category + " adjustExistingPowers modify " + agentForm.getAgentName() + " : " + powerBefore + " -> " + powerToSet);
							modifyAgent(agentForm);
							// Refresh currentTotal
							partialHomeContent.getAgentByDeviceName(nextDevice.getName());
							partialHomeContent = getPartialHomeContent(category, isProducer);
							float newTotal = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
							if(Math.abs(newTotal - lastTotal) < 0.001) {
								logger.warning("Loop #" + loopCounter + " " + category + " adjustExistingPowers : same total before and after modifyAgent");
							} else {
								updatedDevices.add(nextDevice.getName());
							}
							logger.info("Loop #" + loopCounter + " " + category + " adjustExistingPowers : lastTotal = " + lastTotal + ", toAdd= " + toCorrect + ", new total = " + newTotal);
						}
					}
				}
			}
		}
		if (currentPower >= powerTargetMin && currentPower <= powerTargetMax) {
			logger.info("Loop #" + loopCounter + " " + category + "  adjustExistingPowers success ");
		}
		if(debugLevel>0) {
			logger.info("Loop #" + loopCounter + " " + category + "  adjustExistingPowers end");
		}
	}

	public static void stopExistingDevices(boolean isProducer, String category, double powerTarget, boolean applyMaxFilter) {
		AgentForm agentForm = null;
		//List<String> removedDevices = new ArrayList<String>();
		//refreshHomeContent();
		partialHomeContent= getPartialHomeContent(category, isProducer);
		double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
		int nbTry = 0;
		double powerTargetMin = powerTarget * 0.95;
		logger.info("#loop "+ loopCounter + " "  + category + " stopExistingDevices target = " + SapereUtil.df.format(powerTarget) +  " current "+ currentPower);
		while (currentPower > powerTarget
				&& (agentForm = chooseRunningAgent(partialHomeContent, isProducer, category, 0, applyMaxFilter ? currentPower - powerTargetMin : 0,  stoppedDevices)) != null
				&& nbTry < 100) {
			logger.info("Loop #" + loopCounter + " " + category + " stop device" + agentForm.getDeviceName()
					+ "(" + agentForm.getPower() + "W) " + " : current = " + currentPower
					+ " target : " + powerTarget);
			double lastTotal = currentPower;
			if (!applyMaxFilter || (currentPower - agentForm.getPower() >= powerTargetMin)) {
				auxStopAgent(agentForm);
				//currentTotal =  getCurrentCategoryPower(homeContent, category, isProducer);
				partialHomeContent = getPartialHomeContent(category, isProducer);
				double newTotal = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
				if(Math.abs(lastTotal - newTotal) < 0.001) {
					logger.warning("Loop #" + loopCounter + " " + category + " stopExistingDevices stop device '" + agentForm.getDeviceName() + "' did not work");
				} else {
					// It worked
					stoppedDevices.add(agentForm.getDeviceName());
					logger.info("Loop #" + loopCounter + " " + category + " stopExistingDevices stop device '" + agentForm.getDeviceName() + " OK");
				}
			} else {
				logger.warning("Step456 : stopExistingDevices : chosen agentForm is KO ");
			}
			nbTry++;
		}
	}

	public static boolean auxStartDevice(boolean isProducer, Device nextDevice, double powerToSet) {
		String category = nextDevice.getCategory();
		nextDevice.setCurrentPower((float) powerToSet);
		boolean result = false;
		partialHomeContent = getPartialHomeContent(category, isProducer);
		double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
		if (partialHomeContent.hasDevice(nextDevice.getName())) {
			// Device already contained in home content but disabled : we have to restart it
			AgentForm agentForm = partialHomeContent.getAgentByDeviceName(nextDevice.getName());
			agentForm = generateAgentForm(nextDevice, powerToSet, agentForm);
			double totalBefore = currentPower;
			agentForm = restartAgent(agentForm);
			partialHomeContent = getPartialHomeContent(category, isProducer);
			currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
			if(Math.abs(currentPower - totalBefore) < 0.001) {
				logger.warning("loop #" + loopCounter + " " + category + " auxStartDevice restartAgent " + agentForm.getDeviceName() + " did not work ");
			} else {
				result = true;
			}
			//logger.info("loop #" + loopCounter + " " + category + " auxStartDevice restartAgent W=" + agentForm.getPower() + " " + agentForm.getDeviceName());
		} else {
			// Device not contained in home content : we have to start it
			AgentForm agentForm = generateAgentForm(nextDevice, powerToSet, null);
			double totalBefore = currentPower;
			agentForm = addAgent(agentForm);
			logger.info("loop #" + loopCounter + " " + category + " auxStartDevice addAgent  W=" + agentForm.getPower() + " " + agentForm.getDeviceName());
			partialHomeContent = getPartialHomeContent(category, isProducer);
			currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
			if(Math.abs(currentPower - totalBefore) < 0.001) {
				logger.warning("loop #" + loopCounter + " " + category + " auxStartDevice addAgent " + agentForm.getDeviceName() + " did not work ");
			} else {
				result = true;
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			logger.error(e);
		}
		return result;
	}
	public static void startDevices(boolean isProducer, String category, double powerTarget, List<String> excludedDevices) {
		double powerTargetMax = powerTarget * 1.05;
		partialHomeContent = getPartialHomeContent(category, isProducer);
		double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
		//List<Device> result = new ArrayList<Device>();
		//targetOK.put(category, false);
		if (currentPower < powerTarget) {
			if (hasDevice(partialHomeContent, isProducer, category, Device.STATUS_SLEEPING, 0,
					powerTargetMax - currentPower, excludedDevices)) {
				// Add producer device
				// Add consumer devices until power is high enough
				// choose some sleeping device
				List<Device> selectedDevices = chooseListDevices(partialHomeContent, isProducer, category, Device.STATUS_SLEEPING, powerTarget, excludedDevices);
				if (selectedDevices.size() > 0) {
					double sumPowerMin = getPowerMin(selectedDevices);
					double sumPowerMax = getPowerMax(selectedDevices);
					if (powerTarget <= powerTargetMax) {
						double toAdd = Math.min(sumPowerMax - sumPowerMin, powerTarget - sumPowerMin);
						double toAddRatio = toAdd / (sumPowerMax - sumPowerMin);
						for (Device nextDevice : selectedDevices) {
							double powerToSet = nextDevice.getPowerMin()
									+ toAddRatio * (nextDevice.getPowerMax() - nextDevice.getPowerMin());
							boolean isStarted = auxStartDevice(isProducer, nextDevice, powerToSet);
							if(isStarted) {
								startedDevices.add(nextDevice.getName());
							}
						}
					}
				} else {
					logger.warning("list is empty for categroy " + category);
				}
			} else {
				/*
				logger.warning(
						"Loop #" + loopCounter + " " + category + " No device found for category " + category
								+ " and power max <= " + (powerTargetMax - currentTotal.getRequested()));
								*/
			}
		}
	}

	public static Map<String, Device> findTargetDevicesCombination(HomeContent partialHomeContent, boolean isProducer, String category, double powerTarget /*, List<String> excludedDevices*/) {
		Map<String,Device> result = new HashMap<String, Device>();
		List<String> excludedDevices = new ArrayList<String>();
		if(debugLevel>0) {
			logger.info( "Loop #" + loopCounter + " " + category + " findTargetDevicesCombination : begin");
		}
		double powerTargetMax = powerTarget * 1.05;
		if (hasDevice(partialHomeContent, isProducer, category, null, 0, powerTargetMax , null)) {
			// Add devices until power is high enough
			List<Device> selectedDevices = chooseListDevices(partialHomeContent, isProducer, category, null, powerTarget, excludedDevices);
			if (selectedDevices.size() > 0) {
				double sumPowerMin = getPowerMin(selectedDevices);
				double sumPowerMax = getPowerMax(selectedDevices);
				if(sumPowerMin <= powerTarget &&  sumPowerMax >= powerTarget) {
					double toAdd = Math.min(sumPowerMax - sumPowerMin, powerTarget - sumPowerMin);
					double toAddRatio = toAdd / (sumPowerMax - sumPowerMin);
					for (Device nextDevice : selectedDevices) {
						double powerToSet = nextDevice.getPowerMin()
								+ toAddRatio * (nextDevice.getPowerMax() - nextDevice.getPowerMin());
						try {
							Device deviceCopy = nextDevice.clone();
							deviceCopy.setCurrentPower((float) powerToSet);
							//result.add(deviceCopy);
							result.put(deviceCopy.getName(), deviceCopy);
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
							logger.error(e);
						}
					}
				} else {
					logger.info( "Loop #" + loopCounter + " " + category + " findTargetDevicesCombination : no combination found ");
				}
			} else {
				logger.warning("list is empty for categroy " + category);
			}
		} else {
			/*
			logger.warning(
					"Loop #" + loopCounter + " " + category + " No device found for category " + category
							+ " and power max <= " + (powerTargetMax - currentTotal.getRequested()));
			*/
		}
		return result;
	}

	public static boolean restartAllDevices(boolean isProducer, String category, double powerTarget) {
		// Still not OK : try to find a device combination from zero
		if(debugLevel>0) {
			logger.info("Loop #" + loopCounter + " " + category + " restartAllDevices begin");
		}
		partialHomeContent = getPartialHomeContent(category, isProducer);
		boolean targetDeviceCombinationFound = false;
		Map<String, Device>  mapTargetDevices = findTargetDevicesCombination(partialHomeContent, isProducer, category, powerTarget);
		if(mapTargetDevices.size()>0) {
			double sumPower = getCurrentPower(mapTargetDevices.values());
			double powerTargetMin = powerTarget * 0.95;
			double powerTargetMax = powerTarget * 1.05;
			double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
			if(sumPower>= powerTargetMin && sumPower <= powerTargetMax) {
				targetDeviceCombinationFound = true;
				logger.info("Loop #" + loopCounter + " " + category + " restartAllDevices : " + mapTargetDevices.keySet() + " power " + sumPower);
				// Stop device not included in this list
				List<Device> runningDevices =  getDevices(partialHomeContent, isProducer, category, Device.STATUS_RUNNING, 0, 0, null);
				List<Device> sleepingDevices =  getDevices(partialHomeContent, isProducer, category, Device.STATUS_SLEEPING, 0, 0, null);
				List<String> sleepingDeviceNames = getDeviceNames(sleepingDevices);
				for(Device device : runningDevices) {
					AgentForm agentForm = homeContent.getAgentByDeviceName(device.getName());
					if(agentForm!=null) {
						double totaBefore = currentPower;
						if(mapTargetDevices.containsKey(device.getName())) {
							// modify the device power
							Device targetDevice = mapTargetDevices.get(device.getName());
							float powerToSet = targetDevice.getCurrentPower();
							float powerBefore = agentForm.getPower();
							if(Math.abs(powerToSet - powerBefore) >= 0.001) {
								agentForm = generateAgentForm(device, powerToSet, agentForm);
								logger.info("Loop #" + loopCounter + " " + category + " restartAllDevices : modify " + powerBefore + " -> " + powerToSet);
								modifyAgent(agentForm);
								partialHomeContent = getPartialHomeContent(category, isProducer);
								currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
								double newTotal = currentPower;
								if(Math.abs(newTotal - totaBefore) < 0.001) {
									logger.warning("Loop #" + loopCounter + " " + category + " restartAllDevices : same total before and after modifyAgent");
								} else {
									updatedDevices.add(agentForm.getDeviceName());
								}
							}
						} else {
							// Remove the device
							auxStopAgent(agentForm);
							partialHomeContent = getPartialHomeContent(category, isProducer);
							currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
							double newTotal = currentPower;
							if(Math.abs(totaBefore - newTotal) < 0.001) {
								logger.warning("Loop #" + loopCounter + " " + category + "  restartAllDevices stop device '" + agentForm.getDeviceName() + "' did not work");
							} else {
								// It worked
								logger.info("Loop #" + loopCounter + " " + category + "  restartAllDevicese '" + agentForm.getDeviceName() + " OK");
								stoppedDevices.add(agentForm.getDeviceName());
							}
						}
					}
				}
				// Add devices
				for(String targetDeviceName : mapTargetDevices.keySet()) {
					if(sleepingDeviceNames.contains(targetDeviceName)) {
						// start the device
						Device targetDevice = mapTargetDevices.get(targetDeviceName);
						boolean isStarted = auxStartDevice(isProducer, targetDevice, targetDevice.getCurrentPower());
						if(isStarted) {
							startedDevices.add(targetDevice.getName());
						}
					}
				}
			}
			refreshHomeContent();
		}
		if(debugLevel>0) {
			logger.info("Loop #" + loopCounter + " " + category + " restartAllDevices end : targetDeviceCombinationFound = " + targetDeviceCombinationFound);
		}
		return targetDeviceCombinationFound;
	}

	public static void executeLoop() {
		loopCounter++;
		//int nbTry = 0;
		logger.info("executeLoop " + loopCounter + " begin");
		homeContent = getHomeContent();
		deviceStatistics = retrieveDeviceStatistics(categoryFilter, forcedHourOfDay);
		Map<String, Boolean> targetKO = new HashMap<String, Boolean>();
		try {
			for (String category : deviceStatistics.keySet()) {
				boolean isProducer = category.endsWith("_ENG");
				if(isProducer) {
					logger.info("Loop #" + loopCounter + " " + category + " : producer");
				}
				logger.info("Loop #" + loopCounter + " " + category + " : step0");
				updatedDevices = new ArrayList<String>();
				stoppedDevices = new ArrayList<String>();
				startedDevices = new ArrayList<String>();

				if("ICT".equals(category)) {
					logger.info("for debug :  category = " + category);
				}

				double avgPower = deviceStatistics.get(category);
				double powerTarget = avgPower * (1 + 0.05 * random.nextGaussian());
				double powerTargetMin = powerTarget * 0.95;
				double powerTargetMax = powerTarget * 1.05;
				boolean targetDeviceCombinationFound = false;
				// First : try to adjust power with running devices
				adjustRunningDevices(isProducer, category, powerTarget, null);
				//HomeTotal currentTotal = getCurrentTotal(homeContent, category);
				partialHomeContent = getPartialHomeContent(category, isProducer);
				double currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
				if (currentPower >= powerTargetMin && currentPower < powerTargetMax) {
					// Target reached : OK
				}

				// Remove devices if current total is over target max
				if (currentPower > powerTargetMax) {
					// we have to reduce power
					stopExistingDevices(isProducer, category, powerTargetMax, true);
					partialHomeContent = getPartialHomeContent(category, isProducer);
					currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
					logger.info("Loop #" + loopCounter + " " + category + " after stopExistingDevices1 : target  = "
							+ SapereUtil.df.format(powerTarget) + " , current = " + SapereUtil.df.format(currentPower));

				}
				if (currentPower > powerTargetMax) {
					// still over target : stop deivces whith not filter no filter
					stopExistingDevices(isProducer, category, powerTargetMax, false);
					partialHomeContent = getPartialHomeContent(category, isProducer);
					currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
					logger.info("Loop #" + loopCounter + " " + category + " after stopExistingDevices2 : target  = "
							+ SapereUtil.df.format(powerTarget) + " , current = " + SapereUtil.df.format(currentPower));
				}
				// Add devices if current total is under target min
				if (currentPower < powerTargetMin) {
					startDevices(isProducer, category, powerTarget, stoppedDevices);
					partialHomeContent = getPartialHomeContent(category, isProducer);
					currentPower = aux_computeCurrentCategoryPower(partialHomeContent,category, isProducer);
					logger.info("Loop #" + loopCounter + " " + category + " after startDevices : target  = "
							+ SapereUtil.df.format(powerTarget) + " , current = " + SapereUtil.df.format(currentPower));
				}
				// Check if OK
				if (currentPower < powerTargetMin || currentPower > powerTargetMax) {
					// Still not OK : try to adjust with running agents
					adjustRunningDevices(isProducer, category, powerTarget, stoppedDevices);
					currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
				}
				if (currentPower < powerTargetMin || currentPower > powerTargetMax) {
					// Still not OK : try to find a device combination from zero
					Thread.sleep(5*1000);
					targetDeviceCombinationFound = restartAllDevices(isProducer, category, powerTarget);
					partialHomeContent = getPartialHomeContent(category, isProducer);
					currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
				}
				if (currentPower < powerTargetMin || currentPower > powerTargetMax) {
					// NOT OK : log warning
					//refreshHomeContent();
					//partialHomeContent = getPartialHomeContent(category, isProducer);
					//currentPower = aux_computeCurrentCategoryPower(partialHomeContent, category, isProducer);
					StringBuffer msgBuffer = new StringBuffer();
					msgBuffer.append("Sleeping devices");
					List<Device> sleepingDevices = getDevices(partialHomeContent, isProducer, category, Device.STATUS_SLEEPING, 0, 0, null);
					for(Device device : sleepingDevices) {
						msgBuffer.append(CR).append(device.toString2());
					}
					msgBuffer.append(CR).append("Running devices");
					List<Device> runningDevices = getDevices(partialHomeContent, isProducer, category, Device.STATUS_RUNNING, 0, 0, null);
					for(Device device : runningDevices) {
						msgBuffer.append(CR).append(device.toString2());
					}
					logger.warning(
							"Loop #" + loopCounter + " " + category + " target not reached : powerTarget = "
									+ SapereUtil.df.format(powerTarget) + " , reached = " + SapereUtil.df.format(currentPower)
									+ CR + msgBuffer.toString());
					/*
					if(currentPower < powerTargetMin) {
						// For debug
						startDevices(isProducer, category, powerTarget, removedDevice);
						currentPower = computeCurrentCategoryPower(homeContent, category, isProducer);
					}
					if(currentPower > powerTargetMax) {
						// For debug
						stopExistingDevices(isProducer, category, powerTarget, true);
						currentPower = computeCurrentCategoryPower(homeContent, category, isProducer);
					}*/
				}
				boolean isOK = (currentPower >= powerTargetMin) && (currentPower < powerTargetMax);
				int nbDevices = isProducer? partialHomeContent.getProducers().size()
							:partialHomeContent.getConsumers().size();
				simulatorLog = new SimulatorLog(loopCounter,  category, powerTarget, powerTargetMin,
						 powerTargetMax, currentPower, isOK, nbDevices);
				simulatorLog.setNbModified(updatedDevices.size());
				simulatorLog.setNbStarted(startedDevices.size());
				simulatorLog.setNbStopped(stoppedDevices.size());
				simulatorLog.setTargetDeviceCombinationFound(isOK || targetDeviceCombinationFound);
				addSimulatorLog(simulatorLog);
				if(!isOK) {
					targetKO.put(category, false);
					logger.warning("For debug simulatorLog = " + simulatorLog);
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
		String result = " *** All is OK *** ";
		if(targetKO.size()>0) {
			result = "target not reached for the following categories : " + targetKO.keySet();
		}
		logger.info("executeLoop " + loopCounter + " : end " + result);
	}

	public static void auxStopAgent(AgentForm agentForm) {
		String category = agentForm.getDeviceCategoryCode();
		boolean isProducer = AgentType.PRODUCER.getLabel().equals(agentForm.getAgentType());
		double currentPower = computeCurrentCategoryPower(category, isProducer);
		double totalRequestedBeforeStop = currentPower;
		boolean stopOK = false;
		int nbTry = 0;
		while(!stopOK && nbTry<5) {
			boolean result = stopAgent(agentForm);
			if(!result) {
				logger.info("Loop #" + loopCounter + " " + category + " auxStopAgent " + agentForm.getDeviceName() + " stopAgent returns false");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error(e);
				}
			}
			int refreshTry = 0;
			currentPower = computeCurrentCategoryPower(category, isProducer);
			stopOK = currentPower < totalRequestedBeforeStop;
			while (  !stopOK && refreshTry < 10) {
				// Refresh home content
				if (refreshTry > 1) {
					//logger.warning("auxStopAgent " + agentForm.getDeviceName() + " : nbTry = " + nbTry + ", refreshTry = " + refreshTry);
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.error(e);
				}
				homeContent = getHomeContent();
				currentPower = computeCurrentCategoryPower(category, isProducer);
				stopOK = currentPower < totalRequestedBeforeStop;
				refreshTry++;
			}
			logger.info("Loop #" + loopCounter + " " + category + " auxStopAgent " + agentForm.getDeviceName() + " nbTry = " + nbTry );
			nbTry++;
		}
	}

	public static void main(String args[]) {
		debugLevel = 0;
		if (debugLevel > 0) {
			logger.info("Main " + args);
		}
		init(args);
		format_datetime.setTimeZone(TimeZone.getTimeZone("GMT"));
		String scenario = HomeSimulator.class.getSimpleName();
		InitializationForm initForm = new InitializationForm();
		initForm.setScenario(scenario);
		forcedHourOfDay = null;
		//forcedHourOfDay = new Integer(21);//
		initForm.setForcedHourOfDay(forcedHourOfDay);
		//initForm.setInitialState("consumed", 7);
		initEnergyService(initForm);
		deviceStatistics = retrieveDeviceStatistics(categoryFilter, forcedHourOfDay);
		Date current = new Date();
		int waitItNb = 60;
		resetSimulatorLogs();
		boolean retrieveLastContent = false;
		if (retrieveLastContent) {
			homeContent = restartLastHomeContent();
		} else {
			executeLoop();
			for(int idx = 0; idx<waitItNb ; idx++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error(e);
				}
			}
		}
		Date end = SapereUtil.shiftDateMinutes(current, 1800 * 1);
		while (current.before(end)) {
			current = new Date();
			//double random = Math.random();
			try {
				executeLoop();
				logger.info("after executeLoop ");
				Thread.sleep(60 * 1000);
			} catch (InterruptedException e) {
				logger.error(e);
			}
		}
	}
}
