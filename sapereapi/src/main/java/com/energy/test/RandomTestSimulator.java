package com.energy.test;

import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import com.energy.markov.HomeMarkovStates;
import com.energy.model.AgentType;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.HomeTotal;
import com.energy.model.PriorityLevel;
import com.sapereapi.model.AgentForm;
import com.sapereapi.model.InitializationForm;
import com.sapereapi.model.SapereUtil;

public class RandomTestSimulator extends TestSimulator {

	public static void main(String args[]) {
		if (debugLevel > 0) {
			logger.info("Main " + args);
		}
		init(args);
		String scenario = RandomTestSimulator.class.getSimpleName();
		InitializationForm initForm = new InitializationForm();
		initForm.setScenario(scenario);
		initEnergyService(initForm);

		format_datetime.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date current = new Date();
		boolean retrieveLastContent = false;
		if (retrieveLastContent) {
			homeContent = restartLastHomeContent();
		} else {
			// Add producer agent
			addAgent(new AgentForm(AgentType.PRODUCER, "", "EDF", DeviceCategory.EXTERNAL_ENG, new Float(31.0), current,
					SapereUtil.shiftDateMinutes(current, new Float(24 * 60 * 365))));
			addAgent(new AgentForm(AgentType.PRODUCER, "", "Wind Turbin-1", DeviceCategory.WIND_ENG, new Float(18),
					current, SapereUtil.shiftDateMinutes(current, new Float(60))));
			// Add consumer agent
			addAgent(new AgentForm(AgentType.CONSUMER, "", "Refrigerator", DeviceCategory.COLD_APPLIANCES,
					new Float(23.1), current, SapereUtil.shiftDateMinutes(current, new Float(24 * 60 * 365)),
					PriorityLevel.HIGH, new Float(24 * 60 * 365)));
			/*
			 * addAgent( new AgentForm(AgentType.CONSUMER, "", new Float(2000), current,
			 * SapereUtil.shiftDateMinutes(current, new Float(60)) , PriorityLevel.LOW, new
			 * Float(60)) );
			 */
			homeContent = getHomeContent();
		}
		Date end = SapereUtil.shiftDateMinutes(current, 180 * 1);
		while (current.before(end)) {
			current = new Date();
			double random = Math.random();
			try {
				if (random < 10*0.1) {
					// Refresh home content
					homeContent = getHomeContent();
					HomeTotal homeTotal = homeContent.getTotal();
					Float prodMargin = Math.max(0, HomeMarkovStates.MAX_TOTAL_POWER - homeTotal.getProduced());
					Float prodDiscountFactor = prodMargin / HomeMarkovStates.MAX_TOTAL_POWER;
					Float requestMargin = Math.max(0,
							new Float(1.5) * HomeMarkovStates.MAX_TOTAL_POWER - homeTotal.getRequested());
					Float requestDiscountFactor = requestMargin / (new Float(1.5) * HomeMarkovStates.MAX_TOTAL_POWER);

					// create random object
					Random r = new Random();
					double randomIsProducer = Math.random();
					boolean isProducer = (randomIsProducer < 0.4);
					AgentType agentType = isProducer ? AgentType.PRODUCER : AgentType.CONSUMER;
					// Generate random agent
					Float discountFactor = (isProducer ? prodDiscountFactor : requestDiscountFactor);
					Float amp1 = new Float(discountFactor * 0.1 * HomeMarkovStates.MAX_TOTAL_POWER);
					float power = new Float(amp1 * Math.abs(r.nextGaussian()));
					power = (float) ((float) (Math.round(power * 100))) / 100;
					if (power < 0.01) {
						// Do not accept very small powers
						logger.info("For debug : value = " + power);
					} else {
						float durationMinutes = 0 + new Float(60 * Math.abs(r.nextGaussian()));
						Date endDate = SapereUtil.shiftDateMinutes(current, durationMinutes);
						AgentForm agentForm = new AgentForm();
						agentForm.setPower(power);
						agentForm.setBeginDate(current);
						agentForm.setEndDate(endDate);
						agentForm.setDuration(durationMinutes);
						agentForm.setDelayToleranceMinutes(durationMinutes);
						agentForm.setAgentType(agentType.getLabel());
						agentForm.setDeviceName("");
						if (AgentType.CONSUMER.equals(agentType)) {
							double rPriority = Math.random();
							PriorityLevel priority = (rPriority < 0.1) ? PriorityLevel.HIGH : PriorityLevel.LOW;
							// : (rPriority < 0.3 ? PriorityLevel.MEDIUM : PriorityLevel.LOW);
							priority = PriorityLevel.LOW;
							agentForm.setPriorityLevel(priority.getLabel());
							Device chosenDevice = chooseSleepingDevice(homeContent, false, null, power / 1.5, power * 1.5, null);
							if (chosenDevice != null) {
								DeviceCategory category = DeviceCategory.getByName(chosenDevice.getCategory());
								agentForm.setDeviceName(chosenDevice.getName());
								agentForm.setDeviceCategory(category);
							} else {
								agentForm.setDeviceCategory(DeviceCategory.UNKNOWN);
								agentForm.setDeviceName("Not-identified");
								logger.info(" device is null");
							}
						} else {
							int nb = 1+homeContent.getProducers().size();
							agentForm.setDeviceName("Wind turbin #" + nb);
							agentForm.setDeviceCategory(DeviceCategory.WIND_ENG);
						}
						int nbActiveAgents = homeContent.getNbActiveAgents(agentType);
						double probaDoOperation = Math.exp(-0.1 * nbActiveAgents);
						if (Math.random() < probaDoOperation) {
							// restart agent
							int nbExpiredAgents = homeContent.getNbExpiredAgents(agentType);
							double probaAddNewAgent = Math.exp(-0.5 * nbExpiredAgents);
							if (Math.random() < probaAddNewAgent) {
								addAgent(agentForm);
							} else {
								AgentForm agentFormToRestart = isProducer ? homeContent.getRandomInactiveProducer()
										: homeContent.getRandomInactiveConsumer();
								agentForm.setAgentName(agentFormToRestart.getAgentName());
								agentForm.setDeviceName(agentFormToRestart.getDeviceName());
								agentForm.setDeviceCategoryCode(agentFormToRestart.getDeviceCategoryCode());
								agentForm.setDeviceCategoryLabel(agentFormToRestart.getDeviceCategoryLabel());
								agentForm.setId(agentFormToRestart.getId());
								restartAgent(agentForm);
							}
						}
						refreshHomeContent();
					}
				}
				Thread.sleep(1000);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e);
			}
		}
	}

}
