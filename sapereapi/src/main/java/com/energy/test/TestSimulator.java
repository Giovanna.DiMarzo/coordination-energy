package com.energy.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.energy.model.AgentType;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.PriorityLevel;
import com.energy.model.SimulatorLog;
import com.energy.util.SimulatorLogger;
import com.sapereapi.model.AgentFilter;
import com.sapereapi.model.AgentForm;
import com.sapereapi.model.HomeContent;
import com.sapereapi.model.InitializationForm;
import com.sapereapi.model.SapereUtil;
import com.sapereapi.model.UtilJsonParser;

public class TestSimulator {
	protected static String baseUrl = "http://localhost:9090/energy/";
	protected static SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public final static String QUOTE = "\"";
	protected static int debugLevel = 0;
	protected static SimulatorLogger logger = SimulatorLogger.getInstance();
	protected static List<Device> allDevices = new ArrayList<Device>();
	protected static HomeContent homeContent = null;
	static Random random = new Random();

	protected static void init(String args[]) {
		String baseUrlTag = "-baseUrl:";
		for(String arg : args) {
			if(arg.startsWith(baseUrlTag)) {
				int len = baseUrlTag.length();
				String content = arg.substring(len);
				baseUrl = content;
				// -baseUrl:http://localhost:9191/energy/
			}
		}
		allDevices = getHomeDevices();
		homeContent = getHomeContent();
	}

	protected static Device chooseSleepingDevice(HomeContent partialHomeContent, boolean isProducer, String category, double powerMin,
			double powerMax, List<String> excludedDevices) {
		List<Device> devices = getDevices(partialHomeContent, isProducer, category, Device.STATUS_SLEEPING, powerMin, powerMax, excludedDevices);
		if (devices.size() > 0) {
			Collections.shuffle(devices);
			return devices.get(0);
		}
		return null;
	}

	protected static Device findDeviceToRemove(List<Device> listDevices, double minPowerLimit) {
		for (Device device : listDevices) {
			if (device.getPowerMin() > minPowerLimit) {
				return device;
			}
		}
		return null;
	}

	protected static List<Device> aux_selectDevices(List<Device> listDevices, double powerTarget) {
		double powerMax = 0;
		List<Device> devices = new ArrayList<Device>();
		for (Device device : listDevices) {
			if (device.getPowerMin() <= powerTarget) {
				devices.add(device);
			}
		}
		List<Device> selectedDevices = new ArrayList<Device>();
		// while(powerMax < powerTarget && devices.size() > 0) {
		while (powerMax < powerTarget && devices.size() > 0) {
			double maxPowerMin = powerTarget - getPowerMin(selectedDevices);
			Device toRemove = null;
			while ((toRemove = findDeviceToRemove(devices, maxPowerMin)) != null) {
				devices.remove(toRemove);
			}
			// Remove devices where powerMin > toAdd
			if (devices.size() > 0) {
				Collections.shuffle(devices);
				Device selected = devices.remove(0);
				selectedDevices.add(selected);
				powerMax += selected.getPowerMax();
			}
		}
		return selectedDevices;
	}

	protected static double getPowerMin(List<Device> listDevices) {
		double powerMin = 0;
		for (Device device : listDevices) {
			powerMin += device.getPowerMin();
		}
		return powerMin;
	}

	protected static double getPowerMax(List<Device> listDevices) {
		double powerMax = 0;
		for (Device device : listDevices) {
			powerMax += device.getPowerMax();
		}
		return powerMax;
	}

	protected static double getCurrentPower(Collection<Device> listDevices) {
		double powerMin = 0;
		for (Device device : listDevices) {
			powerMin += device.getCurrentPower();
		}
		return powerMin;
	}

	protected static List<String> getDeviceNames(List<Device> listDevices) {
		List<String> result = new ArrayList<String>();
		for (Device device : listDevices) {
			result.add(device.getName());
		}
		return result;
	}

	protected static boolean checkup(List<Device> listDevices, double powerTarget) {
		return powerTarget >= getPowerMin(listDevices) && powerTarget <= getPowerMax(listDevices);
	}


	protected static List<Device> chooseListDevices(HomeContent partialHomeContent, boolean isProducer, String category, String deviceStatus, double powerTarget, List<String> excludedDevices) {
		List<Device> devices = getDevices(partialHomeContent, isProducer, category, deviceStatus, 0, powerTarget, excludedDevices);
		if (getPowerMax(devices) < powerTarget) {
			// not enough available power
			logger.info(
					"chooseListSleepingDevices " + category + " : not enough power available to reach "
							+ powerTarget + " watts. available : " + getPowerMax(devices) + "  in " + devices);
			List<Device> selected = aux_selectDevices(devices, powerTarget);
			return selected;
			// return new ArrayList<Device>();
		}
		List<Device> selected = aux_selectDevices(devices, powerTarget);
		boolean isOK = checkup(selected, powerTarget);
		int nbTry = 1;

		while (!isOK && nbTry < 100 && selected.size() > 0) {
			selected = aux_selectDevices(devices, powerTarget);
			logger.info("power min = " + getPowerMin(selected));
			logger.info("power max = " + getPowerMax(selected));
			isOK = checkup(selected, powerTarget);
			nbTry++;
		}

		if (isOK) {
			return selected;
		}
		return selected;
	}

	protected static AgentForm chooseRunningAgent(HomeContent partialContent, boolean isProducer, String deviceCategory, double minPowerfilter,
			double maxPowerFilter, List<String> deviceNamesToExclude ) {
		Map<String, AgentForm> mapRunningAgents = partialContent.getMapRunningAgents();
		AgentType agentTypeFilter = isProducer ? AgentType.PRODUCER : AgentType.CONSUMER;
		if (mapRunningAgents.size() > 0) {
			List<AgentForm> runningAgents = new ArrayList<AgentForm>();
			for (AgentForm agentForm : mapRunningAgents.values()) {
				if (agentForm.getPower()> 0) {
					if (agentTypeFilter.getLabel().equals(agentForm.getAgentType())) {
						if (deviceCategory == null || deviceCategory.equals(agentForm.getDeviceCategoryCode())) {
							if (minPowerfilter <= 0 || agentForm.getPower() >= minPowerfilter) {
								if (maxPowerFilter <= 0 || agentForm.getPower() <= maxPowerFilter) {
									if(deviceNamesToExclude==null || !deviceNamesToExclude.contains(agentForm.getDeviceName())) {
										runningAgents.add(agentForm);
									}
								}
							}
						}
					}
				}
			}
			if (runningAgents.size() > 0) {
				Collections.shuffle(runningAgents);
				return runningAgents.get(0);
			}
		}
		return null;
	}

	protected static boolean aux_isRuinning(Device aDevice, Map<String, AgentForm> mapRunningAgents) {
		if (!mapRunningAgents.containsKey(aDevice.getName())) {
			return false;
		}
		AgentForm agentForm = mapRunningAgents.get(aDevice.getName());
		if(agentForm.getHasExpired()) {
			return false;
		}
		if(agentForm.getIsDisabled()) {
			return false;
		}
		return true;
	}

	protected static List<Device> getDevices(HomeContent partialhomeContent, boolean isProducer, String deviceCategory, String statusFilter,
			double minPowerfilter, double maxPowerFilter, List<String> excludedDevices) {
		List<Device> result = new ArrayList<Device>();
		Map<String, AgentForm> mapRunningAgents = partialhomeContent.getMapRunningAgents();
		for (Device nextDevice : allDevices) {
			boolean isRunning = aux_isRuinning(nextDevice, mapRunningAgents);
			String deviceStatus = isRunning ? Device.STATUS_RUNNING : Device.STATUS_SLEEPING;
			if (nextDevice.isProducer() == isProducer) {
				if (deviceCategory == null || deviceCategory.equals(nextDevice.getCategory())) {
					if (statusFilter == null || statusFilter.equals(deviceStatus)) {
						if (minPowerfilter <= 0 || nextDevice.getPowerMin() >= minPowerfilter) {
							if (maxPowerFilter <= 0 || nextDevice.getPowerMin() <= maxPowerFilter)
								if(excludedDevices==null || !excludedDevices.contains(nextDevice.getName())) {
									result.add(nextDevice);
								}
						}
					}
				}
			}
		}
		return result;
	}

	protected static Device getDeviceByName(String name) {
		if (name != null) {
			for (Device device : allDevices) {
				if (name.equals(device)) {
					return device;
				}
			}
		}
		return null;
	}

	protected static boolean hasDevice(HomeContent homeContent, boolean isProducer, String category, String statusFilter, double minPowerFilter,
			double maxPowerFilter, List<String> excludedDevices) {
		List<Device> devices = getDevices(homeContent, isProducer, category, statusFilter, minPowerFilter, maxPowerFilter, excludedDevices);
		return !devices.isEmpty();
	}

	protected static void executeScenario(List<Event> scenario) {
		List<Event> eventQueue = new ArrayList<Event>();
		for (Event event : scenario) {
			eventQueue.add(event);
		}
		Date current = new Date();
		while (eventQueue.size() > 0) {
			List<Event> toRemove = new ArrayList<Event>();
			for (Event event : eventQueue) {
				AgentForm agentForm = new AgentForm();
				agentForm.setPower(event.getPower());
				agentForm.setBeginDate(event.getBeginDate());
				agentForm.setEndDate(event.getEndDate());
				agentForm.setDuration(event.getDuration());
				agentForm.setDeviceName(event.getDeviceName());
				agentForm.setDeviceCategory(event.getDeviceCategory());
				if (EventType.PRODUCTION.equals(event.getType())) {
					agentForm.setAgentType(AgentType.PRODUCER.getLabel());
				} else if (EventType.REQUEST.equals(event.getType())) {
					agentForm.setAgentType(AgentType.CONSUMER.getLabel());
					float tolerance = SapereUtil.computeDurationMinutes(event.getBeginDate(), event.getEndDate());
					agentForm.setDelayToleranceMinutes(tolerance);
					agentForm.setPriorityLevel(PriorityLevel.LOW.getLabel());
				}
				if (event.getBeginDate().before(current)) {
					addAgent(agentForm);
					toRemove.add(event);
				}
			}
			for (Event event : toRemove) {
				eventQueue.remove(event);
			}
		}
	}

	protected static HomeContent restartLastHomeContent() {
		String postResponse = sendGetRequest(baseUrl + "restartLastHomeContent");
		JSONObject jsonHomeContent = new JSONObject(postResponse);
		if (debugLevel > 2) {
			logger.info("obj : " + jsonHomeContent);
		}
		HomeContent result = UtilJsonParser.parseHomeContent(jsonHomeContent);
		return result;
	}


	protected static List<Device> getHomeDevices() {
		String postResponse = sendGetRequest(baseUrl + "retrieveHomeDevices");
		List<Device> result = new ArrayList<Device>();
		if(postResponse==null) {
			return result;
		}
		JSONArray jsonHomeDevices = new JSONArray(postResponse);
		if (debugLevel > 2) {
			logger.info("obj : " + jsonHomeDevices);
		}
		for (int i = 0; i < jsonHomeDevices.length(); i++) {
			JSONObject jsonDevice= jsonHomeDevices.getJSONObject(i);
			try {
				result.add(UtilJsonParser.parseDevice(jsonDevice));
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		return result;
	}

	protected static Map<String, Double> retrieveDeviceStatistics(DeviceCategory[] categories, Integer hourOfDay) {
		Map<String, Double> result = new HashMap<>();
		List<String> listCategoryNames = new ArrayList<String>();
		for(DeviceCategory deviceCategory : categories) {
			listCategoryNames.add(deviceCategory.name());
		}
		Map<String, String> params = new HashMap<String, String>();
		if(hourOfDay!=null) {
			params.put("hourOfDay", ""+hourOfDay);
		}
		String postResponse = sendGetRequest(baseUrl + "retrieveDeviceStatistics");
		if(postResponse==null) {
			return new HashMap<String, Double>();
		}
		JSONObject jsonMap = new JSONObject(postResponse);
		try {
			Map<String, Double> resultToFolter = UtilJsonParser.parseJsonMap2(jsonMap);
			for(String category : resultToFolter.keySet()) {
				if(listCategoryNames.size()==0 || listCategoryNames.contains(category)) {
					result.put(category, resultToFolter.get(category));
				}
			}
			return result;
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
		return new HashMap<String, Double>();
	}

	protected static HomeContent getPartialHomeContent(String category, boolean isProducer) {
		AgentFilter filter = new AgentFilter();
		String[] consumerDeviceCategories= {};
		String[] producerDeviceCategories = {};
		if(isProducer) {
			producerDeviceCategories = new String[] {category};
		} else {
			consumerDeviceCategories = new String[] {category};
		}
		filter.setConsumerDeviceCategories(consumerDeviceCategories);
		filter.setProducerDeviceCategories(producerDeviceCategories);
		Map<String, String> params = new HashMap<String, String>();
		params.put("producerDeviceCategories", isProducer ? category : "");
		params.put("consumerDeviceCategories", isProducer ? "" : category );
		String postResponse = sendGetRequest(baseUrl + "homeContent", params);
		if(postResponse==null) {
			return null;
		}
		JSONObject jsonHomeContent = new JSONObject(postResponse);
		if (debugLevel > 2) {
			logger.info("obj : " + jsonHomeContent);
		}
		HomeContent result = UtilJsonParser.parseHomeContent(jsonHomeContent);
		if(result==null) {
			logger.warning("getHomeContent result is null");
		}
		return result;
	}

	protected static HomeContent getHomeContent() {
		String postResponse = sendGetRequest(baseUrl + "homeContent");
		if(postResponse==null) {
			return null;
		}
		JSONObject jsonHomeContent = new JSONObject(postResponse);
		if (debugLevel > 2) {
			logger.info("obj : " + jsonHomeContent);
		}
		HomeContent result = UtilJsonParser.parseHomeContent(jsonHomeContent);
		if(result==null) {
			logger.warning("getHomeContent result is null");
		}
		return result;
	}

	protected static AgentForm addAgent(AgentForm agentForm) {
		try {
			String postResponse = sendPostRequest(baseUrl + "addAgent", generateAgentFormParams(agentForm));
			if (postResponse == null) {
				logger.warning("addAgent : For debug : postResponse is null ");
			}
			JSONObject jsonAgent = new JSONObject(postResponse);
			agentForm = UtilJsonParser.parseAgentForm(jsonAgent);
			//refreshHomeContent();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}
		return agentForm;
	}

	protected static void refreshHomeContent() {
		homeContent = getHomeContent();
		if(homeContent==null)  {
			logger.warning("refreshHomeContent : homeContent is null");
			int nbTry=0;
			while(homeContent==null &&  nbTry<10) {
				homeContent = getHomeContent();
				 nbTry++;
				 try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.error(e);
				}
			}
		} else {
			for(Device device : allDevices) {
				AgentForm agentForm = homeContent.getAgentByDeviceName(device.getName());
				if(agentForm!=null)  {
					device.setCurrentPower(agentForm.getPower());
				} else {
					device.setCurrentPower(0);
				}
			}
		}
	}

	public static Map<String, String> generateInitializationParams(InitializationForm initForm) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("scenario", "" + initForm.getScenario());
		params.put("initialStateId", "" + initForm.getInitialStateId());
		params.put("initialStateVariable", "" + initForm.getInitialStateVariable());
		params.put("forcedHourOfDay", ""+ initForm.getForcedHourOfDay());
		return params;
	}

	public static Map<String, String> generateAgentFormParams(AgentForm agentForm) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("power", "" + agentForm.getPower());
		params.put("deviceName", "" + agentForm.getDeviceName());
		params.put("deviceCategory", "" + agentForm.getDeviceCategoryCode());
		params.put("delayToleranceMinutes", "" + agentForm.getDelayToleranceMinutes());
		params.put("beginDate", format_datetime.format(agentForm.getBeginDate()));
		params.put("endDate", format_datetime.format(agentForm.getEndDate()));
		params.put("priorityLevel", agentForm.getPriorityLevel());
		params.put("agentType", agentForm.getAgentType());
		params.put("agentName", agentForm.getAgentName());
		// return params;
		Map<String, String> params1 = generateRequestParams(agentForm);
		params1.put("deviceCategory", "" + agentForm.getDeviceCategoryCode());
		try {
			SapereUtil.checkParams(params1, params);
		} catch (Exception e) {
			logger.error(e);
		}
		// logger.info("generateParams : agentName = " + agentForm.getAgentName() + ",
		return params1;
	}



	public static Map<String, String> generateSimulatorLogParams(SimulatorLog simulatorLog) {
		Map<String, String> params1 = generateRequestParams(simulatorLog);
		Map<String, String> params = new HashMap<String, String>();
		params.put("loopNumber", "" + simulatorLog.getLoopNumber());
		params.put("deviceCategoryCode", "" + simulatorLog.getDeviceCategoryCode());
		params.put("powerTarget", "" + simulatorLog.getPowerTarget());
		params.put("powerTargetMin", ""+simulatorLog.getPowerTargetMin());
		params.put("powerTargetMax", ""+simulatorLog.getPowerTargetMax());
		params.put("power", "" + simulatorLog.getPower());
		params.put("reached", ""+simulatorLog.isReached());
		params.put("nbStarted", ""+simulatorLog.getNbStarted());
		params.put("nbModified", ""+simulatorLog.getNbModified());
		params.put("nbStopped", ""+simulatorLog.getNbStopped());
		params.put("nbDevices", ""+simulatorLog.getNbDevices());
		params.put("targetDeviceCombinationFound", ""+simulatorLog.isTargetDeviceCombinationFound());
		params.put("class", ""+simulatorLog.getClass());
		// logger.info("generateParams : agentName = " + agentForm.getAgentName() + ",
		// originEndDate = " + agentForm.getEndDate() + " , result endDate = " +
		// params.get("endDate") + " timezone = " + format_datetime.getTimeZone());
		// {,  ,  , , ,   , , class=class com.energy.model.SimulatorLog}
		boolean test = params1.equals(params);
		if(!test) {
			logger.error("generateSimulatorLogParams Not equals");
		}
		return params1;
	}

	public static Map<String, String> generateRequestParams(Object anObject) {
		Map<String, String> params = new HashMap<String, String>();
		Class<?> targetObjectClass = anObject.getClass();
		for(Method method : targetObjectClass.getMethods())  {
			if(method.getParameterCount()==0) {
				String fieldName = null;
				if(method.getName().startsWith("get")) {
					fieldName = SapereUtil.firstCharToLower(method.getName().substring(3));
				} else if(method.getName().startsWith("is")) {
					//System.out.print("For debug : is method");
					fieldName = SapereUtil.firstCharToLower(method.getName().substring(2));
				}
				if(fieldName!=null) {
					try {
						Object value = method.invoke(anObject);
						if(value!=null) {
							if(value instanceof Date) {
								params.put(fieldName, format_datetime.format(value));
							} else {
								params.put(fieldName, ""+value);
							}
						}
					} catch (Throwable e) {
						SimulatorLogger.getInstance().error(e);
						if(anObject instanceof AgentForm) {
							AgentForm agentForm = (AgentForm) anObject;
							boolean test = agentForm.isRunning();
							System.out.print(test);
						}
					}
				}
			}
		}
		return params;
	}

	public static void initEnergyService(InitializationForm initForm ) {
		try {
			Map<String, String> params = generateInitializationParams(initForm);
			String postResponse = sendPostRequest(baseUrl + "initEnergyService", params);
			if(postResponse==null) {
				logger.info("initEnergyService : no post response");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}

	protected static AgentForm modifyAgent(AgentForm agentForm) {
		try {
			Map<String, String> params = generateAgentFormParams(agentForm);
			//Thread.sleep(5000);
			String postResponse = sendPostRequest(baseUrl + "modifyAgent", params);
			logger.info("modifyAgent : " + agentForm.getAgentName() + " , power = " + params.get("power"));
			JSONObject jsonAgent = new JSONObject(postResponse);
			AgentForm result = UtilJsonParser.parseAgentForm(jsonAgent);
			Thread.sleep(1000);
			//refreshHomeContent();
			float powerToSet = agentForm.getPower();
			int nbWaits = 0;
			while((Math.abs(result.getPower() - powerToSet) > 0.001) && nbWaits<3) {
				Thread.sleep(1000);
				HomeContent partialContent = getPartialHomeContent(agentForm.getDeviceCategoryCode(), agentForm.isProducer());
				result = partialContent.getAgent(agentForm.getAgentName());
				nbWaits++;
			}
			/*
			refreshHomeContent();
			result  = homeContent.getAgent(agentForm.getAgentName());
			*/
			return result;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}
		return null;
	}

	protected static AgentForm restartAgent(AgentForm agentForm) {
		try {
			Map<String, String> params = generateAgentFormParams(agentForm);
			//Thread.sleep(5000);
			String postResponse = sendPostRequest(baseUrl + "restartAgent", params);
			logger.info("restartAgent : " + agentForm.getAgentName() + " , endDate = " + params.get("endDate"));
			JSONObject jsonAgent = new JSONObject(postResponse);
			AgentForm result = UtilJsonParser.parseAgentForm(jsonAgent);
			//refreshHomeContent();
			/*
			refreshHomeContent();
			result  = homeContent.getAgent(agentForm.getAgentName());
			*/
			return result;
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
		return null;
	}

	protected static boolean stopAgent(AgentForm agentForm) {
		String agentName = agentForm.getAgentName();
		Map<String, String> params = new HashMap<String, String>();
		params.put("agentName", "" + agentName);
		boolean isOK = false;
		try {
			String postResponse = sendPostRequest(baseUrl + "stopAgent", params);
			JSONObject jsonAgent = new JSONObject(postResponse);
			AgentForm stopedAgent = UtilJsonParser.parseAgentForm(jsonAgent);
			/*
			 * Thread.sleep(1000); homeContent = getHomeContent(); AgentForm stopedAgent =
			 * homeContent.getAgent(agentName);
			 */
			isOK = stopedAgent.getHasExpired();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}
		//refreshHomeContent();
		return isOK;
	}

	protected static void addSimulatorLog(SimulatorLog simulatorLog) {
		Map<String, String> params = generateSimulatorLogParams(simulatorLog);
		try {
			String postResponse = sendPostRequest(baseUrl + "addSimulatorLog", params);
			if(postResponse==null) {
				logger.info("addSimulatorLog : no post response");
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	protected static void resetSimulatorLogs() {
		try {
			Map<String, String> params = new HashMap<String, String>();
			String postResponse = sendPostRequest(baseUrl + "resetSimulatorLogs", params);
			if(postResponse==null) {
				logger.info("resetSimulatorLogs : no post response");
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	protected static String sendGetRequest(String url) {
		 Map<String, String> params = new HashMap<String, String>();
		 return sendGetRequest(url, params);
	}


	protected static String sendGetRequest(String url, Map<String, String> params) {
		try {
			String readLine = null;
			if(params.size()>0) {
				//logger.info("sendGetRequest : params = " + params);
			}
			boolean encodeUtf8 = true;
			String sUrl = formatRequest(url, params, encodeUtf8);
			URL uri = new URL(sUrl);
			HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
			if(encodeUtf8) {
				connection.setRequestProperty("Accept-Charset", "UTF-8");
				connection.setRequestProperty("charset", "utf-8");
				connection.setRequestProperty("Content-Length", String.valueOf(sUrl.length()));

			}
			connection.setRequestMethod("GET");
			//connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			// conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample
			// here
			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in.readLine()) != null) {
					response.append(readLine);
				}
				in.close();
				// print result
				if (debugLevel > 2) {
					logger.info("JSON String Result " + response.toString());
				}
				return response.toString();
				// GetAndPost.POSTRequest(response.toString());
			} else {
				logger.warning("GET NOT WORKED");
			}
		} catch (Throwable e) {
			logger.error(e);
		}
		return null;
	}

	public static String formatRequest(String baseUrl, Map<String, String> params, boolean useUtf8)  throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        result.append(false && useUtf8 ? URLEncoder.encode(baseUrl, "UTF-8") : baseUrl);
        result.append("?");
        for (Map.Entry<String, String> entry : params.entrySet()) {
          result.append(useUtf8 ? URLEncoder.encode(entry.getKey(), "UTF-8") : entry.getKey());
          result.append("=");
          result.append(useUtf8 ? URLEncoder.encode(entry.getValue(), "UTF-8") : entry.getValue());
          result.append("&");
        }
        String resultString = result.toString();
        return resultString.length() > 0
          ? resultString.substring(0, resultString.length() - 1)
          : resultString;
    }

	protected static String quote(String str) {
		return QUOTE + str + QUOTE;
	}

	protected static String sendPostRequest(String sUrl, Map<String, String> params) throws Exception {
		boolean encodeUtf8 = true;
		try {
			URL url = new URL(sUrl);
			StringBuilder bPostParams = new StringBuilder();
			bPostParams.append("{");
			String sep = "";
			for (String field : params.keySet()) {
				String pvalue = params.get(field);
				/*
				 * if(pvalue!=null && pvalue.indexOf("TV ") >=0 ) { logger.info("For debug " +
				 * pvalue); }
				 */
				if (pvalue != null && pvalue.indexOf("\"") >= 0) {
					String replace = "\\" + "\"";
					pvalue = pvalue.replace("\"", replace);
				}
				bPostParams.append(sep).append(quote(field)).append(":").append(quote(pvalue));
				sep = ",";
			}

			bPostParams.append("}");
			if (debugLevel > 2) {
				logger.info(bPostParams.toString());
			}
			byte[] postDataBytes = bPostParams.toString().getBytes("UTF-8");
			HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
			postConnection.setRequestMethod("POST");
			// conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			postConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			// postConnection.setRequestProperty( "Content-Type",
			// "application/x-www-form-urlencoded");
			if (encodeUtf8) {
				postConnection.setRequestProperty("charset", "utf-8");
				postConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			}
			postConnection.setDoOutput(true);
			if (encodeUtf8) {
				// postConnection.getOutputStream().write(postDataBytes);
				postConnection.getOutputStream().write(bPostParams.toString().getBytes("utf-8"));
			} else {
				postConnection.getOutputStream().write(bPostParams.toString().getBytes());
			}
			int responseCode = postConnection.getResponseCode();
			if (debugLevel > 0) {
				logger.info("POST Response Code :  " + responseCode);
				logger.info("POST Response Message : " + postConnection.getResponseMessage());
			}

			if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				// print result
				if (debugLevel > 2) {
					logger.info(response.toString());
				}
				return response.toString();
			} else {
				if (responseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
					System.out.print("For debug request = " + params);
				}
				logger.error("POST NOT WORKED " + params);
				throw new Exception("Post request failed : code " + responseCode);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			logger.error(e1);
			throw e1;
		}
	}
}
