package com.energy.agent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.energy.model.AgentType;
import com.energy.model.ConfirmationItem;
import com.energy.model.ConfirmationTable;
import com.energy.model.EnergyRequest;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.HomeTotal;
import com.energy.model.PriorityLevel;
import com.energy.model.ProtectedConfirmationTable;
import com.energy.model.ProtectedContract;
import com.energy.model.ProtectedSingleOffer;
import com.energy.model.RegulationWarning;
import com.energy.model.RescheduleItem;
import com.energy.model.RescheduleTable;
import com.energy.model.SingleOffer;
import com.energy.model.WarningType;
import com.energy.util.DoublonException;
import com.energy.util.EnergyDbHelper;
import com.energy.util.PermissionException;
import com.energy.util.SapereLogger;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;

import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class ProducerAgent extends SapereAgent implements IEnergyAgent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static int OFFER_VALIDITY_SECONDS = 8;
	public final static int OK_PROD_DECAY = 1;
	public final static int CHECKUP_PERDIOD_SEC = 6;
	public final static int POLICY_RANDOM = 10;
	public final static int POLICY_MAXWARNING_MINPOWER = 11;
	public final static int POLICY_MIX = 12;
	public final static double PROBA_RANDOM_CHOICE = 0.002;
	public final static int DISABLED_DURATION_MINUTES = 10;
	public final static int OFFER_EXPIRATION_MARGIN_SEC = 1;
	Random rand = new Random();
	//private String url;
	private EnergySupply globalSupply = null;
	private int id;
	private Map<String, EnergyRequest> tableWaitingRequest = null;
	private Map<String, SingleOffer> tableWaitingOffers = null;
	private Map<String, ProtectedContract> tableContracts = null;
	private Map<String,String> tableConsumerConfirmTag = null;
	//private Map<String, Integer> pOkDecays = null;
	private Map<String, Lsa> tableChosenLsa = null;
	//private Date postEventTime = null;
	private boolean firstDecay = true;
	private Event startEvent = null;
	private Event expiryEvent = null;
	private Event stopEvent = null;
	private int eventDecay = 0;
	//private int checkupDecay = 0;
	private boolean instanceDoublon = false;
	private boolean activateOfferRemoveLog = false;
	private List<Property> propertiesToPost = new ArrayList<Property>();
	private static SapereLogger logger = SapereLogger.getInstance();
	private boolean alwaysSendConfirmations = true;
	private boolean showWaitingOffers = false; // used for debug to display waiting offers on LSA in WAITING_OFFERS  property
	private boolean showAgreements = false; // used for  debug to display confirmations to contract agents in AGREEMENTS   property
	//private boolean isInSpace = false;
	private List<RegulationWarning> receivedWarnings = new ArrayList<RegulationWarning>();
	private Map<String, ConfirmationItem> receivedConfirmations = new HashMap<String, ConfirmationItem>();
	private Date timeLastDecay = null;
	private String location = NodeManager.getLocation();


	public ProducerAgent(int _id, AgentAuthentication authentication, EnergySupply _globalSupply) {
		super(authentication.getAgentName()
				,authentication
				,new String[] { "REQ", "CONTRACT", "SATISFIED", "WARNING", "RESCHEDULE"}
				,new String[] { "PROD", "EVENT", "OFFER", "PROD_CONFIRM"}, LsaType.Service);
		initFields( _id,  authentication, _globalSupply);
	}

	private void initFields(int _id, AgentAuthentication _authentication, EnergySupply _globalSupply) {
		setEpsilon(0); // No greedy policy
		this.id = _id;
		this.authentication = _authentication;
		super.setInput(new String[] { "REQ", "CONTRACT", "SATISFIED", "WARNING", "RESCHEDULE"});
		super.setOutput(new String[] { "PROD", "EVENT", "OFFER", "PROD_CONFIRM", "DISABLED" });
		setUrl(_authentication.getAgentLocation());
		this.globalSupply = _globalSupply;
		this.tableWaitingRequest = new HashMap<String, EnergyRequest>();
		this.tableWaitingOffers = new HashMap<String, SingleOffer>();
		this.tableContracts = new HashMap<String, ProtectedContract>();
		this.tableConsumerConfirmTag = new HashMap<String, String>();
		this.tableChosenLsa = new HashMap<String, Lsa>();
		this.receivedConfirmations = new HashMap<String, ConfirmationItem>();
		//this.pOkDecays = new HashMap<String, Integer>();
		int decay = 1 + getTimeLeftSec();
		this.addDecay(decay);
		//this.postEventTime = new Date();
		this.debugLevel = 0;
		this.activateOfferRemoveLog = true;
		//logger.info("ProducerAgent : lsa = " + lsa.toVisualString());
	}

	public void reinitialize(int _id, AgentAuthentication _authentication, EnergySupply _globalSupply) {
		initFields( _id,  _authentication, _globalSupply);
		this.lsa.removeAllProperties();
		this.lsa.setAgentName("");
		// Added
		firstDecay = true;
		lsa.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
		lsa.removeSyntheticProperty(SyntheticPropertyName.BOND);
		lsa.removeSyntheticProperty(SyntheticPropertyName.DIFFUSE);
		lsa.removeSyntheticProperty(SyntheticPropertyName.QUERY);
		lsa.removeSyntheticProperty(SyntheticPropertyName.SOURCE);
		lsa.removeSyntheticProperty(SyntheticPropertyName.GRADIENT_HOP);
		this.authentication = _authentication;
		logger.info(this.agentName + " reinitialize : lsa = " + lsa.toVisualString()+ " this="+ this + " memory address =" + this.hashCode());
	}

	public EnergySupply getGlobalSupply() {
		return globalSupply;
	}

	// Request comparator : priority and warning counter and power
	private static final Comparator<EnergyRequest> requestComparatorDistancePriorityAndWarningAndPower = new Comparator<EnergyRequest>() {
		public int compare(EnergyRequest req1, EnergyRequest req2) {
			return req1.compareDistanceAndPriorityDescAndWarningDescAndPower(req2);
		}
	};

	// Request comparator : priority and power
	/*
	private static final Comparator<EnergyRequest> requestComparatorPriorityAndPower = new Comparator<EnergyRequest>() {
		public int compare(EnergyRequest req1, EnergyRequest req2) {
			return req1.comparePriorityDescAndPower(req2);
		}
	};*/

	// Request comparator : priority
	private static final Comparator<EnergyRequest> requestComparatorPriority = new Comparator<EnergyRequest>() {
		public int compare(EnergyRequest req1, EnergyRequest req2) {
			return req1.comparePriorityDesc(req2);
		}
	};

	/*
	private static final Comparator<Contract> contractComparator = new Comparator<Contract>() {
		public int compare(Contract contract1, Contract contract2) {
			return contract1.comparePower(contract2);
		}
	};

	private static final Comparator<EnergySupply> supplyComparator = new Comparator<EnergySupply>() {
		public int compare(EnergySupply supply1, EnergySupply supply2) {
			int comaprePower = supply1.comparePower(supply2);
			if(comaprePower==0) {
				return supply1.comparTimeLeft(supply2);
			} else {
				return comaprePower;
			}
		}
	};*/

	public Map<String, SingleOffer> getTableWaitingOffers() {
		return tableWaitingOffers;
	}

	public void refreshWaitingOffersProperty() {
		lsa.removePropertiesByName("DEBUG_WAITING_OFFERS");
		if(showWaitingOffers) {
			if(tableWaitingOffers!=null) {
				addProperty(new Property("DEBUG_WAITING_OFFERS", tableWaitingOffers.toString()));
			}
		}
	}

	public void refreshAggrementsProperty() {
		lsa.removePropertiesByName("DEBUG_AGREEMENTS");
		if(showAgreements) {
			if(tableContracts!=null) {
				StringBuffer content = new StringBuffer();
				String sep="";
				for(ProtectedContract nextContract : tableContracts.values()) {
					content.append(sep);
					content.append(nextContract);
					sep = "    -----------------    ";
				}
				addProperty(new Property("DEBUG_AGREEMENTS", content));
			}
		}
	}

	public Map<String, ProtectedContract> getTableContracts(boolean addValid, boolean addWaiting, boolean addInvalid) {
		Map<String, ProtectedContract> result = new HashMap<String, ProtectedContract>();
		for(ProtectedContract nextContract : tableContracts.values()) {
			String consumer = nextContract.getConsumerAgent();
			if (addValid && nextContract.hasAllAgreements()) {
				result.put(consumer, nextContract);
			} else if (addWaiting && nextContract.waitingValidation()) {
				result.put(consumer, nextContract);
			} else if (addInvalid && nextContract.hasDisagreement()) {
				result.put(consumer, nextContract);
			}
		}
		return result;
	}

	public Map<String, ProtectedContract> getTableAllContracts() {
		return getTableContracts(true, true, true);
	}

	public Map<String, ProtectedContract> getTableValidContracts() {
		return getTableContracts(true, false, false);
	}

	public Collection<ProtectedContract> getValidContracts() {
		return getTableValidContracts().values();
	}

	public Map<String, ProtectedContract> getTableCanceledContracts() {
		return getTableContracts(false, false, true);
	}

	public Map<String, ProtectedContract> getTableWaitingContracts() {
		return getTableContracts(false, true, false);
	}

	public Float computePowerOfWaitingContrats() {
		Float waitingContractsPower = new Float(0);
		for (ProtectedContract contract : getTableWaitingContracts().values()) {
			try {
				waitingContractsPower += contract.getProducerPower(this);
			} catch (PermissionException e) {
				logger.error(e);
			}
		}
		return waitingContractsPower;
	}

	public List<String> getConsumersOfWaitingContrats() {
		List<String> waitingContractsConsumers = new ArrayList<String>();
		for (ProtectedContract contract : getTableWaitingContracts().values()) {
			waitingContractsConsumers.add(contract.getConsumerAgent());
		}
		return waitingContractsConsumers;
	}

	public Set<String> getLinkedAgents() {
		Set<String> result = new HashSet<>();
		for (ProtectedContract contract : getValidContracts()) {
			if (!contract.hasExpired()) {
				result.add(contract.getConsumerAgent());
			}
		}
		return result;
	}

	public Float getContractsPower(String location) {
		Float power = new Float(0);
		for (ProtectedContract contract : getValidContracts()) {
			if (!contract.hasExpired()) {
				try {
					String consumerLocation = contract.getConsumerLocation();
					if(location==null || location.equals(consumerLocation)) {
						power += contract.getProducerPower(this);
					} else {
						logger.info("For debug : location not found");
					}
				} catch (PermissionException e) {
					logger.error(e);
				}
			}
		}
		return power;
	}

	public Map<String, Float> getContractsRepartition() {
		Map<String, Float> result = new HashMap<String, Float>();
		for (ProtectedContract contract : getValidContracts()) {
			if (!contract.hasExpired()) {
				try {
					boolean isLocal = Sapere.getInstance().isLocalAgent(contract.getConsumerAgent());
					String agentName = contract.getConsumerAgent() + (isLocal? "" : "*");
					result.put(agentName, contract.getProducerPower(this));
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		return result;
	}

	public Float getOffersTotal() {
		Float result = new Float(0);
		for (SingleOffer offer : this.getTableWaitingOffers().values()) {
			if (!offer.hasExpired(OFFER_EXPIRATION_MARGIN_SEC)) {
				result+=offer.getPower();
			}
		}
		return result;
	}

	public Map<String, Float> getOffersRepartition() {
		Map<String, Float> result = new HashMap<String, Float>();
		for (SingleOffer offer : this.getTableWaitingOffers().values()) {
			if (!offer.hasExpired(OFFER_EXPIRATION_MARGIN_SEC)) {
				result.put(offer.getConsumerAgent(), offer.getPower());
			}
		}
		return result;
	}

	public Float computeAvailablePower(boolean ignoreOffers, boolean ignoreWaitingContracts) {
		Float reusult = globalSupply.getPower();
		Map<String, ProtectedContract> tablleContrats = getTableContracts(true, !ignoreWaitingContracts, false);
		for (ProtectedContract contract : tablleContrats.values()) {
			if (!contract.hasExpired()) {
				Float used = new Float(0);
				try {
					used = contract.getProducerPower(this);
				} catch (PermissionException e) {
					logger.error(e);
				}
				reusult -= used;
			}
		}
		if (!ignoreOffers) {
			for (SingleOffer offer : getTableWaitingOffers().values()) {
				if (!offer.hasExpired(OFFER_EXPIRATION_MARGIN_SEC)) {
					Float used = offer.getPower();
					reusult -= used;
				}
			}
		}
		return reusult;
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("PROD", this.globalSupply));
		try {
			this.startEvent = generateStartEvent();
		} catch (Exception e) {
			logger.error(e);
		}
		this.submitOperation();
	}

	public Event generateStartEvent() {
		startEvent = new Event(EventType.PRODUCTION, this.globalSupply);
		try {
			this.startEvent = EnergyDbHelper.registerEvent(startEvent);
		} catch (DoublonException e) {
			logger.error(e);
			startEvent =  EnergyDbHelper.retrieveEvent(EventType.PRODUCTION, agentName, globalSupply.getBeginDate());
		}
		if(startEvent.getId() != null) {
			this.globalSupply.setEventId(startEvent.getId());
		}
		// Remove other events
		stopEvent = null;expiryEvent = null;
		// Post event in LSA
		this.postEvent(startEvent);
		return startEvent;
	}

	public Event generateUpdateEvent(WarningType warningType) {
		if(startEvent!=null) {
			Event originEvent = null;
			try {
				originEvent = startEvent.clone();
			} catch (CloneNotSupportedException e1) {
				logger.error(e1);
			}
			Event newEvent = new Event(EventType.PRODUCTION_UPDATE, this.globalSupply);
			if(newEvent.getIssuerLocation()==null) {
				logger.info("generateUpdateEvent : for debug");
			}
			newEvent.setWarningType(warningType);
			newEvent.setOriginEvent(originEvent);
			startEvent = EnergyDbHelper.registerEvent2(newEvent);
			// Remove other events
			stopEvent = null;expiryEvent = null;
			// Post event in LSA
			postEvent(startEvent);
		}
		return startEvent;
	}

	public Event generateStopEvent(RegulationWarning warning) {
		Date timeStop = SapereUtil.getCurrentSeconde();
		stopEvent = new Event(EventType.PRODUCTION_STOP, agentName, globalSupply.getIssuerLocation(), this.globalSupply.getPower(), timeStop, timeStop, globalSupply.getDeviceName(), globalSupply.getDeviceCategory());
		try {
			stopEvent.setOriginEvent(startEvent.clone());
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		if(warning!=null) {
			stopEvent.setWarningType(warning.getType());
		}
		stopEvent = EnergyDbHelper.registerEvent2(stopEvent);
		// Remove other events
		startEvent = null;expiryEvent = null;
		// Post event in LSA
		postEvent(stopEvent);
		return stopEvent;
	}

	public Event generateExpiryEvent() {
		expiryEvent = new Event(EventType.PRODUCTION_EXPIRY, agentName, globalSupply.getIssuerLocation(), globalSupply.getPower(), globalSupply.getEndDate(),  globalSupply.getEndDate(),  globalSupply.getDeviceName(), globalSupply.getDeviceCategory());
		try {
			if(startEvent!=null) {
				expiryEvent.setOriginEvent(startEvent.clone());
			}
		} catch (Exception e) {
			logger.error(e);
		}
		expiryEvent = EnergyDbHelper.registerEvent2(expiryEvent);
		// Remove other events
		startEvent = null; stopEvent = null;
		// Post event in LSA
		postEvent(expiryEvent);
		return expiryEvent;
	}

	public void postEvent(Event eventToPost) {
		this.lsa.removePropertiesByName("EVENT");
		try {
			addProperty(new Property("EVENT", eventToPost.clone()));
			eventDecay = SapereUtil.EVENT_INIT_DECAY;
			firstDecay = true;
			//this.postEventTime = new Date();
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
	}
/*
	private SingleOffer getWaitingOffer(String consumer) {
		return tableWaitingOffers.get(consumer);
	}
*/
	private void addOffer(SingleOffer newOffer) {
		String consumer = newOffer.getConsumerAgent();
		Lsa chosenLSA = tableChosenLsa.get(consumer);
		String ipSource = (chosenLSA==null)? "" : chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
		String sState = "";
		removeOffer(consumer, "addOffer");
		try {
			sState = SapereUtil.addOutputsToLsaState(chosenLSA, new String[] { "OFFER" });
		} catch (Exception e) {
			logger.error(e);
		}
		try {
			// add offer in OFFER property
			ProtectedSingleOffer protectedOffer = new ProtectedSingleOffer(newOffer.clone());
			Property pOffer = new Property("OFFER", protectedOffer.clone(), consumer, consumer, sState,	ipSource, false);
			addProperty(pOffer);
			/*
			if(lsa.getProperties().size() >= Lsa.PROPERTIESSIZE - 1) {
				propertiesToPost.add(pOffer);
				logger.info(this.agentName + " addOffer : cannot post offer " + newOffer.getId() + " in lsa. Offer added in in propertiesToPost queue.");
			} else {
				lsa.addProperty(pOffer);
			}*/
			// add offer in WAITING_OFFERS property
			this.tableWaitingOffers.put(consumer, newOffer.clone());
			refreshWaitingOffersProperty();
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		checkup();
	}

	private void removeOffer(String consumer, String logTag) {
		// For debug
		debug_checkOfferAcquitted(consumer);
		lsa.removePropertiesByQueryAndName(consumer, "OFFER");
		SingleOffer offer = tableWaitingOffers.get(consumer);
		// Remove the offer from WAITING_OFFERS property
		if(this.activateOfferRemoveLog && tableWaitingOffers.containsKey(consumer)) {
			EnergyDbHelper.addLogOnOffer(offer.getId(), " remove at " + SapereUtil.getCurrentTimeStr() + " : " + logTag);
		}
		this.tableWaitingOffers.remove(consumer);
		refreshWaitingOffersProperty();
	}

	private void removeContract(String consumer, String logTag) {
		if(tableContracts.containsKey(consumer)) {
			ProtectedContract protectedContract = tableContracts.get(consumer);
			if(this.isConcerned(protectedContract) && !protectedContract.hasDisagreement() && !protectedContract.hasExpired()) {
				logger.info(" removeContract " + this.agentName + " remove contract " + logTag + " " + protectedContract);
			}
			tableContracts.remove(consumer);
			refreshAggrementsProperty();
		}
	}

	private void updateContract(ProtectedContract aContract, String tag) {
		if(aContract==null) {
			return;
		}
		String consumer = aContract.getConsumerAgent();
		// Remove the offer if necessary
		this.removeOffer(consumer, "updateContract " + tag);

		// update the contract
		tableContracts.put(consumer, aContract);
		// Refresh AGREEMENTS property
		refreshAggrementsProperty();
		checkup();
	}

	private boolean checkAvailabilityAndExpiration(ProtectedContract protectedContract) {
		// TO DELETE !!!
		int __foo = 0;
		if(__foo>0) {
			return false;
		}
		try {
			if(protectedContract.validationHasExpired(this)) {
				return false;
			}
		} catch (PermissionException e1) {
			logger.error(e1);
		}
		Float availablePower = this.computeAvailablePower(false, false);	// Do not ignore offers and waiting contracts
		boolean isContractAlreadyInTable = getTableAllContracts().containsKey(protectedContract.getConsumerAgent());
		if(isContractAlreadyInTable) {
			return availablePower >=-0.0001;
		} else {
			boolean isOK = false;
			try {
				isOK = availablePower >= protectedContract.getProducerPower(this) - 0.0001;
			} catch (PermissionException e) {
			}
			return isOK;
		}
	}

	private ProtectedConfirmationTable getProtectedConfirmationTable() {
		Property pOk2 = SapereUtil.getOnePropertyFromLsa(lsa, "PROD_CONFIRM");
		if(pOk2!=null && pOk2.getValue() instanceof ProtectedConfirmationTable) {
			return  (ProtectedConfirmationTable) pOk2.getValue();
		}
		ConfirmationTable confirmationTable = new ConfirmationTable(this.agentName);
		return new ProtectedConfirmationTable(confirmationTable);
	}

	private void sendConfirmation(ProtectedContract protectedContract, boolean ok) {
		ProtectedConfirmationTable protectedConfirmationTable = getProtectedConfirmationTable();
		try {
			protectedConfirmationTable.confirm(this, protectedContract.getContractAgent(), ok);
			lsa.removePropertiesByName("PROD_CONFIRM");
			addProperty(new Property("PROD_CONFIRM", protectedConfirmationTable));
		} catch (PermissionException e) {
			logger.error(e);
		}
		if(!ok) {
			receivedConfirmations.remove(protectedContract.getConsumerAgent());
		}
	}

	private boolean confirmAgreementToContractAgent(ProtectedContract newContract, Lsa chosenLSA) {
		//String contractConsumer = newContract.getConsumerAgent();
		//String pBond = chosenLSA.getAgentName();
		//String ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
		boolean isOK = checkAvailabilityAndExpiration(newContract);
		try {
			if (!isOK) {
				Float availablePower = this.computeAvailablePower(false, false);	// Do not ignore offers and waiting contracts
				logger.info("confirmAgreementToContractAgent"  + agentName +  " Remain =" + availablePower + " contractPower = " + newContract.getProducerPower(this));
				//boolean forDebug = checkAvailabilityAndExpiration(newContract);
			}
			if(isOK && !newContract.hasProducerAgreement(this)) {
				newContract.addProducerAgreement(this, true);
				sendConfirmation(newContract, true);
			} else if (isOK && newContract.hasAllAgreements() && alwaysSendConfirmations ) {
				sendConfirmation(newContract, true);
			}
			if(!isOK && !newContract.hasProducerDisagreement(this)) {
				newContract.addProducerAgreement(this, false);
				sendConfirmation(newContract, false);
			}
		} catch (Throwable e) {
			logger.error(e);
		}
		checkup();
		return isOK;
	}

	private void stopContract(String consumer, String log) {
		if(tableContracts.containsKey(consumer)) {
			ProtectedContract protectedContract =  tableContracts.get(consumer);
			try {
				if (!protectedContract.hasDisagreement()) {
					protectedContract.producerStop(this);
					logger.info("stopContract : protectedContract = " + protectedContract);
				}
				removeContract(consumer, log);

				if (protectedContract != null && tableChosenLsa.containsKey(consumer)) {
					sendConfirmation(protectedContract, false);
					checkup();
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	public int getId() {
		return id;
	}

	private boolean isConcerned(ProtectedContract proctectedContract) {
		boolean result = false;
		try {
			if(proctectedContract!=null && proctectedContract.hasAccesAsProducer(this)) {
				result = proctectedContract.hasProducer(this);
			}
		} catch (PermissionException e) {
			logger.error(e);
		}
		return result;
	}

	@Override
	public void onBondNotification(BondEvent event) {
		try {
			if(this.hasExpired() ) {
				 //NodeManager.instance().getNotifier().unsubscribe(this.agentName);
				 logger.info("onBondNotification : " + this.agentName + " has expired");
			}
			// For debug : check availability
			Lsa bondedLsa = event.getBondedLsa();
			String source = bondedLsa.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
			if(!"".equals(source) && !location.equals(source)) {
				logger.info("onBondNotification source = " + source + " , localIpPort = " + location);
			}
			String query = bondedLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString();
			if(debugLevel>10) {
				logger.info(" ** ProducerAgent " + agentName + " bonding ** " + " Q: " + query + " bond agent : "	+ bondedLsa.getAgentName() + " instance = " + this);
			}
			if(isInstanceDoublon()) {
				logger.warning("Instance doublon of " + this.agentName);
				return;
			}
			if ("_Prod_10".equals(agentName) && bondedLsa.getAgentName().startsWith("Consumer_2")/**/) {
				// For debug
				logger.info(this.agentName + " for DEBUG");
			}
			lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Service); // check
			this.addBondedLSA(bondedLsa);

			int action = getActionToTake(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString()); // add greedy
			if (lsa.hasBondedBefore(bondedLsa.getAgentName(), query)) {
				logger.info("** " + bondedLsa.getAgentName() + " Already bound before query " + query);
			}

			if (lsa.getSubDescription().size() >= 1) { // output
				Lsa chosenLSA = getBondedLsaByQuery(query).get(rand.nextInt(getBondedLsaByQuery(query).size()));
				String bondAgentName = chosenLSA.getAgentName();
				if(bondAgentName.endsWith("*")) {
					bondAgentName = bondAgentName.replace('*', ' ').trim();
				}
				this.tableChosenLsa.put(bondAgentName, chosenLSA);
				String state = chosenLSA.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
				if (action == 0) {
					addState(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString(), action, 0, 0);
					addProperty(new Property(lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString(), null,
							query, chosenLSA.getAgentName(), state,
							chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(), false));
				} else if (action == 1) {
					// Check if there is a contract to bind
					//String pBond = chosenLSA.getAgentName();
					//String ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
					// Bond contract agent
					AgentType bondAgentType = AgentType.getFromLSA(bondedLsa);
					// Check if there is a consumer request to handle
					if (AgentType.CONSUMER.equals(bondAgentType)) {
						if (this.isActive()) {
							if(bondedLsa.getAgentName().contains("*")) {
								logger.info("onBondNotification : eventLsa.getAgentName() = " + bondedLsa.getAgentName());
							}
							Object oRequested = SapereUtil.getOnePropertyValueFromLsa(chosenLSA, query, "REQ");
							//boolean hasWarning =  generateTableRequestWarnings().containsKey(query);
							if (oRequested != null && oRequested instanceof EnergyRequest) {
								try {
									EnergyRequest request = (EnergyRequest) oRequested;
									if(!tableConsumerConfirmTag.containsKey(request.getIssuer())) {
										tableConsumerConfirmTag.put(request.getIssuer(), request.getConfirmTag());
									}
									if (request.canBeSupplied() && ! getTableValidContracts().containsKey(request.getIssuer())) {
										if(!tableWaitingRequest.containsKey(bondedLsa.getAgentName())) {
											request.setAux_expiryDate(new Date());
											request.setIssuerDistance(Sapere.getInstance().getDistance(request.getIssuerLocation(), request.getIssuerDistance()));
											this.tableWaitingRequest.put(bondedLsa.getAgentName(), request.clone());
											chosenLSA.getPropertiesByQueryAndName(query, getInput()[0])
													.get(rand.nextInt(
															chosenLSA.getPropertiesByQueryAndName(query, getInput()[0]).size()))
													.setChosen(true); // one Lsa can contain many property for same query
										} else {
											EnergyRequest localRequest = tableWaitingRequest.get(bondedLsa.getAgentName());
											// Update warning date
											localRequest.setWarningDate(request.getWarningDate());
											localRequest.setCurrentDate(request.getCurrentDate());
										}
									}
									// Check if a valid contract exists for this consumer
									if(tableContracts.containsKey(request.getIssuer())) {
										// Request found on ongoing contract
										ProtectedContract contract = tableContracts.get(request.getIssuer());
										if(contract.hasAllAgreements()) {
											// Stop the contract
											logger.warning( this.agentName + " receives a request of " + request.getIssuer() +
													" but a contract is ongoing : stop the contract");
											stopContract(request.getIssuer(), "Consumer in request");
										}
									}
								} catch (NumberFormatException e) {
									logger.error(e);
								}
							} else if (oRequested == null) {
								//ConsumerAgent consumerAgent = (ConsumerAgent) bondAgent;
								String confirmTag = tableConsumerConfirmTag.get(bondedLsa.getAgentName());
								if(confirmTag==null) {
									String confirmTag2 = Sapere.getInstance().getConsumerConfirmTag(bondedLsa.getAgentName());
									if(SapereUtil.getOnePropertyFromLsa(chosenLSA, confirmTag2) != null) {
										logger.warning("Tag " + confirmTag2 + " should be present in tableConsumerConfirmTag" );
									}
									confirmTag = confirmTag2;
									//logger.info(this.agentName +  " confirm tag not found for consumer " + bondedLsa.getAgentName());
								}
								Property pConfirmaiton = SapereUtil.getOnePropertyFromLsa(chosenLSA, confirmTag);
								ProtectedContract protectedContract = null;
								if(pConfirmaiton!=null && pConfirmaiton.getValue() instanceof ProtectedContract) {
									protectedContract = (ProtectedContract) pConfirmaiton.getValue();
								}
								String consumer = bondedLsa.getAgentName();
								//logger.info(this.agentName + " REQ iS NULL for consumer " + consumer);
								if (tableWaitingRequest.containsKey(consumer)) {
									// Remove waiting request
									if(debugLevel>0) {
										logger.info(this.agentName + " remove waiting request " + tableWaitingRequest.get(consumer));
									}
									tableWaitingRequest.remove(consumer);
								}
								// Check if the consumer is still in waiting offers
								Map<String, SingleOffer> offers = getTableWaitingOffers();
								if(isConcerned(protectedContract)) {
									if(!getTableAllContracts().containsKey(consumer)) {
										ProtectedContract cloneProtectedContract = protectedContract.clone();
										updateContract(cloneProtectedContract, " new ctr from consumer");
										// Check availability
										boolean availabilityOk = checkAvailabilityAndExpiration(cloneProtectedContract);
										cloneProtectedContract.addProducerAgreement(this, availabilityOk);
										if(!availabilityOk) {
											logger.info(this.agentName + " cannont accept this contract availabilityOk2 = " + checkAvailabilityAndExpiration(cloneProtectedContract));
										}
										sendConfirmation(protectedContract, availabilityOk);
									}
								}
								// Remove waiting offer of this consumer
								if (offers.containsKey(consumer)) {
									removeOffer(consumer, "generateNewOffers:onBondNotification: new contract " + consumer + " " + isConcerned(protectedContract));
								}
							}
							Property pSatisfied = SapereUtil.getOnePropertyFromLsa(chosenLSA, "SATISFIED");
							if(pSatisfied!=null && "1".equals(pSatisfied.getValue())) {
								String consumer =  bondedLsa.getAgentName();
								if(tableContracts.containsKey(consumer)) {
									ProtectedContract contract = tableContracts.get(consumer);
									if(contract.hasAllAgreements()) {
										// Add Received confirmation from consumer agent
										receivedConfirmations.put(consumer, new ConfirmationItem(true));
									}
								}
							}
						}
					} else if (AgentType.CONTRACT.equals(bondAgentType)) {
						Property pContract = SapereUtil.getOnePropertyFromLsa(chosenLSA, query, "CONTRACT");
						if (pContract == null) {
							pContract = SapereUtil.getOnePropertyFromLsa(chosenLSA, "CONTRACT");
						}
						if (pContract != null && pContract.getValue() instanceof ProtectedContract) {
							try {
								if("Prod_N2_1".equals(this.agentName)) {
									logger.info(this.agentName + " For debug");
								}
								ProtectedContract protectedContract = ((ProtectedContract) pContract.getValue()).clone();
								boolean isConcerned = isConcerned(protectedContract);
								String contractConsumer = protectedContract.getConsumerAgent();
								boolean isLocalyCanceled = this.isDisabled() || this.getTableCanceledContracts().containsKey(contractConsumer);
								// Set<String> setProducers = contract.getProducerAgents();
								//if(!isConcerned &&  this.getContractProperty(contractConsumer) != null) {
								if(!isConcerned && this.tableContracts.containsKey(contractConsumer)) {
									logger.info(this.agentName + " : remove this contract from data");
									// The contract contained in LSA is obsolete : remove the contract
									this.removeContract(contractConsumer, "this contract does not conern " + this.agentName);
								} else if (isConcerned && !protectedContract.hasExpired() && !protectedContract.hasDisagreement()
										&& !isLocalyCanceled) {
									// Contract
									// Add a new contract
									ProtectedContract newContract = (ProtectedContract) protectedContract.clone();
									confirmAgreementToContractAgent(newContract, chosenLSA);
									updateContract(newContract, "Update from contratAgent");
									cleanExpiredData();
								} else if (isConcerned && protectedContract.hasDisagreement()) {
									logger.info(this.agentName + " : Contract has disagreement : cancel the contract ");
									// Remove the contract
									this.removeContract(protectedContract.getConsumerAgent(), " : stop from contracAgent");
								}
								// Clean waiting request if necessary
								if(protectedContract.hasAllAgreements() &&!protectedContract.hasExpired()) {
									String msgBegin = SapereUtil.getCurrentTimeStr() + " Following to " + protectedContract.getContractAgent() + " validation, ";
									// Remove waiting request
									if(tableWaitingRequest.containsKey(contractConsumer)) {
										logger.info(msgBegin + this.agentName + " will remove request of " + contractConsumer + " from  tableWaitingRequest");
									}
									tableWaitingRequest.remove(contractConsumer);
									// Remove offer
									// Remove waiting offers
									if(getTableWaitingOffers().containsKey(contractConsumer)) {
										logger.info(msgBegin  + this.agentName + " will remove the following offer  " + getTableWaitingOffers().get(contractConsumer));
									}
									removeOffer(contractConsumer, "onBondNotification:cleanOffer");
								}
							} catch (Exception e) {
								//e.printStackTrace();
								logger.error(e);
							}
						} else {
							// OProducer IS NULL
							/*
							if (!bondsWaitingContract.containsKey(pBond)) {
								bondsWaitingContract.put(pBond, 0);
							}
							int nbBonds = bondsWaitingContract.get(pBond);
							if (nbBonds > 10) {
								// Timeout
								// addProperty(new Property("Ignore","0", query, pBond, state, ipSource,
								// false));
							} else {
								bondsWaitingContract.put(pBond, nbBonds + 1);
							}
							*/
						}
					} else if (AgentType.REGULATOR.equals(bondAgentType)) {
						Property pWarning = SapereUtil.getOnePropertyFromLsa(chosenLSA,  "WARNING");
						if(pWarning!=null && pWarning.getValue() instanceof RegulationWarning) {
							RegulationWarning warning = (RegulationWarning) pWarning.getValue();
							if(!receivedWarnings.contains(warning)) {
								receivedWarnings.add(warning);
								//Over consumption
								if(WarningType.OVER_CONSUMPTION.equals(warning.getType())) {
									for(String consumer : getTableValidContracts().keySet()) {
										if(warning.hasAgent(consumer)) {
											stopContract(consumer, ""+warning.getType());
										}
									}
								} else if(WarningType.USER_INTERRUPTION.equals(warning.getType())) {
									if(warning.hasAgent(agentName) && !this.isDisabled()) {
										disableAgent(warning);
									}
									//Map<String, ProtectedContract> validContracts = getTableValidContracts();
									for(String consumer : getTableValidContracts().keySet()) {
										if(warning.hasAgent(consumer)) {
											stopContract(consumer, ""+warning.getType());
										}
									}
								} else if(WarningType.CHANGE_REQUEST.equals(warning.getType())) {
									EnergySupply changeRequest = warning.getChangeRequest();
									if(changeRequest!=null) {
										if(warning.hasAgent(agentName) && !this.isDisabled()) {
											// The change request concerns the agent itelf
											updateSupply(changeRequest);
										}
										Map<String, ProtectedContract> validContracts = getTableValidContracts();
										for(String consumer : validContracts.keySet()) {
											if(warning.hasAgent(consumer) && warning.getChangeRequest()!=null) {
												// The change request concerns on of the provided consumers
												ProtectedContract contract = validContracts.get(consumer);
												// Stop the contract is the requested power has increased
												if(changeRequest.getPower() > contract.getRequest().getPower()) {
													stopContract(consumer, ""+warning.getType());
												} else {
												}
											}
										}
									}
								} else if(WarningType.OVER_PRODUCTION.equals(warning.getType())) {
									if(warning.hasAgent(agentName) && !this.isDisabled()) {
										disableAgent(warning);
									}
								}
							}
						}
						Property pReschedule = SapereUtil.getOnePropertyFromLsa(chosenLSA, "RESCHEDULE");
						if(pReschedule!=null && pReschedule.getValue() instanceof RescheduleTable) {
							RescheduleTable rescheduleTable = (RescheduleTable) pReschedule.getValue();
							if(rescheduleTable.hasItem(agentName)) {
								RescheduleItem rescheduleItem = rescheduleTable.getItem(agentName);
								if(rescheduleItem.getStopBegin().before(globalSupply.getEndDate())) {
									// Modify contract end date
									globalSupply.setBeginDate(new Date());
									globalSupply.setEndDate(rescheduleItem.getStopBegin());
									startEvent = generateUpdateEvent(rescheduleItem.getWarningType());
								}
							}
						}
					}
				}
				this.removeBondedLsasOfQuery(query);
			}
		} catch(Throwable t) {
			logger.error(t);
			logger.error("### Exception thronw in  " +  this.agentName + " bond handling of " + event  + " : " + t.getMessage());
		}
	}

	private void updateSupply(EnergySupply changeRequest) {
		//disableAgent(warning);
		try {
			float powerBefore =  globalSupply.getPower();
			this.globalSupply = changeRequest.clone();
			// Refresh PROD property
			lsa.removePropertiesByName("PROD");
			addProperty(new Property("PROD", this.globalSupply));
			Map<String, ProtectedContract> validContracts = getTableValidContracts();
			if(changeRequest.getPower() < powerBefore) {
				// Stop contract if not enougth availability
				while(computeAvailablePower(false, false) < 0 && validContracts.size() > 0) {
					// Remove contract
					List<String> consumers = new ArrayList<>();
					consumers.addAll(validContracts.keySet());
					Collections.shuffle(consumers);
					// TODO : sort contracts by request priority
					String consumer = consumers.get(0);
					stopContract(consumer, ""+ WarningType.CHANGE_REQUEST);
					validContracts = getTableValidContracts();
				}
			}
			startEvent = generateUpdateEvent(WarningType.CHANGE_REQUEST);
		} catch (Throwable e) {
			logger.error(e);
		}
	}

	private void disableAgent(RegulationWarning warning) {
		this.globalSupply.setDisabled(true);
		if(lsa.getPropertiesByName("DISABLED").isEmpty()) {
			try {
				RegulationWarning warningToSet = warning.clone();
				addProperty(new Property("DISABLED", warningToSet));
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
		}
		/*
		// Stop all contracts
		for(String consumer : this.getTableValidContracts().keySet()) {
			stopContract(consumer, ""+warning.getType());
		}
		for(String consumer : this.getTableWaitingContracts().keySet()) {
			stopContract(consumer, ""+warning.getType());
		}
		*/
		boolean stopEventGenerated = false;
		try {
			if(stopEvent==null) {
				stopEvent = generateStopEvent(warning);
				stopEventGenerated = true;
			}
		} catch (Exception e) {
			logger.error(e);
		}
		if(!stopEventGenerated) {
			logger.error(this.agentName + " disableAgent For debug");
		}
	}
/*
	private boolean canProvideRequest(EnergyRequest request) {
		if (request.canBeSupplied()) {
			String consumer = request.getIssuer();
			Map<String, ProtectedContract> validContracts = getTableValidContracts();
			Map<String, SingleOffer> waitingOffers = getTableWaitingOffers();
			return !validContracts.containsKey(consumer) && !waitingOffers.containsKey(consumer);
		}
		return false;
	}
*/
	private boolean canProvideRequest(EnergyRequest request, Map<String, ProtectedContract> validContracts, Map<String, SingleOffer> waitingOffers) {
		String consumer = request.getIssuer();
		return request.canBeSupplied() && !validContracts.containsKey(consumer) && !waitingOffers.containsKey(consumer);
	}


	public Map<String, Date> generateTableRequestWarnings() {
		Map<String, Date> warnings = new HashMap<String, Date>();
		for(EnergyRequest request : tableWaitingRequest.values()) {
			if(request.getWarningDate()!=null) {
				warnings.put(request.getIssuer(), request.getWarningDate());
			}
		}
		return warnings;
	}

	private String generateRequestLog(List<EnergyRequest> requestList, HomeTotal homeTotal) {
		List<String> consumerList  = new ArrayList<>();
		//boolean homeTotalLogged = false;
		int maxWarningDuration = 0;
		for (EnergyRequest request : requestList) {
			//String warningLog = "";
			if(request.getWarningDate()!=null) {
				//warningLog = "*";
				int warningDuraction = request.getWarningDurationSec();
				if(warningDuraction > maxWarningDuration) {
					maxWarningDuration = warningDuraction;
				}
				/*
				if(warningDuraction>0) {
					warningLog = "*(" + warningDuraction + ")";
				}*/
			}
			int idx = request.getIssuer().lastIndexOf("_");
			String consumerNumber = request.getIssuer().substring(1+idx);
			consumerList.add(consumerNumber);
		}
		String logConsumerList = SapereUtil.implode(consumerList, ",");
		if(maxWarningDuration>=8) {
			String homeTotalLog = "     homeTotal={id:" +  homeTotal.getId() + ",time:" + SapereUtil.format_time.format(homeTotal.getDate()) + " ,avb:" + homeTotal.getAvailable()+ "})";
			logConsumerList = logConsumerList + homeTotalLog;
		}
		if(maxWarningDuration>15) {
			logger.warning(this.agentName + " generateRequestLog : For debug : maxWarningDuration = " + maxWarningDuration);
		}
		return logConsumerList;
	}

	private int generateNewOffers(HomeTotal homeTotal) {
		int nbNewOFfers = 0;
		boolean toLog = "Prod_N2_1".equals(this.agentName);
		if(toLog) {
			logger.info("generateNewOffers  " + agentName + " : begin waitingoffers = " + getTableWaitingOffers().keySet() + " tableWaitingRequest keys = " + tableWaitingRequest.keySet());
		}
		if(this.hasExpired()) {
			logger.info("generateNewOffers " + this.agentName + " has expired");
			return nbNewOFfers;
		}
		float homeTotalAvailable = homeTotal==null? new Float(0) : homeTotal.getAvailable();
		if (tableWaitingRequest.size() > 0 && !this.hasExpired() && !this.isDisabled()) {
			// Wait untill all offers has expired ?
			Map<String, SingleOffer> waitingOffers = getTableWaitingOffers();
			if(waitingOffers.size()>0) {
				return nbNewOFfers;
			}
			if(toLog) {
				logger.info(" generateNewOffers "+ agentName + " : step1");
			}
			// Debug requests under available
			//Date minWarningDate = SapereUtil.shiftDateSec(new Date(), -12);
			Float availablePower = this.computeAvailablePower(false,false);
			List<EnergyRequest> requestList = new ArrayList<EnergyRequest>();
			requestList.addAll(tableWaitingRequest.values());
			Collections.shuffle(requestList);

			// Define the sorting strategy
			int usedPolicy = POLICY_MAXWARNING_MINPOWER;
			if(usedPolicy == POLICY_RANDOM) {
				// Sort only by request priority
				Collections.sort(requestList, requestComparatorPriority);
			} else if (usedPolicy == POLICY_MAXWARNING_MINPOWER) {
				// Sort by request priority, warning level desc, power asc
				Collections.sort(requestList, requestComparatorDistancePriorityAndWarningAndPower);
			} else if (usedPolicy == POLICY_MIX) {
				// Get the max warning level
				int maxWarningDuraitonSec = 0;
				for(EnergyRequest req : requestList) {
					if (req.getWarningDurationSec() > maxWarningDuraitonSec) {
						maxWarningDuraitonSec = req.getWarningDurationSec();
					}
				}
				if(maxWarningDuraitonSec > 7) {
					// Sort by request priority, warning level desc, power asc
					Collections.sort(requestList, requestComparatorDistancePriorityAndWarningAndPower);
				} else  {
					// Apply random polivy : sort only by request priority
					Collections.sort(requestList, requestComparatorPriority);
				}
			}
			/*
			 * double random = Math.random();
				if(random < PROBA_RANDOM_CHOICE) {
					// Sort by priority only
					Collections.sort(requestList, requestComparatorPriority);
				} else {
					// Sort by priority and power
					//Collections.sort(requestList, requestComparatorPriorityAndPower);
					Collections.sort(requestList, requestComparatorPriorityAndWarningAndPower);
				}
			 * */
			String logConsumerList = generateRequestLog(requestList, homeTotal);
			String firstConsumer = null;
			if(generateTableRequestWarnings().size()>1) {
				// log request list to handle
				//logger.info("generateNewOffers " + this.agentName + " requestlist : ");
				for (EnergyRequest request : requestList) {
					//logger.info(request);
					if(request.getWarningDate()!=null && firstConsumer == null) {
						firstConsumer = request.getIssuer();
					}
				}
				//logger.info("foo1 firstConsumer = " + firstConsumer);
			}
			if(toLog) {
				logger.info(" generateNewOffers "+ agentName + " : step2 " + logConsumerList);
			}
			Map<String, ProtectedContract> validContracts = getTableValidContracts();
			//Map<String, SingleOffer> waitingOffers = getTableWaitingOffers();
			for (EnergyRequest request : requestList) {
				String consumer = request.getIssuer();
				//boolean addOffer = false;
				if(getTableWaitingOffers().containsKey(consumer)) {
					logger.info(this.agentName + " generateNewOffers For debug has waiting offer for consumer " + consumer);
				}
				if (canProvideRequest(request, validContracts, waitingOffers)) {
					// Check if the request has a high priority level and cannot be provided with the local
					if (PriorityLevel.HIGH.equals(request.getPriorityLevel()) && availablePower < request.getPower()) {
						logger.info(this.agentName +" High priority request found " + request);
						 // Check if we can meet this demand globaly (with other producers linked to this home)
						 if(homeTotalAvailable < request.getPower()) {
							// Break current offers until there is enough energy left to meet this demand
							Map<String, SingleOffer> tableOffers = this.getTableWaitingOffers();
							String offerKeyToCancel = SapereUtil.getOfferToCancel(tableOffers, OFFER_EXPIRATION_MARGIN_SEC);
							while (availablePower < request.getPower() && offerKeyToCancel != null) {
								this.removeOffer(offerKeyToCancel, "generateNewOffers:urent request");
								tableOffers = this.getTableWaitingOffers();
								offerKeyToCancel = SapereUtil.getOfferToCancel(tableOffers, OFFER_EXPIRATION_MARGIN_SEC);
								availablePower = this.computeAvailablePower(false,false);
							}

							// Break current contracts until there is enough energy left to meet this demand
							Map<String, ProtectedContract> tableValidContracts = this.getTableValidContracts();
							String contractKeyToCancel = SapereUtil.getContractToCancel(tableValidContracts);
							while (availablePower < request.getPower() && contractKeyToCancel != null) {
								this.stopContract(contractKeyToCancel, " high proproty request");
								tableValidContracts = this.getTableValidContracts();
								contractKeyToCancel = SapereUtil.getContractToCancel(tableValidContracts);
								availablePower = this.computeAvailablePower(false,false);
							}
						 } else {
							 // This demand can be meet globaly
							 homeTotalAvailable-= request.getPower();
						 }
					}
					availablePower = this.computeAvailablePower(false,false);
					if (availablePower > 0) {
						// Generate a new single offer
						Date requestedBeginDate = request.getBeginDate();
						//Date requestedEndDate = request.getEndDate();
						Float requestedPower = new Float("" + request.getPower());
						// Float requestedDuration = new Float(""+oRequestedDuration);
						// Date current = new Date();
						Date current = new Date(); //SapereUtil.getCurrentMinute();// .getNextMinute();
						Date providedBeginDate = requestedBeginDate.after(current) ? requestedBeginDate
								: current;
						Float providedPower = Math.min(availablePower, requestedPower);
						// boolean respondToConsumer = false;
						if (this.isInActiveSlot(providedBeginDate) && providedPower >= 0.001) {
							// Prepare the offer
							//String consumer = request.getIssuer();
							if (this.tableChosenLsa.containsKey(consumer)) {
								//Lsa chosenLSA = tableChosenLsa.get(consumer);
								// String pName =
								// lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString();
								// Lsa bondedLsa = tableBondedLsa.get(consumer);
								//String ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
								Date providedEndDate = request.getEndDate().after(globalSupply.getEndDate()) ?
										globalSupply.getEndDate()
										: request.getEndDate();
								EnergySupply supply = new EnergySupply(this.agentName, this.globalSupply.getIssuerLocation(), providedPower, providedBeginDate, providedEndDate
										, this.globalSupply.getDeviceName(), this.globalSupply.getDeviceCategory());
								try {
									// For debug : log contracts
									/*
									String filterPower = "single_offer.power BETWEEN " + Math.floor(supply.getPower()) + " AND " + (1+Math.floor(supply.getPower())); 
									List<SingleOffer> alreadyGeneatedOffers=EnergyDbHelper.retrieveOffers(SapereUtil.shiftDateSec(new Date(), -8), new Date(), consumer, null,  filterPower);
									if(alreadyGeneatedOffers.size()>0) {
										float testAvl = computeAvailablePower(false, false);
										logger.info("---  Befor creation a new offer : available = " + testAvl);
										for(SingleOffer nextOffer : alreadyGeneatedOffers) {
											logger.info("Already generated  " + nextOffer);
										}
										for(ProtectedContract contract : getTableAllContracts2().values()) {
											logger.info(this.agentName + " : next contrat : " + contract);
										}
									}*/
									SingleOffer newOffer = new SingleOffer(this.agentName, supply, OFFER_VALIDITY_SECONDS, request.clone());
									newOffer.setLog(logConsumerList + "  avb="+ SapereUtil.df.format(availablePower));
									Long offerId = EnergyDbHelper.registerSingleOffer(newOffer, this.startEvent);
									newOffer.setId(offerId);
									this.addOffer(newOffer);
									/*
									String sState = SapereUtil.addOutputsToLsaState(chosenLSA, new String[] { "OFFER" });
									removeOffer(consumer, "generateNewOffers step1");
									addProperty(new Property("OFFER", newOffer.clone(), consumer, consumer, sState,	ipSource, false));
									addProperty(new Property("WAITING_OFFER", newOffer.clone()));
									lsa.addSyntheticProperty(SyntheticPropertyName.STATE, sState);
									*/
									nbNewOFfers++;
									//addOffer = true;
									// For debug : check availability
									if(toLog) {
										logger.info("newOffer : " + newOffer);
									}
								} catch (Exception e) {
									logger.error(e);
								}
							} else {
								logger.info("Chosen lsa not found in table");
							}
						} else {
							//logger.info("### For Debug2 : providedBeginDate " + providedBeginDate + " , providedPower = " + providedPower); 
							// addProperty(new Property("Ignore", "0", query, pBond, state, ipSource,
							// false));
						}
					}
				} else {
					logger.info("The following request canot be provided : " + request);
				}
			}
		}
		return nbNewOFfers;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		//logger.info("onPropagationEvent " +  location + " " +  this.agentName + " " + event);
		Lsa eventLsa = event.getLsa();
		String source = eventLsa.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
		if(!"".equals(source) && !location.equals(source)) {
			logger.info("onPropagationEvent : source = " + source + " , localIpPort = " + location);
		}
		if(eventLsa.getAgentName().contains("*")) {
			logger.info("onPropagationEvent : eventLsa.getAgentName() = " + eventLsa.getAgentName());
		}
	}

	private void cleanEventProerty() {
		if(eventDecay>0) {
			eventDecay-=1;
		} else {
			this.lsa.removePropertiesByName("EVENT");
			if(debugLevel>0) {
				logger.info("after removing event property : startEvent = " + startEvent);
			}
		}
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		try {
			// For debug : check availability
			//checkup();
			if(!firstDecay && timeLastDecay!=null) {
				if( new Date().getTime() - timeLastDecay.getTime()> 2*1000) {
					logger.warning("onDecayedNotification " + this.agentName + " : last decay at " + SapereUtil.format_time.format(timeLastDecay));
				}
			}
			timeLastDecay = new Date();
			Lsa decayedLsa = event.getLsa();
			// int hasCode = decayedLsa.hashCode();
			boolean hasExpired = this.hasExpired();
			Integer decay = new Integer("" + decayedLsa.getSyntheticProperty(SyntheticPropertyName.DECAY));
			boolean toLog = "__Prod_1".equals(agentName);
			if(debugLevel > 0 || toLog) {
				logger.info("onDecayedNotification: " + this.agentName + " decay = " + decay + " Time left sec = "	+ getTimeLeftSec() + " instance = " + this);
			}
			if(isInstanceDoublon()) {
				logger.info("Instance doublon of " + this.agentName);
				return;
			}
			if (hasExpired) {
				if(decay > 1) {
					// the agent should expire now : force decay
					logger.warning(this.agentName + " will expire");
					this.addDecay(1);
				}
				// Post expiration event
				if(expiryEvent==null) {
					try {
						if(startEvent!=null) {
							expiryEvent = generateExpiryEvent();
						}
						/*
						lsa.removePropertiesByName("EVENT");
						addProperty(new Property("EVENT", expiryEvent));
						*/
					} catch (Exception e) {
						logger.error(e);
					}
				}
				//  Stop all ongoing contracts
				for(ProtectedContract contract : this.getValidContracts()) {
					sendConfirmation(contract, false);
				}
			}
			if(this.isDisabled()) {
				// Stop all contracts
				for(String consumer : this.getTableValidContracts().keySet()) {
					stopContract(consumer, "agent disabled");
				}
			}

			// Remove sent offers
			String offerKey = getOfferKeyToRemove();
			while(offerKey!=null) {
				// For debug
				debug_checkOfferAcquitted(offerKey);
				lsa.removePropertiesByQueryAndName(offerKey, "OFFER");
				offerKey = getOfferKeyToRemove();
			}

			// Clean expired confirmations
			ProtectedConfirmationTable protectedConfirmationTable = getProtectedConfirmationTable();
			if(protectedConfirmationTable.hasExpiredItem(this)) {
				protectedConfirmationTable.cleanExpiredDate(this);
				lsa.removePropertiesByName("PROD_CONFIRM");
				addProperty(new Property("PROD_CONFIRM", protectedConfirmationTable));
			}
			cleanExpiredData();

			// Check up
			float availability = computeAvailablePower(false, false);
			boolean isOK = availability >= -0.001;
			if(!isOK) {
				logger.warning("general checkup " .concat(this.agentName) + " : availability = " + SapereUtil.df.format(availability));
			}
			// Send confirmation to contract agents
			ConfirmationTable confirmationTable = protectedConfirmationTable.getConfirmationTable(this);
			for(ProtectedContract protectedContract : getValidContracts()) {
				String contractAgent = protectedContract.getContractAgent();
				if(!confirmationTable.hasContractAgent(contractAgent)) {
					sendConfirmation(protectedContract, isOK);
				}
			}
			//checkupDecay = CHECKUP_PERDIOD_SEC;

			// Check received confirmation from consumers
			for(String consumer : getTableValidContracts().keySet()) {
				if(this.receivedConfirmations.containsKey(consumer)) {
					ConfirmationItem producerConfirmation = this.receivedConfirmations.get(consumer);
					if(producerConfirmation.hasExpired(0)) {
						logger.warning(this.agentName  + " : no recent confirmation from " + consumer + " : last received : " + producerConfirmation);
						checConsumerInSpace(consumer);
					}
				} else {
					logger.warning(this.agentName  + " : no confirmation from " + consumer);
					checConsumerInSpace(consumer);
				}
			}

			// Refresh warning request
			// Retrieve the home availability from the last stored home total
			HomeTotal homeTotal = EnergyDbHelper.retrieveLastHomeTotal();
			tryReactivation(homeTotal);
			int nbNewOFfers = 0;
			if(!this.isDisabled() && !this.hasExpired()) {
				nbNewOFfers = generateNewOffers(homeTotal);
			}
			if(nbNewOFfers>0) {
				logger.info("nbNewOFfers = " + nbNewOFfers);
			}
			firstDecay = false;
			// For debug : check availability
			// Post not sent properties
			sendProperties();
			// For propagation
			addGradient(3);
		} catch(Throwable t) {
			//t.printStackTrace();
			logger.error(t);
			logger.error("### Exception thronw in  " +  this.agentName + " decayhandling of " + event + " " + t.getMessage());
		}
	}

	private void checConsumerInSpace(String consumerName) {
		if(!Sapere.getInstance().isInSpace(consumerName)) {
			// Stop the contract
			stopContract(consumerName, "not in space");
		}
	}

	private void debug_checkOfferAcquitted(String consumer) {
		ProtectedSingleOffer protectedOffer = (ProtectedSingleOffer) SapereUtil.getOnePropertyValueFromLsa(lsa, consumer, "OFFER");
		if(protectedOffer!=null && protectedOffer.hasAccesAsProducer(this)) {
			try {
				SingleOffer offer = protectedOffer.getSingleOffer(this);
				if(offer!=null) {
					boolean isAcquitted = EnergyDbHelper.isOfferAcuitted(offer.getId());
					if(!isAcquitted) {
						// The offer is not acquitted
						logger.warning(this.agentName + " debug_checkOfferAcquitted : the offer " + offer + " is not acquitted");
					}
				}
			} catch (PermissionException e) {
				logger.error(e);
			}
		}
	}

	private boolean tryReactivation(HomeTotal homeTotal) {
		boolean result = false;
		Property pDisabled = SapereUtil.getOnePropertyFromLsa(lsa, "DISABLED");
		if(pDisabled!=null && pDisabled.getValue() instanceof RegulationWarning) {
			RegulationWarning warning = (RegulationWarning) pDisabled.getValue();
			if(warning.hasWaitingExpired()) {
				if(WarningType.OVER_PRODUCTION.equals(warning.getType())) {
					boolean overProduction = (homeTotal.getProduced() + (globalSupply.getDisabled() ? globalSupply.getPower() : 0)  > RegulatorAgent.MAX_PRODUCTION);
					if(overProduction) {
						// Still over production
						lsa.removePropertiesByName("DISABLED");
						warning.setWaitingDeadline(SapereUtil.shiftDateMinutes(new Date(), DISABLED_DURATION_MINUTES));
						addProperty(new Property("DISABLED",  warning));
					} else {
						globalSupply.setDisabled(false);
						lsa.removePropertiesByName("DISABLED");
						// Restart Producer agent
						if(startEvent==null) {
							try {
								startEvent = generateStartEvent();
							} catch (Exception e) {
								logger.error(e);
							}
						}
						result = true;
					}
				} else if (WarningType.USER_INTERRUPTION.equals(warning.getType())
						|| WarningType.CHANGE_REQUEST.equals(warning.getType())) {
					// Stop the agent
					this.globalSupply.setEndDate(new Date());
				}
			}
		}
		return result;
	}


	private String getOfferKeyToRemove() {
		for (Property p : lsa.getProperties()) {
			if (true && "OFFER".equals(p.getName()) && p.getValue() instanceof ProtectedSingleOffer) {
				return p.getQuery();
				/*
				ProtectedSingleOffer protectedOffer = (ProtectedSingleOffer) p.getValue();
				try {
					String key = protectedOffer.getConsumerAgent(this);
					return key;
				} catch (PermissionException e) {
					logger.error(e);
				}*/
			}
		}
		return null;
	}

	private void cleanExpiredData() {
		// Clean event
		cleanEventProerty();

		// Clean waiting request
		//Map<String, ProtectedContract> tableValidContracts = this.getTableValidContracts();
		Map<String, ProtectedContract> tableContracts = this.getTableContracts(true, true, false);
		String requestKey = null;
		while ((requestKey= SapereUtil.getExpiredRequestKey(tableWaitingRequest, tableContracts)) != null) {
			if(debugLevel>0) {
				logger.info(this.agentName + " : cleanExpiredData : remove waiting request of consumer " + requestKey);
			}
			tableWaitingRequest.remove(requestKey);
		}

		// Remove expired offers from WAITING_OFFER properties
		String offerKey = null;
		Map<String, SingleOffer> waitingOffers = getTableWaitingOffers();
		while ((offerKey = SapereUtil.getExpiredOfferKey(waitingOffers, tableContracts, OFFER_EXPIRATION_MARGIN_SEC)) != null) {
			//SingleOffer offer = waitingOffers.get(offerKey);
			boolean inContract = (tableContracts != null && tableContracts.containsKey(offerKey));
			this.removeOffer(offerKey, "cleanExpiredData inContract="+ inContract);
			// Refersh waitingOffers
			waitingOffers = getTableWaitingOffers();
			//offer.setDeadline(SapereUtil.shiftDateMinutes(new Date(), 1));
		}

		// Remove expired contracts from valid contracts
		String contractKey = null;
		while ((contractKey = SapereUtil.getExpiredContractKey(tableContracts, this))  != null) {
			removeContract(contractKey, "expired contract");
			this.lsa.removePropertiesByQueryAndName(contractKey, "Ignore");
			// Refresh valid contracts
			tableContracts = this.getTableValidContracts();
		}

		// Remove canceled contracts
		Map<String, ProtectedContract> tableCanceledContracts = getTableCanceledContracts();
		if (!tableCanceledContracts.isEmpty()) {
			for (String consumer : tableCanceledContracts.keySet()) {
				removeContract(consumer, "canceled contract");
			}
		}
		// Clean expired warnings
		RegulationWarning expiredWarning = null;
		while( (expiredWarning=SapereUtil.getExpiredWarning(receivedWarnings)) != null) {
			receivedWarnings.remove(expiredWarning);
		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		logger.info("onLsaUpdatedEvent:" + agentName);
	}

	@Override
	public void onRewardEvent(RewardEvent event) {
		String previousAgent = "";
		String newState = "";
		for (Property prop : event.getLsa().getPropertiesByQuery(event.getQuery())) {
			if (prop.getChosen()) {
				previousAgent = prop.getBond();
				newState = prop.getState();
				break;
			}
		}
		logger.info("State to reward " + newState + " by " + event.getReward() + " - " + event.getMaxSt1());
		if (!newState.equals(""))
			addState(getPreviousState(newState, getOutput()), 1, event.getReward(), event.getMaxSt1());

		logger.info("reward previous service " + previousAgent);

		Lsa lsaReward = NodeManager.instance().getSpace().getLsa(previousAgent);
		if (lsaReward != null && lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Service)) {
			rewardLsa(lsaReward, event.getQuery(), event.getReward(),
					getBestActionQvalue(getPreviousState(newState, getOutput()))); // maxQSt1
			lsaReward.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
		}

		if (lsaReward != null) {
			if (previousAgent.contains("*")
					&& !lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Query)) {
				lsaReward.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Reward);
				lsaReward.addSyntheticProperty(SyntheticPropertyName.QUERY, event.getQuery());
				logger.info("lsaReward " + lsaReward.toVisualString());
				logger.info(
						"send to -> " + lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
				sendTo(lsaReward, lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
			}

		}
	}

	private void addProperty(Property propertyToAdd) {
		if(lsa.getProperties().size() >= Lsa.PROPERTIESSIZE) {
			propertiesToPost.add(propertyToAdd);
			logger.info(this.agentName + " addProperty : cannot post propertiesToPost " + propertyToAdd.getValue() + " in lsa. Put it waiting queue.");
		} else {
			lsa.addProperty(propertyToAdd);
		}
	}

	private void sendProperties() {
		while(this.propertiesToPost.size()>0 && lsa.getProperties().size() < Lsa.PROPERTIESSIZE-1) {
			Property prop = propertiesToPost.remove(0);
			logger.info( this.agentName + " : post not sent property " + prop);
			lsa.addProperty(prop);
		}
	}

	public boolean isActive() {
		return globalSupply.isActive();
	}

	public boolean isInActiveSlot(Date aDate) {
		return globalSupply.isInActiveSlot(aDate);

	}

	public int getTimeLeftSec() {
		return globalSupply.getTimeLeftSec();
	}

	public boolean hasExpired() {
		return globalSupply.hasExpired();
	}

	public boolean isInstanceDoublon() {
		return instanceDoublon;
	}

	public void setInstanceDoublon(boolean instanceDoublon) {
		this.instanceDoublon = instanceDoublon;
	}

	public Map<String, EnergyRequest> getTableWaitingRequest() {
		return tableWaitingRequest;
	}

	public boolean isDisabled() {
		return globalSupply.getDisabled();
	}

	public Event getStopEvent() {
		return stopEvent;
	}

	/*
	public boolean isInSpace() {
		return isInSpace;
	}

	public void setInSpace(boolean isInSpace) {
		this.isInSpace = isInSpace;
	}
*/
	public void checkup() {
		float avl = computeAvailablePower(false, false);
		if(avl<-0.001) {
			logger.warning("### " +  this.agentName + " availability = " + avl);
			logger.warning("For debug tableWaitingOffers =  " + this.tableWaitingOffers);
		}
		if(lsa.getProperties().size()>=Lsa.PROPERTIESSIZE) {
			logger.warning("### " + this.agentName +  " properties size = " + lsa.getProperties().size());
			for(Property prop : lsa.getProperties()) {
				if(prop.getValue() instanceof ProtectedSingleOffer) {
					ProtectedSingleOffer protectedOffer = (ProtectedSingleOffer) prop.getValue();
					try {
						SingleOffer offer = protectedOffer.getSingleOffer(this);
						logger.warning(" offer " + offer.getId() + " from " + offer.getProducerAgent()  + " to " + offer.getConsumerAgent());
					} catch (PermissionException e) {
						logger.error(e);
					}
				}
			}
		}
	}
}
