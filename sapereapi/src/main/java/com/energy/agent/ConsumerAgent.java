package com.energy.agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.energy.markov.HomeMarkovStates;
import com.energy.model.AgentType;
import com.energy.model.Contract;
import com.energy.model.EnergyRequest;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.GlobalOffer;
import com.energy.model.HomeTotal;
import com.energy.model.PriorityLevel;
import com.energy.model.ProtectedContract;
import com.energy.model.ProtectedSingleOffer;
import com.energy.model.RegulationWarning;
import com.energy.model.RescheduleItem;
import com.energy.model.RescheduleTable;
import com.energy.model.SingleOffer;
import com.energy.model.WarningType;
import com.energy.util.DoublonException;
import com.energy.util.EnergyDbHelper;
import com.sapereapi.model.AgentState;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;
import com.sapereapi.sapere.QueryAgent;

import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class ConsumerAgent extends QueryAgent implements IEnergyAgent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EnergyRequest need;
	private int id;
	private String confirmTag = null;
	private String updateTag = null;
	private String disabledTag = null;
	private GlobalOffer globalOffer = null;
	private Map<String, SingleOffer> tableSingleOffers = null;
	private Contract currentContract = null;
	//private Date postEventTime = null;
	//private boolean firstDecay = false;
	private Event startEvent = null;
	private Event stopEvent = null;
	private Event expiryEvent = null;
	private int debugLevel = 1;
	private int eventDecay = 0;
	private int confirmationDecay = 0;
	private String contractAgentName = null;
	public final static int DISABLED_DURATION_MINUTES = 5;
	public final static int DISABLED_SHORT_DURATION_SEC = 5;
	public final static int CONFIRMATION_INIT_DECAY = 3;
	private final static int CONTRACT_VALIDATION_DEALY_SEC = 10;
	private List<RegulationWarning> receivedWarnings = new ArrayList<RegulationWarning>();

	private static String generateTagDisabled(int id) {
		return  "STOP_CTR_" + id;
	}

	private static String generateTagUpdate(int id) {
		return  "UPDATE_CTR_" + id;
	}

	private static String generateTagConfirm(int id) {
		return "NEW_CTR_" + id;
	}

	public static Event aux_generateStartEvent(int consumerId, EnergyRequest _need) throws DoublonException {
		Event newEvent = new Event(EventType.REQUEST, _need);
		newEvent = EnergyDbHelper.registerEvent(newEvent);
		return newEvent;
	}

	public Event generateStartEvent() {
		try {
			startEvent = aux_generateStartEvent(this.id, this.need);
		} catch (DoublonException e) {
			startEvent =  EnergyDbHelper.retrieveEvent(EventType.REQUEST, agentName, need.getBeginDate());
		}
		// Remove other events
		stopEvent = null;expiryEvent = null;
		// Post event in LSA
		postEvent(startEvent);
		return startEvent;
	}

	public Event generateUpdateEvent(WarningType warningType) {
		if(startEvent!=null) {
			Event newEvent = new Event(EventType.REQUEST_UPDATE, this.need);
			newEvent.setWarningType(warningType);
			try {
				newEvent.setOriginEvent(startEvent.clone());
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
			startEvent = EnergyDbHelper.registerEvent2(newEvent);
			// Remove other events
			stopEvent = null;expiryEvent = null;
			// Post event in LSA
			postEvent(startEvent);
		}
		return startEvent;
	}

	public Event generateExpiryEvent() {
		expiryEvent = new Event(EventType.REQUEST_EXPIRY, agentName, authentication.getAgentLocation(), need.getPower(), need.getEndDate(), need.getEndDate(), need.getDeviceName(), need.getDeviceCategory());
		try {
			if(startEvent!=null) {
				expiryEvent.setOriginEvent(this.startEvent.clone());
			}
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		expiryEvent = EnergyDbHelper.registerEvent2(expiryEvent);
		// Remove other events
		startEvent = null; stopEvent = null;
		// Post event in LSA
		postEvent(expiryEvent);
		return expiryEvent;
	}

	public Event generateStopEvent(RegulationWarning warning) {
		Date timeStop = warning.getDate();
		stopEvent = new Event(EventType.REQUEST_STOP, agentName, authentication.getAgentLocation(), need.getPower(), timeStop, timeStop, need.getDeviceName(), need.getDeviceCategory());
		try {
			stopEvent.setOriginEvent(startEvent.clone());
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		if(warning!=null) {
			stopEvent.setWarningType(warning.getType());
		}
		stopEvent = EnergyDbHelper.registerEvent2(stopEvent);
		// Remove other events
		startEvent = null;expiryEvent = null;
		// Post event in LSA
		postEvent(stopEvent);
		return stopEvent;
	}

	public ConsumerAgent(int _id, AgentAuthentication authentication, EnergyRequest _need) throws Exception {
		super(authentication.getAgentName()
			, authentication
			, new String[] { "OFFER", "CONTRACT", "WARNING", "RESCHEDULE"}
			, new String[] { "REQ", "EVENT", "SATISFIED",  generateTagConfirm(_id), generateTagDisabled(_id), generateTagUpdate(_id)}
			, new Object[] { _need, aux_generateStartEvent(_id, _need), null, null , null, null}
			, LsaType.Query);
		initFields(_id, authentication, _need);
	}

	private void initFields(int _id, AgentAuthentication _authentication, EnergyRequest _need) {
		setEpsilon(0); // No greedy policy
		this.id = _id;
		this.authentication = _authentication;
		this.setInput(new String[] { "OFFER", "CONTRACT", "WARNING", "RESCHEDULE"});
		this.setOutput(new String[] { "REQ", "EVENT", "SATISFIED" , generateTagConfirm(_id),  generateTagDisabled(_id), generateTagUpdate(_id)});
		setUrl(_authentication.getAgentLocation());
		this.need = _need;
		this.confirmTag = generateTagConfirm(id);
		this.updateTag = generateTagUpdate(id);
		this.disabledTag = generateTagDisabled(id);
		this.need.setConfirmTag(confirmTag);
		this.tableSingleOffers = new HashMap<String, SingleOffer>();
		this.globalOffer = new GlobalOffer(this);
		int decay = 10 + getTimeLeftSec();
		this.addDecay(decay);
		if(values.length>0 && this.values[1] instanceof Event) {
			this.startEvent = (Event) this.values[1];
			if(startEvent!=null) {
				this.need.setEventId(startEvent.getId());
			}
			postEvent(startEvent);
		}
		debugLevel = 10;
		completeOutputPropertyIfNeeded();
	}

	public void reinitialize(int _id, AgentAuthentication _authentication, EnergyRequest _need) throws Exception {
		super.reinitialize(this.agentName
				, new String[] { "OFFER", "CONTRACT", "WARNING"}
				, new String[] { "REQ", "EVENT" , "SATISFIED" , generateTagConfirm(_id), generateTagDisabled(_id) , generateTagUpdate(_id)}
				, new Object[] { _need, aux_generateStartEvent(_id, _need), null, null, null, null }
				, LsaType.Query
		);
		setEpsilon(0); // No greedy policy
		initFields(_id, _authentication, _need);
		logger.info("reinitialize : lsa = " + lsa.toVisualString()+ " this="+ this + " memory address =" + this.hashCode());
	}


	// Offer comparator : by time cover
	private final static Comparator<SingleOffer> offerComparatorTimeCover = new Comparator<SingleOffer>() {
		public int compare(SingleOffer offer1, SingleOffer offer2) {
			// to sort in descending order
			return -1*offer1.compareTimeCover(offer2);
		}
	};
	// Offer comparator : by issuer distance
	private final static Comparator<SingleOffer> offerComparatorDistance = new Comparator<SingleOffer>() {
		public int compare(SingleOffer offer1, SingleOffer offer2) {
			// to sort in descending order
			return offer1.compareDistance(offer2);
		}
	};
	public void reward(Lsa lsaResult, int reward) {
		logger.info("rewarded-->" + lsaResult.getAgentName() + " by " + reward);
		logger.info("lsaResult: " + lsaResult.toVisualString());

		if (lsaResult.getAgentName().contains("*")) {
			lsaResult.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Reward);
			sendTo(lsaResult, lsaResult.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
		} else
			this.rewardLsa(lsaResult, agentName, reward, 0.0);
	}

	private GlobalOffer generateGlobalOffer() {
		List<SingleOffer> offerList = new ArrayList<>();
		for (SingleOffer singleOffer : tableSingleOffers.values()) {
			if (!singleOffer.hasExpired(0) && singleOffer.getPower()>0) {
				if("_Consumer_2".equals(agentName) && tableSingleOffers.size()>0) {
					logger.info("generateGlobalOffer " + this.agentName + " For debug");
				}
				offerList.add(singleOffer);
			}
		}
		if(offerList.size()>1) {
			logger.info( this.agentName + " For debug");
		}
		// Mix the offers randomly
		Collections.shuffle(offerList);
		// Apply the priority rules to classify offers (for example : take the  nearest first)
		if(PriorityLevel.HIGH.equals(this.need.getPriorityLevel()) && offerList.size()>1) {
			Collections.sort(offerList, offerComparatorTimeCover);
		} else {
			Collections.sort(offerList, offerComparatorDistance);
		}

		globalOffer = new GlobalOffer(this);
		for (SingleOffer nextOffer : offerList) {
			this.globalOffer.addSingleOffer(nextOffer);
		}
		return globalOffer;
	}

	public boolean isOK(GlobalOffer globalOffer) {
		return globalOffer.isActive() && globalOffer.computePower() >= this.need.getPower()
				&& globalOffer.computePower() > 0;
	}

	private boolean hasWantedProperty(Lsa bondedLsa) {
		for(String nextWaiting : waiting) {
			if(!bondedLsa.getPropertiesByQueryAndName(agentName, nextWaiting).isEmpty()) {
				return true;
			}
		}
		return false;
	}

	private void cleanEventProerty() {
		if(eventDecay>0) {
			eventDecay-=1;
		} else {
			this.lsa.removePropertiesByName("EVENT");
			if(debugLevel>0) {
				logger.info("after removing event property : startEvent = " + startEvent);
			}
		}
	}

	public void postEvent(Event eventToPost) {
		this.lsa.removePropertiesByName("EVENT");
		try {
			this.lsa.addProperty(new Property("EVENT", eventToPost.clone()));
			eventDecay = SapereUtil.EVENT_INIT_DECAY;
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
	}



	@Override
	public void onBondNotification(BondEvent event) {
		try {
			Lsa bondedLsa = event.getBondedLsa();
			if(debugLevel>0) {
				if(bondedLsa.getAgentName().startsWith("Prod")) {
					logger.info(" Bond event : ** ConsumerAgent ** " + agentName + " - " + bondedLsa.getAgentName());
				}
			}
			if("_Consumer_86".equals(agentName)) {
				logger.info("onBondNotification " + this.agentName + " For debug");
			}
			int action = getActionToTake(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString()); // add greedy
			if(hasWantedProperty(bondedLsa)) {
				if (true) {
					String query = bondedLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString();
					Lsa chosenLSA = bondedLsa;
					String state = chosenLSA.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
					if (action == 0) {
						addState(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString(), action, 0, 0);
						lsa.addProperty(new Property(lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString(),
								null, query, chosenLSA.getAgentName(), state,
								chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(), false));
					} else if (action == 1) {
						results.add(bondedLsa);
						//String ipSource = bondedLsa.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
						//String pBond = bondedLsa.getAgentName();
						// String state =
						// boundedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString() +","
						// + "CONS";
						try {
							state = SapereUtil.addOutputsToLsaState(bondedLsa, new String[] { this.confirmTag, this.disabledTag, this.updateTag});
							query = this.agentName;
							AgentType bondAgentType = AgentType.getFromLSA(bondedLsa);
							if (AgentType.PRODUCER.equals(bondAgentType)) {
								//bondedLsa.getPropertiesByQuery(query);
								// Get offer of production agent
								Property pOffer = SapereUtil.getOnePropertyFromLsa(bondedLsa, query, "OFFER");
								if(pOffer!=null && pOffer.getValue() instanceof ProtectedSingleOffer) {
									// Analyse the offer
									ProtectedSingleOffer protectedOffer = (ProtectedSingleOffer)  pOffer.getValue();
									SingleOffer receivedOffer =  protectedOffer.getSingleOffer(this);
									SingleOffer newOffer = receivedOffer.clone();
									newOffer.setIssuerDistance(Sapere.getInstance().getDistance(newOffer.getIssuerLocation(), newOffer.getIssuerDistance()));
									boolean offerUsed = false;
									if (this.currentContract == null || this.currentContract.hasExpired()) {
										if(!SapereUtil.hasProperty(lsa, agentName, this.confirmTag)) {
											if (need.isOK(newOffer)) {
												tableSingleOffers.put(newOffer.getProducerAgent(), newOffer);
												offerUsed = true;
												logger.info(" consumer " + this.agentName + " set offer used : id:" + newOffer.getId() + "  "  + newOffer.getProducerAgent() + " W = " + newOffer.getPower());
												//logger.info(" consumer " + this.agentName + " set offer used : " + newOffer);
												//handleOffers();
											}
										}
									}
									EnergyDbHelper.setSingleOfferAcquitted(newOffer, startEvent, offerUsed);
									newOffer.setAcquitted(true);
									if(!offerUsed /* && !isSatisfied()*/) {
										logger.info(this.agentName + " For debug : satisfied = " + isSatisfied());
									}
								}
							} else if (AgentType.CONTRACT.equals(bondAgentType)) {
								Object oProtectedContract = SapereUtil.getOnePropertyValueFromLsa(bondedLsa, query, "CONTRACT");
								if (oProtectedContract != null && oProtectedContract instanceof ProtectedContract) {
									ProtectedContract protecedContract = (ProtectedContract) oProtectedContract;
									if(!agentName.equals(protecedContract.getConsumerAgent())) {
										logger.info("Not same consumer");
									} else {
										// Same agents
										if(this.isDisabled() && protecedContract.hasAllAgreements()) {
											logger.info(this.agentName + " For debug : agent disabled and contract is valided");
										}
										boolean isSatisfiedBefore = this.isSatisfied();
										if (protecedContract.hasAllAgreements() && !protecedContract.hasExpired() && !this.isDisabled()) {
											Contract contract = protecedContract.getContract(this);
											this.currentContract = (Contract) contract.clone();
											checkupContract();
											tableSingleOffers.clear();
											// Reset the global offer
											globalOffer = new GlobalOffer(this);
											refreshLsaProperties();
											if(!isSatisfiedBefore) {
												logger.info(" consumer " + this.agentName + " IS SATISFIED__");
											}
										} else if (protecedContract.hasDisagreement()) {
											// Canceled contract
											this.currentContract = null;
											this.lsa.removePropertiesByName("SATISFIED");
											this.refreshLsaProperties();
										}
									}
								}
							} else if (AgentType.REGULATOR.equals(bondAgentType)) {
								Property pReschedule = SapereUtil.getOnePropertyFromLsa(chosenLSA, agentName, "RESCHEDULE");
								if(pReschedule!=null && pReschedule.getValue() instanceof RescheduleTable) {
									RescheduleTable rescheduleTable = (RescheduleTable) pReschedule.getValue();
									if(currentContract!=null) {
										for(String producer : currentContract.getProducerAgents()) {
											if(rescheduleTable.hasItem(producer)) {
												RescheduleItem item = rescheduleTable.getItem(producer) ;
											}
										}
									}
								}
								Property pWarning = SapereUtil.getOnePropertyFromLsa(chosenLSA, agentName, "WARNING");
								if(pWarning!=null && pWarning.getValue() instanceof RegulationWarning) {
									RegulationWarning warning = (RegulationWarning) pWarning.getValue();
									if(warning.hasAgent(agentName) && !receivedWarnings.contains(warning)) {
										receivedWarnings.add(warning);
										if(	   WarningType.OVER_CONSUMPTION.equals(warning.getType())
											|| WarningType.USER_INTERRUPTION.equals(warning.getType())) {
											if(!this.isDisabled()) {
												// Disable consumer
												need.setDisabled(true);
												if(currentContract!=null) {
													currentContract.stop(agentName);
												}
												if(lsa.getPropertiesByName(this.disabledTag).isEmpty()) {
													lsa.removeAllProperties();
													RegulationWarning warningToSet = warning.clone();
													if(	   WarningType.USER_INTERRUPTION.equals(warning.getType())
														|| WarningType.CHANGE_REQUEST.equals(warning.getType() )) {
														warningToSet.setWaitingDeadline(SapereUtil.shiftDateSec(new Date(), DISABLED_SHORT_DURATION_SEC));
													} else {
														warningToSet.setWaitingDeadline(SapereUtil.shiftDateMinutes(new Date(), DISABLED_DURATION_MINUTES));
													}
													warningToSet.setReceptionDeadline(SapereUtil.shiftDateMinutes(new Date(), DISABLED_DURATION_MINUTES));
													lsa.addProperty(new Property(this.disabledTag, warningToSet));
												}
												if (stopEvent == null) {
													stopEvent = generateStopEvent(warning);
													//eventToPost = stopEvent.clone();
													logger.info(this.agentName + " Request interuption 1 : unset startEvent");
												}
											}
										} else if(WarningType.CHANGE_REQUEST.equals(warning.getType())) {
											if(warning.getChangeRequest()!=null) {
												updateRequest(warning.getChangeRequest());
											}
										}
									}
								}
							}
						} catch (Throwable e) {
							logger.error(e);
						}
						this.removeBondedLsasOfQuery(query);
					}
				}
			}
		} catch (Throwable e) {
			logger.error(e);
			logger.error("Exception thrown in bond evt handler " + agentName + " " + event + " " + e.getMessage());
		}
	}

	public Set<String> getLinkedAgents() {
		Set<String> result = new HashSet<>();
		if (isSatisfied()) {
			for (String agent : currentContract.getProducerAgents()) {
				result.add(agent);
			}
		}
		return result;
	}

	public boolean isSatisfied() {
		if(this.hasExpired()) {
			return false;
		}
		return currentContract != null && !currentContract.hasExpired() && currentContract.hasAllAgreements();
	}

	public boolean hasConfirmed() {
		return !lsa.getPropertiesByName(this.confirmTag).isEmpty();
	}

	public boolean isInWarning() {
		if(need!=null) {
			return need.getWarningDurationSec()>0;
		}
		return false;
	}

	/**
	 * Power provided by producer
	 * 
	 * @return
	 */
	public Float getContractedPower(String locationFilter) {
		Float power = new Float(0);
		if (isSatisfied()) {
			power += currentContract.computePower(locationFilter);
		}
		return power;
	}

	public Map<String, Float> getContractsRepartition() {
		Map<String, Float> result = new HashMap<String, Float>();
		if (isSatisfied()) {
			for (String producer : currentContract.getProducerAgents()) {
				Float power = currentContract.getMapPower().get(producer);
				if (power != null) {
					boolean isLocal = Sapere.getInstance().isLocalAgent(producer);
					String agentName = producer + (isLocal? "" : "*");
					result.put(agentName, power);
				}
			}
		}
		return result;
	}

	public Map<String, Float> getOffersRepartition() {
		Map<String, Float> result = new HashMap<String, Float>();
		for (SingleOffer offer : this.tableSingleOffers.values()) {
			if (!offer.hasExpired(0)) {
				boolean isLocal = Sapere.getInstance().isLocalAgent(offer.getProducerAgent());
				String agentName = offer.getProducerAgent() + (isLocal? "" : "*");
				result.put(agentName, offer.getPower());
			}
		}
		return result;
	}

	public Float getOffersTotal() {
		Float result = new Float(0);
		for (SingleOffer offer : this.tableSingleOffers.values()) {
			if (!offer.hasExpired(0)) {
				result+=offer.getPower();
			}
		}
		return result;
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) { // change to return only one result
		try {
			Lsa decayedLsa = event.getLsa();
			boolean hasExpired = this.hasExpired();
			Integer decay = new Integer("" + decayedLsa.getSyntheticProperty(SyntheticPropertyName.DECAY));
			if("__Consumer_87".equals(this.agentName)) {
				logger.info("For debug agentName = " + agentName + " , decay = " + decay /*+ ", this = " + this*/);
			}
			if(debugLevel>10) {
				logger.info("onDecayedNotification " + this.agentName + " decay = " + decay + " Time left sec = "	+ getTimeLeftSec());
			}
			if (hasExpired) {
				if(decay>1) {
					// the agent should expire now : force decay
					this.addDecay(1);
				}
				cleanEventProerty();
				// Post expiration event
				if(expiryEvent==null && startEvent!=null) {
					try {
						expiryEvent = generateExpiryEvent();
						//firstDecay = true;
						//postEventTime = new Date();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.error(e);
					}
				}

			}
			HomeTotal homeTotal = EnergyDbHelper.retrieveLastHomeTotal();
			refreshRequestWarnings(homeTotal);
			//cleanObsoleteOffers();
			tryReactivation(homeTotal);

			// check contrat
			checkupContract();

			refreshLsaProperties();
			// Remove obsolete offers
			cleanObsoleteData();
			// Generate global offer
			handleReceivedOffers();
			// For propagation
			addGradient(3);
		} catch (Throwable e) {
			logger.error(e);
			logger.error("Exception throw in decay handler of agent " + this.agentName + " " + event + " " + e.getMessage());
		}
	}

	private void checkupContract() {
		if(currentContract!=null && currentContract.hasAllAgreements() && !currentContract.hasExpired()) {
			float contractPower = currentContract.computePower();
			if(Math.abs(contractPower - this.getNeed().getPower()) >= 0.001) {
				if(this.getNeed().getPower() > contractPower) {
					// Stop Contract
					this.lsa.removePropertiesByNames(new String[] { confirmTag, "SATISFIED"});
					this.currentContract = null;
				} else {
					// Update contract
					this.lsa.removePropertiesByNames(new String[] { updateTag});
					currentContract.modifyPower(getNeed().getPower());
					this.lsa.addProperty(new Property(updateTag, new ProtectedContract(currentContract)));
				}
			}
		}
	}

	private boolean tryReactivation(HomeTotal homeTotal) {
		boolean result = false;
		Property pDisabled = SapereUtil.getOnePropertyFromLsa(lsa, this.disabledTag);
		if(pDisabled!=null && pDisabled.getValue() instanceof RegulationWarning ) {
			// Clean expired disabled property
			RegulationWarning warning = (RegulationWarning) pDisabled.getValue();
			if(warning.hasWaitingExpired()) {
				if(WarningType.OVER_CONSUMPTION.equals(warning.getType())) {
					lsa.removePropertiesByName(disabledTag);
					// Check over consumption
					boolean overConsumption = false;
					if(homeTotal!=null) {
						// Check if the request will not generate over-consumption
						logger.info("homeTotal.getConsumed() = " + homeTotal.getConsumed() + ", need power = " + need.getPower());
						overConsumption = (homeTotal.getConsumed()+need.getPower() > 1*HomeMarkovStates.MAX_TOTAL_POWER) ;
					}
					if(overConsumption) {
						warning.setWaitingDeadline(SapereUtil.shiftDateMinutes(new Date(), DISABLED_DURATION_MINUTES));
						lsa.addProperty(new Property(this.disabledTag, warning));
					} else {
						try {
							this.need.setBeginDate(new Date());
							need.setDisabled(false);
							startEvent = generateStartEvent();
						} catch (Exception e) {
							logger.error(e);
						}
						result = true;
					}
				} else if (WarningType.USER_INTERRUPTION.equals(warning.getType())) {
					// Stop the agent
					lsa.removePropertiesByName(disabledTag);
					this.need.setEndDate(new Date());
				/*
				} else if (WarningType.CHANGE_REQUEST.equals(warning.getType())) {
					// Stop the agent
					lsa.removePropertiesByName(disabledTag);
					if(warning.getChangeRequest()!=null) {
						updateRequest(warning.getChangeRequest());
					}
				*/
				}
			}
		}
		return result;
	}

	private void updateRequest(EnergySupply changeRequest) {
		if(this.hasExpired() || this.isDisabled()) {
			logger.warning("updateRequest " + this.agentName + " cannot be updated : agent expired or disabled");
			return;
		} else if(this.startEvent==null) {
			logger.warning("updateRequest " + this.agentName + " cannot be updated : startEvent is null");
			return;
		}
		// Update request
		try {
			EnergySupply changeRequestCopy = changeRequest.clone();
			this.need.setBeginDate(changeRequestCopy.getBeginDate());
			need.checkBeginNotPassed();
			this.need.setEndDate(changeRequestCopy.getEndDate());
			this.need.setPower(changeRequestCopy.getPower());
			if(changeRequestCopy instanceof EnergyRequest) {
				EnergyRequest request = ((EnergyRequest) changeRequestCopy).clone();
				this.need.setDelayToleranceMinutes(request.getDelayToleranceMinutes());
				this.need.setPriorityLevel(request.getPriorityLevel());
			}
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		checkupContract();

		// Generate update event
		try {
			if(startEvent == null) {
				logger.error("updateRequest " + this.agentName + " startEvent is null");
			}
			this.startEvent = generateUpdateEvent(WarningType.CHANGE_REQUEST);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void completeOutputPropertyIfNeeded() {
		String sOutputProperty = "" + lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT);
		for(String nextOutput : this.getOutput())
		if (!sOutputProperty.contains(nextOutput)) {
			sOutputProperty=sOutputProperty+","+nextOutput;
		}
		lsa.addSyntheticProperty(SyntheticPropertyName.OUTPUT,sOutputProperty);
	}

	private void handleReceivedOffers() {
		int warningDuration = need.getWarningDurationSec();
		boolean isInStrongWarning = warningDuration>=5;
		boolean hasConfirmation = !lsa.getPropertiesByName(this.confirmTag).isEmpty();
		String msgTag = "handleReceivedOffers " + this.agentName + " [warningDuration=" + warningDuration + "] ";
		if(isInStrongWarning) {
			// For debug
			logger.warning(msgTag + " begin : requested = " + SapereUtil.df.format(need.getPower()) + " hasConfirmation = " + hasConfirmation + " has offers = " + (tableSingleOffers.size()>0));
			if(!hasConfirmation) {
				if(tableSingleOffers.size()>0) {
					float offerTotal = 0;
					for(SingleOffer offer : this.tableSingleOffers.values()) {
						offerTotal+=offer.getPower();
					}
					logger.warning(msgTag + " received total :" + SapereUtil.df.format(offerTotal)
						+ " received content : "+ this.tableSingleOffers.values());
				}
			}
		}
		if(!this.isSatisfied() && !this.isDisabled() && !hasConfirmation && tableSingleOffers.size()>0) {
			if(isOK(globalOffer)) {
				//logger.info("For debug");
			}
			globalOffer = generateGlobalOffer();
			if(isInStrongWarning) {
				logger.warning(msgTag + " after generateGlobalOffer globalOffer = " + globalOffer);
			}
			// Check if the global offer can met the demand
			if(this.isOK(globalOffer)) {
				if(isInStrongWarning) {
					logger.warning(msgTag + " globalOffer is OK");
				}
				if(!lsa.getPropertiesByName(this.confirmTag).isEmpty()) {
					logger.info(msgTag +  " confirmTag is set");
				}
				// The Offer is OK : Confirm global offer to generate a contract
				AgentState state = new AgentState(Arrays.asList(getInput()), new ArrayList<String>());
				state.addOutput(confirmTag);
				//String aProducer = (String) globalOffer.getProducerAgents().toArray()[0];
				//String contractAgent = this.getContractAgentName();
				//String ipSource = this.tableIpSource.get(aProducer);
				//lsa.addProperty(new Property(this.confirmTag, globalOffer, agentName, contractAgent, state.toString(),ipSource, false));
				Date validationDeadline = SapereUtil.shiftDateSec(new Date(), CONTRACT_VALIDATION_DEALY_SEC);
				Contract newContract = new Contract(this.getContractAgentName(), globalOffer, validationDeadline);
				newContract.addAgreement(this, true);
				ProtectedContract newProcetectContract = new ProtectedContract(newContract);
				//lsa.addProperty(new Property(this.confirmTag, globalOffer));
				lsa.addProperty(new Property(this.confirmTag, newProcetectContract));
				//lsa.removePropertiesByName("REQ");
				lsa.addSyntheticProperty(SyntheticPropertyName.STATE, state.toString());
				//lsa.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
				this.confirmationDecay = CONFIRMATION_INIT_DECAY;
				/*
				String output = "" + lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT);
				if (!output.contains(this.confirmTag)) {
					lsa.addSyntheticProperty(SyntheticPropertyName.OUTPUT,
							output + "," + this.confirmTag);
				}*/
				tableSingleOffers.clear();
				refreshLsaProperties();
				EnergyDbHelper.setSingleOfferAccepted(globalOffer);
			} else {
				if(isInStrongWarning) {
					logger.warning(msgTag + " global offer is still not OK : total = " + globalOffer.computePower() + " content = " +  globalOffer);
				}
			}
		} else {
			// For debug
			if(isInStrongWarning) {
				if(lsa.getPropertiesByName(this.confirmTag).isEmpty() &&  tableSingleOffers.size()>0) {
					// For debug
					logger.warning(msgTag + " step666 : isSatisfied = " + isSatisfied() + "  isDisabled = " + this.isDisabled() + "  confirm property =  "  + lsa.getPropertiesByName(this.confirmTag) );
				}
			}
		}
	}

	private void refreshLsaProperties() {
		// Clean event
		//cleanEventProerty();
		Property pReq = SapereUtil.getOnePropertyFromLsa(lsa, "REQ");
		Property pSatisfied = SapereUtil.getOnePropertyFromLsa(lsa,  "SATISFIED");
		Property pConfirm = SapereUtil.getOnePropertyFromLsa(lsa, confirmTag);
		if(this.isDisabled()) {
			// Agent is disabled
			if(pReq!=null || pConfirm!=null || pSatisfied !=null) {
				lsa.removePropertiesByNames(new String[] {"REQ", confirmTag, updateTag, "SATISFIED"});
			}
		} else if (this.isSatisfied()) {
			// Agent is satified: REQ and confirmTag proprties shoud be null
			if (pReq != null || pConfirm!=null) {
				lsa.removePropertiesByNames(new String[] {"REQ", this.confirmTag});
			}
			// Set SATISFIED property
			if(pSatisfied==null) {
				this.lsa.addProperty(new Property(
					"SATISFIED", "1", this.agentName, "", Arrays.toString(waiting).replace("[", "").replace("]", "")
					+ "|" + Arrays.toString(prop).replace("[", "").replace("]", ""),
					NodeManager.getLocation(), false));
			}
			// reset warning Date
			if(this.isInWarning()) {
				need.setWarningDate(null);
			}
			// Check if update propery should be set
			if(Math.abs(currentContract.computePower() - need.getPower()) >= 0.001) {
				logger.warning(this.agentName + " contract not update to date  : " + currentContract);
			} else {
				//lsa.removePropertiesByName(updateTag);
			}
		} else if (this.hasConfirmed()) {
			// Agent is not satisified by has aleardy confirmed : remove the request to stop offers generaiton
			if (pReq != null) {
				lsa.removePropertiesByName("REQ");
			}
		} else {
			// Agent is not satisfied and has not confirmed an offer: REQ property should be set
			if (pReq == null) {
				this.results.clear();
				this.lsa.addProperty(new Property(
						"REQ", this.need, this.agentName, "", Arrays.toString(waiting).replace("[", "").replace("]", "")
								+ "|" + Arrays.toString(prop).replace("[", "").replace("]", ""),
						NodeManager.getLocation(), false));
			}
			// SATISFIED propery should be unset
			this.lsa.removePropertiesByName(this.updateTag);
			this.lsa.removePropertiesByQueryAndName(this.agentName, "SATISFIED");
		}
		//firstDecay = false;
	}

	private void cleanObsoleteData() {
		if(this.isDisabled() || this.hasExpired() || this.isSatisfied()) {
			tableSingleOffers.clear();
		} else {
			String offerKey = null;
			while ((offerKey = SapereUtil.getExpiredOfferKey(tableSingleOffers, 0)) != null) {
				// For debug: check if the offer is acquitted
				SingleOffer offer = tableSingleOffers.get(offerKey);
				if(!offer.getAcquitted()) {
					logger.warning("cleanObsoleteData " + this.agentName + " this offer is not acquitted " + offer);
				}
				this.tableSingleOffers.remove(offerKey);
			}
		}
		// Remove confirmation if expired
		Property pConfirm = SapereUtil.getOnePropertyFromLsa(lsa, this.confirmTag);
		if(pConfirm!=null) {
			if(confirmationDecay<=0 || this.isSatisfied()) {
				lsa.removePropertiesByName(this.confirmTag);
				lsa.addSyntheticProperty(SyntheticPropertyName.STATE, "");
			} else {
				confirmationDecay-=1;
			}
		}
		// Clean expired warnings
		RegulationWarning expiredWarning = null;
		while( (expiredWarning=SapereUtil.getExpiredWarning(receivedWarnings)) != null) {
			receivedWarnings.remove(expiredWarning);
		}
		// Delete contract if invalid
		if(currentContract!=null) {
			if(currentContract.hasDisagreement() || currentContract.hasExpired()) {
				currentContract = null;
			}
		}
	}

	private void refreshRequestWarnings(HomeTotal homeTotal) {
		Float homeTotalAvailable = (homeTotal==null)? new Float(0) : homeTotal.getAvailable();
		Date current = new Date();
		if(need.getPower()<= homeTotalAvailable) {
			need.incrementWarningCounter(current);
		} else {
			need.resetWarningCounter(current);
		}
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {

	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {

	}

	public String getDisabledTag() {
		return disabledTag;
	}

	@Override
	public void setInitialLSA() {
		this.submitOperation();
	}

	public String getConfirmTag() {
		return confirmTag;
	}

	public boolean isActive() {
		return need.isActive();
	}

	public int getTimeLeftSec() {
		return need.getTimeLeftSec();
	}

	public boolean hasExpired() {
		return need.hasExpired();
	}

	public boolean isDisabled() {
		return (lsa.getPropertiesByName(disabledTag)).size()>0;
	}

	public EnergyRequest getNeed() {
		return need;
	}

	public int getId() {
		return id;
	}

	public void setContractAgentName(String contractAgentName) {
		this.contractAgentName = contractAgentName;
	}

	public String getContractAgentName() {
		return contractAgentName;
	}

	public String getUpdateTag() {
		return updateTag;
	}

	public Event getStopEvent() {
		return stopEvent;
	}
}
