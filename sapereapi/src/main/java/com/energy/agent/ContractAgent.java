package com.energy.agent;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.energy.model.AgentType;
import com.energy.model.ConfirmationItem;
import com.energy.model.Contract;
import com.energy.model.DeviceCategory;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.ProtectedConfirmationTable;
import com.energy.model.ProtectedContract;
import com.energy.model.RegulationWarning;
import com.energy.model.RescheduleItem;
import com.energy.model.RescheduleTable;
import com.energy.model.WarningType;
import com.energy.util.EnergyDbHelper;
import com.energy.util.SapereLogger;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;

import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class ContractAgent extends SapereAgent implements IEnergyAgent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Random rand = new Random();
	private String consumer;
	private Date consumerEndDate;
	private int nbBonds = 0;
	private int consumerId = 0;
	private String confirmTag = null;
	private String updateTag = null;
	private String disabledTag = null;
	private Event startEvent = null;
	private Event stopEvent = null;
	private Event expiryEvent = null;
	private Map<String, Lsa> tableChosenLsa = null;
	private List<RegulationWarning> receivedWarnings = new ArrayList<RegulationWarning>();
	private Map<String, ConfirmationItem> receivedConfirmations = new HashMap<String, ConfirmationItem>();
	private Contract currentContract = null;
	private Date lastPostTime = null;

	// private Event eventToPost = null;
	private int MAX_STOP_DECAY = 3;
	int eventDecay = 0;
	int stopDecay= 0;
	int cContractDecay = 0;
	private String deviceName;
	private DeviceCategory deviceCategory;
	private static SapereLogger logger = SapereLogger.getInstance();
	private boolean isInSpace = false;

	public ContractAgent(ConsumerAgent consumerAgent, AgentAuthentication _agentAuthentication) {
		super(_agentAuthentication.getAgentName()
			, _agentAuthentication
			, new String[] { consumerAgent.getConfirmTag(), consumerAgent.getDisabledTag(), consumerAgent.getUpdateTag(), "PROD_CONFIRM", "WARNING", "RESCHEDULE"}
			, new String[] { "EVENT", "CONTRACT" }, LsaType.Service);
		initFields(consumerAgent, _agentAuthentication);
		debugLevel = 0;
		logger.info("ContractAgent : lsa = " + lsa.toVisualString()+ " this="+ this + " memory address =" + this.hashCode());
	}

	private void initFields(ConsumerAgent consumerAgent, AgentAuthentication _agentAuthentication) {
		setEpsilon(0); // No greedy policy
		if(!AgentType.CONTRACT.getLabel().equals(_agentAuthentication.getAgentType())) {
			throw new RuntimeException("User contrat agent authentication!");
		}
		super.setInput(new String[] { consumerAgent.getConfirmTag(), consumerAgent.getDisabledTag(), consumerAgent.getUpdateTag() , "PROD_CONFIRM", "WARNING", "RESCHEDULE"});
		super.setOutput(new String[] { "EVENT", "CONTRACT"});
		this.setOutput(new String[]  { "EVENT", "CONTRACT"});
		setUrl(consumerAgent.getUrl());
		this.consumer = consumerAgent.getAgentName();
		this.consumerId = consumerAgent.getId();
		this.deviceName = consumerAgent.getNeed().getDeviceName();
		this.deviceCategory = consumerAgent.getNeed().getDeviceCategory();
		this.nbBonds = 0;
		this.confirmTag = consumerAgent.getConfirmTag();
		this.disabledTag = consumerAgent.getDisabledTag();
		this.updateTag = consumerAgent.getUpdateTag();
		consumerEndDate = consumerAgent.getNeed().getEndDate();
		this.tableChosenLsa = new HashMap<String, Lsa>();
		int decay = 1 + this.getTimeLeftSec();
		this.addDecay(decay);
		this.authentication = _agentAuthentication;
		startEvent = null;stopEvent = null;expiryEvent = null;
		receivedConfirmations = new HashMap<String, ConfirmationItem>();
	}

	public void reinitialize(ConsumerAgent consumerAgent, AgentAuthentication _agentAuthentication) {
		initFields(consumerAgent, _agentAuthentication);
		this.lsa.removeAllProperties();
		this.lsa.setAgentName("");
		logger.info("reinitialize : lsa = " + lsa.toVisualString()+ " this="+ this + " memory address =" + this.hashCode());
	}

	@Override
	public void setInitialLSA() {
		this.submitOperation();
	}

	@Override
	public void onBondNotification(BondEvent event) {
		try {
			Lsa bondedLsa = event.getBondedLsa();
			String query = bondedLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString();
			if(debugLevel>0) {
				logger.info("** ContractAgent bonding ** " + agentName + " Q: " + query + " startEvent = " + startEvent + " bonAgent =  " + bondedLsa.getAgentName() + " this="+ this + " memory address =" + this.hashCode());
			}
			lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Service); // check
			this.addBondedLSA(bondedLsa);
			int action = getActionToTake(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString()); // add
			if (lsa.hasBondedBefore(bondedLsa.getAgentName(), query)) {
				logger.info("** " + bondedLsa.getAgentName() + " Already bound before query " + query);
			}

			// if (!this.hasBondedBefore(bondedLsa.getAgentName(), query)) {
			if (lsa.getSubDescription().size() >= 1) { // output
				Lsa chosenLSA = getBondedLsaByQuery(query).get(rand.nextInt(getBondedLsaByQuery(query).size()));
				String bondAgentName = chosenLSA.getAgentName();
				if(bondAgentName.endsWith("*")) {
					bondAgentName = bondAgentName.replace('*', ' ').trim();
				}
				this.tableChosenLsa.put(bondAgentName, chosenLSA);
				String state = chosenLSA.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
				if (action == 0) {
					addState(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString(), action, 0, 0);
					lsa.addProperty(new Property(lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString(), null,
							query, chosenLSA.getAgentName(), state,
							chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(), false));
				} else if (action == 1) {
					//String ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
					//String pBond = chosenLSA.getAgentName();
					AgentType bondAgentType = AgentType.getFromLSA(bondedLsa);
					if(AgentType.CONSUMER.equals(bondAgentType) && !consumer.equals(query)) {
						//logger.info(this.agentName + " For debug");
					}
					if("_Contract_6".equals(this.agentName)) {
						logger.info(this.agentName + " for debug");
					}
					if (debugLevel > 0 && "__Contract_8".equals(agentName) && AgentType.PRODUCER.equals(bondAgentType) &&  currentContract!=null) {
						logger.info(this.agentName + " for debug");
					}
					if (AgentType.CONSUMER.equals(bondAgentType) && consumer.equals(query)) {
						//ConsumerAgent consumerAgent = (ConsumerAgent) bondAgent;
						Property pConfirm = SapereUtil.getOnePropertyFromLsa(chosenLSA,  this.confirmTag);
						Property pUpdate = SapereUtil.getOnePropertyFromLsa(chosenLSA,  this.updateTag);
						Property pDisabled = SapereUtil.getOnePropertyFromLsa(chosenLSA, this.disabledTag);
						if(pUpdate !=null) {
							//logger.info(this.agentName + " For debug : pUpdate = " + pUpdate);
						}
						if(debugLevel>0 && pDisabled != null && pDisabled.getValue() instanceof RegulationWarning) {
							logger.info(this.agentName + " For debug ; diabled property found " + this.disabledTag);
							if(currentContract!=null) {
								stopCurrentContract(chosenLSA, null);
							}
						} else if (pConfirm != null && pConfirm.getValue() instanceof ProtectedContract) {
							ProtectedContract newProtectedContract = (ProtectedContract) pConfirm.getValue();
							if(debugLevel>0 && newProtectedContract.getRequest().getDisabled()) {
								logger.info(this.agentName + " For debug  :contract is disabled");
							}
							Contract oldContract = getCloneOfCurrentContract();
							Contract newContract = newProtectedContract.getContract(this);
							if (newContract.isActive() && newContract.computePower() > 0) {
								state = SapereUtil.addOutputsToLsaState(chosenLSA, new String[] { "CONTRACT" });
								state = confirmTag + "|CONTRACT";
								currentContract = newContract.clone();
								boolean isOk = !currentContract.validationHasExpired();
								if(!isOk) {
									logger.warning(this.agentName + " validationHasExpired : invalidate the new contract to valid");
								}
								currentContract.addAgreement(this, isOk);
								// Update LSA
								setContractProperties(currentContract, oldContract);
								pConfirm.setChosen(true);
								lsa.addSyntheticProperty(SyntheticPropertyName.STATE, state);
							}
						} else if (true && pUpdate!=null && pUpdate.getValue() instanceof ProtectedContract) {
							Contract oldContract = getCloneOfCurrentContract();
							ProtectedContract newProtectedContract = (ProtectedContract) pUpdate.getValue();
							Contract newContract = newProtectedContract.getContract(this);
							if(newContract.getRequest()!=null && newContract.getRequest().getEndDate() !=null) {
								Date toSet = newContract.getRequest().getEndDate();
								if(consumerEndDate==null || consumerEndDate.getTime()!=toSet.getTime()) {
									consumerEndDate = toSet;
									logger.info(this.agentName +  " receives contract update from " + bondedLsa.getAgentName()
												+ " set consumerEndDate to " + SapereUtil.format_time.format(consumerEndDate));
								}
							}
							if (oldContract.hasChanged(newContract) && newContract.isActive() && newContract.computePower() > 0 ) {
								logger.info(this.agentName + " receives contract update from " + bondedLsa.getAgentName() + " " + newContract);
								currentContract = newContract.clone();
								boolean hasChanged = setContractProperties(currentContract, oldContract);
								if(currentContract.hasAllAgreements()) {
									if(hasChanged) {
										startEvent = generateUpdateEvent(WarningType.CHANGE_REQUEST);
									}
								} else {
									if(hasChanged) {
										stopCurrentContract(chosenLSA, new RegulationWarning(WarningType.CHANGE_REQUEST, new Date()));
									}
								}
							}
						}
					} else if (AgentType.PRODUCER.equals(bondAgentType)) {
						String producer = bondAgentName;
						Property pDisabled = SapereUtil.getOnePropertyFromLsa(chosenLSA, "DISABLED");
						Property pConfirm = SapereUtil.getOnePropertyFromLsa(chosenLSA, "PROD_CONFIRM");
						if (pConfirm != null && pConfirm.getValue() instanceof ProtectedConfirmationTable) {
							ProtectedConfirmationTable producerConfirmTable = (ProtectedConfirmationTable) pConfirm.getValue();
							// Update Contract
							Contract oldContract = this.getCloneOfCurrentContract();
							if (currentContract != null && currentContract.hasProducer(producer)) {
								// boolean hasAgreementBefore = oldContract.hasAllAgreements();
								Boolean isOK = null;
								if(producerConfirmTable.hasAccesAsContractAgent(this)) {
									isOK = producerConfirmTable.getConfirmation(this);
									// Add received confirmaitons in memory
									ConfirmationItem item =  producerConfirmTable.getConfirmationItem(this);
									receivedConfirmations.put(producer, item);
								}
								if (currentContract.waitingValidation()) {
									logger.info("Current contract waiting validation " + producer);
								}
								if (currentContract.validationHasExpired()) {
									logger.warning(this.agentName + " validationHasExpired(2) : invalidate contract");
									stopCurrentContract(chosenLSA, null);
								} else if (Boolean.TRUE.equals(isOK) && !currentContract.hasAgreement(producer)) {
									// Contract validation
									currentContract.addProducerAgreement(this, producer, true);
									setContractProperties(currentContract, oldContract);
									if (currentContract.hasAllAgreements() && startEvent == null) {
										logger.info(" --- validaiton of contract= " + currentContract.getContractAgent());
										currentContract.checkBeginNotPassed();
										// Add event to indicate that the contract is valided
										startEvent = generateStartEvent();
										logger.info("Step2a : startEvent = " + startEvent + " current instance = " + this); 
										//eventToPost = startEvent.clone();
									}
								} else if (Boolean.FALSE.equals(isOK) && !currentContract.hasDisagreement(producer)) {
									// Cancel the contract
									logger.info(this.agentName + " receive invalidation from " + chosenLSA.getAgentName() + " : cancel the ontract");
									stopCurrentContract(chosenLSA, null);
								}
							}
						} else if (pDisabled!=null) {
							// Stop Contract if a producer is disabled
							if (currentContract != null && currentContract.hasProducer(producer)) {
								stopCurrentContract(chosenLSA, null);
							}
						}
					} else if (AgentType.REGULATOR.equals(bondAgentType)) {
						// Stop current contract if over consumption
						Property pWarning = SapereUtil.getOnePropertyFromLsa(chosenLSA, "WARNING");
						if(pWarning!=null && pWarning.getValue() instanceof RegulationWarning) {
							RegulationWarning warning = (RegulationWarning) pWarning.getValue();
							if(WarningType.OVER_CONSUMPTION.equals(warning.getType()) ||  WarningType.USER_INTERRUPTION.equals(warning.getType())
								//||  WarningType.CHANGE_REQUEST.equals(warning.getType())
								) {
								if(warning.hasAgent(this.consumer) && !receivedWarnings.contains(warning)) {
									receivedWarnings.add(warning);
									if(currentContract!=null) {
										stopCurrentContract(chosenLSA, warning);
									}
									// Stop the agent in case of user interruption
									if(WarningType.USER_INTERRUPTION.equals(warning.getType())) {
										Date toSet =  SapereUtil.shiftDateSec(new Date(), 1);
										if(consumerEndDate.after(toSet)) {
											consumerEndDate = toSet;
											logger.info(this.agentName + " receive USER_INTERRUPTION : set consumerEndDate to " + SapereUtil.format_time.format(consumerEndDate));
										}
									}
								}
							}
							if(WarningType.CHANGE_REQUEST.equals(warning.getType())) {
								if(warning.hasAgent(this.consumer) && !receivedWarnings.contains(warning)) {									
									receivedWarnings.add(warning);
									if(warning.getChangeRequest() != null) {
										EnergySupply changeRequest = warning.getChangeRequest();
										consumerEndDate = warning.getChangeRequest().getEndDate();
										logger.info(this.agentName + " receive CHANGE_REQUEST : set consumerEndDate to " + SapereUtil.format_time.format(consumerEndDate));
										if(currentContract!=null) {
											if(changeRequest.getPower() >= currentContract.computePower() + 0.001) {
												// Stop Contract
												stopCurrentContract(chosenLSA, warning);			
											} else {
												// update contract
												Contract oldContract = getCloneOfCurrentContract();
												currentContract.modifyPower(changeRequest.getPower());
												currentContract.setBeginDate(new Date());
												if(changeRequest.getEndDate().before(currentContract.getEndDate())) {
													currentContract.setEndDate(changeRequest.getEndDate());
												}
												setContractProperties(currentContract, oldContract);
												startEvent = generateUpdateEvent(WarningType.CHANGE_REQUEST);
											}
										}
									}
								}
							}
							if(WarningType.OVER_PRODUCTION.equals(warning.getType())) {
								if(currentContract!=null) {
									boolean stopContrat = false;
									for(String producer : warning.getDestinationAgents()) {
										if(currentContract.hasProducer(producer)) {
											stopContrat = true;
										}
									}
									if(stopContrat) {
										stopCurrentContract(chosenLSA, warning);
									}
								}
							}
						}
						Property pReschedule = SapereUtil.getOnePropertyFromLsa(chosenLSA, "RESCHEDULE");
						if(pReschedule!=null && pReschedule.getValue() instanceof RescheduleTable) {
							RescheduleTable rescheduleTable = (RescheduleTable) pReschedule.getValue();
							if(currentContract!=null) {
								for(String producer : currentContract.getProducerAgents()) {
									if(rescheduleTable.hasItem(producer)) {
										RescheduleItem rescheduleItem = rescheduleTable.getItem(producer);
										if(rescheduleItem.getStopBegin().before(currentContract.getEndDate())) {
											// Modify contract end date
											Contract oldContract = getCloneOfCurrentContract();
											currentContract.setBeginDate(new Date());
											currentContract.setEndDate(rescheduleItem.getStopBegin());
											setContractProperties(currentContract, oldContract);
											if(currentContract.hasAllAgreements()) {
												// Generate an update event
												startEvent = generateUpdateEvent(rescheduleItem.getWarningType());
											}
										}
									}
								}
							}
						}
					} else {
						//lsa.addProperty(new Property("Ignore", "0", query, pBond, state, ipSource, false));
					}
				}
				this.removeBondedLsasOfQuery(query);
			}
			//lsa.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
			//lsa.addSyntheticProperty(SyntheticPropertyName.GRADIENT_HOP, "3");
		}
		catch(Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
	}

	private void stopCurrentContract(Lsa chosenLSA, RegulationWarning warning) {
		if(currentContract==null) {
			return;
		}
		Contract oldContract = getCloneOfCurrentContract();
		receivedConfirmations.clear();
		currentContract.stop(this.agentName);
		if(currentContract!=null && currentContract.hasDisagreement()) {
			try {
				// Add property to indicate that the contract is canceled
				setContractProperties(currentContract, oldContract);
				if (startEvent!=null && stopEvent == null) {
					stopEvent = generateStopEvent(warning);
					logger.info("Contract interuption " + currentContract);
				}
			} catch (Throwable e) {
				logger.error(e);
			}
		}
	}

	private boolean setContractProperties(Contract newContent, Contract oldContent) {
		boolean hasChanged = (oldContent==null) || oldContent.hasChanged(newContent);
		boolean toPost = (lastPostTime ==null);
		if(lastPostTime!=null) {
			Date postDeadline =SapereUtil.shiftDateSec(lastPostTime, 10);
			toPost = new Date().after(postDeadline);
		}
		try {
			if(newContent.waitingValidation()) {
				logger.info("setContractProperties For debug ");
			}
			if(hasChanged || toPost) {
				ProtectedContract protectedContract = new ProtectedContract(newContent.clone());
				lsa.removePropertiesByNames(new String[] {"CONTRACT"});
				if(newContent.hasDisagreement()) {
					stopDecay  = MAX_STOP_DECAY;
				}
				// Add property to indicate that the contract is canceled
				Lsa chosenLSA = tableChosenLsa.get(consumer);
				String ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
				String pBond = chosenLSA.getAgentName();
				String sState = SapereUtil.addOutputsToLsaState(chosenLSA, new String[] { "CONTRACT" });
				lsa.removePropertiesByName("CONTRACT");
				lsa.addProperty(new Property("CONTRACT",  protectedContract, consumer, pBond,	sState, ipSource, false));
				cContractDecay = MAX_STOP_DECAY;
				lastPostTime = new Date();
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return hasChanged;
	}

	public Event generateStartEvent()  {
		startEvent = new Event(EventType.CONTRACT, this.agentName,authentication.getAgentLocation(), currentContract.computePower()
				, currentContract.getBeginDate(), currentContract.getEndDate()
				, deviceName, deviceCategory);
		startEvent = EnergyDbHelper.registerEvent2(startEvent ,currentContract);
		// Remove other events
		stopEvent = null;expiryEvent = null;
		// Post event in LSA
		postEvent(startEvent);
		return startEvent;
	}

	public Event generateUpdateEvent(WarningType warningType) {
		if(startEvent!=null && currentContract!=null) {
			Event originStartEvent = null;
			try {
				originStartEvent = startEvent.clone();
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
			//Date timeStart = SapereUtil.getCurrentSeconde();
			startEvent = new Event(EventType.CONTRACT_UPDATE, agentName, authentication.getAgentLocation(), currentContract.computePower(), currentContract.getBeginDate(), currentContract.getEndDate(), deviceName, deviceCategory);
			startEvent.setOriginEvent(originStartEvent);
			startEvent.setWarningType(warningType);
			startEvent = EnergyDbHelper.registerEvent2(startEvent, currentContract);
			// Remove other events
			stopEvent = null;expiryEvent = null;
			// Post event in LSA
			postEvent(startEvent);
			return startEvent;
		}
		return null;
	}

	public Event generateExpiryEvent() {
		if(currentContract!=null) {
			Date evtDate = new Date();
			evtDate = currentContract.getEndDate();
			expiryEvent = new Event(EventType.CONTRACT_EXPIRY, agentName, authentication.getAgentLocation(), currentContract.computeInitalPower(), evtDate,  evtDate, deviceName, deviceCategory);
			try {
				if(startEvent!=null) {
					expiryEvent.setOriginEvent(startEvent.clone());
				}
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
			expiryEvent = EnergyDbHelper.registerEvent2(expiryEvent);
			// Post event in LSA
			postEvent(expiryEvent);
			// Remove other events
			startEvent = null; stopEvent = null;
			return expiryEvent;
			}
		return null;
	}

	public Event generateStopEvent(RegulationWarning warning) {
		Date timeStop = SapereUtil.getCurrentSeconde();
		if(warning!=null && warning.getChangeRequest()!=null) {
			timeStop = warning.getChangeRequest().getBeginDate();
		}
		stopEvent = new Event(EventType.CONTRACT_STOP, agentName, authentication.getAgentLocation(), currentContract==null? 0 : currentContract.computePower(), timeStop, timeStop, deviceName, deviceCategory);
		if(startEvent!=null) {
			try {
				stopEvent.setOriginEvent(startEvent.clone());
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
		}
		if(warning!=null) {
			stopEvent.setWarningType(warning.getType());
		}
		stopEvent = EnergyDbHelper.registerEvent2(stopEvent);
		// Remove other events
		startEvent = null;expiryEvent = null;
		// Post event in LSA
		postEvent(stopEvent);
		return stopEvent;
	}

	public Contract getCloneOfCurrentContract() {
		try {
			if(this.currentContract==null) {
				return null;
			} else {
				return currentContract.clone();
			}
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
		return null;
	}
	public Contract getCurrentContract() {
		return this.currentContract;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		Lsa decayedLsa = event.getLsa();
		// int hasCode = decayedLsa.hashCode();
		boolean hasExpired = this.hasExpired();
		Integer decay = new Integer("" + decayedLsa.getSyntheticProperty(SyntheticPropertyName.DECAY));
		if(debugLevel>0) {
			logger.info("onDecayedNotification: " + this.agentName + " decay = " + decay + " Time left sec = "	+ getTimeLeftSec() + " startEvent = " + startEvent);
		}
		if (hasExpired) {
			if (decay > 1) {
				// Force expiration
				logger.info("onDecayedNotification: " +this.agentName + " will expire");
				this.addDecay(1);
			}
		}
		cleanContract();

		// Clean expired warnings
		RegulationWarning expiredWarning = null;
		while( (expiredWarning=SapereUtil.getExpiredWarning(receivedWarnings)) != null) {
			receivedWarnings.remove(expiredWarning);
		}

		// Check producers confirmations
		if(currentContract!=null && currentContract.isActive() && currentContract.hasAllAgreements()) {
			for(String producer : currentContract.getProducerAgents()) {
				if(this.receivedConfirmations.containsKey(producer)) {
					ConfirmationItem producerConfirmation = this.receivedConfirmations.get(producer);
					if(producerConfirmation.hasExpired(5)) {
						logger.warning(this.agentName  + " : no recent confirmation from " + producer + " : last received : " + producerConfirmation);
						checkProducerInSpace(producer);
					}
				} else {
					logger.warning(this.agentName  + " : no confirmation from " + producer);
					checkProducerInSpace(producer);
				}
			}
		}
		// For propagation
		addGradient(3);
	}

	private void checkProducerInSpace(String producerName) {
		if(!Sapere.getInstance().isInSpace(producerName)) {
			// The contract should be broken
			Lsa chosenLSA = tableChosenLsa.get(producerName);
			RegulationWarning warning = new RegulationWarning(WarningType.NOT_IN_SPACE, new Date());
			stopCurrentContract(chosenLSA, warning);
		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		logger.info("onLsaUpdatedEvent:" + agentName);
	}

	@Override
	public void onRewardEvent(RewardEvent event) {
		String previousAgent = "";
		String newState = "";
		for (Property prop : event.getLsa().getPropertiesByQuery(event.getQuery())) {
			if (prop.getChosen()) {
				previousAgent = prop.getBond();
				newState = prop.getState();
				break;
			}
		}
		logger.info("State to reward " + newState + " by " + event.getReward() + " - " + event.getMaxSt1());
		if (!newState.equals(""))
			addState(getPreviousState(newState, getOutput()), 1, event.getReward(), event.getMaxSt1());

		logger.info("reward previous service " + previousAgent);

		Lsa lsaReward = NodeManager.instance().getSpace().getLsa(previousAgent);
		if (lsaReward != null && lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Service)) {
			rewardLsa(lsaReward, event.getQuery(), event.getReward(),
					getBestActionQvalue(getPreviousState(newState, getOutput()))); // maxQSt1
			lsaReward.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
		}

		if (lsaReward != null) {
			if (previousAgent.contains("*")
					&& !lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Query)) {
				lsaReward.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Reward);
				lsaReward.addSyntheticProperty(SyntheticPropertyName.QUERY, event.getQuery());
				logger.info("lsaReward " + lsaReward.toVisualString());
				logger.info(
						"send to -> " + lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
				sendTo(lsaReward, lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
			}

		}
	}

	public void postEvent(Event eventToPost) {
		this.lsa.removePropertiesByName("EVENT");
		try {
			this.lsa.addProperty(new Property("EVENT", eventToPost.clone()));
			eventDecay = SapereUtil.EVENT_INIT_DECAY;
		} catch (CloneNotSupportedException e) {
			logger.error(e);
		}
	}

	private void manageEvent() {
		if(eventDecay>0) {
			eventDecay-=1;
		} else {
			this.lsa.removePropertiesByName("EVENT");
			if(debugLevel>0) {
				logger.info("after removing event property : startEvent = " + startEvent);
			}
		}
	}

	private void cleanContract() {
		manageEvent();
		if(currentContract == null) {
			// Do nothing
		} else {
			if (currentContract.hasAllAgreements() && !currentContract.hasExpired()) {
			} else if (currentContract.hasExpired() ) {
				this.lsa.removePropertiesByName("CONTRACT");
				// Post expiration event
				if(expiryEvent==null && startEvent!=null) {
					try {
						expiryEvent = generateExpiryEvent();
						logger.info("Contract expiration : unset start event");
					} catch (Exception e) {
						logger.error(e);
					}
				}
			} else if (currentContract.hasDisagreement()) {
				if(stopDecay<=0 && true) {
					currentContract = null;
				} else {
					stopDecay--;
				}
			} else if(currentContract.validationHasExpired()) {
				// stop the contract is the validation has expired
				Lsa chosenLSA = tableChosenLsa.get(consumer);
				this.stopCurrentContract(chosenLSA, null);
			}
		}
		// Clean CONTRACT property
		if(!lsa.getPropertiesByName("CONTRACT").isEmpty()) {
			if(cContractDecay<=0 && true) {
				lsa.removePropertiesByName("CONTRACT");
			} else {
				cContractDecay--;
			}
		}
	}

	public void incrNbBonds() {
		this.nbBonds += 1;
	}

	public boolean inTimeout() {
		return nbBonds >= 20;
	}

	public String getConsumer() {
		return consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	public int getConsumerId() {
		return consumerId;
	}

	public boolean hasProducer() {
		return currentContract != null && currentContract.getProducerAgents().size() > 0;
	}

	public Set<String> getProducers() {
		if (currentContract == null) {
			return new HashSet<String>();
		}
		return currentContract.getProducerAgents();
	}

	public int getTimeLeftSec() {
		long currentMS = (new Date()).getTime();
		long timeLeftMS = consumerEndDate.getTime() - currentMS;
		if (currentMS > 0) {
			return (int) timeLeftMS / 1000;
		}
		return 0;
	}

	public boolean hasExpired() {
		Date current = new Date();
		return current.after(this.consumerEndDate);
		// return current.after(this.endDate);
	}

	public boolean isInSpace() {
		return isInSpace;
	}

	public void setInSpace(boolean isInSpace) {
		this.isInSpace = isInSpace;
	}

	public Date getConsumerEndDate() {
		return consumerEndDate;
	}

	public void setConsumerEndDate(Date consumerEndDate) {
		this.consumerEndDate = consumerEndDate;
	}

	public Event getStopEvent() {
		return stopEvent;
	}

}
