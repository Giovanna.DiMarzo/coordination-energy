package com.energy.agent;

import com.energy.model.Event;
import com.energy.model.RegulationWarning;
import com.energy.model.WarningType;

public interface IEnergyAgent {
	boolean hasExpired();
	void postEvent(Event eventToPost);
	Event generateStartEvent();
	Event generateUpdateEvent(WarningType warningType);
	Event generateExpiryEvent();
	Event generateStopEvent(RegulationWarning warning);
	Event getStopEvent();
}
