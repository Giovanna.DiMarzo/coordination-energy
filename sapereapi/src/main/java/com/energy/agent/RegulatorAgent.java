package com.energy.agent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;

import com.energy.markov.HomeMarkovStates;
import com.energy.markov.MarkovState;
import com.energy.model.AgentType;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.ExtendedEvent;
import com.energy.model.HomeTotal;
import com.energy.model.RegulationWarning;
import com.energy.model.RescheduleItem;
import com.energy.model.RescheduleTable;
import com.energy.model.WarningType;
import com.energy.util.EnergyDbHelper;
import com.energy.util.SapereLogger;
import com.sapereapi.model.PredictionData;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;

import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class RegulatorAgent extends SapereAgent {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	Random rand = new Random();
	private Date beginDate = null;
	private String ipSource = null;
	Timer timer = new Timer();
	public final static int REFRESH_PERIOD_SEC = 30;
	public final static int WARNING_DEFAULT_VALIDITY_SECONDS = 30;
	public final static int INTERRUPTION_VALIDITY_SECONDS = 2;
	public final static int CHANGE_REQUEST_VALIDITY_SECONDS = 3;
	public final static int NOT_IN_SPACE_VALIDITY_SECONDS = 5;
	public final static int NOT_CONTRACT_AGENT_VALIDITY_SECONDS = 5;
	public final static float MAX_CONSUMPTION = HomeMarkovStates.MAX_TOTAL_POWER;
	public final static float MAX_PRODUCTION = HomeMarkovStates.MAX_TOTAL_POWER;
	private Map<String,RegulationWarning> warningsNotInSpace = new HashMap<String,RegulationWarning>();
	private Map<String,RegulationWarning> warningsNoContractAgent = new HashMap<String,RegulationWarning>();
	private List<Property> propertiesToPost = new ArrayList<Property>();
	private static SapereLogger logger = SapereLogger.getInstance();
	private int logSubScriptionDecay = 0;
	private PredictionData lastPrediction = null;
	private boolean activateReschedule = true;

	private static final Comparator<EnergySupply> supplyComparator = new Comparator<EnergySupply>() {
		public int compare(EnergySupply supply1, EnergySupply supply2) {
			int comaprePower = supply1.comparePower(supply2);
			if (comaprePower == 0) {
				return supply1.comparTimeLeft(supply2);
			} else {
				return comaprePower;
			}
		}
	};

	public RegulatorAgent(String _agentName,  AgentAuthentication _authentication) {
		super(_agentName, _authentication, new String[] {"PRED"}, new String[] { "WARNING", "RESCHEDULE" }, LsaType.Service);
		setEpsilon(0); // No greedy policy
		super.setInput(new String[] {"PRED"});
		super.setOutput(new String[] { "WARNING", "RESCHEDULE" });
		this.agentName = _agentName;
		setUrl(_authentication.getAgentLocation());

		//homeTotal = new HomeTotal();
		this.beginDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.beginDate);
		logger.info("RegulatorAgent : lsa = " + lsa.toVisualString());
		// EnergyDbHelper.cleanHistoryDB();
		this.addDecay(REFRESH_PERIOD_SEC);
		debugLevel = 0;
		warningsNotInSpace = new HashMap<String,RegulationWarning>();
		warningsNoContractAgent = new HashMap<String,RegulationWarning>();
		// forcedCurrentTime = (ALL_TIME_WINDOWS.get(18)).getStartDate();

	}

	@Override
	public void setInitialLSA() {
		this.submitOperation();
	}

	@Override
	public void onBondNotification(BondEvent event) {
		try {
			Lsa bondedLsa = event.getBondedLsa();
			String query = bondedLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString();
			// lastQuery = query;
			if(debugLevel>0) {
				logger.info("** RegulatorAgent bonding ** " + agentName + " Q: " + query);
			}
			lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Service); // check
			this.addBondedLSA(bondedLsa);

			if (lsa.hasBondedBefore(bondedLsa.getAgentName(), query)) {
				logger.info("** " + bondedLsa.getAgentName() + " Already bound before query " + query);
			}

			int action = getActionToTake(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString()); // add
																													// greedy
			// lastEvent = null;
			// if (!this.hasBondedBefore(bondedLsa.getAgentName(), query)) {
			if (lsa.getSubDescription().size() >= 1) { // output
				Lsa chosenLSA = getBondedLsaByQuery(query).get(rand.nextInt(getBondedLsaByQuery(query).size()));
				String state = chosenLSA.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
				if (action == 0) {
					addState(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString(), action, 0, 0);
					lsa.addProperty(new Property(lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString(), null,
							query, chosenLSA.getAgentName(), state,
							chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(), false));
				} else if (action == 1) {
					ipSource = chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
					Property pPrediction = SapereUtil.getOnePropertyFromLsa(chosenLSA, "PRED");
					if (pPrediction != null && pPrediction.getValue() instanceof PredictionData) {
						PredictionData prediction = (PredictionData) pPrediction.getValue();
						if(prediction.hasLastResult("produced")) {
							AgentType bondAgentType = AgentType.getFromLSA(bondedLsa);
							if (AgentType.LEARNING_AGENT.equals(bondAgentType)) {
								if(false || lastPrediction==null || SapereUtil.shiftDateMinutes(lastPrediction.getInitialDate(),0).before(prediction.getInitialDate())) {
									lastPrediction = prediction;
									int stateNb = HomeMarkovStates.STATES_LIST.size();
									//List<Double> producedPrediction = prediction.getResult().get("produced");
									MarkovState randomState = prediction.getLastRandomTargetState("produced");
									if(randomState!=null && randomState.getId() == stateNb) {
										// Over production
										logger.info("Prediction of overProduction");
										activateReschedule = false;
										RescheduleTable rescheduleTable = getRescheduleTable();
										float rescheduledPower = rescheduleTable.computeRescheduledPower(prediction.getLastTargetDate(), WarningType.OVER_PRODUCTION_FORCAST);
										if(activateReschedule && rescheduledPower < 0.2*HomeMarkovStates.MAX_TOTAL_POWER) {
											//float horizonInMin = prediction.getTimeHorizonMinutes();
											Date stopBegin = SapereUtil.shiftDateMinutes(prediction.getLastTargetDate(), -10);
											Date stopEnd = SapereUtil.shiftDateMinutes(prediction.getLastTargetDate(), 50);
											List<EnergySupply> listSupplies = retrieveListSupplies(stopBegin, stopEnd);
											if (listSupplies.size() > 0) {
												Collections.sort(listSupplies, supplyComparator);
												int supplyIdx = 0;
												while (rescheduledPower < 0.2*HomeMarkovStates.MAX_TOTAL_POWER && supplyIdx < listSupplies.size()) {
													EnergySupply nextSupply = listSupplies.get(supplyIdx);
													String producer = nextSupply.getIssuer();
													RescheduleItem recheduleItem = new RescheduleItem(producer, WarningType.OVER_PRODUCTION_FORCAST, stopBegin, stopEnd, nextSupply.getPower());
													rescheduleTable.addItem(producer, recheduleItem);
													rescheduledPower = rescheduleTable.computeRescheduledPower(prediction.getLastTargetDate(), WarningType.OVER_PRODUCTION_FORCAST);
													supplyIdx++;
												}
												// Update rescheduleTable in LSA
												lsa.removePropertiesByName("RESCHEDULE");
												lsa.addProperty(new Property("RESCHEDULE", rescheduleTable));
											}
										}
									}
								}
							}
						}
					}
				}
				this.removeBondedLsasOfQuery(query);
			}
		} catch (Throwable t) {
			logger.error(t);
		}
	}

	private List<EnergySupply> retrieveListConsumption() {
		List<EnergySupply> result = new ArrayList<EnergySupply>();
		for (ExtendedEvent nextEvent : EnergyDbHelper.retrieveCurrentSessionEvents()) {
			if (EventType.CONTRACT.equals(nextEvent.getType()) ||  EventType.CONTRACT_UPDATE.equals(nextEvent.getType())) {
				EnergySupply supply = new EnergySupply(nextEvent.getLinkedConsumer(), nextEvent.getLinkedConsumerLocation()
						, nextEvent.getPower(),nextEvent.getBeginDate(), nextEvent.getEndDate(), nextEvent.getDeviceName(), nextEvent.getDeviceCategory());
				result.add(supply);
			}
		}
		return result;
	}

	private List<EnergySupply> retrieveListSupplies() {
		List<EnergySupply> result = new ArrayList<EnergySupply>();
		for (Event nextEvent : EnergyDbHelper.retrieveCurrentSessionEvents()) {
			if (EventType.PRODUCTION.equals(nextEvent.getType()) || EventType.PRODUCTION_UPDATE.equals(nextEvent.getType()) ) {
				EnergySupply supply = new EnergySupply(nextEvent.getIssuer(), nextEvent.getIssuerLocation(), nextEvent.getPower(),
						nextEvent.getBeginDate(), nextEvent.getEndDate(), nextEvent.getDeviceName(), nextEvent.getDeviceCategory());
				result.add(supply);
			}
		}
		return result;
	}
	private List<EnergySupply> retrieveListSupplies(Date stopBegin, Date stopEnd) {
		List<EnergySupply> allSupplies = retrieveListSupplies();
		List<EnergySupply> listsupplies = new ArrayList<EnergySupply>();
		for(EnergySupply nextSupply : allSupplies) {
			if(		nextSupply.getEndDate().after(stopBegin)
				 && nextSupply.getBeginDate().before(stopBegin)) {
				listsupplies.add(nextSupply);
			}
		}
		return listsupplies;
	}

	private boolean checkOverConsumption(HomeTotal homeTotal) {
		boolean overConsumption = false;
		boolean addWarning = false;
		if (homeTotal != null) {
			if (debugLevel > 0) {
				logger.info("consumed = " + homeTotal.getConsumed());
			}
			if (getMapWarnings(WarningType.OVER_CONSUMPTION).size() > 0) {
				return addWarning;
			}
			overConsumption = (homeTotal.getConsumed() > MAX_CONSUMPTION);
			if (overConsumption) {
				List<EnergySupply> consumptionList = retrieveListConsumption();
				if (consumptionList.size() > 0) {
					Collections.sort(consumptionList, Collections.reverseOrder(supplyComparator));
					RegulationWarning warning = new RegulationWarning(WarningType.OVER_CONSUMPTION,
							homeTotal.getDate());
					warning.setDeadlines(SapereUtil.shiftDateSec(new Date(), WARNING_DEFAULT_VALIDITY_SECONDS));
					float consumptionToWarn = 0;
					int consumptionIdx = 0;
					while (homeTotal.getConsumed() - consumptionToWarn > MAX_CONSUMPTION) {
						EnergySupply nextConsumption = consumptionList.get(consumptionIdx);
						String consumer = nextConsumption.getIssuer();
						warning.addAgent(consumer);
						consumptionToWarn = consumptionToWarn + nextConsumption.getPower();
					}
					for (String consumer : warning.getDestinationAgents()) {
						addProperty(new Property("WARNING", warning, consumer, consumer, "", ipSource, false));
						addWarning = true;
					}
				}
			}
		}
		return addWarning;
	}

	private boolean checkOverProduction(HomeTotal homeTotal) {
		boolean addWarning = false;
		if (homeTotal != null) {
			if (debugLevel > 0) {
				logger.info("produced = " + homeTotal.getProduced());
			}
			if(getMapWarnings().size()>0) {
				logger.info("checkOverProduction For debug : getMapWarnings().size() = " + getMapWarnings().size());
			}
			if (getMapWarnings(WarningType.OVER_PRODUCTION).size() > 0) {
				return addWarning;
			}
			boolean overProduction = (homeTotal.getProduced() > MAX_PRODUCTION);
			if (overProduction) {
				List<EnergySupply> allSupplies = retrieveListSupplies();
				if (allSupplies.size() > 0) {
					Collections.sort(allSupplies, Collections.reverseOrder(supplyComparator));
					RegulationWarning warning = new RegulationWarning(WarningType.OVER_PRODUCTION, homeTotal.getDate());
					warning.setDeadlines(SapereUtil.shiftDateSec(new Date(), WARNING_DEFAULT_VALIDITY_SECONDS));
					float productionToWarn = 0;
					int supplyIdx = 0;
					while (homeTotal.getProduced() - productionToWarn > MAX_PRODUCTION) {
						EnergySupply nextSupply = allSupplies.get(supplyIdx);
						String producer = nextSupply.getIssuer();
						warning.addAgent(producer);
						productionToWarn = productionToWarn + nextSupply.getPower();
					}
					for (String producer : warning.getDestinationAgents()) {
						addProperty(new Property("WARNING", warning, producer, producer, "", ipSource, false));
						addWarning = true;
					}
				}
			}
		}
		return addWarning;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		try {
			Lsa decayedLsa = event.getLsa();
			// logger.info("onDecayedNotification: decayedLsa = " +
			// decayedLsa.toVisualString());
			Integer decay = new Integer("" + decayedLsa.getSyntheticProperty(SyntheticPropertyName.DECAY));
			if (decay < 1) {
				this.addDecay(REFRESH_PERIOD_SEC);
			}
			// lsa.removePropertiesByName("WARNING");
			cleanExpiredDate();
			HomeTotal homeTotal = EnergyDbHelper.retrieveLastHomeTotal();
			if(!Sapere.isDisableRegulation()) {
				checkOverConsumption(homeTotal);
				checkOverProduction(homeTotal);
			}
			warningsNotInSpace = checkupNotInSpace(warningsNotInSpace);
			warningsNoContractAgent = checkupNoContractAgent(warningsNoContractAgent);
			// Post not sent properties
			sendProperties();
			// log notifier subscriptions
			if(logSubScriptionDecay<=0) {
				int nbSubscriptions = NodeManager.instance().getNotifier().getNbSubscriptions();
				//Map<String, Integer> subsciptionsByAgent = NodeManager.instance().getNotifier().getNbSubscriptionsByAgent();
				logger.info(this.agentName + " total subscriptions : " + nbSubscriptions);
							//+ " \n, subsciptions by agent : " +  subsciptionsByAgent);
				logSubScriptionDecay = 30;
			} else {
				logSubScriptionDecay--;
			}
		} catch (Throwable e) {
			logger.error(e);
			logger.error("Exception throw in onDecayedNotification :" + agentName + " " + event + " "
					+ e.getLocalizedMessage());

		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		logger.info("onLsaUpdatedEvent:" + agentName);
	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

	Map<String, RegulationWarning> getMapWarnings() {
		return getMapWarnings(null);
	}

	Map<String, RegulationWarning> getMapWarnings(WarningType typeFilter) {
		Map<String, RegulationWarning> result = new HashMap<String, RegulationWarning>();
		for (Property property : lsa.getProperties()) {
			if (property.getValue() instanceof RegulationWarning) {
				RegulationWarning warning = (RegulationWarning) property.getValue();
				if(typeFilter==null || typeFilter.equals(warning.getType())) {
					result.put(property.getQuery(), (RegulationWarning) property.getValue());
				}
			}
		}
		return result;
	}

	private String getExpiredWarningKey(Map<String, RegulationWarning> mapWarnings) {
		for (String agentName : mapWarnings.keySet()) {
			RegulationWarning warning = mapWarnings.get(agentName);
			if (warning.hasReceptionExpired()) {
				return agentName;
			}
		}
		return null;
	}

	private boolean hasAlreadyWarningType (String agentName, WarningType warningType) {
		List<Property> props = lsa.getPropertiesByQueryAndName(agentName, "WARNING");
		for(Property nextProp : props) {
			if(nextProp.getValue() instanceof RegulationWarning) {
				RegulationWarning warning = (RegulationWarning) nextProp.getValue();
				if(warningType.equals(warning.getType()) && warning.hasAgent(agentName)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isAlreadyInterruupted(String agentName) {
		return hasAlreadyWarningType(agentName, WarningType.USER_INTERRUPTION);
	}

	private boolean hasAlreadyChangeRequest(String agentName) {
		return hasAlreadyWarningType(agentName, WarningType.CHANGE_REQUEST);
	}

	public void interruptAgent(String agentName) {
		if(!isAlreadyInterruupted(agentName)) {
			RegulationWarning warning = new RegulationWarning(WarningType.USER_INTERRUPTION, new Date());
			warning.setDeadlines(SapereUtil.shiftDateSec(new Date(), INTERRUPTION_VALIDITY_SECONDS));
			warning.addAgent(agentName);
			addProperty(new Property("WARNING", warning, agentName, agentName, "", ipSource, false));
		}
	}

	public void modifyAgent(String agentName, EnergySupply changeRequest) {
		if(hasAlreadyChangeRequest(agentName)) {
			logger.info("modifyAgent : already change request on " + agentName);
		} else if(Sapere.getInstance().isAgentStopped(agentName)) {
			logger.info("modifyAgent : agent " + agentName + " is stopped");
		} else {
			RegulationWarning warning = new RegulationWarning(WarningType.CHANGE_REQUEST, new Date());
			changeRequest.checkBeginNotPassed();
			warning.setChangeRequest(changeRequest);
			warning.setDeadlines(SapereUtil.shiftDateSec(new Date(), CHANGE_REQUEST_VALIDITY_SECONDS));
			warning.addAgent(agentName);
			addProperty(new Property("WARNING", warning, agentName, agentName, "", ipSource, false));
		}
	}

	public Map<String,RegulationWarning> checkupNotInSpace(Map<String,RegulationWarning> lastWarnings) {
		Map<String,RegulationWarning>  result = new HashMap<String, RegulationWarning>();
		List<String> agentNotInSpace = Sapere.getInstance().checkupNotInSpace();
		for(String agentName : agentNotInSpace) {
			if(lastWarnings.containsKey(agentName)) {
				RegulationWarning notInSapce = lastWarnings.get(agentName);
				logger.warning(this.agentName + " checkupNotInSpace agent not in space " + notInSapce );
				if(notInSapce.hasWaitingExpired()) {
					logger.warning(this.agentName + " checkupNotInSpace : stop agent " + agentName);
					Sapere.getInstance().stopAgent(agentName, notInSapce);
					//Sapere.getInstance().generateStopEvent(agentName, notInSapce);
					String consumerAgent = Sapere.getInstance().getConsumerAgentName(agentName);
					if(consumerAgent!=null) {
						// Stop the linked consumer agent
						if(Sapere.getInstance().isInSpace(consumerAgent)) {
							// Send an interruption to agent LSA
							interruptAgent(consumerAgent);
						} else {
							RegulationWarning noContractWarning = new RegulationWarning(WarningType.NO_CONTRACT_AGENT, new Date());
							Sapere.getInstance().stopAgent(consumerAgent, noContractWarning);
							Sapere.getInstance().generateStopEvent(agentName, notInSapce);
						}
					}
				} else {
					result.put(agentName, notInSapce);
				}
			} else {
				RegulationWarning notInSapce = new RegulationWarning(WarningType.NOT_IN_SPACE, new Date());
				notInSapce.addAgent(agentName);
				notInSapce.setDeadlines(SapereUtil.shiftDateSec(new Date(), RegulatorAgent.NOT_IN_SPACE_VALIDITY_SECONDS));
				result.put(agentName, notInSapce);
			}
		}
		return result;
	}

	public Map<String,RegulationWarning> checkupNoContractAgent(Map<String,RegulationWarning> lastWarnings) {
		Map<String,RegulationWarning>  result = new HashMap<String, RegulationWarning>();
		List<String> agentInWarning = Sapere.getInstance().checkupNoContractAgent();
		for(String agentName : agentInWarning) {
			if(lastWarnings.containsKey(agentName)) {
				RegulationWarning noContractWarning = lastWarnings.get(agentName);
				logger.warning(this.agentName + " checkupNoContractAgent consumer with not contract " + noContractWarning );
				if(noContractWarning.hasWaitingExpired()) {
					logger.warning(this.agentName + " checkupNoContractAgent : stop agent " + agentName);
					Sapere.getInstance().stopAgent(agentName, noContractWarning);
					//Sapere.getInstance().generateStopEvent(agentName, noContractWarning);
				} else {
					result.put(agentName, noContractWarning);
				}
			} else {
				RegulationWarning noContractWarning = new RegulationWarning(WarningType.NO_CONTRACT_AGENT, new Date());
				noContractWarning.addAgent(agentName);
				noContractWarning.setDeadlines(SapereUtil.shiftDateSec(new Date(), RegulatorAgent.NOT_CONTRACT_AGENT_VALIDITY_SECONDS));
				result.put(agentName, noContractWarning);
			}
		}
		return result;
	}
	private void cleanExpiredDate() {
		Map<String, RegulationWarning> mapWarnings = getMapWarnings();
		String warningKey = null;
		while ((warningKey = getExpiredWarningKey(mapWarnings)) != null) {
			// SingleOffer offer = waitingOffers.get(offerKey);
			lsa.removePropertiesByQueryAndName(warningKey, "WARNING");
			// Refersh warning map
			mapWarnings = getMapWarnings();
			// offer.setDeadline(SapereUtil.shiftDateMinutes(new Date(), 1));
		}
		RescheduleTable rescheduleTable = getRescheduleTable();
		rescheduleTable.cleanExpiredDate();
	}

	private void addProperty(Property propertyToAdd) {
		if(lsa.getProperties().size() >= Lsa.PROPERTIESSIZE) {
			propertiesToPost.add(propertyToAdd);
			logger.info(this.agentName + " addProperty : cannot post propertiesToPost " + propertyToAdd.getValue() + " in lsa. Added in queue.");
		} else {
			lsa.addProperty(propertyToAdd);
		}
	}

	private void sendProperties() {
		while(this.propertiesToPost.size()>0 && lsa.getProperties().size() < Lsa.PROPERTIESSIZE-1) {
			Property prop = propertiesToPost.remove(0);
			logger.info( this.agentName + " : post not sent property " + prop);
			lsa.addProperty(prop);
		}
	}

	private RescheduleTable getRescheduleTable() {
		Property pRescheduleTable = SapereUtil.getOnePropertyFromLsa(lsa, "RESCHEDULE");
		if(pRescheduleTable!=null && pRescheduleTable.getValue() instanceof RescheduleTable) {
			return (RescheduleTable) pRescheduleTable.getValue();
		}
		return new RescheduleTable();
	}
}
