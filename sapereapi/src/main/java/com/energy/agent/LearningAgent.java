package com.energy.agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.energy.markov.HomeMarkovStates;
import com.energy.markov.HomeMarkovTransitions;
import com.energy.markov.HomeTransitionMatrices;
import com.energy.markov.MarkovState;
import com.energy.markov.MarkovTimeWindow;
import com.energy.model.AgentType;
import com.energy.model.Contract;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.HomeTotal;
import com.energy.model.ProtectedContract;
import com.energy.util.EnergyDbHelper;
import com.energy.util.SapereLogger;
import com.sapereapi.api.PredictionStep;
import com.sapereapi.model.PredictionData;
import com.sapereapi.model.PredictionResult;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;

import Jama.Matrix;
import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class LearningAgent extends SapereAgent {
	private static final long serialVersionUID = 1L;
	public final static int REFRESH_PERIOD_SEC = 60;
	private final static int LEARNING_TIME_STEP_MINUTES = 1;
	//public final static int PREDICTION_HORIZON_MINUTES = 60;
	public final static int[] LIST_PREDICTION_HORIZON_MINUTES = {5,10,30,60};
	public final static Set<Integer> DAYS_OF_WEEK = new  HashSet<>(Arrays.asList(Calendar.MONDAY, Calendar.TUESDAY,Calendar.WEDNESDAY,Calendar.THURSDAY, Calendar.FRIDAY,Calendar.SATURDAY,Calendar.SUNDAY));
	public final static List<MarkovTimeWindow> ALL_TIME_WINDOWS = EnergyDbHelper.retrieveTimeWindows();
	private Random rand = new Random();
	private List<String> querys = new ArrayList<String>();
	private Date beginDate = null;
	private Date registerDate = null;
	private List<String> eventsHistory = null;
	private Map<String, Event> eventTable = null;
	private int stepNumber = 1;
	// Home data
	private HomeTotal homeTotal = null;
	private HomeMarkovTransitions homeMarkovTransitions = null;
	private HomeTransitionMatrices homeTransitionMatrices = null;
	// Neighborhood data
	private Map<String, HomeTotal> mapNeighborhoodTotal = null;
	private Map<String, HomeMarkovTransitions> mapNeighborhoodMakovTransitions = null;
	private Map<String, HomeTransitionMatrices> mapNeighborhoodTransitionMatrices = null;

	private String variables[] = {"requested", "produced", "consumed", "provided", "available", "missing"};
	private PredictionStep currentTimeSlot = null;
	private Date forcedCurrentTime = null;
	private int learningWindow = 100;
	private String scenario = "";
	private static SapereLogger logger = SapereLogger.getInstance();
	private Integer forcedHourOfDay = null;

	public LearningAgent(String _agentName,  AgentAuthentication _authentication, String _scenario, Integer _forcedHourOfDay) {
		super(_agentName, _authentication, new String[] { "EVENT" }, new String[] { "PRED" }, LsaType.Service);
		setEpsilon(0); // No greedy policy
		super.setInput(new String[] { "EVENT" });
		super.setOutput(new String[] { "PRED" });
		this.agentName = _agentName;
		this.scenario = _scenario;
		this.forcedHourOfDay = _forcedHourOfDay;
		setUrl(_authentication.getAgentLocation());
		this.homeTotal = new HomeTotal();
		this.mapNeighborhoodTotal = new HashMap<String, HomeTotal>();
		this.beginDate = new Date();
		this.eventTable = new HashMap<String, Event>();
		this.eventsHistory = new ArrayList<String>();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.beginDate);
		logger.info(this.agentName + " lsa = " + lsa.toVisualString());
		EnergyDbHelper.cleanHistoryDB();
		this.addDecay(REFRESH_PERIOD_SEC);
		debugLevel = 0;
		registerDate = new Date();
		homeMarkovTransitions = new HomeMarkovTransitions(variables, NodeManager.getLocation(), scenario, registerDate);
		mapNeighborhoodMakovTransitions  = new HashMap<String, HomeMarkovTransitions>();
		//forcedCurrentTime = (ALL_TIME_WINDOWS.get(18)).getStartDate();
		if(forcedHourOfDay!=null) {
			forcedCurrentTime = SapereUtil.getNewDate(forcedHourOfDay);
		}
		Date markovDate = forcedCurrentTime==null? registerDate : forcedCurrentTime;
		try {
			currentTimeSlot = getTimeSlot(markovDate);
		} catch (Exception e) {
			logger.error(e);
		}
		if(currentTimeSlot!=null) {
			homeTransitionMatrices = loadHomeTransitionMatrices(NodeManager.getLocation(), markovDate);
			mapNeighborhoodTransitionMatrices = new HashMap<String,HomeTransitionMatrices>();
		}
		stepNumber = 1;
		this.periodicRefresh();
	}

	@Override
	public void setInitialLSA() {
		this.submitOperation();
	}

	public String[] getVariables() {
		return variables;
	}

	public String getScenario() {
		return scenario;
	}
/*
	private static String[] getNeighbors() {
		return NodeManager.instance().getNetworkDeliveryManager().getNeighbours();
	}
*/

	@Override
	public void onBondNotification(BondEvent event) {
		try {
			Lsa bondedLsa = event.getBondedLsa();
			String query = bondedLsa.getSyntheticProperty(SyntheticPropertyName.QUERY).toString();
			// lastQuery = query;
			if(debugLevel>0) {
				logger.info("** LearningAgent bonding ** " + agentName + " Q: " + query);
			}
			lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Service); // check
			this.addBondedLSA(bondedLsa);

			if (lsa.hasBondedBefore(bondedLsa.getAgentName(), query)) {
				logger.info("** " + bondedLsa.getAgentName() + " Already bound before query " + query);
			}

			int action = getActionToTake(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString()); // add greedy
			Event nextEvent = null;
			if (lsa.getSubDescription().size() >= 1) { // output
				Lsa chosenLSA = getBondedLsaByQuery(query).get(rand.nextInt(getBondedLsaByQuery(query).size()));
				String state = chosenLSA.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
				if (action == 0) {
					addState(bondedLsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString(), action, 0, 0);
					lsa.addProperty(new Property(lsa.getSyntheticProperty(SyntheticPropertyName.OUTPUT).toString(), null,
							query, chosenLSA.getAgentName(), state,
							chosenLSA.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(), false));
				} else if (action == 1) {
					nextEvent = null;
					// Agent linked to the Bonding LSA
					Property pEvent = SapereUtil.getOnePropertyFromLsa(chosenLSA, "EVENT");
					if (pEvent != null && pEvent.getValue() instanceof Event) {
						nextEvent = (Event) pEvent.getValue();
						if(EventType.CONTRACT_UPDATE.equals(nextEvent.getType())) {
							logger.info(this.agentName + " Contract update");
						}
						AgentType bondAgentType = AgentType.getFromLSA(bondedLsa);
						if (AgentType.CONSUMER.equals(bondAgentType) || AgentType.PRODUCER.equals(bondAgentType) || AgentType.CONTRACT.equals(bondAgentType)) {
							String nextEventKey = nextEvent.getKey();
							if (!eventsHistory.contains(nextEventKey)) {
								eventsHistory.add(nextEventKey);
								// nextEvent = EnergyDbHelper.registerEvent(nextEvent);
								eventTable.put(nextEvent.getIssuer(), nextEvent);
								// Check if the event is local
								String evtLocation = nextEvent.getIssuerLocation();
								if(!NodeManager.getLocation().equals(evtLocation) && true) {
									Contract contract = null;
									if(AgentType.CONTRACT.equals(bondAgentType)) {
										// Get contract from contrat agent
										Property pContract = SapereUtil.getOnePropertyFromLsa(chosenLSA, "CONTRACT");
										logger.info("For debug : register distant event " + nextEvent);
										if(pContract.getValue() instanceof ProtectedContract) {
											ProtectedContract protectedContrat = (ProtectedContract) pContract.getValue();
											contract = protectedContrat.getContract(this);
										}
									}
									nextEvent.setIssuerDistance(Sapere.getInstance().getDistance(evtLocation));
									EnergyDbHelper.registerEvent2(nextEvent, contract);
								}
								//lastEvent = nextEvent;
								refreshHomeTotal(nextEvent);
								if ((!"".equals(query)) && (!querys.contains(query))) {
									querys.add(query);
								}
								refreshLSA();
								//refreshMarkovChains(false);
							} else {
								if(debugLevel>0) {
									logger.info("Event already added " + nextEvent);
								}
							}
						}
					}
				}
				this.removeBondedLsasOfQuery(query);
			}
		} catch (Throwable t) {
			logger.error(t);
		}
	}

	private void refreshLSA() {
		//String sBonds = SapereUtil.implode(bond_agents, ",");
		try {
			String state = lsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
			state = SapereUtil.addOutputsToSate(state, new String[] {});
			//String sQueries = "";
			lsa.removePropertiesByName("TOTAL");
			lsa.addProperty(new Property("TOTAL", homeTotal));
			//logger.info("add properties on query " + sQueries + " and bond " + sBonds);
		} catch (Throwable e) {
			logger.error(e);
		}
	}

	/**
	 * Get next expired event key (for table cleaning)
	 * 
	 * @return
	 */
	private String getNextExpiredEventKey() {
		for (String key : eventTable.keySet()) {
			Event event = eventTable.get(key);
			if (event.hasExpired()) {
				return key;
			}
		}
		return null;
	}


	private void refreshHomeTotal(Event event) {
		registerDate = (event==null)? new Date() : event.getBeginDate();

		// Clean expired events
		String keyToRemove = getNextExpiredEventKey();
		while (keyToRemove != null) {
			eventTable.remove(keyToRemove);
			keyToRemove = getNextExpiredEventKey();
		}
		List<String> lsaAgents = new ArrayList<String>();
		for (Lsa lsa : NodeManager.instance().getSpace().getAllLsa().values()) {
			lsaAgents.add(lsa.getAgentName());
		}
		// Refresh Home total
		Long idLast = homeTotal==null? null : homeTotal.getId();
		homeTotal = EnergyDbHelper.generateHomeTotal(registerDate, idLast, event, getUrl(), agentName, NodeManager.getLocation());
		mapNeighborhoodTotal.clear();
		for(String neighborLocation : NodeManager.instance().getNetworkDeliveryManager().getNeighbours()) {
			HomeTotal neighborTotal = EnergyDbHelper.generateHomeTotal(registerDate, idLast, event, getUrl(), agentName, neighborLocation);
			if(neighborTotal.hasActivity()) {
				mapNeighborhoodTotal.put(neighborLocation, neighborTotal);
			}
		}

	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		try {
			Lsa decayedLsa = event.getLsa();
			// logger.info("onDecayedNotification: decayedLsa = " +
			// decayedLsa.toVisualString());
			Integer decay = new Integer("" + decayedLsa.getSyntheticProperty(SyntheticPropertyName.DECAY));
			if (decay < 1) {
				periodicRefresh();
				this.addDecay(REFRESH_PERIOD_SEC);
			}
		} catch (Throwable e) {
			logger.error(e);
			logger.info("Exception thrown in onDecayedNotification :" + agentName + " " + event + " " + e.getLocalizedMessage());

		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		logger.info("onLsaUpdatedEvent:" + agentName);
	}

	@Override
	public void onRewardEvent(RewardEvent event) {
		String previousAgent = "";
		String newState = "";
		for (Property prop : event.getLsa().getPropertiesByQuery(event.getQuery())) {
			if (prop.getChosen()) {
				previousAgent = prop.getBond();
				newState = prop.getState();
				break;
			}
		}
		logger.info("State to reward " + newState + " by " + event.getReward() + " - " + event.getMaxSt1());
		if (!newState.equals(""))
			addState(getPreviousState(newState, getOutput()), 1, event.getReward(), event.getMaxSt1());

		logger.info("reward previous service " + previousAgent);

		Lsa lsaReward = NodeManager.instance().getSpace().getLsa(previousAgent);
		if (lsaReward != null && lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Service)) {
			rewardLsa(lsaReward, event.getQuery(), event.getReward(),
					getBestActionQvalue(getPreviousState(newState, getOutput()))); // maxQSt1
			lsaReward.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
		}

		if (lsaReward != null) {
			if (previousAgent.contains("*")
					&& !lsaReward.getSyntheticProperty(SyntheticPropertyName.TYPE).equals(LsaType.Query)) {
				lsaReward.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Reward);
				lsaReward.addSyntheticProperty(SyntheticPropertyName.QUERY, event.getQuery());
				logger.info("lsaReward " + lsaReward.toVisualString());
				logger.info(
						"send to -> " + lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
				sendTo(lsaReward, lsaReward.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
			}

		}
	}


	public void refreshHistory(Event event) {
		refreshHomeTotal(event);
		refreshLSA();
	}

	public static MarkovTimeWindow getMarkovTimeWindow(Date aDate) {
		for(MarkovTimeWindow timeWindow : ALL_TIME_WINDOWS) {
			if(timeWindow.containsDate(aDate)) {
				return timeWindow;
			}
		}
		return null;
	}

	public static PredictionStep getTimeSlot(Date aDate) throws Exception {
		MarkovTimeWindow markovTimeWindow = getMarkovTimeWindow(aDate);
		if(markovTimeWindow!=null) {
			Date startDate = aDate;
			Date markovWindowEndDate = markovTimeWindow.getEndDate(aDate);
			if(startDate.after(markovWindowEndDate)) {
				throw new Exception("PredictionTimeSlot : startDate " + SapereUtil.format_date_time.format(startDate) + " is after end date " +  SapereUtil.format_date_time.format(markovWindowEndDate));
			}
			Date endDate = SapereUtil.shiftDateMinutes(aDate, LEARNING_TIME_STEP_MINUTES);
			if(endDate.after(markovWindowEndDate)) {
				endDate = markovWindowEndDate;
			}
			return new PredictionStep(markovTimeWindow, startDate, endDate);
		}
		return null;
	}


	public void periodicRefresh() {
		boolean generatePrediction = (stepNumber>1);
		boolean saveTransitionMatrix = (stepNumber>1);
		//EnergyDbHelper.correctHisto();
		refreshHistory(null);
		try {
			refreshMarkovChains(saveTransitionMatrix);
			if(generatePrediction) {
				//Date current = new Date();
			    logger.info("Before prediction");
				PredictionData prediction = computePrediction(LIST_PREDICTION_HORIZON_MINUTES, NodeManager.getLocation());
				logger.info("After prediction");
				lsa.removePropertiesByName("PRED");
				lsa.addProperty(new Property("PRED", prediction));
				EnergyDbHelper.savePredictionResult(prediction);
			}
		} catch (Throwable e) {
			logger.error(e);
		}
		stepNumber++;
		// Log memroy state
		int mb = 1024 * 1024; 
		logger.info("Total memory " +  Runtime.getRuntime().totalMemory()/ mb);
		logger.info("Free memory " +  Runtime.getRuntime().freeMemory()/ mb);
		logger.info("Free maxMemory " +  Runtime.getRuntime().maxMemory()/ mb);
	}

	private HomeTransitionMatrices loadHomeTransitionMatrices(String location, Date markovDate) {
		if(currentTimeSlot!=null) {
			EnergyDbHelper.refreshTransitionMatrixCell(location, scenario, learningWindow);
			return EnergyDbHelper.loadHomeTransitionMatrices(agentName, variables, location, scenario, currentTimeSlot.getMarkovTimeWindow(), markovDate);
		}
		return null;
	}

	public void refreshMarkovChains(boolean saveTransMatrix) throws Exception {
		// Refresh Markov state : only when no specific event (ie at periodic refresh)
		Date markovDate = registerDate;
		if(forcedHourOfDay!=null) {
			forcedCurrentTime = SapereUtil.getNewDate(forcedHourOfDay);
			markovDate = forcedCurrentTime;
		}
		//TransitionMatrixScope scope = homeCurrentMakovStates.getScope();
		homeMarkovTransitions.refreshTransitions(homeTotal);

		// Set states index in history
		if(forcedHourOfDay!=null) {
			HomeTotal homeTotal2 = homeTotal.clone();
			homeTotal2.setDate(forcedCurrentTime);
			EnergyDbHelper.setHistoryStates(homeTotal2, homeMarkovTransitions, scenario);
		} else {
			EnergyDbHelper.setHistoryStates(homeTotal, homeMarkovTransitions, scenario);
		}
		for(String neighborLocation : mapNeighborhoodTotal.keySet()) {
			HomeTotal neighborTotal = mapNeighborhoodTotal.get(neighborLocation);
			// Initialize a Markov transition it does not exists for this neighbor
			if(!mapNeighborhoodMakovTransitions.containsKey(neighborLocation)) {
				mapNeighborhoodMakovTransitions.put(
					neighborLocation
					, new HomeMarkovTransitions(variables,neighborLocation, scenario, registerDate));
			}
			HomeMarkovTransitions neighborTransition = mapNeighborhoodMakovTransitions.get(neighborLocation);
			neighborTransition.refreshTransitions(neighborTotal);
			if(neighborTotal.hasActivity() && neighborTransition.getLastState("requested")==null) {
				neighborTransition.initializeLast();
			}
			mapNeighborhoodMakovTransitions.put(neighborLocation, neighborTransition);
			// Initialize new Markov transition matrix it does not exists for this neighbor
			if(!mapNeighborhoodTransitionMatrices.containsKey(neighborLocation)) {
				HomeTransitionMatrices nextHomeTransitionMatrices = loadHomeTransitionMatrices(neighborLocation, markovDate);
				mapNeighborhoodTransitionMatrices.put(neighborLocation, nextHomeTransitionMatrices);
			}
		}
		if(currentTimeSlot!=null && markovDate.before(currentTimeSlot.getEndDate())) {
			// OK
		} else {
			// Time Slot change
			currentTimeSlot= getTimeSlot(markovDate);
		}
		// load of reload transition matrix if necessary
		if(currentTimeSlot==null || homeTransitionMatrices.getTimeWindowId() != currentTimeSlot.getMarovTimeWindowId()) {
			// load transition matrix or home
			currentTimeSlot= getTimeSlot(markovDate);
			homeTransitionMatrices = loadHomeTransitionMatrices(homeTotal.getLocation(), markovDate);
			for(String neighborLocation : mapNeighborhoodTotal.keySet()) {
				HomeTransitionMatrices nextHomeTransitionMatrices = loadHomeTransitionMatrices(neighborLocation, markovDate);
				mapNeighborhoodTransitionMatrices.put(neighborLocation, nextHomeTransitionMatrices);
			}
		}
		// Update home transition matrix
		boolean updateted = homeTransitionMatrices.updateMatrices(markovDate, this.homeMarkovTransitions);
		if(saveTransMatrix && updateted) {
			EnergyDbHelper.saveHomeTransitionMatrices(homeTransitionMatrices, learningWindow);
		}
		// Update neighbors transition matrix
		for(String neighborLocation : mapNeighborhoodMakovTransitions.keySet()) {
			HomeMarkovTransitions neighborTransitions = mapNeighborhoodMakovTransitions.get(neighborLocation);
			if(mapNeighborhoodTransitionMatrices.containsKey(neighborLocation)) {
				HomeTransitionMatrices neighborTransitionMatrices = mapNeighborhoodTransitionMatrices.get(neighborLocation);
				boolean updateted2 = neighborTransitionMatrices.updateMatrices(markovDate, neighborTransitions);
				if(saveTransMatrix && updateted2) {
					EnergyDbHelper.saveHomeTransitionMatrices(neighborTransitionMatrices, learningWindow);
				}
			}
		}
		if(debugLevel>0) {
			logger.info("State transition = " + homeMarkovTransitions);
			logger.info("homeTransitionMatrix = " + homeTransitionMatrices);
		}
	}

	public HomeTransitionMatrices getHomeTransitionMatrices() {
		return homeTransitionMatrices;
	}

	public List<PredictionStep> computePredictionSteps(Date initialDate, Date targetDate) throws Exception {
		List<PredictionStep> result = new ArrayList<PredictionStep>();
		//Date current = SapereUtil.getCurrentMinute();
		PredictionStep  nextTimeSlot = getTimeSlot(initialDate);
		result.add(nextTimeSlot);
		Date endDate = nextTimeSlot.getEndDate();
		while(endDate.before(targetDate)) {
			Date nextBeginDate = nextTimeSlot.getEndDate();
			//logger.info("nextBeginDate = " + SapereUtil.format_date_time.format(nextBeginDate));
			nextTimeSlot = getTimeSlot(nextBeginDate);
			if(nextTimeSlot==null) {
				throw new Exception("Time window not found for date " + nextBeginDate );
			}
			result.add(nextTimeSlot);
			endDate = nextTimeSlot.getEndDate();
		}
		return result;
	}

	private static Matrix getTransitionMatrix(Map<Integer, HomeTransitionMatrices> mapTransitionMatrices, int timeWindowId, String variable) {
		HomeTransitionMatrices nextTransitionMatrices = mapTransitionMatrices.get(timeWindowId);
		if(nextTransitionMatrices!=null) {
			return nextTransitionMatrices.getNormalizedMatrix(variable);
		}
		return null;
	}

	private static double getSum(Matrix aMatrix) {
		double result = 0;
		for(double d : aMatrix.getRowPackedCopy()) {
			result+=d;
		}
		return result;
	}

	public PredictionData computePrediction(int[] listHorizonMinutes, String location) {
		List<Date> targetDates = new ArrayList<Date>();
		Date current = SapereUtil.getNewDate(forcedHourOfDay);// new Date();
		for(Integer horizonMinute : listHorizonMinutes) {
			targetDates.add(SapereUtil.shiftDateMinutes(current, horizonMinute));
		}
		return computePrediction(current, targetDates, location);
	}

	public PredictionData computePrediction(Date initDate, List<Date> targetDates, String location) {
		//learningWindow = 15;
		boolean isLocal = NodeManager.getLocation().equals(location);
		PredictionData prediction = new PredictionData();
		prediction.setVariables(this.variables);
		prediction.setLocation(location);
		prediction.setScenario(scenario);
		prediction.setInitialDate(initDate);
		for(Date targetDate : targetDates) {
			prediction.addTargetDate(targetDate);
		}
		try {
			//resultForm.setScope(TransitionMatrixScope.HOME);
			prediction.setLearningWindow(learningWindow);
			Date lastTargetDate = prediction.getLastTargetDate();
			List<PredictionStep> listSteps = computePredictionSteps(prediction.getInitialDate(), lastTargetDate);
			prediction.setListTimeSlot(listSteps);
			if(isLocal) {
				prediction.setInitialContent(homeMarkovTransitions);
			} else {
				if(mapNeighborhoodMakovTransitions.containsKey(location)){
					HomeMarkovTransitions neighborMarkovTransitions = mapNeighborhoodMakovTransitions.get(location);
					prediction.setInitialContent(neighborMarkovTransitions);
				} else {
					prediction.addError("No initial state found at location : " + location);
				}
			}
			if(prediction.hasInitialContent()) {
				List<MarkovTimeWindow> listTimeWindows = new ArrayList<MarkovTimeWindow>();
				for(PredictionStep timeSlot : listSteps) {
					MarkovTimeWindow timeWindow = timeSlot.getMarkovTimeWindow();
					if(!listTimeWindows.contains(timeWindow)) {
						listTimeWindows.add(timeWindow);
					}
				}
				// Retrieve needed transtition matrices
				Map<Integer, HomeTransitionMatrices> mapTransitionMatrices =
						EnergyDbHelper.loadListHomeTransitionMatrice(this.agentName, this.variables, location, this.scenario, listTimeWindows, initDate);
				for(String variable : this.variables) {
					prediction = this.computeVariablePrediction(mapTransitionMatrices, prediction, variable);
				}
				// Generate random prediction
				prediction.generateRandomState();
			}
		} catch (Exception e) {
			logger.error(e);
			prediction.addError(e.getMessage());
		}
		return prediction;
	}

	public boolean checkTransitionMatrixCompletion(Matrix predictionRow , Matrix transitionMatrix, String variable , String location, MarkovTimeWindow timeWindow) throws Exception {
		//int idx=0;
		int nbCol = predictionRow.getColumnDimension();
		double[] predictionArray = predictionRow.getRowPackedCopy();
		for(int rowIdx= 0;  rowIdx < predictionArray.length; rowIdx++) {
			double probItem = predictionArray[rowIdx];
			if(probItem > 0) {
				// check the sum at the corresponding transition matrix row
				double matrixSum = getSum(transitionMatrix.getMatrix(rowIdx, rowIdx, 0, nbCol-1));
				String sRow = "S"+(rowIdx+1);
				String variable2 = "\"" + variable + "\"";
				if(matrixSum==0) {
					throw new Exception("Cannot compute prediction of " + variable2 + " : Transition matrix row " + sRow + " is empty for variable " + variable2 + " and timeWindow " + timeWindow + " and location \"" + location + "\"");
				}
				if(Math.abs(matrixSum - 1.0)>0.0001) {
					throw new Exception("Cannot compute prediction : Sum of transition matrix row " + sRow + " is not equals to 1 for variable  " + variable2 + " timeWindow " + timeWindow + " and location \"" + location + "\"");
				}
			}
		}
		return true;
	}

	public PredictionData computeVariablePrediction(Map<Integer, HomeTransitionMatrices> mapTransitionMatrices
			, PredictionData prediction
			, String variable
			) throws Exception {
		if(homeMarkovTransitions==null) {
			throw new Exception("No current state");
		}
		MarkovState initalState = homeMarkovTransitions.getState(variable);
		if(initalState==null) {
			throw new Exception("No current state for " + variable + " variable");
		}
		int stateIdx = HomeMarkovStates.STATES_LIST.indexOf(initalState);
		int nbStates = HomeMarkovStates.STATES_LIST.size();
		Matrix predictionRow = new Matrix(1, nbStates);
		predictionRow.set(0, stateIdx, 1);
		double checkSum = getSum(predictionRow);
		//logger.info("prediction testSum=" + checkSum);
		for(PredictionStep nextStep : prediction.getListTimeSlot()) {
			try {
				Matrix nextTransitionMatrix = getTransitionMatrix(mapTransitionMatrices, nextStep.getMarovTimeWindowId(), variable);
				if(nextTransitionMatrix==null) {
					throw new Exception("No transition matrix found for  " + variable + " at " + nextStep);
				} else {
					checkTransitionMatrixCompletion(predictionRow, nextTransitionMatrix, variable, prediction.getLocation(), nextStep.getMarkovTimeWindow());
					predictionRow = predictionRow.times(nextTransitionMatrix);
					checkSum = getSum(predictionRow);
					if(Math.abs(checkSum - 1.0)>0.0001) {
						throw new Exception("Prediction sum should be equals to 1 for variable " + variable + " , timewindow " + nextStep + "[sum:" + checkSum+"]");
					}
					//logger.error(e);("prediction testSum=" + checkSum);
				}
			} catch (Exception e) {
				logger.error(e);
				prediction.addError(e.getMessage());
				return prediction;
			}
			for(Date nextTargetDate : prediction.getTargetDatesWithoutResult(variable)) {
				if(nextStep.isInSlot(nextTargetDate)) {
					PredictionResult result = new PredictionResult(prediction.getInitialDate(), nextTargetDate, variable);
					result.setStateProbabilities(predictionRow);
					prediction.setResult(variable, nextTargetDate, result);
				}
			}
		}
		Date lastTargetDate = prediction.getLastTargetDate();
		if(!prediction.hasResult(variable, lastTargetDate)) {
			PredictionResult result = new PredictionResult(prediction.getInitialDate(), lastTargetDate, variable);
			result.setStateProbabilities(predictionRow);
			prediction.setResult(variable, lastTargetDate, result);
		}
		// For log
		boolean toLog = false;
		if(toLog) {
			for(Date nextTargetDate : prediction.getTargetDates()) {
				if(prediction.hasResult(variable, nextTargetDate)) {
					logger.info("computeVariablePrediction : " + variable + " : " + prediction.getResult(variable, nextTargetDate));
				}
			}
		}
		return prediction;
	}
}
