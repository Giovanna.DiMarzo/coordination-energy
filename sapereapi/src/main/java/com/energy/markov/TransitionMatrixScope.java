package com.energy.markov;

public enum TransitionMatrixScope {
	HOME("Home"), NEIGHBORHOOD("Neighborhood");

	private String label;

	TransitionMatrixScope(String _label) {
		this.label = _label;
	}

	public String getLabel() {
		return label;
	}

	public static TransitionMatrixScope getByLabel(String label) {
		String label2 = (label == null) ? "" : label;
		for (TransitionMatrixScope pLevel : TransitionMatrixScope.values()) {
			if (pLevel.getLabel().equals(label2)) {
				return pLevel;
			}
		}
		return null;
	}
}
