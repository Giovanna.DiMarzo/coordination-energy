package com.energy.markov;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.energy.agent.LearningAgent;
import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

import Jama.Matrix;

public class HomeTransitionMatrices {
	public final static String CR = System.getProperty("line.separator"); // Carriage return
	private String learningAgentName = null;
	protected String variables[] = {};
	protected MarkovTimeWindow timeWindow;
	protected List<MarkovState> statesList = HomeMarkovStates.STATES_LIST;
	protected Map<String, Matrix> mapIterObsMatrices = new HashMap<String, Matrix>();
	protected Map<String, Matrix> mapAllObsMatrices = new HashMap<String, Matrix>();
	protected Map<String, Matrix> mapNormalizedMatrices = new HashMap<String, Matrix>();
	protected Map<String, Integer> mapNbOfObservations = new HashMap<String, Integer>();
	protected Map<String, Integer> mapNbOfIterations = new HashMap<String, Integer>();
	protected Date computeDate;
	//protected TransitionMatrixScope scope;
	protected String location;
	protected String scenario;

	public static Matrix initTransitionMatrix() {
		int statesNb = HomeMarkovStates.STATES_LIST.size();
		Matrix result = new Matrix(statesNb, statesNb);
		return result;
	}

	public HomeTransitionMatrices(LearningAgent learningAgent, MarkovTimeWindow aTimeWindow, String _location/*, TransitionMatrixScope _scope*/) {
		super();
		this.learningAgentName = learningAgent.getAgentName();
		this.variables = learningAgent.getVariables();
		//this.scope = _scope;
		this.location = _location;
		this.scenario = learningAgent.getScenario();
		this.timeWindow = aTimeWindow;
		reset();
	}

	public HomeTransitionMatrices(String _learningAgentName, String[] _variables, String _location/*TransitionMatrixScope _scope*/, String _scenario, MarkovTimeWindow aTimeWindow) {
		super();
		this.learningAgentName = _learningAgentName;
		this.variables = _variables;
		this.timeWindow = aTimeWindow;
		//this.scope = _scope;
		this.location = _location;
		this.scenario = _scenario;
		reset();
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Map<String, Matrix> getMapIterObsMatrices() {
		return mapIterObsMatrices;
	}

	public Map<String, Matrix> getMapAllObsMatrices() {
		return mapAllObsMatrices;
	}

	public Map<String, Matrix> getMapNormalizedMatrices() {
		return mapNormalizedMatrices;
	}

	public Matrix getAllObsMatrice(String variable) {
		return mapAllObsMatrices.get(variable);
	}

	public Matrix getIterObsMatrice(String variable) {
		return mapIterObsMatrices.get(variable);
	}

	public Matrix getNormalizedMatrix(String variable) {
		return mapNormalizedMatrices.get(variable);
		/*
		 * Matrix matrix = getMatrice(variable); if (matrix != null) { return
		 * normalize(matrix); } return null;
		 */
	}

	public List<MarkovState> getStatesList() {
		return statesList;
	}

	public Map<String, Integer> getMapNbOfIterations() {
		return mapNbOfIterations;
	}

	public void setValue(String variable, int rowIdx, int columnIndx, double iterationObservatiionNb, double nbOfObservations) {
		if (mapIterObsMatrices.get(variable) != null) {
			Matrix iterObsMatrices = mapIterObsMatrices.get(variable);
			iterObsMatrices.set(rowIdx, columnIndx,  iterationObservatiionNb);
		}
		if (mapAllObsMatrices.get(variable) != null) {
			Matrix allObsMatrix = mapAllObsMatrices.get(variable);
			allObsMatrix.set(rowIdx, columnIndx, nbOfObservations);
			Matrix noramlizedMatrix = mapNormalizedMatrices.get(variable);
			refreshNormalizedMatrix(allObsMatrix, noramlizedMatrix, rowIdx);
		}
		if(mapNbOfObservations.get(variable) != null) {
			Integer sumNbOfObservations = mapNbOfObservations.get(variable);
			sumNbOfObservations = sumNbOfObservations + (new Double(nbOfObservations)).intValue();
			mapNbOfObservations.put(variable, sumNbOfObservations);
		}
	}

	public void reset() {
		mapAllObsMatrices.clear();
		mapIterObsMatrices.clear();
		mapNormalizedMatrices.clear();
		mapNbOfObservations.clear();
		mapNbOfIterations.clear();
		for (String variable : this.variables) {
			mapAllObsMatrices.put(variable, initTransitionMatrix());
			mapIterObsMatrices.put(variable, initTransitionMatrix());
			mapNormalizedMatrices.put(variable, initTransitionMatrix());
			mapNbOfObservations.put(variable, 0);
			mapNbOfIterations.put(variable, 0);
		}
	}

	public String[] getVariables() {
		return variables;
	}

	private boolean updateTransiationMatrix(Matrix iterObsMatrix, Matrix allObsMatrix, Matrix noramlizedMatrix, MarkovState lastSate,
			MarkovState currentSate) {
		boolean result = false;
		if (lastSate != null && currentSate != null) {
			int fromState = lastSate.getId();
			int toState = currentSate.getId();
			int rowIdx = fromState - 1;
			int columnIdx = toState - 1;
			if (rowIdx < iterObsMatrix.getRowDimension() && columnIdx < iterObsMatrix.getColumnDimension()) {
				iterObsMatrix.set(rowIdx, columnIdx, 1 + iterObsMatrix.get(rowIdx, columnIdx));
			}
			if (rowIdx < allObsMatrix.getRowDimension() && columnIdx < allObsMatrix.getColumnDimension()) {
				// Increment the number of transitions of state indx i to state index j
				allObsMatrix.set(rowIdx, columnIdx, 1 + allObsMatrix.get(rowIdx, columnIdx));
				refreshNormalizedMatrix(allObsMatrix, noramlizedMatrix, rowIdx);
				result = true;
			}
		}
		return result;
	}

	private void refreshNormalizedMatrix(Matrix allObsMatrix, Matrix noramlizedMatrix, int rowIdx) {
		double rowSum = getRowSum(allObsMatrix, rowIdx);
		// Refresh all item of row number rowItem
		for (int colIdx = 0; colIdx < noramlizedMatrix.getRowDimension(); colIdx++) {
			// To normalize : divide each item by row sum
			noramlizedMatrix.set(rowIdx, colIdx,
					rowSum == 0 ? 0 : allObsMatrix.get(rowIdx, colIdx) / rowSum);
		}
	}

	public boolean updateMatrices(Date registerDate, HomeMarkovTransitions transition) {
		boolean result = false;
		try {
			this.computeDate = registerDate;
			for (String variable : mapAllObsMatrices.keySet()) {
				if(transition.hasTransition(variable)) {
					MarkovState lastSate = transition.getLastState(variable);
					MarkovState currentState = transition.getCurrentState(variable);
					boolean result2 = updateTransiationMatrix(mapIterObsMatrices.get(variable), mapAllObsMatrices.get(variable), mapNormalizedMatrices.get(variable), lastSate, currentState);
					result = result || result2;
					int nbObs = mapNbOfObservations.get(variable);
					mapNbOfObservations.put(variable, 1+nbObs);
				}
			}
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		return result;
	}
/*
	public void updateMatrices(Date registerDate, HomeMarkovStates homeLastState, HomeMarkovStates homeCurrentSates) {
		try {
			this.computeDate = registerDate;
			for (String variable : mapAllObsMatrices.keySet()) {
				MarkovState lastSate = homeLastState.getState(variable);
				MarkovState currentState = homeCurrentSates.getState(variable);
				updateTransiationMatrix(mapIterObsMatrices.get(variable), mapAllObsMatrices.get(variable), mapNormalizedMatrices.get(variable), lastSate, currentState);
				int nbObs = mapNbOfObservations.get(variable);
				mapNbOfObservations.put(variable, 1+nbObs);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			SapereLogger.getInstance().error(e);
			//e.printStackTrace();
		}
	}
*/
	private static double getRowSum(Matrix aMatrix, int rowIdx) {
		if (aMatrix == null) {
			return 0;
		}
		double rowSum = 0;
		if (rowIdx < aMatrix.getRowDimension()) {
			for (int colIdx = 0; colIdx < aMatrix.getColumnDimension(); colIdx++) {
				rowSum += aMatrix.get(rowIdx, colIdx);
			}
		}
		return rowSum;
	}

	public double getRowSum(String variable, int rowIdx) {
		return getRowSum(getAllObsMatrice(variable), rowIdx);
	}

	public Matrix normalize(Matrix aMatrix) {
		Matrix result = new Matrix(aMatrix.getRowDimension(), aMatrix.getColumnDimension());
		for (int rowIdx = 0; rowIdx < aMatrix.getRowDimension(); rowIdx++) {
			// Compute row sum
			double rowSum = getRowSum(aMatrix, rowIdx);
			for (int colIdx = 0; colIdx < aMatrix.getRowDimension(); colIdx++) {
				// To normalize : divide each item by row sum
				result.set(rowIdx, colIdx, rowSum == 0 ? 0 : aMatrix.get(rowIdx, colIdx) / rowSum);
			}
		}
		return result;
	}

	public String matrix2str(Matrix aMatrix) {
		// Matrix aMatrixNorm = normalize(aMatrix);
		StringBuffer result = new StringBuffer();
		for (int rowIdx = 0; rowIdx < aMatrix.getRowDimension(); rowIdx++) {
			String sep = "";
			for (int colIdx = 0; colIdx < aMatrix.getColumnDimension(); colIdx++) {
				result.append(sep);
				result.append(SapereUtil.df.format(aMatrix.get(rowIdx, colIdx)));
				sep = " ";
			}
			result.append(CR);
		}
		return result.toString();
	}

	public MarkovTimeWindow getTimeWindow() {
		return timeWindow;
	}

	public Date getComputeDate() {
		return computeDate;
	}

	public void setComputeDate(Date computeDate) {
		this.computeDate = computeDate;
	}

	public int getTimeWindowId() {
		return this.timeWindow.getId();
	}

	public String getLearningAgentName() {
		return learningAgentName;
	}

	public Map<String, Integer> getMapNbOfObservations() {
		return mapNbOfObservations;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		if (computeDate != null) {
			result.append("time ").append(SapereUtil.format_time.format(this.computeDate));
			result.append(CR).append("cumulative obs number");
			for (String variable : mapAllObsMatrices.keySet()) {
				result.append(CR).append(variable).append(":").append(CR)
						.append(matrix2str(this.mapAllObsMatrices.get(variable)));
			}
			result.append(CR).append("normalized matrices");
			for (String variable : mapNormalizedMatrices.keySet()) {
				result.append(CR).append(variable).append(":").append(CR)
						.append(matrix2str(this.mapNormalizedMatrices.get(variable)));
			}
		}
		return result.toString();
	}
}
