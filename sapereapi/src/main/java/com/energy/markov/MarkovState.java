package com.energy.markov;

import java.text.DecimalFormat;

public class MarkovState implements Cloneable {
	private static DecimalFormat df = new DecimalFormat("# W");
	private Integer id = null;
	private BooleanOperator minCondition = null;
	private Float minValue = null;
	private BooleanOperator maxCondition = null;
	private Float maxValue = null;
	private String label = null;

	public MarkovState(Integer id, BooleanOperator minCondition, Float minValue, BooleanOperator maxCondition,
			Float maxValue) {
		super();
		this.id = id;
		this.minCondition = minCondition;
		this.minValue = minValue;
		this.maxCondition = maxCondition;
		this.maxValue = maxValue;
		this.label = "";
		boolean isEqualCond = BooleanOperator.EQUALS.equals(minCondition);
		if(this.minCondition !=null) {
			this.label = minCondition.getIntervalMarker(minValue, df);
		} else {
			this.label = "]-∞";
		}
		if(!isEqualCond) {
			this.label = this.label + "," ;
			if(this.maxCondition !=null) {
				if(this.label.length()>0) {
				}
				this.label = this.label + maxCondition.getIntervalMarker(maxValue, df);
			} else {
				this.label = this.label +  "+∞[";
			}
		}
	}

	public boolean containsValue(Float value) {
		boolean result = true;
		if (minCondition != null && minValue != null) {
			result = result && (minCondition.apply(value, minValue));
		}
		if (result && maxCondition != null && maxValue != null) {
			result = result && (maxCondition.apply(value, maxValue));
		}
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BooleanOperator getMinCondition() {
		return minCondition;
	}

	public void setMinCondition(BooleanOperator minCondition) {
		this.minCondition = minCondition;
	}

	public Float getMinValue() {
		return minValue;
	}

	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}

	public BooleanOperator getMaxCondition() {
		return maxCondition;
	}

	public void setMaxCondition(BooleanOperator maxCondition) {
		this.maxCondition = maxCondition;
	}

	public Float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public MarkovState clone() {
		MarkovState result = new MarkovState(id, minCondition, minValue, maxCondition, maxValue);
		return result;
	}

	@Override
	public String toString() {
		if(label.length()>0) {
			return label;
		}
		StringBuffer result = new StringBuffer();
		result.append("MarkovState [id=" + id + " : ");
		String separator= "";
		if (minCondition != null) {
			result.append("?").append(minCondition.getLabel()).append(" ").append(minValue);
			separator = " && ";
		}
		if (maxCondition != null) {
			result.append(separator);
			result.append("?").append(maxCondition.getLabel()).append(" ").append(maxValue);
		}
		result.append("]");
		return result.toString();
	}
}
