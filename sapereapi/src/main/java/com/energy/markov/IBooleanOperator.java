package com.energy.markov;

public interface IBooleanOperator {
	boolean apply(Float value1, Float value2);
}
