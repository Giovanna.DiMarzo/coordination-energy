package com.energy.markov;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.energy.model.HomeTotal;
import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

public class HomeMarkovTransitions extends HomeMarkovStates {
	protected Map<String, Float> mapLastValues = new HashMap<String, Float>();
	protected Map<String, MarkovState> mapLastStates = new HashMap<String, MarkovState>();

	public HomeMarkovTransitions(String[] _variables, String _location, String _scenario, Date aComputeDate) {
		super(_variables, _location, _scenario, aComputeDate);
	}

	private void copyToLast() {
		mapLastValues.clear();
		for (String key : mapValues.keySet()) {
			mapLastValues.put(key, mapValues.get(key));
		}
		mapLastStates.clear();
		for (String key : mapStates.keySet()) {
			MarkovState clone = mapStates.get(key) == null ? null : mapStates.get(key).clone();
			mapLastStates.put(key, clone);
		}
	}

	public void initializeLast() {
		mapLastValues.clear();
		mapLastStates.clear();
		for (String variable : super.variables) {
			float value = 0;
			mapLastValues.put(variable, value);
			try {
				mapLastStates.put(variable, this.getMarkovState(value));
			} catch (Exception e) {
				SapereLogger.getInstance().error(e);
			}
		}
	}

	public void refreshTransitions(HomeTotal homeTotal) {
		copyToLast();
		reset();
		super.refreshStates(homeTotal);
	}

	public MarkovState getCurrentState(String variable) {
		return mapStates.get(variable);
	}

	public boolean hasTransition(String variable) {
		MarkovState lastSate = getLastState(variable);
		MarkovState currentState = getCurrentState(variable);
		return (lastSate != null) && (currentState != null);
	}

	public Integer getCurrentStateId(String variable) {
		MarkovState state = getCurrentState(variable);
		if (state != null) {
			return state.getId();
		}
		return null;
	}

	public MarkovState getLastState(String variable) {
		return mapLastStates.get(variable);
	}

	public Integer getLastStateId(String variable) {
		MarkovState state = getLastState(variable);
		if (state != null) {
			return state.getId();
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		if (computeDate != null) {
			result.append("time ").append(SapereUtil.format_time.format(this.computeDate)).append(CR);
			for (String variableName : mapStates.keySet()) {
				MarkovState current = mapStates.get(variableName);
				MarkovState last = mapLastStates.get(variableName);
				result.append(CR).append(variableName).append(" : ").append(last).append(" -> ").append(current);
			}
		}
		return result.toString();
	}
}
