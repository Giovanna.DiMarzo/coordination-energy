package com.energy.markov;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.energy.model.HomeTotal;
import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

public class HomeMarkovStates {
	public final static float MAX_TOTAL_POWER = 3000;
	public final static String CR = System.getProperty("line.separator"); // Carriage return
	protected String[]  variables= {};
	protected String location;
	//protected TransitionMatrixScope scope;
	protected String scenario;
	protected Map<String, Float> mapValues = new HashMap<String, Float>();
	protected Map<String, MarkovState> mapStates = new HashMap<String, MarkovState>();
	protected Date computeDate;

	public final static List<MarkovState> STATES_LIST = Arrays.asList(new MarkovState[] {
			new MarkovState(1, BooleanOperator.EQUALS, new Float(0), null, null),
			new MarkovState(2, BooleanOperator.GREATER_THAN, new Float(0), BooleanOperator.LESS_THAN, new Float(0.2 * MAX_TOTAL_POWER)),
			new MarkovState(3, BooleanOperator.GREATER_THAN_OR_EQUALS, new Float(0.2 * MAX_TOTAL_POWER), BooleanOperator.LESS_THAN, new Float(0.4 * MAX_TOTAL_POWER)),
			new MarkovState(4, BooleanOperator.GREATER_THAN_OR_EQUALS, new Float(0.4 * MAX_TOTAL_POWER), BooleanOperator.LESS_THAN, new Float(0.6 * MAX_TOTAL_POWER)),
			new MarkovState(5, BooleanOperator.GREATER_THAN_OR_EQUALS, new Float(0.6 * MAX_TOTAL_POWER), BooleanOperator.LESS_THAN, new Float(0.8 * MAX_TOTAL_POWER)),
			new MarkovState(6, BooleanOperator.GREATER_THAN_OR_EQUALS, new Float(0.8 * MAX_TOTAL_POWER), BooleanOperator.LESS_THAN, new Float(MAX_TOTAL_POWER)),
			new MarkovState(7, BooleanOperator.GREATER_THAN_OR_EQUALS, new Float(MAX_TOTAL_POWER), null, null) });

	protected MarkovState getMarkovState(Float value) throws Exception {
		MarkovState result = null;
		for (MarkovState markovState : STATES_LIST) {
			if (markovState.containsValue(value)) {
				if (result != null) {
					throw new Exception("Markov state doublon for value " + value + " " + result + " " + markovState);
				}
				result = markovState;
			}
		}
		if (result == null) {
			throw new Exception("Markov state not found for value " + value);
		}
		return result;
	}

	public static MarkovState getById(Integer id) {
		if(id!=null) {
			for (MarkovState markovState : STATES_LIST) {
				if(id.equals(markovState.getId())) {
					return markovState;
				}
			}
		}
		return null;
	}

	public HomeMarkovStates(String[] _variables, /*TransitionMatrixScope _scope*/String _location, String _scenario, Date aComputeDate) {
		super();
		this.variables = _variables;
		//this.scope = _scope;
		this.location = _location;
		this.scenario = _scenario;
		this.computeDate = aComputeDate;
		reset();
	}

	public void reset() {
		mapValues.clear();
		mapStates.clear();
		for (String variable : this.variables) {
			mapStates.put(variable, null);
			mapValues.put(variable, null);
		}
	}

	public void refreshStates(HomeTotal homeTotal) {
		try {
			for (String variableName : mapStates.keySet()) {
				Float power = homeTotal.getVariablePower(variableName);
				if (power != null) {
					mapStates.put(variableName, getMarkovState(power));
					mapValues.put(variableName, power);
				}
			}
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
	}

	public Date getComputeDate() {
		return computeDate;
	}

	public void setComputeDate(Date computeDate) {
		this.computeDate = computeDate;
	}

	public MarkovState getState(String variable) {
		return mapStates.get(variable);
	}

	public Map<String, MarkovState> getMapStates() {
		return mapStates;
	}

	public Map<String, Float> getMapValues() {
		return mapValues;
	}

	public String[] getVariables() {
		return variables;
	}

	public void setVariables(String[] variables) {
		this.variables = variables;
	}

	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public static MarkovState getRandomState(List<Double> probList) {
		if(probList.size()==STATES_LIST.size()) {
			double random = Math.random();
			double probSum = 0;
			int stateIndex = 0;
			for(double nextProb : probList) {
				probSum+=nextProb;
				if(random < probSum) {
					return STATES_LIST.get(stateIndex);
				}
				stateIndex++;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		if (computeDate != null) {
			result.append("time ").append(SapereUtil.format_time.format(this.computeDate)).append(CR);
			for (String variableName : mapStates.keySet()) {
				result.append(CR).append(variableName).append(":").append((mapStates.get(variableName)));
			}
		}
		return result.toString();
	}
}
