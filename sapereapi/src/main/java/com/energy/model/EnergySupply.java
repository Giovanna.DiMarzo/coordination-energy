package com.energy.model;

import java.io.Serializable;
import java.util.Date;

import com.sapereapi.model.SapereUtil;

public class EnergySupply implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;
	protected String issuer;
	protected String issuerLocation;
	protected int issuerDistance;
	protected Float power; // electric power in watts
	protected Date beginDate;
	protected Date endDate;
	protected Long eventId;
	protected String deviceName;
	protected DeviceCategory deviceCategory;
	protected Boolean disabled = Boolean.FALSE;

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerLocation() {
		return issuerLocation;
	}

	public void setIssuerLocation(String issuerLocation) {
		this.issuerLocation = issuerLocation;
	}

	public Float getPower() {
		return power;
	}

	public void setPower(Float _power) {
		this.power = _power;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Float getDuration() {
		Float duration = SapereUtil.computeDurationMinutes(beginDate, endDate);
		return duration;
	}

	public Float getKWH() {
		Float durationHours = SapereUtil.computeDurationHours(beginDate, endDate);
		return (durationHours * power) / 1000;
	}

	public Float getWH() {
		Float durationHours = SapereUtil.computeDurationHours(beginDate, endDate);
		return (durationHours * power);
	}

	public void checkBeginNotPassed() {
		Date current = new Date();
		if (current.after(beginDate)) {
			beginDate = current;
		}
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public DeviceCategory getDeviceCategory() {
		return deviceCategory;
	}

	public void setDeviceCategory(DeviceCategory deviceCategory) {
		this.deviceCategory = deviceCategory;
	}

	public int getIssuerDistance() {
		return issuerDistance;
	}

	public void setIssuerDistance(int issuerDistance) {
		this.issuerDistance = issuerDistance;
	}

	public EnergySupply(String _issuer, String _issuerLocation, Float _power, Date beginDate, Date endDate, String _deviceName,
			DeviceCategory _category) {
		super();
		this.issuer = _issuer;
		this.issuerLocation = _issuerLocation;
		this.power = _power;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.deviceName = _deviceName;
		this.deviceCategory = _category;
		SapereUtil.checkRound(this.power, 2);
	}

	public boolean hasExpired() {
		Date current = new Date();
		return !current.before(this.endDate);
	}

	public boolean isActive() {
		Date current = new Date();
		return (!current.before(beginDate)) && current.before(this.endDate);
	}

	public boolean isInActiveSlot(Date aDate) {
		return aDate != null && (!aDate.before(beginDate)) && aDate.before(this.endDate);

	}

	public boolean isBeginDateOK(Date maxBeginDate) {
		return !this.beginDate.after(maxBeginDate);
	}

	public long getTimeLeftMS() {
		long currentMS = (new Date()).getTime();
		long beginMS = beginDate.getTime();
		if (beginMS > currentMS) {
			return Math.max(0, endDate.getTime() - beginMS);
		} else {
			return Math.max(0, endDate.getTime() - currentMS);
		}
	}

	public int getTimeLeftSec() {
		long timeLeftMS = getTimeLeftMS();
		if (timeLeftMS > 0) {
			return (int) timeLeftMS / 1000;
		}
		return 0;
	}

	public int comparePower(EnergySupply other) {
		float powerDiff = this.power.floatValue() - other.getPower();
		return (int) (powerDiff);
	}

	public int comparTimeLeft(EnergySupply other) {
		return (this.getTimeLeftSec() - other.getTimeLeftSec());
	}

	public int compareDistance(EnergySupply other) {
		return this.issuerDistance - other.getIssuerDistance();
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(this.issuer).append(":");
		result.append(SapereUtil.df.format(power)).append(" W from ");
		result.append(SapereUtil.formatTimeOrDate(beginDate));
		result.append(" to ");
		result.append(SapereUtil.formatTimeOrDate(endDate));
		if (this.disabled) {
			result.append("# DISABLED #");
		}
		return result.toString();
	}

	@Override
	public EnergySupply clone() throws CloneNotSupportedException {
		EnergySupply result = new EnergySupply(issuer, issuerLocation, new Float(power),
				beginDate == null ? null : new Date(beginDate.getTime()),
				endDate == null ? null : new Date(endDate.getTime()), deviceName, deviceCategory);
		result.setEventId(eventId);
		result.setIssuerDistance(issuerDistance);
		return result;
	}
}
