package com.energy.model;

import java.io.Serializable;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ContractAgent;
import com.energy.agent.ProducerAgent;
import com.energy.util.PermissionException;

import eu.sapere.middleware.agent.AgentAuthentication;

public class ProtectedConfirmationTable extends ProtectedObject implements Serializable {
	private static final long serialVersionUID = 2L;
	private ConfirmationTable confirmationTable = null;

	public ProtectedConfirmationTable(ConfirmationTable confirmationTable) {
		super();
		this.confirmationTable = confirmationTable;
	}

	@Override
	public boolean hasAccesAsConsumer(ConsumerAgent consumerAgent) {
		return checkAccessAsConsumer(consumerAgent.getAuthentication());
	}

	@Override
	public boolean hasAccesAsContractAgent(ContractAgent contractAgent) {
		return checkAccessAsContractAgent(contractAgent.getAuthentication());
	}

	@Override
	public boolean hasAccesAsProducer(ProducerAgent prodAgent) {
		return checkAccessAsProducer(prodAgent.getAuthentication());
	}

	@Override
	boolean checkAccessAsProducer(AgentAuthentication authentication) {
		if (AgentType.PRODUCER.getLabel().equals(authentication.getAgentType())
				&& confirmationTable.hasProducer(authentication.getAgentName())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	@Override
	boolean checkAccessAsConsumer(AgentAuthentication authentication) {
		return false;
	}

	@Override
	boolean checkAccessAsContractAgent(AgentAuthentication authentication) {
		// TODO Auto-generated method stub
		if (AgentType.CONTRACT.getLabel().equals(authentication.getAgentType())
				&& confirmationTable.hasContractAgent(authentication.getAgentName())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	public ConfirmationTable getConfirmationTable(ProducerAgent producerAgent) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		return confirmationTable;
	}

	public boolean hasExpiredItem(ProducerAgent producerAgent) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		return confirmationTable.hasExpiredItem();
	}

	public void cleanExpiredDate(ProducerAgent producerAgent) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		confirmationTable.cleanExpiredDate();
	}

	public void removeConfirmation(ProducerAgent producerAgent, String contractAgentName) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		confirmationTable.removeConfirmation(contractAgentName);
	}

	public void confirm(ProducerAgent producerAgent, String contractAgentName, Boolean value)
			throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		confirmationTable.confirm(contractAgentName, value);
	}

	public Boolean getConfirmation(ContractAgent contractAgent) throws PermissionException {
		if (!hasAccesAsContractAgent(contractAgent)) {
			throw new PermissionException("Access denied for agent " + contractAgent.getAgentName());
		}
		return confirmationTable.getConfirmation(contractAgent.getAgentName());
	}

	public ConfirmationItem getConfirmationItem(ContractAgent contractAgent) throws PermissionException {
		if (!hasAccesAsContractAgent(contractAgent)) {
			throw new PermissionException("Access denied for agent " + contractAgent.getAgentName());
		}
		return confirmationTable.getConfirmationItem(contractAgent.getAgentName());
	}

	public ProtectedConfirmationTable clone() throws CloneNotSupportedException {
		ConfirmationTable confirmationTableClone = confirmationTable.clone();
		return new ProtectedConfirmationTable(confirmationTableClone);
	}

	@Override
	public String toString() {
		return "ProtectedConfirmationTable " + confirmationTable ;
	}
}
