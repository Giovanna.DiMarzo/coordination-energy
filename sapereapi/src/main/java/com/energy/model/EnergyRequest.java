package com.energy.model;

import java.io.Serializable;
import java.util.Date;

import com.sapereapi.model.SapereUtil;

public class EnergyRequest extends EnergySupply implements Serializable {
	private static final long serialVersionUID = 14567L;
	private PriorityLevel priorityLevel;
	private Float delayToleranceMinutes;
	private Date aux_expiryDate = null;
	private Date warningDate = null;
	private Date currentDate = null;
	private String confirmTag = null;

	public EnergyRequest(String issuer, String _location, Float _power, Date beginDate, Date endDate, Float _delayToleranceMinutes,
			PriorityLevel _priority, String _deviceName, DeviceCategory category) {
		super(issuer, _location, _power, beginDate, endDate, _deviceName, category);
		this.delayToleranceMinutes = _delayToleranceMinutes;
		this.priorityLevel = _priority;
		this.aux_expiryDate = new Date();
		this.currentDate = new Date();
		this.deviceCategory = category;
	}

	public PriorityLevel getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(PriorityLevel priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Float getDelayToleranceMinutes() {
		return delayToleranceMinutes;
	}

	public void setDelayToleranceMinutes(Float delayToleranceMinutes) {
		this.delayToleranceMinutes = delayToleranceMinutes;
	}

	public Date getMaxBeginDate() {
		Date maxBeginDate = SapereUtil.shiftDateMinutes(this.beginDate, delayToleranceMinutes);
		return maxBeginDate;
	}

	public Date getAux_expiryDate() {
		return aux_expiryDate;
	}

	public void setAux_expiryDate(Date aux_expiryDate) {
		this.aux_expiryDate = aux_expiryDate;
	}

	public boolean canBeSupplied() {
		Date current = new Date();
		return !current.after(getMaxBeginDate()) && current.before(this.endDate);
	}

	public boolean isOK(SingleOffer singleOffer) {
		Date maxBeginDate = getMaxBeginDate();
		return singleOffer.isActive() && singleOffer.getPower() > 0 && singleOffer.isBeginDateOK(maxBeginDate);
	}

	public long getWarningDurationMS(Date current) {
		if(warningDate==null) {
			return 0;
		} else {
			return 1000+ Math.max(0, current.getTime() - warningDate.getTime());
		}
	}

	public int getWarningDurationSec() {
		long warningDuraitonMS = getWarningDurationMS(currentDate);
		return (int) (warningDuraitonMS/1000);
	}

	public int compareWarningDesc(EnergyRequest other) {
		return -1 * (this.getWarningDurationSec() - other.getWarningDurationSec());
	}

	public int comparePriorityDesc(EnergyRequest other) {
		return -1 * this.priorityLevel.compare(other.getPriorityLevel());
	}

	public int compareDistance(EnergyRequest other) {
		return this.issuerDistance - other.getIssuerDistance();
	}

	public int comparePriorityDescAndPower(EnergyRequest other) {
		int comparePriority = comparePriorityDesc(other);
		if (comparePriority == 0) {
			float powerComparison = this.power - other.getPower();
			return (int) (100*powerComparison);
		} else {
			return comparePriority;
		}
	}

	public int compareDistanceAndPriorityDescAndWarningDescAndPower(EnergyRequest other) {
		int compareDistance = compareDistance(other);
		if(compareDistance == 0) {
			int comparePriority = comparePriorityDesc(other);
			if (comparePriority == 0) {
				int compareWarningDuration = this.compareWarningDesc(other);
				if (compareWarningDuration == 0) {
					float powerComparison = this.power - other.getPower();
					return (int) (100*powerComparison);
				} else {
					return compareWarningDuration;
				}
			} else {
				return comparePriority;
			}
		} else {
			return compareDistance;
		}
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Date getWarningDate() {
		return warningDate;
	}

	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}

	public String getConfirmTag() {
		return confirmTag;
	}

	public void setConfirmTag(String confirmTag) {
		this.confirmTag = confirmTag;
	}

	public void resetWarningCounter(Date _current) {
		currentDate = _current;
		warningDate = null;
	}

	public void incrementWarningCounter(Date _current) {
		currentDate = _current;
		if (warningDate == null) {
			warningDate = _current;
		}
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer(super.toString());
		if(priorityLevel==null) {
			System.out.println("EnergyRequest : priorityLevel is null");
		}
		if(priorityLevel!=null && this.priorityLevel.compare(PriorityLevel.LOW) > 0) {
			result.append(", urgency : ").append(this.priorityLevel.getLabel());
		}
		int warningDurationSec = this.getWarningDurationSec();
		if(warningDurationSec > 0) {
			result.append(", warningDurationSec : ").append(warningDurationSec);
		}
		if(this.warningDate != null) {
			result.append(" warning since ").append(SapereUtil.format_time.format(warningDate));
		}
		if(this.disabled) {
			result.append("# DISABLED #");
		}
		return result.toString();
	}

	@Override
	public EnergyRequest clone() throws CloneNotSupportedException {
		EnergySupply supply = super.clone();
		EnergyRequest clone = new EnergyRequest(supply.getIssuer(), supply.getIssuerLocation(), supply.getPower()
				, supply.getBeginDate()==null? null : new Date(supply.getBeginDate().getTime())
				, supply.getBeginDate()==null? null : new Date(supply.getEndDate().getTime())
				, this.delayToleranceMinutes
				, this.priorityLevel
				, this.deviceName
				, this.deviceCategory
				);
		clone.setEventId(eventId);
		clone.setCurrentDate(currentDate);
		clone.setConfirmTag(confirmTag);
		if(warningDate!=null) {
			clone.setWarningDate(new Date(warningDate.getTime()));
		}
		return clone;
	}
}
