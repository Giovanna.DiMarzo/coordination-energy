package com.energy.model;

import java.util.ArrayList;
import java.util.List;

public enum PriorityLevel {
	  UNKNOWN("", 0)
	, LOW("Low", 1)
	, MEDIUM("Medium", 2)
	, HIGH("High", 3);

	private String label;
	private Integer level;

	PriorityLevel(String _label, Integer _level) {
		this.label = _label;
		this.level = _level;
	}

	public String getLabel() {
		return label;
	}

	public Integer getId() {
		return level;
	}

	public static PriorityLevel getByLabel(String label) {
		String label2 = (label == null) ? "" : label;
		for (PriorityLevel pLevel : PriorityLevel.values()) {
			if (pLevel.getLabel().equals(label2)) {
				return pLevel;
			}
		}
		return null;
	}

	public boolean isLowerThan(PriorityLevel other) {
		return this.compare(other) < 0;
	}

	public boolean isHigherThan(PriorityLevel other) {
		return this.compare(other) > 0;
	}

	public int compare(PriorityLevel other) {
		return this.level - other.level;
	}

	public static List<String> getLabels() {
		List<String> reuslt = new ArrayList<>();
		for (PriorityLevel pLevel : PriorityLevel.values()) {
			reuslt.add(pLevel.getLabel());
		}
		return reuslt;
	}

	public static List<PriorityLevel> getList() {
		List<PriorityLevel> reuslt = new ArrayList<PriorityLevel>();
		for (PriorityLevel pLevel : PriorityLevel.values()) {
			reuslt.add(pLevel);
		}
		return reuslt;
	}

	public static List<OptionItem> getOptionList() {
		List<OptionItem> reuslt = new ArrayList<OptionItem>();
		for (PriorityLevel pLevel : PriorityLevel.values()) {
			reuslt.add(new OptionItem(pLevel.name(), pLevel.getLabel()));
		}
		return reuslt;
	}
}
