package com.energy.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExtendedHomeTotal extends HomeTotal {
	private float minMissingRequests;
	private float maxWarningDuration;
	private float sumWarningPower;
	private Long nbMissingRequests;
	private Long nbOffers;
	private Date dateLast;
	private Date dateNext;
	private String consumersMissingRequests;
	private String consumersWarningRequests;
	private String contractDoublons;
	private List<SingleOffer> offers;

	public float getMinMissingRequests() {
		return minMissingRequests;
	}

	public void setMinMissingRequests(float minMissingRequests) {
		this.minMissingRequests = minMissingRequests;
	}

	public Long getNbMissingRequests() {
		return nbMissingRequests;
	}

	public void setNbMissingRequests(Long nbMissingRequests) {
		this.nbMissingRequests = nbMissingRequests;
	}

	public String getConsumersMissingRequests() {
		return consumersMissingRequests;
	}

	public void setConsumersMissingRequests(String consumersMissingRequests) {
		this.consumersMissingRequests = consumersMissingRequests;
	}

	public String getContractDoublons() {
		return contractDoublons;
	}

	public void setContractDoublons(String contractDoublons) {
		this.contractDoublons = contractDoublons;
	}

	public String getConsumersWarningRequests() {
		return consumersWarningRequests;
	}

	public void setConsumersWarningRequests(String consumersWarningRequests) {
		this.consumersWarningRequests = consumersWarningRequests;
	}

	public float getMaxWarningDuration() {
		return maxWarningDuration;
	}

	public void setMaxWarningDuration(float maxWarningDuration) {
		this.maxWarningDuration = maxWarningDuration;
	}

	public Long getNbOffers() {
		return nbOffers;
	}

	public void setNbOffers(Long nbOffers) {
		this.nbOffers = nbOffers;
	}

	public Date getDateLast() {
		return dateLast;
	}

	public void setDateLast(Date dateLast) {
		this.dateLast = dateLast;
	}

	public Date getDateNext() {
		return dateNext;
	}

	public void setDateNext(Date dateNext) {
		this.dateNext = dateNext;
	}

	public float getSumWarningPower() {
		return sumWarningPower;
	}

	public void setSumWarningPower(float _sumWarningPower) {
		this.sumWarningPower = _sumWarningPower;
	}

	public List<SingleOffer> getOffers() {
		return offers;
	}

	public void setOffers(List<SingleOffer> offers) {
		this.offers = offers;
	}

	public void addOffer(SingleOffer offer) {
		if(this.offers == null){
			this.offers = new ArrayList<SingleOffer>();
			offers.add(offer);
		}
	}
	// min_missing_request

}
