package com.energy.model;

import java.io.Serializable;
import java.util.Date;

import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

public class SingleOffer extends EnergySupply implements Cloneable, Serializable {
	private static final long serialVersionUID = 17L;
	private String producerAgent;
	private EnergyRequest request;
	private Date creationTime;
	private Date usedTime;
	private Date acceptanceTime;
	private Date deadline;
	private Long id;
	private Boolean acquitted;
	private Boolean used;
	private Boolean accepted;
	private String log;
	private Long histoId;

	public SingleOffer(String producerAgent, String _location, Date _beginDate, Date _endDate, Float _power,
			int _validityDurationMn, EnergyRequest _request,  String producerDeviceName, DeviceCategory producerDeviceCategory) {
		super(producerAgent, _location, _power, _beginDate, _endDate, producerDeviceName, producerDeviceCategory);
		this.producerAgent = producerAgent;
		this.request = _request;
		this.creationTime = new Date();
		this.deadline = SapereUtil.shiftDateMinutes(creationTime, _validityDurationMn);
		this.acquitted = Boolean.FALSE;;
		this.used = Boolean.FALSE;
		this.log = "";
	}

	public SingleOffer(String producerAgent, EnergySupply energySupply,
			int _validityDurationSeconds, EnergyRequest _request) {
		super(producerAgent, energySupply.getIssuerLocation(), energySupply.getPower(), energySupply.getBeginDate(), energySupply.getEndDate(), energySupply.getDeviceName(), energySupply.getDeviceCategory());
		Date current = new Date();
		if (beginDate.before(current)) {
			beginDate = current;
		}
		this.producerAgent = producerAgent;
		this.request = _request;
		this.creationTime = new Date();
		this.deadline = SapereUtil.shiftDateSec(creationTime, _validityDurationSeconds);
		this.acquitted = Boolean.FALSE;;
		this.used = Boolean.FALSE;
		this.log = "";
	}

	public String getProducerAgent() {
		return producerAgent;
	}

	public void setProducerAgent(String producerAgent) {
		this.producerAgent = producerAgent;
	}

	public String getConsumerAgent() {
		if(request==null) {
			return null;
		}
		return this.request.getIssuer();
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public EnergyRequest getRequest() {
		return request;
	}

	public void setRequest(EnergyRequest request) {
		this.request = request;
	}

	public PriorityLevel getPriorityLevel() {
		if(request!=null) {
			return request.getPriorityLevel();
		}
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAcquitted() {
		return acquitted;
	}

	public void setAcquitted(Boolean acquitted) {
		this.acquitted = acquitted;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(Date usedTime) {
		this.usedTime = usedTime;
	}

	public Date getAcceptanceTime() {
		return acceptanceTime;
	}

	public void setAcceptanceTime(Date acceptanceTime) {
		this.acceptanceTime = acceptanceTime;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public Long getHistoId() {
		return histoId;
	}

	public void setHistoId(Long histoId) {
		this.histoId = histoId;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public Float getTimeCoverRate() {
		if(request==null) {
			SapereLogger.getInstance().warning("### SingleOffer.getTimeCoverRate request is null");
			return new Float(0.);
			//throw new Exception("SingleOffer.getTimeCoverRate request is null");
		}
		double requestDuration = this.request.getTimeLeftMS();
		double supplyDuration = this.getTimeLeftMS();
		if(supplyDuration >= requestDuration) {
			return new Float(1.0);
		} else {
			double result = supplyDuration/requestDuration;
			//float test2 = 2/3;
			return new Float(result);
		}
	}

	public int compareDistance(SingleOffer other) {
		return this.issuerDistance - other.getIssuerDistance();
	}

	public int compareTimeCover(SingleOffer other) {
		float cover1 = this.getTimeCoverRate();
		float cover2 = other.getTimeCoverRate();
		float delta = cover1 - cover2;
		return (int) (1000* delta);
	}

	@Override
	public boolean hasExpired() {
		Date current = new Date();
		return current.after(this.endDate) || current.after(this.deadline);
	}

	/**
	 * We apply a margin for the producing agent. The offer expires a few seconds later for the producer agent: it guarantees its availability at all times
	 * (the margin corresponds to the time limit for acceptance of the offer by the consumer after the selection of the offers)
	 * @param marginSeconds
	 * @return
	 */
	public boolean hasExpired(int marginSeconds) {
		Date current = new Date();
		Date dealine2 = SapereUtil.shiftDateSec(deadline, marginSeconds);
		return current.after(this.endDate) || current.after(dealine2);
	}

	/*
	 * public boolean isActive() { Date current = new Date(); return
	 * (!current.before(beginDate)) && current.before(this.endDate); }
	 */

	@Override
	public String toString() {
		return "[" + this.id + "] "  + this.producerAgent + "->" + this.getConsumerAgent() + " at " +  SapereUtil.format_time.format(creationTime) + " until " + SapereUtil.format_time.format(deadline)
				+ " W = " + SapereUtil.df.format(this.power) + " from "
				+ SapereUtil.format_time.format(beginDate) + " to "	+ SapereUtil.format_time.format(endDate)
				+ " (" + SapereUtil.df.format(this.getTimeCoverRate()) + ")"
				+ (acquitted!=null && this.acquitted? " acquitted " : "### NOT ACQUITTED ###")
				+ (used!=null && this.used? " used " : "")
				+ (accepted!=null && this.accepted? " accepted " : "")
				;
	}

	@Override
	public SingleOffer clone() throws CloneNotSupportedException {
		EnergySupply supplyClone = super.clone();
		EnergyRequest requestClone = this.request.clone();
		SingleOffer clone = new SingleOffer(new String(producerAgent), supplyClone, 0, requestClone);
		if(deadline!=null) {
			clone.setDeadline(new Date(this.deadline.getTime()));
		}
		if(creationTime!=null) {
			clone.setCreationTime(new Date(this.creationTime.getTime()));
		}
		clone.setId(this.id);
		clone.setAcquitted(this.acquitted);
		clone.setUsed(this.used);
		return clone;
	}
}
