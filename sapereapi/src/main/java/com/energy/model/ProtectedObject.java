package com.energy.model;

import java.util.HashSet;
import java.util.Set;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ContractAgent;
import com.energy.agent.ProducerAgent;
import com.sapereapi.model.Sapere;

import eu.sapere.middleware.agent.AgentAuthentication;

public abstract class ProtectedObject {
	protected Set<AgentAuthentication> validAuthentications = new HashSet<AgentAuthentication>();

	protected boolean checkAuthentication(AgentAuthentication authentication) {
		if (validAuthentications.contains(authentication)) {
			return true;
		}
		boolean authenticated = Sapere.getInstance().isAuthenticated(authentication);
		if (authenticated) {
			validAuthentications.add(authentication);
		}
		return authenticated;
	}

	abstract boolean hasAccesAsConsumer(ConsumerAgent consumerAgent);

	abstract boolean hasAccesAsProducer(ProducerAgent prodAgent);

	abstract boolean hasAccesAsContractAgent(ContractAgent contractAgent);

	abstract boolean checkAccessAsProducer(AgentAuthentication authentication);

	abstract boolean checkAccessAsConsumer(AgentAuthentication authentication);

	abstract boolean checkAccessAsContractAgent(AgentAuthentication authentication);
}
