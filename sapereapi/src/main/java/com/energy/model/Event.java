package com.energy.model;

import java.io.Serializable;
import java.util.Date;

public class Event extends EnergySupply implements Cloneable, Serializable {
	private static final long serialVersionUID = 14408L;
	protected Long id;
	protected Long histoId;
	protected EventType type;
	protected Event originEvent = null;
	protected WarningType warningType = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public Event getOriginEvent() {
		return originEvent;
	}

	public void setOriginEvent(Event originEvent) {
		this.originEvent = originEvent;
	}

	public WarningType getWarningType() {
		return warningType;
	}

	public void setWarningType(WarningType warningType) {
		this.warningType = warningType;
	}

	public Long getHistoId() {
		return histoId;
	}

	public void setHistoId(Long histoId) {
		this.histoId = histoId;
	}

	public Event(EventType type, String _agent, String _location, Float _power, Date beginDate, Date endDate, String _deviceName, DeviceCategory deviceCategory) {
		super(_agent, _location, _power, beginDate, endDate, _deviceName, deviceCategory);
		this.type = type;
		this.power = _power;
		this.beginDate = beginDate;
		this.endDate = endDate;
	}

	public Event(EventType type, EnergySupply energySupply) {
		super(energySupply.getIssuer(), energySupply.getIssuerLocation(), energySupply.getPower(), energySupply.getBeginDate(), energySupply.getEndDate(), energySupply.getDeviceName(), energySupply.getDeviceCategory());
		this.type = type;
	}

	public String getKey() {
		StringBuffer result = new StringBuffer();
		result.append(this.issuer).append("#");
		if(type!=null) {
			result.append(type.getLabel());
		}
		result.append("#");
		if(beginDate!=null) {
			long timeSec = this.beginDate.getTime()/1000;
			result.append(timeSec);
		}
		return result.toString();
	}

	public void setSupplyFields(EnergySupply energySupply) {
		this.power = energySupply.getPower();
		this.beginDate = energySupply.getBeginDate();
		this.endDate = energySupply.getEndDate();
	}

	public boolean hasExpired() {
		Date current = new Date();
		return current.after(this.endDate);
	}

	public boolean isActive() {
		Date current = new Date();
		return (!current.before(beginDate)) && current.before(this.endDate);
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(this.type.getLabel()).append(" ")
		.append(this.issuer).append(" ")
		.append((super.toString()));
		if(warningType!=null) {
			result.append(" ").append(warningType.getLabel());
		}
		return result.toString();
	}

	@Override
	public Event clone() throws CloneNotSupportedException {
		EnergySupply supply = super.clone();
		EventType type2 = EventType.getByLabel(this.type.getLabel());
		Event clone = new Event(type2, supply);
		clone.setId(id);
		return clone;
	}
}
