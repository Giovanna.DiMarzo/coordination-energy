package com.energy.model;

import java.io.Serializable;
import java.util.Date;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ContractAgent;
import com.energy.agent.LearningAgent;
import com.energy.agent.ProducerAgent;
import com.energy.util.PermissionException;

import eu.sapere.middleware.agent.AgentAuthentication;

public class ProtectedContract extends ProtectedObject implements Serializable {
	private static final long serialVersionUID = 5L;
	private Contract contract;

	public ProtectedContract(Contract contract) {
		super();
		this.contract = contract;
	}

	public boolean checkAccessAsProducer(AgentAuthentication authentication) {
		if (AgentType.PRODUCER.getLabel().equals(authentication.getAgentType())
				&& contract.hasProducer(authentication.getAgentName())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	protected boolean checkAccessAsConsumer(AgentAuthentication authentication) {
		if (AgentType.CONSUMER.getLabel().equals(authentication.getAgentType())
				&& contract.getConsumerAgent().equals(authentication.getAgentName())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	protected boolean checkAccessAsStakeholder(AgentAuthentication authentication) {
		if (AgentType.CONSUMER.getLabel().equals(authentication.getAgentType())) {
			return checkAccessAsConsumer(authentication);
		} else if (AgentType.CONTRACT.getLabel().equals(authentication.getAgentType())) {
			return checkAccessAsContractAgent(authentication);
		} else if (AgentType.PRODUCER.getLabel().equals(authentication.getAgentType())) {
			return checkAccessAsProducer(authentication);
		}
		return false;
	}

	protected boolean checkAccessAsContractAgent(AgentAuthentication authentication) {
		if (AgentType.CONTRACT.getLabel().equals(authentication.getAgentType())
				&& contract.getContractAgent().equals(authentication.getAgentName())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	protected boolean checkAccessAsLearningAgent(AgentAuthentication authentication) {
		if (AgentType.LEARNING_AGENT.getLabel().equals(authentication.getAgentType())) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	public Contract getContract(LearningAgent learningAgent) throws PermissionException {
		return getContract(learningAgent.getAuthentication());
	}

	public Contract getContract(ContractAgent contractAgent) throws PermissionException {
		return getContract(contractAgent.getAuthentication());
	}

	public Contract getContract(ConsumerAgent consumerAgent) throws PermissionException {
		return getContract(consumerAgent.getAuthentication());
	}

	private Contract getContract(AgentAuthentication authentication) throws PermissionException {
		if (!(checkAccessAsConsumer(authentication)
				|| checkAccessAsContractAgent(authentication)
				|| checkAccessAsLearningAgent(authentication)
				)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract;
	}

	public boolean hasAccesAsConsumer(ConsumerAgent consumerAgent) {
		return checkAccessAsConsumer(consumerAgent.getAuthentication());
	}

	boolean hasAccesAsContractAgent(ContractAgent contractAgent) {
		return checkAccessAsContractAgent(contractAgent.getAuthentication());
	}

	public boolean hasAccesAsProducer(ProducerAgent prodAgent) {
		return checkAccessAsProducer(prodAgent.getAuthentication());
	}

	public boolean hasProducer(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.hasProducer(authentication.getAgentName());
	}

	/*
	 * private String getConsumerAgent(AgentAuthentication authentication) throws
	 * PermissionException { if (!(checkAccessAsStakeholder(authentication))) { throw
	 * new PermissionException("Access denied for agent " +
	 * authentication.getAgentName()); } return contract.getConsumerAgent(); }
	 */
	public String getConsumerAgent() {
		return contract.getConsumerAgent();
	}

	public String getConsumerLocation() {
		return contract.getConsumerLocation();
	}

	public String getContractAgent() {
		return contract.getContractAgent();
	}

	public boolean hasDisagreement() {
		return contract.hasDisagreement();
	}

	public boolean hasProducerAgreement(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.hasAgreement(authentication.getAgentName());
	}

	public boolean hasProducerDisagreement(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.hasDisagreement(authentication.getAgentName());
	}

	public void addProducerAgreement(ProducerAgent prodAgent, boolean isOk) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		contract.addAgreement(prodAgent, isOk);
	}

	public void producerStop(ProducerAgent prodAgent) throws PermissionException {
		addProducerAgreement(prodAgent, false);
	}

	public Float getProducerPower(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.getPowerFromAgent(authentication.getAgentName());
	}

	public boolean hasAllAgreements() {
		return contract.hasAllAgreements();
	}

	public boolean hasExpired() {
		return contract.hasExpired();
	}

	public boolean waitingValidation() {
		return contract.waitingValidation();
	}

	public boolean isActive() {
		return contract.isActive();
	}

	public Date getValidationDeadline(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.getValidationDeadline();
	}

	public boolean validationHasExpired(ProducerAgent prodAgent) throws PermissionException {
		AgentAuthentication authentication = prodAgent.getAuthentication();
		if (!checkAccessAsProducer(authentication)) {
			throw new PermissionException("Access denied for agent " + authentication.getAgentName());
		}
		return contract.validationHasExpired();
	}

	public ProtectedContract clone() throws CloneNotSupportedException {
		Contract contractClone = contract.clone();
		return new ProtectedContract(contractClone);
	}

	public EnergyRequest getRequest() {
		return contract.getRequest();
	}

	public String getStatus() {
		return contract.getStatus();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return contract.formatContent(false);
	}

}
