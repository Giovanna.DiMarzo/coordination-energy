package com.energy.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

public class HomeTotal implements Cloneable, Serializable{
	private static final long serialVersionUID = 15078L;
	protected Long id;
	protected Long idLast;
	protected Long idNext;
	protected String sessionId;
	protected String location;
	protected int distance;
	protected Float requested;
	protected Float produced;
	protected Float consumed;
	protected Float consumedLocally;
	protected Float provided;
	protected Float providedLocally;
	protected Float available;
	protected Float missing;
	protected Float sentOffersTotal;
	protected Float receivedOffersTotal;
	protected Date date;
	protected String agentName;
	protected List<Event> linkedEvents;
	protected Map<String, Float> receivedOffersRepartition;
	protected Map<String, Float> sentOffersRepartition;

	protected Map<String, Field> variableFields = new HashMap<String, Field>();

	public HomeTotal() {
		super();
		reset();
		linkedEvents = new ArrayList<Event>();
		variableFields.clear();
		for(Field field : this.getClass().getDeclaredFields()) {
			if(Float.class.equals(field.getType())) {
				variableFields.put(field.getName(), field);
			}
		}
	}

	public void reset() {
		requested = new Float(0);
		produced = new Float(0);
		consumed = new Float(0);
		consumedLocally = new Float(0);
		available = new Float(0);
		missing = new Float(0);
		provided = new Float(0);
		providedLocally = new Float(0);
		sentOffersTotal = new Float(0);
		receivedOffersTotal = new Float(0);
	}

	public boolean hasActivity() {
		return requested>0 || produced>0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getRequested() {
		return requested;
	}

	public Float getProduced() {
		return produced;
	}

	public Float getConsumed() {
		return consumed;
	}

	public Float getConsumedLocally() {
		return consumedLocally;
	}

	public Float getProvidedLocally() {
		return providedLocally;
	}

	public void setConsumedLocally(Float consumedLocally) {
		this.consumedLocally = consumedLocally;
	}

	public void setProvidedLocally(Float providedLocally) {
		this.providedLocally = providedLocally;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Event> getLinkedEvents() {
		return linkedEvents;
	}

	public void setLinkedEvents(List<Event> linkedEvents) {
		this.linkedEvents = linkedEvents;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public void setRequested(Float requested) {
		this.requested = requested;
	}

	public void setProduced(Float produced) {
		this.produced = produced;
	}

	public void setConsumed(Float consumed) {
		this.consumed = consumed;
	}

	public Float getAvailable() {
		return available;
	}

	public void setAvailable(Float available) {
		this.available = available;
	}

	public Float getMissing() {
		return missing;
	}

	public void setMissing(Float missing) {
		this.missing = missing;
	}

	public Float getProvided() {
		return provided;
	}

	public void setProvided(Float provided) {
		this.provided = provided;
	}

	public Float getSentOffersTotal() {
		return sentOffersTotal;
	}

	public void setSentOffersTotal(Float sentOffersTotal) {
		this.sentOffersTotal = sentOffersTotal;
	}

	public Float getReceivedOffersTotal() {
		return receivedOffersTotal;
	}

	public void setReceivedOffersTotal(Float receivedOffersTotal) {
		this.receivedOffersTotal = receivedOffersTotal;
	}

	public Map<String, Float> getReceivedOffersRepartition() {
		return receivedOffersRepartition;
	}

	public void setReceivedOffersRepartition(Map<String, Float> receivedOffersRepartition) {
		this.receivedOffersRepartition = receivedOffersRepartition;
	}

	public Map<String, Float> getSentOffersRepartition() {
		return sentOffersRepartition;
	}

	public void setSentOffersRepartition(Map<String, Float> sentOffersRepartition) {
		this.sentOffersRepartition = sentOffersRepartition;
	}

	public Long getIdLast() {
		return idLast;
	}

	public void setIdLast(Long idLast) {
		this.idLast = idLast;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Long getIdNext() {
		return idNext;
	}

	public void setIdNext(Long idNext) {
		this.idNext = idNext;
	}

	public String getLocation() {
		return location;
	}

	public int getDistance() {
		return distance;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Float getVariablePower(String fieldName) {
		try {
			Field field = variableFields.get(fieldName);
			Object value = field.get(this);
			if(value instanceof Float) {
				Float vPower = (Float) value;
				if(vPower<0) {
					vPower = new Float(0);
				}
				return vPower;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			SapereLogger.getInstance().error(e);
			//e.printStackTrace();
		} 
		return null;
	}
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("time ").append(SapereUtil.format_time.format(this.date)).append(" requested:")
				.append(SapereUtil.df.format(this.requested))
				.append(" produced:").append(SapereUtil.df.format(this.produced))
				.append(" provided:").append(SapereUtil.df.format(this.provided))
				.append(" consumed:").append(SapereUtil.df.format(this.consumed))
				.append(" available:").append(SapereUtil.df.format(this.available))
				.append(" missing:").append(SapereUtil.df.format(this.missing));
		return result.toString();
	}

	@Override
	public HomeTotal clone() throws CloneNotSupportedException {
		HomeTotal result = new HomeTotal();
		result.setId(id);
		result.setDate(date);
		result.setAgentName(agentName);
		result.setIdLast(idLast);
		result.setIdNext(idNext);
		result.setSessionId(sessionId);
		result.setLocation(location);
		result.setDistance(distance);
		result.setRequested(requested);
		result.setProduced(produced);
		result.setConsumed(consumed);
		result.setConsumedLocally(consumedLocally);
		result.setProvided(provided);
		result.setProvidedLocally(providedLocally);
		result.setAvailable(available);
		result.setMissing(missing);
		result.setSentOffersTotal(sentOffersTotal);
		result.setReceivedOffersTotal(receivedOffersTotal);
		if(receivedOffersRepartition!=null) {
			Map<String, Float> _receivedOffersRepartition = new HashMap<>();
			_receivedOffersRepartition.putAll(receivedOffersRepartition);
			result.setReceivedOffersRepartition(_receivedOffersRepartition);
		}
		if(sentOffersRepartition!=null) {
			Map<String, Float> _sentOffersRepartition = new HashMap<>(); ;
			_sentOffersRepartition.putAll(sentOffersRepartition);
			result.setSentOffersRepartition(_sentOffersRepartition);
		}
		if(linkedEvents!=null) {
			List<Event> _linkedEvent = new ArrayList<>();
			_linkedEvent.addAll(linkedEvents);
			result.setLinkedEvents(_linkedEvent);
		}
		return result;
	}
}
