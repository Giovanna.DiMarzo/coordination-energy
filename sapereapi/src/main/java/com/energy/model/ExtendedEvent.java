package com.energy.model;

import java.util.Date;

public class ExtendedEvent extends Event {
	private static final long serialVersionUID = 21L;
	private Date effectiveEndDate;
	private String linkedConsumer;
	private String linkedConsumerLocation;

	public ExtendedEvent(EventType type, EnergySupply energySupply) {
		super(type, energySupply);
	}

	public ExtendedEvent(EventType type, String _agent, String _location, Float _power, Date beginDate, Date endDate, String deviceName, DeviceCategory deviceCategory) {
		super(type, _agent, _location, _power, beginDate, endDate, deviceName, deviceCategory);
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public String getLinkedConsumer() {
		return linkedConsumer;
	}

	public void setLinkedConsumer(String linkedConsumer) {
		this.linkedConsumer = linkedConsumer;
	}

	public String getLinkedConsumerLocation() {
		return linkedConsumerLocation;
	}

	public void setLinkedConsumerLocation(String _linkedConsumerLocation) {
		this.linkedConsumerLocation = _linkedConsumerLocation;
	}

}
