package com.energy.model;

public enum EventType {
	PRODUCTION("PRODUCTION", 0, false, false),
	REQUEST("REQUEST", 1, false, false),
	CONTRACT("CONTRACT", 2, false, false),
	PRODUCTION_STOP("PRODUCTION_STOP", 10, true, true),
	REQUEST_STOP("REQUEST_STOP", 11, true, true),
	CONTRACT_STOP("CONTRACT_STOP", 12, true, true),
	PRODUCTION_EXPIRY("PRODUCTION_EXPIRY", 20, false, true),
	REQUEST_EXPIRY("REQUEST_EXPIRY", 21, false, true),
	CONTRACT_EXPIRY("CONTRACT_EXPIRY", 22, false, true),
	PRODUCTION_UPDATE("PRODUCTION_UPDATE", 30, true, false),
	REQUEST_UPDATE("REQUEST_UPDATE", 31, true, false),
	CONTRACT_UPDATE("CONTRACT_UPDATE", 32, true, false)
	;

	private String label;
	private Integer id;
	private Boolean isCancel;	// True if cancels the previous event
	private Boolean isEnding;

	private EventType(String _label, Integer _id, Boolean _isCancel, Boolean _isEnding) {
		this.label = _label;
		this.id = _id;
		this.isCancel = _isCancel;
		this.isEnding = _isEnding;
	}

	public String getLabel() {
		return label;
	}

	public Integer getId() {
		return id;
	}

	public Boolean getIsCancel() {
		return isCancel;
	}

	public Boolean getIsEnding() {
		return isEnding;
	}

	public static EventType getByLabel(String label) {
		String label2 = (label == null) ? "" : label;
		for (EventType pLevel : EventType.values()) {
			if (pLevel.getLabel().equals(label2)) {
				return pLevel;
			}
		}
		return null;
	}
}
