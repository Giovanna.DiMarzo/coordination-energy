package com.energy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.energy.agent.ContractAgent;
import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

import eu.sapere.middleware.agent.SapereAgent;

public class Contract extends GlobalOffer implements Cloneable, Serializable {
	private static final long serialVersionUID = 4L;
	private String contractAgent;
	private Date validationDeadline;
	private Set<String> agreements;
	private Set<String> disagreements;

	public Contract(String _contractAgent, GlobalOffer globalOffer, Date _validationDeadline) {
		super();
		this.contractAgent = _contractAgent;
		// this.producerAgents = globalOffer.getProducerAgents();
		this.request = globalOffer.getRequest();
		this.consumerAgent = globalOffer.getConsumerAgent();
		Date current = new Date();
		this.beginDate = globalOffer.getBeginDate();
		if (beginDate.before(current)) {
			beginDate = current;
		}
		this.endDate = globalOffer.getEndDate();
		this.mapPower = globalOffer.getMapPower();
		this.mapLocation = globalOffer.getMapLocation();
		this.agreements = new HashSet<>();
		this.disagreements = new HashSet<>();
		this.validationDeadline = _validationDeadline;
	}

	public String getContractAgent() {
		return contractAgent;
	}

	public void setContractAgent(String contractAgent) {
		this.contractAgent = contractAgent;
	}

	public void stop(String agentName) {
		addAgreement(agentName, false);
	}

	public Set<String> getStakeholderAgents() {
		Set<String> result = new HashSet<>();
		try {
			result.addAll(getProducerAgents());
			result.add(consumerAgent);
			result.add(contractAgent);
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		return result;
	}

	public boolean isStackholder(String agentName) {
		return getStakeholderAgents().contains(agentName);
	}

	private void addAgreement(String agentName, boolean isOK) {
		if (isStackholder(agentName)) {
			if (isOK) {
				this.agreements.add(agentName);
				this.disagreements.remove(agentName);
			} else {
				this.disagreements.add(agentName);
				this.agreements.remove(agentName);
			}
		}
	}

	public Set<String> getAgreements() {
		return agreements;
	}

	public void setAgreements(Set<String> agreements) {
		this.agreements = agreements;
	}

	public Set<String> getDisagreements() {
		return disagreements;
	}

	public void setDisagreements(Set<String> disagreement) {
		this.disagreements = disagreement;
	}

	public void addProducerAgreement(ContractAgent contractAgent, String producerName, boolean isOK) {
		if (contractAgent != null) {
			addAgreement(producerName, isOK);
		}
	}

	public void addAgreement(SapereAgent agent, boolean isOK) {
		addAgreement(agent.getAgentName(), isOK);
	}

	public boolean hasAgreement(String agentName) {
		return agreements.contains(agentName);
	}

	public boolean hasDisagreement(String agentName) {
		return disagreements.contains(agentName);
	}

	public Date getValidationDeadline() {
		return validationDeadline;
	}

	public void setValidationDeadline(Date validationDeadline) {
		this.validationDeadline = validationDeadline;
	}

	public boolean hasAllAgreements() {
		/*
		 * if(this.hasExpired()) { return false; }
		 */
		if (disagreements.size() > 0) {
			return false;
		}
		for (String agent : this.getStakeholderAgents()) {
			if (!agreements.contains(agent)) {
				return false;
			}
		}
		return true;
	}

	public boolean hasDisagreement() {
		return disagreements.size() > 0;
	}

	public boolean waitingValidation() {
		return disagreements.size() == 0 && !hasAllAgreements();
	}

	public boolean hasSameAgrrements(Contract other) {
		return this.agreements.equals(other.getAgreements()) && this.disagreements.equals(other.getDisagreements());
	}

	public Set<String> getContractors() {
		Set<String> result = new HashSet<String>();
		result.add(this.consumerAgent);
		for (String producer : this.mapPower.keySet()) {
			result.add(producer);
		}
		return result;
	}

	public Set<String> getContractorNoConfirmed() {
		Set<String> result = new HashSet<String>();
		for (String agentName : getContractors()) {
			if (!this.agreements.contains(agentName)) {
				result.add(agentName);
			}
		}
		return result;
	}

	public String getStatus() {
		if (this.hasExpired()) {
			return "Expired";
		} else if (this.hasDisagreement()) {
			return "Canceled";
		} else if (this.hasAllAgreements()) {
			return "Confirmed";
		} else if (this.waitingValidation()) {
			return "Waiting " + getContractorNoConfirmed();
		}
		return "";
	}

	public String formatContent(boolean restricted) {
		StringBuffer result = new StringBuffer();
		result.append(restricted ? "(Restricted contract) " : "(Contract) ").append(this.consumerAgent);
		if (!restricted) {
			result.append(" : ").append(SapereUtil.formaMapValues(this.mapPower));
		}
		result.append(" Sum W= " + SapereUtil.df.format(this.computePower())).append(" From ")
				.append(beginDate==null?"" : SapereUtil.format_time.format(this.beginDate))
				.append(" To ")
				.append(endDate==null?"" : SapereUtil.format_time.format(this.endDate))
				.append(" status:").append(getStatus());
		if (restricted && waitingValidation() && validationDeadline != null) {
			try {
				result.append(" validationDeadline : ").append(SapereUtil.format_time.format(this.validationDeadline));
				result.append(" validationHasExpired :").append(this.validationHasExpired());
			} catch (Throwable e) {
				SapereLogger.getInstance().error(e);
			}
		}
		return result.toString();
	}

	public String toString() {
		return formatContent(false);
	}

	public boolean validationHasExpired() {
		if (this.waitingValidation() && this.validationDeadline != null) {
			Date current = new Date();
			return current.after(this.validationDeadline);
		}
		return false;
	}

	@Override
	public Contract clone() throws CloneNotSupportedException {
		GlobalOffer globalOffer = (GlobalOffer) super.clone();
		Date cloneValidationDealine = (this.validationDeadline == null) ? null : new Date(validationDeadline.getTime());
		Contract reuslt = new Contract(this.contractAgent, globalOffer, cloneValidationDealine);
		Set<String> copyAgreements = new HashSet<>();
		for (String next : agreements) {
			copyAgreements.add(next);
		}
		reuslt.setAgreements(copyAgreements);
		Set<String> copyDisagreements = new HashSet<>();
		for (String next : disagreements) {
			copyDisagreements.add(next);
		}
		reuslt.setDisagreements(copyDisagreements);
		return reuslt;
	}

	public void modifyPower(float powerToSet) {
		float currentPower = this.computePower();
		if (powerToSet < currentPower) {
			Map<String, Float> oldMapPower = cloneMapPower();
			float discountFactor = powerToSet / currentPower;
			for (String producer : this.mapPower.keySet()) {
				float newPower = SapereUtil.round(discountFactor * this.mapPower.get(producer),2);
				mapPower.put(producer, newPower);
			}
			float delta = powerToSet - this.computePower();
			if(delta!=0 && Math.abs(delta)>=0.0001) {
				List<String> producerList = new ArrayList<String>();
				for(String producer : this.mapPower.keySet()) {
					float oldValue = oldMapPower.get(producer);
					if(mapPower.get(producer) + delta <= oldValue) {
						producerList.add(producer);
					}
				}
				if(producerList.size()>0) {
					// Chose one random producer to modify
					Collections.shuffle(producerList);
					String producer = producerList.get(0);
					float newPower = mapPower.get(producer) + delta;
					mapPower.put(producer, newPower);
				}
			}
		}
	}

	public boolean hasChanged(Contract newContent) {
		if (newContent == null) {
			return true;
		}
		if (!this.consumerAgent.equals(newContent.getConsumerAgent())) {
			return true;
		}
		if (!this.contractAgent.equals(newContent.getContractAgent())) {
			return true;
		}
		if (mapPower.size() != newContent.getMapPower().size()) {
			return true;
		}
		if (Math.abs(computePower() - newContent.computePower()) >= 0.0001) {
			return true;
		}
		if (!this.agreements.equals(newContent.getAgreements())) {
			return true;
		}
		if (!this.disagreements.equals(newContent.getDisagreements())) {
			return true;
		}
		for (String producer : mapPower.keySet()) {
			Float power = mapPower.get(producer);
			if (!newContent.getMapPower().containsKey(producer)) {
				return true;
			}
			Float newPower = newContent.getMapPower().get(producer);
			if (Math.abs(power - newPower) >= 0.0001) {
				return true;
			}
		}
		return false;
	}
}
