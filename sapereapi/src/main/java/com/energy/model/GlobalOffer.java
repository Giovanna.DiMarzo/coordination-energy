package com.energy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.energy.agent.ConsumerAgent;
import com.energy.util.SapereLogger;
import com.sapereapi.model.SapereUtil;

import eu.sapere.middleware.node.NodeManager;

public class GlobalOffer implements Cloneable, Serializable {
	private static final long serialVersionUID = 10L;
	protected String consumerAgent;
	protected EnergyRequest request;
	protected Date beginDate;
	protected Date endDate;
	protected String consumerDeviceName;
	protected DeviceCategory consumerDeviceCategory;
	protected Map<String, Float> mapPower;
	protected Map<String, String> mapLocation;
	protected List<Long> singleOffersIds = new ArrayList<Long>();

	public GlobalOffer() {
		super();
		this.mapPower = new HashMap<String, Float>();
		this.mapLocation = new HashMap<String, String>();
		this.singleOffersIds =  new ArrayList<Long>();
	}

	public GlobalOffer(ConsumerAgent aConsumerAgent) {
		super();
		this.consumerAgent = aConsumerAgent.getAgentName();
		this.consumerDeviceName = aConsumerAgent.getNeed().getDeviceName();
		this.consumerDeviceCategory = aConsumerAgent.getNeed().getDeviceCategory();
		this.request = aConsumerAgent.getNeed();
		this.beginDate = aConsumerAgent.getNeed().getBeginDate();
		this.endDate = aConsumerAgent.getNeed().getEndDate();
		this.mapPower = new HashMap<String, Float>();
		this.mapLocation = new HashMap<String, String>();
		String consumerLoc = aConsumerAgent.getAuthentication().getAgentLocation();
		this.mapLocation.put(consumerAgent, consumerLoc);
		this.mapLocation.put(aConsumerAgent.getContractAgentName(), consumerLoc);
		this.singleOffersIds =  new ArrayList<Long>();
	}

	public EnergyRequest getRequest() {
		return request;
	}

	public void setRequest(EnergyRequest request) {
		this.request = request;
	}

	public Set<String> getProducerAgents() {
		return mapPower.keySet();
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Map<String, Float> getMapPower() {
		return mapPower;
	}

	public void setMapPower(Map<String, Float> _mapPowers) {
		this.mapPower = _mapPowers;
	}

	public Map<String, String> getMapLocation() {
		return mapLocation;
	}

	public void setMapLocation(Map<String, String> mapLocation) {
		this.mapLocation = mapLocation;
	}

	public void setSingleOffersIds(List<Long> singleOffersIds) {
		this.singleOffersIds = singleOffersIds;
	}

	public String getConsumerDeviceName() {
		return consumerDeviceName;
	}

	public void setConsumerDeviceName(String consumerDeviceName) {
		this.consumerDeviceName = consumerDeviceName;
	}

	public DeviceCategory getConsumerDeviceCategory() {
		return consumerDeviceCategory;
	}

	public void setConsumerDeviceCategory(DeviceCategory consumerDeviceCategory) {
		this.consumerDeviceCategory = consumerDeviceCategory;
	}

	public void addSingleOffer(SingleOffer singleOffer) {
		if (this.consumerAgent.equals(singleOffer.getConsumerAgent()) && !singleOffer.hasExpired(0)) {
			Float missing = request.getPower() - computePower();
			if (missing > 0) {
				Float addedPower = Math.min(missing, singleOffer.getPower());
				if (addedPower > 0) {
					Date nextBeginDate = singleOffer.getBeginDate();
					if (nextBeginDate.after(beginDate)) {
						beginDate = nextBeginDate;
					}
					Date nextEndDate = singleOffer.getEndDate();
					if (nextEndDate.before(endDate)) {
						endDate = nextEndDate;
					}
					mapPower.put(singleOffer.getProducerAgent(), addedPower);
					mapLocation.put(singleOffer.getProducerAgent(), singleOffer.getIssuerLocation());
					if(singleOffer.getId()>0) {
						singleOffersIds.add(singleOffer.getId());
					}
				}
			}
		}
	}

	public String getConsumerAgent() {
		return consumerAgent;
	}

	public String getConsumerLocation() {
		if(request==null) {
			return "";
		}
		return request.getIssuerLocation();
	}

	public void setConsumerAgent(String consumerAgent) {
		this.consumerAgent = consumerAgent;
	}

	public Date getDate() {
		return beginDate;
	}

	public void setDate(Date date) {
		this.beginDate = date;
	}

	public boolean hasExpired() {
		if(this.endDate==null) {
			return false;
		}
		Date current = new Date();
		return current.after(this.endDate);
	}

	public List<Long> getSingleOffersIds() {
		return singleOffersIds;
	}

	public String getSingleOffersIdsStr() {
		StringBuffer result = new StringBuffer();
		String sep="";
		for(Long id : singleOffersIds) {
			result.append(sep).append(id);
			sep=",";
		}
		return result.toString();
	}

	public boolean isActive() {
		Date current = new Date();
		return (!current.before(beginDate)) && current.before(this.endDate);
	}

	public Float computeInitalPower() {
		Float result = new Float(0);
		for (Float nextPower : this.mapPower.values()) {
			result += nextPower;
		}
		return result;
	}

	public Float computePower() {
		Float result = new Float(0);
		if (!hasExpired() && this.mapPower!=null) {
			for (Float nextPower : this.mapPower.values()) {
				result += nextPower;
			}
		}
		return result;
	}

	public Float computePower(String location) {
		Float result = new Float(0);
		if (!hasExpired() && this.mapPower!=null && this.mapLocation!=null ) {
			for (String nextAgent : this.mapPower.keySet()) {
				float nextPower = mapPower.get(nextAgent);
				if(location!=null) {
					String nextLocation = mapLocation.get(nextAgent);
					if(location.equals(nextLocation) && mapPower.containsKey(nextAgent)) {
						result += nextPower;
					}
				} else {
					result += nextPower;
				}
			}
		}
		return result;
	}

	public float computeGap() {
		if(this.request!=null && !hasExpired()) {
			float totalPower = this.computePower();
			return Math.abs(request.getPower() - totalPower);
		}
		return 0;
	}

	public boolean hasGap() {
		float gap=computeGap();
		return gap>=0.0001;
	}

	public int comparePower(Contract other) {
		return (int) (this.computePower() - other.computePower());
	}

	public boolean hasProducer(String agentName) {
		return mapPower.containsKey(agentName);
	}

	public Float getPowerFromAgent(String agent) {
		return this.mapPower.get(agent);
	}

	public String getLocationFromAgent(String agent) {
		return this.mapLocation.get(agent);
	}

	public void checkBeginNotPassed() {
		Date current = new Date();
		if (current.after(beginDate)) {
			beginDate = current;
			 SapereLogger.getInstance().info("date correction : "  + SapereUtil.format_time.format(beginDate)) ;
		}
	}

	public EnergySupply toEnergySupply() {
		return new EnergySupply(this.consumerAgent, NodeManager.getLocation(), this.computePower(), beginDate, endDate, consumerDeviceName, consumerDeviceCategory);
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("GlobalOffer ").append(this.consumerAgent).append(" ").append(this.getProducerAgents())
				.append(" ").append(this.computePower()).append(" From ")
				.append(SapereUtil.format_time.format(this.beginDate)).append(" To ")
				.append(SapereUtil.format_time.format(this.endDate));
		return result.toString();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		GlobalOffer result = new GlobalOffer();
		result.setConsumerAgent(consumerAgent);
		result.setRequest(request.clone());
		result.setBeginDate(beginDate);
		result.setEndDate(endDate);
		result.setConsumerDeviceName(consumerDeviceName);
		result.setConsumerDeviceCategory(consumerDeviceCategory);
		Map<String, Float> mapPowersCopy = new HashMap<>();
		mapPowersCopy.putAll(this.mapPower);
		result.setMapPower(mapPowersCopy);
		HashMap<String, String> mapLocation = new HashMap<>();
		mapLocation.putAll(this.mapLocation);
		result.setMapLocation(mapLocation);
		return result;
	}

	protected Map<String, Float> cloneMapPower() {
		Map<String, Float> mapPowersCopy = new HashMap<>();
		mapPowersCopy.putAll(this.mapPower);
		return mapPowersCopy;
	}
}
