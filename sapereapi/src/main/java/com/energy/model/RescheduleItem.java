package com.energy.model;

import java.util.Date;

import com.sapereapi.model.SapereUtil;

public class RescheduleItem {
	private String agentName;
	private Date stopBegin;
	private Date stopEnd;
	private float power;
	private WarningType warningType;

	public RescheduleItem(String agentName, WarningType warningType, Date stopBegin, Date stopEnd, float power) {
		super();
		this.agentName = agentName;
		this.warningType = warningType;
		this.stopBegin = stopBegin;
		this.stopEnd = stopEnd;
		this.power = power;
	}
	public String getAgentName() {
		return agentName;
	}
	public Date getStopBegin() {
		return stopBegin;
	}
	public Date getStopEnd() {
		return stopEnd;
	}
	public float getPower() {
		return power;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public void setStopBegin(Date stopBegin) {
		this.stopBegin = stopBegin;
	}
	public void setStopEnd(Date stopEnd) {
		this.stopEnd = stopEnd;
	}
	public void setPower(float power) {
		this.power = power;
	}
	public WarningType getWarningType() {
		return warningType;
	}
	public void setWarningType(WarningType warningType) {
		this.warningType = warningType;
	}
	public boolean hasExpired() {
		Date current = new Date();
		return current.after(stopEnd);
	}
	public boolean hasExpired(int marginSec) {
		Date current = new Date();
		return current.after(SapereUtil.shiftDateSec(stopEnd,marginSec));
	}

	@Override
	public RescheduleItem clone() throws CloneNotSupportedException {
		RescheduleItem result = new RescheduleItem(agentName, warningType, stopBegin, stopEnd, power);
		return result;
	}

	public boolean isInSlot(Date aDate) {
		return !aDate.before(stopBegin) && aDate.before(stopEnd);
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer("Reschedule ");
		result.append(this.agentName).append("(");
		result.append(SapereUtil.df.format(power)).append("W) from ");
		result.append(SapereUtil.formatTimeOrDate(stopBegin));
		result.append(" to ");
		result.append(SapereUtil.formatTimeOrDate(stopEnd));
		return result.toString();
		/*
		return "RescheduleItem [agentName=" + agentName + ", stopBegin=" + stopBegin + ", stopEnd=" +   stopEnd
				+ ", power=" + power + ", warningType=" + warningType + "]";
		*/
	}
}
