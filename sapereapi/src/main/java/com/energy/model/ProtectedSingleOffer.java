package com.energy.model;

import java.io.Serializable;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ContractAgent;
import com.energy.agent.ProducerAgent;
import com.energy.util.PermissionException;

import eu.sapere.middleware.agent.AgentAuthentication;

public class ProtectedSingleOffer extends ProtectedObject implements Serializable {
	private static final long serialVersionUID = 7L;
	private SingleOffer singleOffer;

	public ProtectedSingleOffer(SingleOffer offer) {
		super();
		this.singleOffer = offer;
	}

	public boolean hasAccesAsConsumer(ConsumerAgent consumerAgent) {
		return checkAccessAsConsumer(consumerAgent.getAuthentication());
	}

	public boolean hasAccesAsContractAgent(ContractAgent contractAgent) {
		return checkAccessAsContractAgent(contractAgent.getAuthentication());
	}

	public boolean hasAccesAsProducer(ProducerAgent prodAgent) {
		return checkAccessAsProducer(prodAgent.getAuthentication());
	}

	@Override
	boolean checkAccessAsProducer(AgentAuthentication authentication) {
		if (AgentType.PRODUCER.getLabel().equals(authentication.getAgentType())
				&& singleOffer.getProducerAgent().equals((authentication.getAgentName()))) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	@Override
	boolean checkAccessAsConsumer(AgentAuthentication authentication) {
		if (AgentType.CONSUMER.getLabel().equals(authentication.getAgentType())
				&& singleOffer.getConsumerAgent().equals((authentication.getAgentName()))) {
			return checkAuthentication(authentication);
		}
		return false;
	}

	public String getConsumerAgent(ProducerAgent producerAgent) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		return singleOffer.getConsumerAgent();
	}

	public SingleOffer getSingleOffer(ProducerAgent producerAgent) throws PermissionException {
		if (!hasAccesAsProducer(producerAgent)) {
			throw new PermissionException("Access denied for agent " + producerAgent.getAgentName());
		}
		return singleOffer;
	}

	public SingleOffer getSingleOffer(ConsumerAgent consumerAgent) throws PermissionException {
		if (!hasAccesAsConsumer(consumerAgent)) {
			throw new PermissionException("Access denied for agent " + consumerAgent.getAgentName());
		}
		return singleOffer;
	}

	@Override
	boolean checkAccessAsContractAgent(AgentAuthentication authentication) {
		return false;
	}

	@Override
	public ProtectedSingleOffer clone() throws CloneNotSupportedException {
		SingleOffer offerClone = singleOffer.clone();
		return new ProtectedSingleOffer(offerClone);
	}

}
