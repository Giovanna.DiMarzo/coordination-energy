package com.energy.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sapereapi.model.SapereUtil;

public class RegulationWarning implements Cloneable {
	private WarningType type;
	private List<String> destinationAgents;
	private Date date;
	private Date waitingDeadline;
	private Date receptionDeadline;
	private EnergySupply changeRequest;

	public WarningType getType() {
		return type;
	}

	public void setType(WarningType type) {
		this.type = type;
	}

	public List<String> getDestinationAgents() {
		return destinationAgents;
	}

	public void setDestinationAgents(List<String> _destinationAgents) {
		this.destinationAgents = _destinationAgents;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getWaitingDeadline() {
		return waitingDeadline;
	}

	public void setDeadlines(Date deadline) {
		this.waitingDeadline = deadline;
		this.receptionDeadline = deadline;
	}

	public void setWaitingDeadline(Date waitingDeadline) {
		this.waitingDeadline = waitingDeadline;
	}

	public Date getReceptionDeadline() {
		return receptionDeadline;
	}

	public void setReceptionDeadline(Date validityDeadline) {
		this.receptionDeadline = validityDeadline;
	}

	public EnergySupply getChangeRequest() {
		return changeRequest;
	}

	public void setChangeRequest(EnergySupply changeRequest) {
		this.changeRequest = changeRequest;
	}

	public boolean hasWaitingExpired() {
		Date current = new Date();
		return current.after(this.waitingDeadline);
	}

	public boolean hasReceptionExpired() {
		Date current = new Date();
		return current.after(this.receptionDeadline);
	}

	public RegulationWarning(WarningType type, Date date) {
		super();
		this.type = type;
		this.destinationAgents = new ArrayList<String>();
		this.date = date;
	}

	public void addAgent(String agentName) {
		if (!destinationAgents.contains(agentName)) {
			this.destinationAgents.add(agentName);
		}
	}

	public boolean hasAgent(String agentName) {
		return this.destinationAgents.contains(agentName);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer result = new StringBuffer();
		result.append(type.getLabel()).append(" at ").append(SapereUtil.format_time.format(date)).append(" agents ")
				.append(destinationAgents);
		return result.toString();
	}

	@Override
	public RegulationWarning clone() throws CloneNotSupportedException {
		RegulationWarning clone = new RegulationWarning(type, date);
		for (String destAgent : destinationAgents) {
			clone.addAgent(destAgent);
		}
		clone.setReceptionDeadline(receptionDeadline);
		clone.setWaitingDeadline(waitingDeadline);
		if (this.changeRequest != null) {
			clone.setChangeRequest(changeRequest.clone());
		}
		return clone;
	}

}
