package com.energy.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ConfirmationTable implements Serializable {
	private static final long serialVersionUID = 3L;
	private String producer = null;
	private Map<String, ConfirmationItem> table = new HashMap<>();

	public ConfirmationTable(String _producer) {
		super();
		this.producer = _producer;
		this.table = new HashMap<String, ConfirmationItem>();
		;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public void removeConfirmation(String contractAgentName) {
		this.table.remove(contractAgentName);
	}

	public void confirm(String contractAgentName, Boolean value) {
		this.table.put(contractAgentName, new ConfirmationItem(value));
	}

	public Boolean getConfirmation(String contractAgentName) {
		if (table.containsKey(contractAgentName)) {
			ConfirmationItem confirmation = table.get(contractAgentName);
			return confirmation.getIsOK();
		}
		return null;
	}

	public ConfirmationItem getConfirmationItem(String contractAgentName) {
		if (table.containsKey(contractAgentName)) {
			return table.get(contractAgentName);
		}
		return null;
	}

	public void putConfirmationItem(String contractAgentName, ConfirmationItem item) {
		table.put(contractAgentName, item);
	}

	public String getExpiredItemKey() {
		for (String contractAgentName : table.keySet()) {
			ConfirmationItem item = table.get(contractAgentName);
			if (item.hasExpired()) {
				return contractAgentName;
			}
		}
		return null;
	}

	public boolean hasExpiredItem() {
		String key = getExpiredItemKey();
		return (key != null);
	}

	public void cleanExpiredDate() {
		String key = null;
		while ((key = getExpiredItemKey()) != null) {
			table.remove(key);
		}
	}

	public boolean hasProducer(String agentName) {
		return producer!=null && this.producer.equals(agentName);
	}

	public boolean hasContractAgent(String agentName) {
		return table.containsKey(agentName);
	}

	@Override
	public String toString() {
		return producer + "   " + table.toString();
	}

	@Override
	public ConfirmationTable clone() throws CloneNotSupportedException {
		ConfirmationTable result = new ConfirmationTable(this.producer);
		for(String contractAgentName : table.keySet()) {
			ConfirmationItem item = table.get(contractAgentName);
			result.putConfirmationItem(contractAgentName, item.clone());
		}
		return result;
	}
}
