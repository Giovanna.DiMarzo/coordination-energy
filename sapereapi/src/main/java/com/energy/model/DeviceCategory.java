package com.energy.model;

import java.util.ArrayList;
import java.util.List;

public enum DeviceCategory {
	WATER_HEATING("Water Heating", false)
	,HEATING("Heating", false)
	,COOKING("Cooking", false)
	,SHOWERS("Showers", false)
	,WASHING_DRYING("Washing/drying", false)
	,LIGHTING("Lighting", false)
	,AUDIOVISUAL("Audiovisual", false)
	,COLD_APPLIANCES("Cold applicances", false)
	,ICT("ICT", false)
	,OTHER("Other", false)
	,UNKNOWN("Unknown", false)
	// Producer category
	,EXTERNAL_ENG("External supply", true)
	,WIND_ENG("Wind", true)
	,SOLOR_ENG("Solar", true)
	,BIOMASS_ENG("Biomass", true)
	,HYDRO_ENG("Hydro", true)
	;

	private String label;
	private boolean isProducer;

	DeviceCategory(String _label, boolean _isProducer) {
		this.label = _label;
		this.isProducer = _isProducer;
	}

	public String getLabel() {
		return label;
	}

	public boolean isProducer() {
		return isProducer;
	}

	public static DeviceCategory getByLabel(String label) {
		String label2 = (label == null) ? "" : label;
		for (DeviceCategory pLevel : DeviceCategory.values()) {
			if (pLevel.getLabel().equals(label2)) {
				return pLevel;
			}
		}
		return DeviceCategory.UNKNOWN;
	}

	public static DeviceCategory getByName(String name) {
		String name2 = (name == null) ? "" : name;
		for (DeviceCategory deviceCategory : DeviceCategory.values()) {
			if (deviceCategory.name().equals(name2)) {
				return deviceCategory;
			}
		}
		return DeviceCategory.UNKNOWN;
	}

	public static List<String> getLabels() {
		List<String> reuslt = new ArrayList<String>();
		for (DeviceCategory pLevel : DeviceCategory.values()) {
			reuslt.add(pLevel.getLabel());
		}
		return reuslt;
	}

	public static List<DeviceCategory> getList() {
		List<DeviceCategory> reuslt = new ArrayList<DeviceCategory>();
		for (DeviceCategory item : DeviceCategory.values()) {
			reuslt.add(item);
		}
		return reuslt;
	}

	public static List<OptionItem> getOptionList(boolean isProducer) {
		List<OptionItem> reuslt = new ArrayList<OptionItem>();
		for (DeviceCategory item : DeviceCategory.values()) {
			if(item.isProducer == isProducer) {
				reuslt.add(new OptionItem(item.name(), item.getLabel()));
			}
		}
		return reuslt;
	}
}
