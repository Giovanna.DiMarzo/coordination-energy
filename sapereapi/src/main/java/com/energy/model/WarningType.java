package com.energy.model;

public enum WarningType {
		OVER_PRODUCTION("Over production")
	, OVER_PRODUCTION_FORCAST("Over production forcast")
	, OVER_CONSUMPTION("Over consumption")
	, OVER_CONSUMPTION_FORCAST("Over consumption")
	, NOT_IN_SPACE("Not in space")
	, NO_CONTRACT_AGENT("No contract agent")
	, USER_INTERRUPTION("User interruption")
	, CHANGE_REQUEST("Change request");

	private String label;

	WarningType(String _label) {
		this.label = _label;
	}

	public String getLabel() {
		return label;
	}

	public static WarningType getByLabel(String label) {
		String label2 = (label == null) ? "" : label;
		for (WarningType warningType : WarningType.values()) {
			if (warningType.getLabel().equals(label2)) {
				return warningType;
			}
		}
		return null;
	}
}
