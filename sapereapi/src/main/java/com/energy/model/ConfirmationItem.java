package com.energy.model;

import java.io.Serializable;
import java.util.Date;

import com.sapereapi.model.SapereUtil;

public class ConfirmationItem implements Serializable {
	private static final long serialVersionUID = 6L;
	private Boolean isOK;
	private Date date;
	private Date expiryDeadline;

	final static int EXPIRY_SECONDS = 10;

	public Boolean getIsOK() {
		return isOK;
	}

	public void setIsOK(Boolean isOK) {
		this.isOK = isOK;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setExpiryDeadline(Date expiryDeadline) {
		this.expiryDeadline = expiryDeadline;
	}

	public ConfirmationItem(Boolean isOK) {
		super();
		this.isOK = isOK;
		this.date = new Date();
		this.expiryDeadline = SapereUtil.shiftDateSec(date, EXPIRY_SECONDS);
	}

	public Date getExpiryDeadline() {
		return expiryDeadline;
	}

	public boolean hasExpired() {
		Date current = new Date();
		return current.after(expiryDeadline);
	}

	public boolean hasExpired(int marginSec) {
		Date current = new Date();
		return current.after(SapereUtil.shiftDateSec(expiryDeadline,marginSec));
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if(this.hasExpired()) {
			return isOK + " (" +  SapereUtil.format_time.format(date) + " )";
		}
		return ""+isOK ;
	}

	@Override
	public ConfirmationItem clone() throws CloneNotSupportedException {
		ConfirmationItem result = new ConfirmationItem(this.isOK);
		result.setDate(result.getDate());
		result.setExpiryDeadline(result.getExpiryDeadline());
		return result;
	}
}
