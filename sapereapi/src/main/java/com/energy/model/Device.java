package com.energy.model;

import com.sapereapi.model.SapereUtil;

public class Device {
	private Long id;
	private String name;
	private float powerMin;
	private float powerMax;
	private float currentPower;
	private float averageDurationMinutes;
	private int priorityLevel;
	private boolean isProducer;
	private String category;
	private String status;
	private String runningAgentName;

	public final static String STATUS_SLEEPING = "Sleeping";
	public final static String STATUS_RUNNING = "Running";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPowerMin() {
		return powerMin;
	}

	public void setPowerMin(float powerMin) {
		this.powerMin = powerMin;
	}

	public float getPowerMax() {
		return powerMax;
	}

	public void setPowerMax(float powerMax) {
		this.powerMax = powerMax;
	}

	public float getAverageDurationMinutes() {
		return averageDurationMinutes;
	}

	public void setAverageDurationMinutes(float averageDurationMinutes) {
		this.averageDurationMinutes = averageDurationMinutes;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLvel) {
		this.priorityLevel = priorityLvel;
	}

	public boolean isProducer() {
		return isProducer;
	}

	public void setProducer(boolean isProducer) {
		this.isProducer = isProducer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRunningAgentName() {
		return runningAgentName;
	}

	public void setRunningAgentName(String runningAgentName) {
		this.runningAgentName = runningAgentName;
	}

	public float getCurrentPower() {
		return currentPower;
	}

	public void setCurrentPower(float currentPower) {
		this.currentPower = currentPower;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(name);
		result.append("(").append(category).append(")");
		result.append(" W: ").append(SapereUtil.df.format(powerMin)).append("-")
			.append(SapereUtil.df.format(powerMax));
		if(currentPower > 0) {
			result.append(" current : ").append(SapereUtil.round(currentPower,2));
		}
		return result.toString();
	}

	public String toString2() {
		StringBuffer result = new StringBuffer();
		result.append(" W: ").append(SapereUtil.df.format(powerMin)).append("-")
			.append(SapereUtil.df.format(powerMax));
		if(currentPower > 0) {
			result.append(" (current = ").append(SapereUtil.df.format(currentPower)).append(")  ");
		}
		result.append("  ").append(name);
		//result.append("  (").append(category).append(")");
		return result.toString();
	}

	public Device() {
		super();
	}

	public Device(Long id, String name, String category, float powerMin, float powerMax, float averageDurationMinutes) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.powerMin = powerMin;
		this.powerMax = powerMax;
		this.averageDurationMinutes = averageDurationMinutes;
		this.priorityLevel = 0;
		this.status = STATUS_SLEEPING;
		this.runningAgentName = "";
	}

	@Override
	public Device clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Device result = new Device(id, name, category, powerMin, powerMax, averageDurationMinutes);
		result.setCurrentPower(currentPower);
		result.setPriorityLevel(priorityLevel);
		result.setProducer(isProducer);
		//result.setRunningAgentName(runningAgentName.clone);
		return result;
	}

}
