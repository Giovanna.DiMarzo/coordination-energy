package com.sapereapi.model;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.energy.model.Device;
import com.energy.model.HomeTotal;
import com.energy.util.SimulatorLogger;

public class UtilJsonParser {

	public static HomeContent parseHomeContent(JSONObject jsonHomeContent) {
		HomeContent result = new HomeContent(new AgentFilter());
		JSONObject jsonHomeTotal = jsonHomeContent.getJSONObject("total");
		try {
			HomeTotal homeTotal = parseHomeTotal(jsonHomeTotal);
			result.setTotal(homeTotal);
		} catch (Exception e1) {
			SimulatorLogger.getInstance().error(e1);
		}

		JSONArray json_producers = jsonHomeContent.getJSONArray("producers");
		for (int i = 0; i < json_producers.length(); i++) {
			JSONObject jsonAgentForm = json_producers.getJSONObject(i);
			try {
				AgentForm producer = parseAgentForm(jsonAgentForm);
				result.addProducer(producer);
			} catch (Exception e) {
				SimulatorLogger.getInstance().error(e);
			}
		}
		JSONArray json_consumers = jsonHomeContent.getJSONArray("consumers");
		for (int i = 0; i < json_consumers.length(); i++) {
			JSONObject jsonAgentForm = json_consumers.getJSONObject(i);
			try {
				AgentForm consumer = parseAgentForm(jsonAgentForm);
				result.addConsumer(consumer);
			} catch (Exception e) {
				SimulatorLogger.getInstance().error(e);
			}
		}
		return result;
	}

	public static Map<String, Float> parseJsonMap(JSONObject jsonobj) throws Exception {
		Map<String, Float> map = new HashMap<String, Float>();
		Iterator<String> keys = jsonobj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Float value = (float) jsonobj.getDouble(key);
			map.put(key, value);
		}
		return map;
	}

	public static Map<String, Double> parseJsonMap2(JSONObject jsonobj) throws Exception {
		Map<String, Double> map = new HashMap<String, Double>();
		Iterator<String> keys = jsonobj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Double value = jsonobj.getDouble(key);
			map.put(key, value);
		}
		return map;
	}

	public static Device parseDevice(JSONObject jsonDevice) throws Exception {
		Device device = new Device();
		parseJSONObject(device, jsonDevice);
		return device;
		/*
		Long id = jsonDevice.getLong("id");
		String name = jsonDevice.getString("name");
		String category = jsonDevice.getString("category");
		Float powerMin =  (float) jsonDevice.getDouble("powerMin");
		Float powerMax = (float) jsonDevice.getDouble("powerMax");
		Float averageDurationMinutes = (float) jsonDevice.getDouble("averageDurationMinutes");
		Device device = new Device(id, name, category, powerMin, powerMax, averageDurationMinutes);
		device.setPriorityLevel(jsonDevice.getInt("priorityLevel"));
		Boolean isProducer = jsonDevice.getBoolean("producer");
		device.setProducer(isProducer);
		return device;
		*/
	}


	public static Object parseJSONObject(Object targetObject, JSONObject jsonObject)  {
		Class<?> targetObjectClass = targetObject.getClass();
		for(Method method : targetObjectClass.getMethods())  {
			if(1==method.getParameterCount() && method.getName().startsWith("set")) {
				Parameter param = method.getParameters()[0];
				String fieldName =  SapereUtil.firstCharToLower(method.getName().substring(3));
				if(jsonObject.has(fieldName) && !jsonObject.isNull(fieldName))  {
					Class<?> paramType = param.getType();
					Object valueToSet = null;
					try {
						if(paramType.equals(String.class)) {
							valueToSet = jsonObject.getString(fieldName);
						} else if(paramType.equals(Double.class) || paramType.equals(double.class)) {
							valueToSet = jsonObject.getDouble(fieldName);
						} else if(paramType.equals(Float.class) || paramType.equals(float.class)) {
							Double dbValue = jsonObject.getDouble(fieldName);
							if(dbValue !=null) {
								valueToSet = dbValue.floatValue();
							}
						} else if(paramType.equals(Integer.class) || paramType.equals(int.class)) {
							valueToSet = (int) jsonObject.getInt(fieldName);
						} else if(paramType.equals(Long.class) || paramType.equals(long.class)) {
							valueToSet = (long) jsonObject.getLong(fieldName);
						} else if(paramType.equals(Boolean.class) || paramType.equals(boolean.class))  {
							valueToSet = (boolean) jsonObject.getBoolean(fieldName);
						}else if(paramType.equals(Date.class))  {
							valueToSet = parseJsonDate(jsonObject.getString(fieldName));
						}
					} catch (Throwable e) {
						SimulatorLogger.getInstance().error(e);
					}
					if(valueToSet!=null)  {
						try {
							//String toto = valueToSet.toString();
							method.invoke(targetObject, valueToSet);
						} catch (Throwable e) {
							SimulatorLogger.getInstance().error(e);
						}

					}
				}
			}
		}
		return targetObject;
	}

	public static AgentForm parseAgentForm(JSONObject jsonAgent) throws Exception {
		AgentForm agentForm = new AgentForm();
		parseJSONObject(agentForm, jsonAgent);
		Map<String, Float> offersRepartition2 = parseJsonMap(jsonAgent.getJSONObject("offersRepartition"));
		agentForm.setOffersRepartition(offersRepartition2);
		Map<String, Float> contractsRepartition2 = parseJsonMap(jsonAgent.getJSONObject("contractsRepartition"));
		agentForm.setContractsRepartition(contractsRepartition2);
		return agentForm;
		/*
		AgentForm agent = new AgentForm();
		agent.setPower((float) jsonAgent.getDouble("power"));
		agent.setAgentName(jsonAgent.getString("agentName"));
		agent.setId(jsonAgent.getInt("id"));
		agent.setBeginDate(parseJsonDate(jsonAgent.getString("beginDate")));
		agent.setEndDate(parseJsonDate(jsonAgent.getString("endDate")));
		agent.setDuration((float) jsonAgent.getDouble("duration"));
		agent.setUrl(jsonAgent.getString("url"));
		agent.setHasExpired(jsonAgent.getBoolean("hasExpired"));
		agent.setIsDisabled(jsonAgent.getBoolean("isDisabled"));
		agent.setIsSatisfied(jsonAgent.getBoolean("isSatisfied"));
		agent.setIsInSpace(jsonAgent.getBoolean("isInSpace"));
		agent.setAgentType(jsonAgent.getString("agentType"));
		agent.setDeviceName(jsonAgent.getString("deviceName"));
		agent.setDeviceCategoryCode(jsonAgent.getString("deviceCategoryCode"));
		agent.setDeviceCategoryLabel(jsonAgent.getString("deviceCategoryLabel"));
		Map<String, Float> offersRepartition = parseJsonMap(jsonAgent.getJSONObject("offersRepartition"));
		agent.setOffersRepartition(offersRepartition);
		Map<String, Float> contractsRepartition = parseJsonMap(jsonAgent.getJSONObject("contractsRepartition"));
		agent.setContractsRepartition(contractsRepartition);
		agent.setAgentName(jsonAgent.getString("agentName"));
		agent.setContractsTotal((float) jsonAgent.getDouble("contractsTotal"));
		agent.setAvailablePower((float) jsonAgent.getDouble("availablePower"));
		agent.setDisabledPower((float) jsonAgent.getDouble("disabledPower"));
		agent.setDelayToleranceMinutes((float) jsonAgent.getDouble("delayToleranceMinutes"));
		if (true || AgentType.CONSUMER.getLabel().equals(agent.getAgentType())) {
			agent.setPriorityLevel(jsonAgent.getString("priorityLevel"));
			agent.setDelayToleranceMinutes((float) jsonAgent.getDouble("delayToleranceMinutes"));
		}
		boolean testEquals = agent.equals(agent2);
		if(!testEquals) {
			System.out.print("UtilJsonParser.parseAgentForm for debug");
			testEquals = agent.equals(agent2);
		}
		return agent;
		*/
	}

	private static HomeTotal parseHomeTotal(JSONObject jsonHomeTotal) throws Exception {
		HomeTotal homeTotal = new HomeTotal();
		parseJSONObject(homeTotal, jsonHomeTotal);
		/*
		if (!jsonHomeTotal.isNull("agentName")) {
			homeTotal.setAgentName(jsonHomeTotal.getString("agentName"));
		}
		homeTotal.setDate(parseJsonDate(jsonHomeTotal.getString("date")));
		homeTotal.setAvailable((float) jsonHomeTotal.getDouble("available"));
		homeTotal.setConsumed((float) jsonHomeTotal.getDouble("consumed"));
		if (!jsonHomeTotal.isNull("provided")) {
			homeTotal.setProvided((float) jsonHomeTotal.getDouble("provided"));
		}
		homeTotal.setMissing((float) jsonHomeTotal.getDouble("missing"));
		homeTotal.setProduced((float) jsonHomeTotal.getDouble("produced"));
		homeTotal.setRequested((float) jsonHomeTotal.getDouble("requested"));
		if (!jsonHomeTotal.isNull("location")) {
			homeTotal.setLocation(jsonHomeTotal.getString("location"));
		}*/
		return homeTotal;
	}

	private static Date parseJsonDate(String jsonDate) throws Exception {
		String testDate = jsonDate.substring(0, 19).replace('T', ' ');
		Date test = SapereUtil.format_sql.parse(testDate);
		return test;
	}

}
