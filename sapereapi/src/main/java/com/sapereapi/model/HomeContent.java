package com.sapereapi.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ProducerAgent;
import com.energy.model.AgentType;
import com.energy.model.DeviceCategory;
import com.energy.model.HomeTotal;
import com.energy.model.OptionItem;
import com.energy.model.PriorityLevel;
import com.energy.util.SapereLogger;

public class HomeContent {
	private AgentFilter filter;
	private List<AgentForm> consumers;
	private List<AgentForm> producers;
	private List<OptionItem> listPriorityLevel;
	private List<OptionItem> listDeviceCategoryConsumer;
	private List<OptionItem> listDeviceCategoryProducer;
	private List<OptionItem> listYesNo;
	private HomeTotal total;
	private List<String> warnings;
	private List<String> errors;
	private boolean noFilter = true;

	public List<AgentForm> getConsumers() {
		return consumers;
	}

	public void setConsumers(List<AgentForm> consumers) {
		this.consumers = consumers;
	}

	public List<AgentForm> getProducers() {
		return producers;
	}

	public void setProducers(List<AgentForm> producers) {
		this.producers = producers;
	}

	public List<OptionItem> getListPriorityLevel() {
		return listPriorityLevel;
	}

	public void setListPriorityLevel(List<OptionItem> listPriorityLevel) {
		this.listPriorityLevel = listPriorityLevel;
	}

	public List<OptionItem> getListDeviceCategoryConsumer() {
		return listDeviceCategoryConsumer;
	}

	public void setListDeviceCategoryConsumer(List<OptionItem> listDeviceCategoryConsumer) {
		this.listDeviceCategoryConsumer = listDeviceCategoryConsumer;
	}

	public List<OptionItem> getListDeviceCategoryProducer() {
		return listDeviceCategoryProducer;
	}

	public void setListDeviceCategoryProducer(List<OptionItem> listDeviceCategoryProducer) {
		this.listDeviceCategoryProducer = listDeviceCategoryProducer;
	}

	public HomeTotal getTotal() {
		return total;
	}

	public void setTotal(HomeTotal total) {
		this.total = total;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

	public AgentFilter getFilter() {
		return filter;
	}

	public void setFilter(AgentFilter filter) {
		this.filter = filter;
	}

	public List<OptionItem> getListYesNo() {
		return listYesNo;
	}

	public void setListYesNo(List<OptionItem> listYesNo) {
		this.listYesNo = listYesNo;
	}

	public List<String> getProducerNames() {
		List<String> producerNames = new ArrayList<String>();
		for(AgentForm producer : producers) {
			producerNames.add(producer.getAgentName());
		}
		return producerNames;
	}

	public List<String> getConsumerNames() {
		List<String> consumerNames = new ArrayList<String>();
		for(AgentForm consumer : consumers) {
			consumerNames.add(consumer.getAgentName());
		}
		return consumerNames;
	}

	public HomeContent(AgentFilter filter) {
		super();
		consumers = new ArrayList<AgentForm>();
		producers = new ArrayList<AgentForm>();
		errors = new ArrayList<String>();
		warnings = new ArrayList<String>();
		total = new HomeTotal();
		listPriorityLevel = PriorityLevel.getOptionList();
		listDeviceCategoryConsumer = DeviceCategory.getOptionList(false);
		listDeviceCategoryProducer = DeviceCategory.getOptionList(true);
		listYesNo = new ArrayList<OptionItem>();
		listYesNo.add(new OptionItem("", " "));
		listYesNo.add(new OptionItem("YES", "Yes"));
		listYesNo.add(new OptionItem("NO", "No"));
	}

	// AgentForm comparator : by id
	private final Comparator<AgentForm> agentComparator = new Comparator<AgentForm>() {
		public int compare(AgentForm o1, AgentForm o2) {
			return o1.compareTo(o2);
		}
	};

	public void addConsumer(ConsumerAgent consumer, boolean isInSapce, boolean isContractAgentInSpace) {
		AgentForm newConsumer = new AgentForm(consumer, isInSapce, isContractAgentInSpace);
		this.consumers.add(newConsumer);
	}

	public void addConsumer(AgentForm newConsumer) {
		this.consumers.add(newConsumer);
	}

	public void addProducer(ProducerAgent producer, boolean isInSapce) {
		AgentForm newProducer = new AgentForm(producer, isInSapce);
		this.producers.add(newProducer);
	}

	public void addProducer(AgentForm newProducer) {
		this.producers.add(newProducer);
	}

	public void sortAgents() {
		Collections.sort(consumers, agentComparator);
		Collections.sort(producers, agentComparator);
	}

	public AgentForm getProducer(String agentName) {
		if (agentName == null) {
			return null;
		}
		for (AgentForm producer : this.producers) {
			if (agentName.equals(producer.getAgentName())) {
				return producer;
			}
		}
		return null;
	}

	public AgentForm getConsumer(String agentName) {
		if (agentName == null) {
			return null;
		}
		for (AgentForm consumer : this.consumers) {
			if (agentName.equals(consumer.getAgentName())) {
				return consumer;
			}
		}
		return null;
	}

	public AgentForm getAgent(String agentName) {
		AgentForm agent = getConsumer(agentName);
		if (agent != null) {
			return agent;
		}
		agent = getProducer(agentName);
		return agent;
	}

	private int getNbActiveAgents(List<AgentForm> listAgents, boolean expired) {
		int reuslt = 0;
		for (AgentForm producer : listAgents) {
			if (expired == producer.getHasExpired()) {
				reuslt++;
			}
		}
		return reuslt;
	}

	public int getNbActiveAgents(AgentType agentType) {
		if (AgentType.CONSUMER.equals(agentType)) {
			return getNbActiveAgents(consumers, false);
		} else if (AgentType.PRODUCER.equals(agentType)) {
			return getNbActiveAgents(producers, false);
		}
		return 0;
	}

	public int getNbExpiredAgents(AgentType agentType) {
		if (AgentType.CONSUMER.equals(agentType)) {
			return getNbActiveAgents(consumers, true);
		} else if (AgentType.PRODUCER.equals(agentType)) {
			return getNbActiveAgents(producers, true);
		}
		return 0;
	}

	public AgentForm getFirstInactiveConsumer() {
		for (AgentForm consumer : consumers) {
			if (consumer.getHasExpired()) {
				return consumer;
			}
		}
		return null;
	}

	public AgentForm getFirstInactiveProducer() {
		for (AgentForm producer : producers) {
			if (producer.getHasExpired()) {
				return producer;
			}
		}
		return null;
	}

	public AgentForm getRandomInactiveProducer(List<AgentForm> listAgents) {
		List<AgentForm> agents = new ArrayList<AgentForm>();
		for (AgentForm producer : listAgents) {
			if (producer.getHasExpired()) {
				agents.add(producer);
			}
		}
		if (agents.size() > 0) {
			Collections.shuffle(agents);
			return agents.get(0);
		}
		return null;
	}

	public AgentForm getRandomInactiveProducer() {
		return getRandomInactiveProducer(producers);
	}

	public AgentForm getRandomInactiveConsumer() {
		return getRandomInactiveProducer(consumers);
	}

	public boolean isNoFilter() {
		return noFilter;
	}

	public void setNoFilter(boolean noFilter) {
		this.noFilter = noFilter;
	}

	public HomeTotal getPartialTotal(String deviceCategory) {
		HomeTotal result = new HomeTotal();
		Float requested = new Float(0);
		Float produced = new Float(0);
		Float consumed = new Float(0);
		Float provided = new Float(0);
		Float sentOffersTotal = new Float(0);
		Float receivedOffersTotal = new Float(0);
		// Map<String, Float> receivedOffersRepartition = new HashMap<String, Float>();
		// // By Producers
		for (AgentForm consumer : consumers) {
			if (!consumer.getHasExpired()) {
				if (deviceCategory == null || deviceCategory.equals(consumer.getDeviceCategoryCode())) {
					requested += consumer.getPower();
					consumed += consumer.getContractsTotal();
					receivedOffersTotal += consumer.getOffersTotal();
				}
			}
		}
		for (AgentForm producer : producers) {
			if (!producer.getHasExpired()) {
				if (deviceCategory == null || deviceCategory.equals(producer.getDeviceCategoryCode())) {
					produced += producer.getPower();
					provided += producer.getContractsTotal();
					sentOffersTotal += producer.getOffersTotal();
				}
			}
		}
		result.setReceivedOffersTotal(receivedOffersTotal);
		result.setSentOffersTotal(sentOffersTotal);
		result.setRequested(requested);
		result.setProduced(produced);
		result.setConsumed(consumed);
		result.setAvailable(Math.max(0, produced - consumed));
		result.setProvided(provided);
		result.setMissing(Math.max(0,requested - consumed));
		return result;
	}

	public List<AgentForm> getAgents() {
		List<AgentForm> result = new ArrayList<AgentForm>();
		result.addAll(consumers);
		result.addAll(producers);
		return result;
	}

	public Map<String, AgentForm> getMapRunningAgents() {
		Map<String, AgentForm> result = new HashMap<String, AgentForm>();
		for (AgentForm agentForm : getAgents()) {
			if (agentForm.isRunning()) {
				result.put(agentForm.getDeviceName(), agentForm);
			}
		}
		return result;
	}

	public AgentForm getAgentByDeviceName(String deviceName) {
		if(deviceName==null) {
			return null;
		}
		for(AgentForm agentForm : getAgents()) {
			if(deviceName.equals(agentForm.getDeviceName())) {
				return agentForm;
			}
		}
		return null;
	}

	public boolean hasDevice(String deviceName) {
		if(deviceName==null) {
			return false;
		}
		for(AgentForm agentForm : getAgents()) {
			if(deviceName.equals(agentForm.getDeviceName())) {
				return true;
			}
		}
		return false;
	}

	public void computeTotal() {
		total = new HomeTotal();
		Float requested = new Float(0);
		Float produced = new Float(0);
		Float consumed = new Float(0);
		Float consumedLocally = new Float(0);
		Float provided = new Float(0);
		Float providedLocally = new Float(0);
		Float sentOffersTotal = new Float(0);
		Float receivedOffersTotal = new Float(0);
		try {
			Map<String, Float> receivedOffersRepartition = new HashMap<String, Float>(); // By Producers
			for (AgentForm consumer : consumers) {
				if (!consumer.getHasExpired()) {
					requested += consumer.getPower();
					consumed += consumer.getContractsTotal();
					consumedLocally += consumer.getContractsTotalLocal();
					float testDelta = Math.abs(consumer.getContractsTotal() - consumer.getContractsTotalLocal());
					if(testDelta > 0.01) {
						SapereLogger.getInstance().info("For debug : " + consumer.getAgentName() + " provided <> providedLocally : delta = " + testDelta);
					}
					receivedOffersTotal += consumer.getOffersTotal();
					// update received offers repartition
					for (String producer : consumer.getOffersRepartition().keySet()) {
						Float toAdd = consumer.getOffersRepartition().get(producer);
						if (!receivedOffersRepartition.containsKey(producer)) {
							receivedOffersRepartition.put(producer, new Float(0));
						}
						Float power = receivedOffersRepartition.get(producer);
						receivedOffersRepartition.put(producer, power + toAdd);
					}
				}
			}
			// total.setReceivedOffersRepartition(SapereUtil.formaMapValues(receivedOffersRepartition));
			List<String> filteredConsumers = this.getConsumerNames();
			List<String> filteredProducers = this.getProducerNames();
			//noFilter = false;
			total.setReceivedOffersRepartition(receivedOffersRepartition);
			Map<String, Float> sentOffersRepartition = new HashMap<String, Float>(); // By consumers
			for (AgentForm producer : producers) {
				if (!producer.getHasExpired()) {
					produced += producer.getPower();
					provided += producer.getContractsTotal();
					providedLocally += producer.getContractsTotalLocal();
					sentOffersTotal += producer.getOffersTotal();
					// update sent offers repartition
					for (String consumer : producer.getOffersRepartition().keySet()) {
						if(noFilter || filteredConsumers.contains(consumer)) {
							Float toAdd = producer.getOffersRepartition().get(consumer);
							if (!sentOffersRepartition.containsKey(consumer)) {
								sentOffersRepartition.put(consumer, new Float(0));
							}
							Float power = sentOffersRepartition.get(consumer);
							sentOffersRepartition.put(consumer, power + toAdd);
						}
					}
					for (String consumerName : producer.getWaitingContractsConsumers()) {
						AgentForm consumer = this.getConsumer(consumerName);
						if(noFilter || filteredConsumers.contains(consumerName)) {
							if (consumer.getIsSatisfied()) {
								// Add warning
								String warningMsg = "The valided contract of " + consumerName
										+ " is still in waiting status in " + producer.getAgentName() + " LSA.";
								warningMsg += "(Sum power:" + SapereUtil.df.format(producer.getWaitingContractsPower()) + ")";
								if (!warnings.contains(warningMsg)) {
									warnings.add(warningMsg);
								}
							}
						}
						if(consumer!=null && consumer.getIsInSpace() && !consumer.getIsContractAgentInSpace()) {
							String errorMsg = "The agent " + consumer.getContractAgentName() + " is not in space";
							if(!errors.contains(errorMsg)) {
								errors.add(errorMsg);
							}
						}
					}
					for (String consumerName : producer.getContractsRepartition().keySet()) {
						if(noFilter || filteredConsumers.contains(consumerName)) {
							AgentForm consumer = this.getConsumer(consumerName);
							float consumerContribution = producer.getContractsRepartition().get(consumerName);
							if (consumer!=null && !consumer.getIsSatisfied()) {
								// Add warning
								String warningMsg = "The contract of " + consumerName + " is already valid in "
										+ producer.getAgentName() + " lsa but not valid in " + consumerName + " lsa.";
								warningMsg += " (Contribution : " + SapereUtil.df.format(consumerContribution) + ")";
								if (!warnings.contains(warningMsg)) {
									warnings.add(warningMsg);
								}
							}
						}
					}
				}
			}
			for (AgentForm consumer : consumers) {
				// Check request = contract
				if(consumer.getIsSatisfied() && consumer.getEnergyRequest()!=null) {
					String consumerName = consumer.getAgentName();
					float totalContract = consumer.getContractsTotal();
					float requestPower = consumer.getEnergyRequest().getPower();
					if( totalContract>0 && Math.abs( totalContract - requestPower) >= 0.001) {
						String warningMsg = "The total supplied to " + consumerName + " (" + SapereUtil.df.format(totalContract) + " W)"
								+ " does not correspond to the requested power : " + SapereUtil.df.format(requestPower) + " W.";
						if (!warnings.contains(warningMsg)) {
							warnings.add(warningMsg);
						}
					}
					for(String producerName : consumer.getContractsRepartition().keySet()) {
						if(filteredProducers.contains(producerName)) {
							Float providedLsaConsumer = consumer.getContractsRepartition().get(producerName);
							AgentForm producer = this.getProducer(producerName);
							if(producer!=null) {
								float providedLsaProducer = 0;
								if(producer.getContractsRepartition()!=null && producer.getContractsRepartition().containsKey(consumerName)) {
									providedLsaProducer = producer.getContractsRepartition().get(consumerName);
								}
								float gap = Math.abs( providedLsaProducer - providedLsaConsumer);
								if(gap >= 0.001) {
									String warningMsg = "The power supplied by " + producerName  + " to " + consumerName
											+ " has differents value in consumer lsa (" + SapereUtil.df.format(providedLsaConsumer) + " W) " 
											+ " and in producer lsa (" + SapereUtil.df.format(providedLsaProducer) + " W. )" 
											+ " gap = " + SapereUtil.df.format(gap);
									if (!warnings.contains(warningMsg)) {
										warnings.add(warningMsg);
									}
								}
							}
						}
					}
				}
			}
			/*
			 * for (AgentForm consumer : consumers) { Map<String, Float>
			 * contractsRepartition = consumer.getContractsRepartition(); }
			 */
			total.setSentOffersRepartition(sentOffersRepartition);
			// total.setSentOffersRepartition(SapereUtil.filterRepartition(sentOffersRepartition, filteredProducers));
			total.setDate(new Date());
			total.setRequested(requested);
			total.setProduced(produced);
			total.setConsumedLocally(consumedLocally);
			total.setConsumed(consumed);
			total.setAvailable(produced - consumed);
			total.setProvided(provided);
			total.setProvidedLocally(providedLocally);
			total.setMissing(requested - consumed);
			total.setSentOffersTotal(sentOffersTotal);
			total.setReceivedOffersTotal(receivedOffersTotal);
			if(noFilter) {
				float delta = Math.abs(consumedLocally.floatValue() - providedLocally.floatValue());
				if (consumedLocally != null && providedLocally != null && delta >= 0.01) {
					errors.add("Total locally consumed power " + SapereUtil.df.format(consumedLocally)
							+ " is not equals to the total power locally provided by producers : " + SapereUtil.df.format(providedLocally)
							+ " gap=" + SapereUtil.df.format(delta));
				} else {
					// errors.add("OK");
				}
			}
		} catch (Throwable e) {
			SapereLogger.getInstance().error(e);
			e.printStackTrace();
		}
	}
}
