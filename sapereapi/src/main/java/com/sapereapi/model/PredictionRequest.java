package com.sapereapi.model;

import java.util.Date;

public class PredictionRequest {
	Date initDate;
	Date targetDate;
	String location;

	public Date getInitDate() {
		return initDate;
	}

	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public PredictionRequest() {
		super();
	}

}
