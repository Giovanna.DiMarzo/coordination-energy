package com.sapereapi.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;

import com.energy.agent.ProducerAgent;
import com.energy.model.EnergyRequest;
import com.energy.model.PriorityLevel;
import com.energy.model.ProtectedContract;
import com.energy.model.RegulationWarning;
import com.energy.model.SingleOffer;
import com.energy.util.PermissionException;
import com.energy.util.SapereLogger;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;

public class SapereUtil {
	public final static SimpleDateFormat format_date_time = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	public final static SimpleDateFormat format_time = new SimpleDateFormat("HH:mm:ss");
	public final static SimpleDateFormat format_sql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public final static SimpleDateFormat format_day = new SimpleDateFormat("yyyyMMdd");
	public final static SimpleDateFormat format_sessionid = new SimpleDateFormat("yyyyMMdd_HHmmss");

	public final static DecimalFormat df = new DecimalFormat("#.##");
	public final static DecimalFormat df2 = new DecimalFormat("#.#####");

	public final static int MS_IN_MINUTE = 1000 * 60;
	public final static int MS_IN_HOUR = 1000 * 60 * 60;
	public final static int EVENT_INIT_DECAY = 3;

	public static String getCurrentTimeStr() {
		Date current = new Date();
		return format_time.format(current);
	}

	public static String generateSessionId() {
		Random random = new Random();
		int rand3 = random.nextInt(10000);
		return format_sessionid.format(new Date()) + "_" + String.format("%04d", rand3);
	}


	public static Date getNewDate(Integer forcedHourOfDay) {
		if(forcedHourOfDay==null) {
			return new Date();
		}
		Calendar aCalandar = Calendar.getInstance();
		aCalandar.set(Calendar.HOUR_OF_DAY, forcedHourOfDay);
		return aCalandar.getTime();
	}

	public static String formatTimeOrDate(Date aDate) {
		if(aDate==null) {
			return "";
		}
		String currentDay = format_day.format(new Date());
		if (currentDay.equals(format_day.format(aDate))) {
			return format_time.format(aDate);
		} else {
			return format_date_time.format(aDate);
		}
	}

	public static Property getOnePropertyFromLsa(Lsa chosenLSA, String query, String propName) {
		List<Property> listProp = chosenLSA.getPropertiesByQueryAndName(query, propName);
		if (listProp.size() > 0) {
			Collections.shuffle(listProp);
			Property valueProp = listProp.get(0);
			return valueProp;
		}
		return null;
	}

	public static Property getOnePropertyFromLsa(Lsa chosenLSA, String propName) {
		List<Property> listProp = chosenLSA.getPropertiesByName(propName);
		if (listProp.size() > 0) {
			Collections.shuffle(listProp);
			Property valueProp = listProp.get(0);
			return valueProp;
		}
		return null;
	}

	public static Object getOnePropertyValueFromLsa(Lsa chosenLSA, String query, String propName) {
		List<Property> listProp = chosenLSA.getPropertiesByQueryAndName(query, propName);
		if (listProp.size() > 0) {
			Collections.shuffle(listProp);
			Property valueProp = listProp.get(0);
			return valueProp.getValue();
		}
		return null;
	}

	public static boolean hasProperty(Lsa chosenLSA, String query, String propName) {
		List<Property> listProp = chosenLSA.getPropertiesByQueryAndName(query, propName);
		return (listProp.size() > 0);
	}

	/**/
	public static AgentState parseState(String sState) throws Exception {
		AgentState result = new AgentState();
		if ("".equals(sState) || sState == null) {
			// throw new Exception("parseState : empty state given");
			return result;
		}
		String[] splitPipe = sState.split("\\|");
		if (splitPipe.length > 0) {
			String sinputs = splitPipe[0];
			String souputs = (splitPipe.length > 1) ? splitPipe[1] : "";
			StringTokenizer stInput = new StringTokenizer(sinputs, ",");
			while (stInput.hasMoreTokens()) {
				String input = stInput.nextToken();
				if (!"".equals(input)) {
					result.addInput(input);
				}
			}
			StringTokenizer stOutput = new StringTokenizer(souputs, ",");
			while (stOutput.hasMoreTokens()) {
				String output = stOutput.nextToken();
				if (!"".equals(output)) {
					result.addOutput(output);
				}
			}
			return result;
		} else {
			throw new Exception("parseState : wrong state format : the given state ('" + sState
					+ "') must contain one (and only one) pipe character");
		}
	}

	public static String addOutputsToSate(String stateValue, String[] outputs) throws Exception {
		if ("".equals(stateValue)) {
			// Empty state : do nothing
			return stateValue;
		}
		AgentState state = parseState(stateValue);
		for (String output : outputs) {
			state.addOutput(output);
		}
		return state.toString();
	}

	public static String addOutputsToLsaState(Lsa lsa, String[] outputs) throws Exception {
		String stateStr = lsa.getSyntheticProperty(SyntheticPropertyName.STATE).toString();
		AgentState state = parseState(stateStr);
		for (String output : outputs) {
			state.addOutput(output);
		}
		return state.toString();
	}

	public static String implode(List<String> _list, String sep) {
		StringBuffer buffBonds = new StringBuffer();
		String sep2 = "";
		for (String item : _list) {
			buffBonds.append(sep2).append(item);
			sep2 = sep;
		}
		return buffBonds.toString();
	}

	public static String implode(Set<String> _list, String sep) {
		StringBuffer buffBonds = new StringBuffer();
		String sep2 = "";
		for (String item : _list) {
			buffBonds.append(sep2).append(item);
			sep2 = sep;
		}
		return buffBonds.toString();
	}

	public static boolean isInStrArray(String[] strArray, String tofind) {
		for(String next : strArray) {
			if(next.equals(tofind)) {
				return true;
			}
		}
		return false;
	}

	public static Object getNext(Object[] array, int idx) {
		if (array.length == 0) {
			return "";
		}
		int idx2 = idx;
		if (idx2 >= array.length) {
			idx2 = array.length - 1;
		}
		return array[idx2];
	}

	public static Date getCurrentSeconde() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		Date result = new Date(cal.getTimeInMillis());
		return result;
	}

	public static Date getCurrentMinute() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date result = new Date(cal.getTimeInMillis());
		return result;
	}

	public static Date getNextMinute() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		// shift to next minute
		cal.add(Calendar.MINUTE, 1);
		Date result = new Date(cal.getTimeInMillis());
		return result;
	}

	/**
	 * Shifts the given Date to the same time at the next sconds. This uses the
	 * current time zone.
	 */
	public static Date shiftDateSec(Date d, float nbSeconds) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.SECOND, (int) nbSeconds);
		Date result = new Date(c.getTimeInMillis());
		return result;
	}

	/**
	 * Shifts the given Date to the same time at the next minute. This uses the
	 * current time zone.
	 */
	public static Date shiftDateMinutes(Date d, float nbMinutes) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.MINUTE, (int) nbMinutes);
		Date result = new Date(c.getTimeInMillis());
		return result;
	}

	public static float computeDurationMinutes(Date beginDate, Date endDate) {
		long deltaMS = endDate.getTime() - beginDate.getTime();
		return deltaMS / MS_IN_MINUTE;
	}

	public static float computeDurationHours(Date beginDate, Date endDate) {
		long deltaMS = endDate.getTime() - beginDate.getTime();
		return deltaMS / MS_IN_HOUR;
	}

	public static String getExpiredRequestKey(Map<String, EnergyRequest> tableWaitingRequest,
			Map<String, ProtectedContract> tableContracts) {
		Date current = new Date();
		for (String key : tableWaitingRequest.keySet()) {
			EnergyRequest energyRequest = tableWaitingRequest.get(key);
			if (energyRequest.getAux_expiryDate().after(current) || !energyRequest.canBeSupplied()) {
				return key;
			}
			/*
			 * if (tableSingleOffers != null && tableSingleOffers.containsKey(key)) { return
			 * key; }
			 */
			if (tableContracts != null && tableContracts.containsKey(key)) {
				return key;
			}
		}
		return null;
	}

	public static String getExpiredContractKey(Map<String, ProtectedContract> tableContracts, ProducerAgent prodAgent) {
		for (String key : tableContracts.keySet()) {
			ProtectedContract protectedContract = tableContracts.get(key);
			if (protectedContract.hasExpired()) {
				return key;
			}
			try {
				if(protectedContract.validationHasExpired(prodAgent)) {
					SapereLogger.getInstance().warning("### Contract validation has expired " + protectedContract);
					return key;
				}
			} catch (PermissionException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				SapereLogger.getInstance().error(e);
			}
		}
		return null;
	}

	public static RegulationWarning getExpiredWarning(List<RegulationWarning> warnings)	{
		for(RegulationWarning warning : warnings) {
			if(warning.hasReceptionExpired()) {
				return warning;
			}
		}
		return null;
	}


	public static String getExpiredOfferKey(Map<String, SingleOffer> tableSingleOffers, int marginSeconds) {
		return getExpiredOfferKey(tableSingleOffers, null, marginSeconds);
	}

	public static String getExpiredOfferKey(Map<String, SingleOffer> tableWaitingOffers,Map<String, ProtectedContract> tableContracts,
			int marginSeconds) {
		for (String consumerKey : tableWaitingOffers.keySet()) {
			SingleOffer offer = tableWaitingOffers.get(consumerKey);
			if(offer.hasExpired(marginSeconds)) {
				return consumerKey;
			} else if (tableContracts != null && tableContracts.containsKey(consumerKey)) {
				return consumerKey;
			}
			/*
			if (offer.hasExpired(marginSeconds) || (tableContracts != null && tableContracts.containsKey(consumerKey))) {
				return consumerKey;
			}*/
		}
		return null;
	}

	public static String getOfferToCancel(Map<String, SingleOffer> tableSingleOffers, int marginSeconds) {
		for (String consumerKey : tableSingleOffers.keySet()) {
			SingleOffer offer = tableSingleOffers.get(consumerKey);
			if (!offer.hasExpired(marginSeconds) && offer.getPriorityLevel().isLowerThan(PriorityLevel.HIGH)) {
				return consumerKey;
			}
		}
		return null;
	}

	public static String getContractToCancel(Map<String, ProtectedContract> tableValidContracts) {
		for (String consumerKey : tableValidContracts.keySet()) {
			ProtectedContract protectedContract = tableValidContracts.get(consumerKey);
			if (!protectedContract.hasExpired() && protectedContract.hasAllAgreements()
					&& protectedContract.getRequest().getPriorityLevel().isLowerThan(PriorityLevel.HIGH)) {
				return consumerKey;
			}
		}
		return null;
	}

	public static Map<String, Float> filterRepartition(Map<String, Float> mapValues, List<String> keysFilter) {
		Map<String, Float> result = new HashMap<String, Float>();
		for(String key : mapValues.keySet()) {
			if(keysFilter.contains(key)) {
				result.put(key, mapValues.get(key));
			}
		}
		return result;
	}

	public static String formaMapValues(Map<String, Float> mapValues) {
		if(mapValues==null) {
			return "";
		}
		StringBuffer result = new StringBuffer();
		String sep = "";
		// Set<String> keys = mapValues.keySet();
		List<String> listKeys = new ArrayList<>();
		for (String key : mapValues.keySet()) {
			listKeys.add(key);
		}
		Collections.sort(listKeys);
		for (String key : listKeys) {
			Float value = mapValues.get(key);
			result.append(sep);
			result.append(key).append("(").append(SapereUtil.df.format(value)).append(")");
			sep = ",";
		}
		return result.toString();
	}

	public static double getDoubleValue(Map<String, Object> row, String columnName) {
		Object oValue = row.get(columnName);
		if(oValue instanceof BigDecimal) {
			BigDecimal bdValue = (BigDecimal) oValue;
			return bdValue.doubleValue();
		} else if (oValue instanceof Long) {
			Long longValue = (Long) oValue;
			return longValue.doubleValue();
		} else if (oValue instanceof Integer) {
			Integer intValue = (Integer) oValue;
			return intValue.doubleValue();
		}
		return 0;
	}

	public static int getIntValue(Map<String, Object> row, String columnName) {
		Object oValue = row.get(columnName);
		if(oValue instanceof BigDecimal) {
			BigDecimal bdValue = (BigDecimal) oValue;
			return bdValue.intValue();
		} else if (oValue instanceof Long) {
			Long longValue = (Long) oValue;
			return longValue.intValue();
		} else if (oValue instanceof Integer) {
			Integer intValue = (Integer) oValue;
			return intValue.intValue();
		}
		return 0;
	}

	public static long getLongValue(Map<String, Object> row, String columnName) {
		Object oValue = row.get(columnName);
		if(oValue instanceof BigDecimal) {
			BigDecimal bdValue = (BigDecimal) oValue;
			return bdValue.longValue();
		} else if (oValue instanceof Long) {
			Long longValue = (Long) oValue;
			return longValue.longValue();
		} else if (oValue instanceof Integer) {
			Integer intValue = (Integer) oValue;
			return intValue.longValue();
		} else if (oValue instanceof BigInteger) {
			BigInteger intValue = (BigInteger) oValue;
			return intValue.longValue();
		}
		return 0;
	}

	public static float getFloatValue(Map<String, Object> row, String columnName) {
		Object oValue = row.get(columnName);
		if(oValue instanceof BigDecimal) {
			BigDecimal bdValue = (BigDecimal) oValue;
			return bdValue.floatValue();
		} else if (oValue instanceof Long) {
			Long longValue = (Long) oValue;
			return longValue.floatValue();
		} else if (oValue instanceof Integer) {
			Integer intValue = (Integer) oValue;
			return intValue.floatValue();
		}
		return 0;
	}

	public static String stackTraceToString(Throwable e) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

	 public static float round(float d, int decimalPlace) {
         return BigDecimal.valueOf(d).setScale(decimalPlace,BigDecimal.ROUND_HALF_UP).floatValue();
    }

	 public static boolean checkRound(float d, int decimalPlace) {
		 float rounded = round(d, decimalPlace);
		 float roundGap = Math.abs(d- rounded);
		 if(roundGap>= 0.0001) {
			 SapereLogger.getInstance().info("checkRound " +  d + " roundGap = "+ roundGap);
		 }
         return (roundGap<0.0001);
    }

	public static String firstCharToLower(String aStr) {
		if(aStr!=null && aStr.length()>0) {
			char fistChar = Character.toLowerCase(aStr.charAt(0));
			String result = fistChar + aStr.substring(1);
			return result;
		}
		return aStr;
	}

	public static boolean checkParams(Map<String, String> toCheck , Map<String, String> paramsReference) throws Exception {
		for(String field : paramsReference.keySet()) {
			String refValue = paramsReference.get(field);
			if(refValue!=null && !"null".equals(refValue)) {
				if(toCheck.containsKey(field)) {
					String tocheckValue = toCheck.get(field);
					if(!refValue.equals(tocheckValue)) {
						throw new Exception ("generateAgentFormParams " + field + " values"  + " not equals in param and params1 " + refValue + " " + tocheckValue);
					}
				} else {
					throw new Exception ("generateAgentFormParams " + field + " not in params1 ");
				}
			}
		}
		return true;
	}
}
