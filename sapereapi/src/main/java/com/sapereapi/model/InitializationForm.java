package com.sapereapi.model;

public class InitializationForm {
	private String scenario;
	private Integer initialStateId;
	private String initialStateVariable;
	private Integer forcedHourOfDay;

	public InitializationForm() {
		super();
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public Integer getInitialStateId() {
		return initialStateId;
	}

	public void setInitialStateId(Integer initialStateId) {
		this.initialStateId = initialStateId;
	}

	public String getInitialStateVariable() {
		return initialStateVariable;
	}

	public void setInitialStateVariable(String initialStateVariable) {
		this.initialStateVariable = initialStateVariable;
	}

	public void setInitialState(String _initialStateVariable, Integer _initialStateId) {
		this.initialStateVariable = _initialStateVariable;
		this.initialStateId = _initialStateId;
	}

	public Integer getForcedHourOfDay() {
		return forcedHourOfDay;
	}

	public void setForcedHourOfDay(Integer forcedHour) {
		this.forcedHourOfDay = forcedHour;
	}


}
