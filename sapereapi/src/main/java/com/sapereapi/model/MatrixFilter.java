package com.sapereapi.model;

import com.energy.markov.MarkovTimeWindow;

public class MatrixFilter {
	private String location;
	private Integer startHourMin;
	private Integer startHourMax;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getStartHourMin() {
		return startHourMin;
	}

	public Integer getStartHourMax() {
		return startHourMax;
	}

	public void setStartHourMin(Integer startHourMin) {
		this.startHourMin = startHourMin;
	}

	public void setStartHourMax(Integer startHourMax) {
		this.startHourMax = startHourMax;
	}

	public boolean applyFilter(MarkovTimeWindow timeWindow) {
		boolean isOk = true;
		if(startHourMin!=null) {
			isOk = isOk && startHourMin.intValue()<=timeWindow.getStartHour();
		}
		if(isOk && startHourMax!=null) {
			isOk = isOk && startHourMax.intValue()>=timeWindow.getStartHour();
		}
		return isOk;
	}
	/*
	public boolean applyFilter(SapereAgent agent) {
		boolean isOk = true;
		if (agent instanceof ConsumerAgent) {
			ConsumerAgent consumer = (ConsumerAgent) agent;
			if (consumerDeviceCategories != null && consumerDeviceCategories.length > 0) {
				String deviceCategory = consumer.getNeed().getDeviceCategory().name();
				isOk = isOk && SapereUtil.isInStrArray(consumerDeviceCategories, deviceCategory);
			}
			if (hideExpiredAgents) {
				isOk = isOk && !consumer.hasExpired();
			}
		}
		if (agent instanceof ProducerAgent) {
			ProducerAgent producer = (ProducerAgent) agent;
			if (producerDeviceCategories != null && producerDeviceCategories.length > 0) {
				String deviceCategory = producer.getGlobalSupply().getDeviceCategory().name();
				isOk = isOk && SapereUtil.isInStrArray(producerDeviceCategories, deviceCategory);
			}
			if (hideExpiredAgents) {
				isOk = isOk && !producer.hasExpired();
			}
		}
		return isOk;
	}*/
}
