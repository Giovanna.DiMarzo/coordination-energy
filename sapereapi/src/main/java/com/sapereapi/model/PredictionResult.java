package com.sapereapi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.energy.markov.HomeMarkovStates;
import com.energy.markov.MarkovState;

import Jama.Matrix;

public class PredictionResult implements Serializable {
	private static final long serialVersionUID = 1755L;
	private Date initialDate;
	private Date targetDate;
	private String variable;
	private List<Double> stateProbabilities;
	private MarkovState radomTargetState;

	public PredictionResult(Date _initialDate, Date _targetDate, String _variable) {
		super();
		this.initialDate = _initialDate;
		this.targetDate = _targetDate;
		this.variable = _variable;
		stateProbabilities = new ArrayList<Double>();
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public List<Double> getStateProbabilities() {
		return stateProbabilities;
	}


	public void setStateProbabilities(List<Double> stateProbabilities) {
		this.stateProbabilities = stateProbabilities;
	}

	public void setStateProbabilities(Matrix predictionRowMatrix) {
		stateProbabilities = new ArrayList<>();
		for(double value : predictionRowMatrix.getRowPackedCopy()) {
			stateProbabilities.add(value);
		}
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public MarkovState getRadomTargetState() {
		return radomTargetState;
	}

	public void setRadomTargetState(MarkovState radomTargetState) {
		this.radomTargetState = radomTargetState;
	}

	public void generateRandomState() {
		radomTargetState = HomeMarkovStates.getRandomState(stateProbabilities);
	}

	public float getTimeHorizonMinutes() {
		if (targetDate == null) {
			return 0;
		}
		return SapereUtil.computeDurationMinutes(initialDate, targetDate);
	}

	@Override
	public String toString() {
		return "PredictionResult [initialDate=" + SapereUtil.format_date_time.format(initialDate)
				+ ",targetDate=" + SapereUtil.format_date_time.format(targetDate) + ", variable=" + variable + ", stateProbabilities="
				+ stateProbabilities + ", radomTargetState=" + radomTargetState + "]";
	}

}
