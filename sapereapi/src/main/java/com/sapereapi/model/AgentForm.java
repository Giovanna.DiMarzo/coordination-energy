package com.sapereapi.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ProducerAgent;
import com.energy.model.AgentType;
import com.energy.model.DeviceCategory;
import com.energy.model.EnergyRequest;
import com.energy.model.EnergySupply;
import com.energy.model.PriorityLevel;
import com.energy.util.SapereLogger;

import eu.sapere.middleware.agent.SapereAgent;

public class AgentForm {
	private String agentType;
	private int id;
	private String url;
	private String agentName;
	private String location;
	private String deviceName;
	private String deviceCategoryLabel;
	private String deviceCategoryCode;
	private String contractAgentName;
	private Float power;
	private Float disabledPower;
	private Date beginDate;
	private Date endDate;
	private Float duration;
	private String[] linkedAgents;
	private Map<String, Float> contractsRepartition;
	private Float offersTotal;
	private Map<String, Float> offersRepartition;
	private Float contractsTotal;
	private Float contractsTotalLocal;
	private Boolean hasExpired;
	private Float delayToleranceMinutes;
	private Float delayToleranceRatio;
	private Float availablePower;
	private Float missingPower;
	private String priorityLevel;
	private Float waitingContractsPower;
	private List<String> waitingContractsConsumers;
	private Boolean isSatisfied;
	private Boolean isInSpace;
	private Boolean isContractAgentInSpace;
	private Boolean isDisabled;
	private int warningDurationSec;

	public AgentForm() {
		super();
		this.power = (float) 0.0;
		this.disabledPower = (float) 0.0;
		this.duration = (float) 0.0;
		this.offersTotal = (float) 0.0;
		this.contractsTotal = (float) 0.0;
		this.contractsTotalLocal = (float) 0.0;
		this.delayToleranceMinutes = (float) 0.0;
		this.availablePower = (float) 0.0;
		this.missingPower = (float) 0.0;
		this.availablePower = (float) 0.0;
		this.missingPower = (float) 0.0;
		this.waitingContractsPower = (float) 0.0;
		this.hasExpired = false;
		this.isDisabled = false;
		this.isContractAgentInSpace = false;
		this.isInSpace = false;
		this.contractAgentName = "";
		contractsRepartition = new HashMap<>();
		waitingContractsConsumers = new ArrayList<>();
		linkedAgents = new String[] {};
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Float getContractsTotal() {
		return contractsTotal;
	}

	public void setContractsTotal(Float contractsTotal) {
		this.contractsTotal = contractsTotal;
	}

	public Float getContractsTotalLocal() {
		return contractsTotalLocal;
	}

	public void setContractsTotalLocal(Float contractsTotalLocal) {
		this.contractsTotalLocal = contractsTotalLocal;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Float getPower() {
		return power;
	}

	public void setPower(Float _power) {
		this.power = _power;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Float getDuration() {
		return duration;
	}

	public void setDuration(Float duration) {
		this.duration = duration;
	}

	public String[] getLinkedAgents() {
		return linkedAgents;
	}

	public void setLinkedAgents(String[] linkedAgents) {
		this.linkedAgents = linkedAgents;
	}

	public Boolean getHasExpired() {
		return hasExpired;
	}

	public Map<String, Float> getContractsRepartition() {
		return contractsRepartition;
	}

	public void setContractsRepartition(Map<String, Float> _contractsRepartition) {
		this.contractsRepartition = _contractsRepartition;
	}

	public Float getDelayToleranceMinutes() {
		return delayToleranceMinutes;
	}

	public void setDelayToleranceMinutes(Float delayToleranceMinutes) {
		this.delayToleranceMinutes = delayToleranceMinutes;
	}

	public Float getAvailablePower() {
		return availablePower;
	}

	public void setAvailablePower(Float _availablePower) {
		this.availablePower = _availablePower;
	}

	public Float getMissingPower() {
		return missingPower;
	}

	public void setMissingPower(Float _missingPower) {
		this.missingPower = _missingPower;
	}

	public String getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Boolean getIsInSpace() {
		return isInSpace;
	}

	public void setIsInSpace(Boolean isInSpace) {
		this.isInSpace = isInSpace;
	}

	public EnergySupply getEnergySupply() {
		return new EnergySupply(this.agentName, this.location, this.power, this.beginDate, this.endDate, this.deviceName,
				DeviceCategory.getByName(deviceCategoryCode));
	}

	public EnergyRequest getEnergyRequest() {
		if(delayToleranceMinutes==null) {
			SapereLogger.getInstance().warning("AgentForm.getEnergyRequest " + this.agentName + " : delayToleranceMinutes is null");
			delayToleranceMinutes = new Float(0);
		}
		if(delayToleranceRatio==null) {
			delayToleranceRatio = new Float(0);
		}
		if(this.delayToleranceMinutes==0 && delayToleranceRatio > 0) {
			delayToleranceMinutes = delayToleranceRatio * SapereUtil.computeDurationMinutes(beginDate, endDate);
		}
		return new EnergyRequest(this.agentName, this.location, this.power, this.beginDate, this.endDate, this.delayToleranceMinutes,
				PriorityLevel.getByLabel(this.priorityLevel), this.deviceName,
				DeviceCategory.getByName(this.deviceCategoryCode));
	}

	public Float getWaitingContractsPower() {
		return waitingContractsPower;
	}

	public void setWaitingContractsPower(Float _waitingContractsPower) {
		this.waitingContractsPower = _waitingContractsPower;
	}

	public List<String> getWaitingContractsConsumers() {
		return waitingContractsConsumers;
	}

	public void setWaitingContractsConsumers(List<String> waitingContractsConsumers) {
		this.waitingContractsConsumers = waitingContractsConsumers;
	}

	public Boolean getIsSatisfied() {
		return isSatisfied;
	}

	public void setIsSatisfied(Boolean isSatisfied) {
		this.isSatisfied = isSatisfied;
	}

	public void setHasExpired(Boolean hasExpired) {
		this.hasExpired = hasExpired;
	}

	public Float getOffersTotal() {
		return offersTotal;
	}

	public void setOffersTotal(Float offersTotal) {
		this.offersTotal = offersTotal;
	}
/*
	public String getOffersRepartitionStr() {
		return offersRepartitionStr;
	}

	public void setOffersRepartitionStr(String offersRepartitionStr) {
		this.offersRepartitionStr = offersRepartitionStr;
	}
*/
	public Map<String, Float> getOffersRepartition() {
		return offersRepartition;
	}

	public void setOffersRepartition(Map<String, Float> offersRepartition) {
		this.offersRepartition = offersRepartition;
	}

	public Boolean getIsDisabled() {
		return isDisabled;
	}

	public void setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public Float getDisabledPower() {
		return disabledPower;
	}

	public void setDisabledPower(Float _disabledPower) {
		this.disabledPower = _disabledPower;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceCategoryLabel() {
		return deviceCategoryLabel;
	}

	public void setDeviceCategory(DeviceCategory category) {
		this.deviceCategoryCode = category.name();
		this.deviceCategoryLabel = category.getLabel();
	}

	public void setDeviceCategoryLabel(String deviceCategoryLabel) {
		this.deviceCategoryLabel = deviceCategoryLabel;
	}

	public String getDeviceCategoryCode() {
		return deviceCategoryCode;
	}

	public void setDeviceCategoryCode(String deviceCategoryCode) {
		this.deviceCategoryCode = deviceCategoryCode;
	}

	public Float getDelayToleranceRatio() {
		return delayToleranceRatio;
	}

	public void setDelayToleranceRatio(Float delayToleranceRatio) {
		this.delayToleranceRatio = delayToleranceRatio;
	}

	public int getWarningDurationSec() {
		return warningDurationSec;
	}

	public void setWarningDurationSec(int warningDurationSec) {
		this.warningDurationSec = warningDurationSec;
	}

	public String getContractAgentName() {
		return contractAgentName;
	}

	public Boolean getIsContractAgentInSpace() {
		return isContractAgentInSpace;
	}

	public void setContractAgentName(String contractAgentName) {
		this.contractAgentName = contractAgentName;
	}

	public void setIsContractAgentInSpace(Boolean isContractAgentInSpace) {
		this.isContractAgentInSpace = isContractAgentInSpace;
	}

	public boolean isProducer() {
		return  AgentType.PRODUCER.getLabel().equals(agentType);
	}

	public boolean isConsumer() {
		return  AgentType.CONSUMER.getLabel().equals(agentType);
	}

	public boolean checkInSpace() {
		if(!isInSpace) {
			return false;
		} else if(isConsumer() && !isContractAgentInSpace) {
			return false;
		}
		return true;
	}

	public boolean isRunning() {
		if(isDisabled==null || hasExpired==null) {
			return false;
		}
		return !hasExpired && !isDisabled;
	}

	public AgentForm(AgentType _agentType, String _agentName, String _deviceName, DeviceCategory _deviceCategory,
			Float _power, Date _beginDate, Date _enDate) {
		super();
		this.agentType = _agentType.getLabel();
		this.agentName = _agentName;
		this.deviceName = _deviceName;
		this.deviceCategoryCode = _deviceCategory.name();
		this.deviceCategoryLabel = _deviceCategory.getLabel();
		this.power = _power;
		this.offersTotal = new Float(0);
		this.beginDate = _beginDate;
		this.endDate = _enDate;
		Date current = new Date();
		this.hasExpired = !current.before(endDate);
	}

	public AgentForm(AgentType _agentType, String _agentName, String _deviceName, DeviceCategory _deviceCategory,
			Float _power, Date _beginDate, Date _enDate, PriorityLevel _priorityLevel, Float _delayToleranceMinutes) {
		this(_agentType, _agentName, _deviceName, _deviceCategory, _power, _beginDate, _enDate);
		if (AgentType.CONSUMER.getLabel().equals(this.agentType)) {
			this.priorityLevel = _priorityLevel.getLabel();
			this.delayToleranceMinutes = _delayToleranceMinutes;
			this.duration = SapereUtil.computeDurationHours(_beginDate, _enDate);
			this.delayToleranceRatio = new Float(0);
			if(duration>0) {
				this.delayToleranceRatio = (float) Math.min(1.0, delayToleranceMinutes / duration);
			}
			this.contractAgentName = "";
		}
	}

	public void init(SapereAgent agent, boolean _isInSpace) {
		this.agentName = agent.getAgentName();
		this.url = agent.getAgentName();
		this.isInSpace = _isInSpace;
		if(agent.getAuthentication()!=null) {
			this.agentType = agent.getAuthentication().getAgentType();
			this.location = agent.getAuthentication().getAgentLocation();
		}
	}

	public AgentForm(ProducerAgent producer, boolean isInSpace) {
		super();
		init(producer, isInSpace);
		this.id = producer.getId();
		if (producer.getGlobalSupply() != null) {
			this.deviceName = producer.getGlobalSupply().getDeviceName();
			this.deviceCategoryCode = producer.getGlobalSupply().getDeviceCategory().name();
			this.deviceCategoryLabel = producer.getGlobalSupply().getDeviceCategory().getLabel();
		}
		this.power = (producer.hasExpired() || producer.isDisabled()) ? new Float(0.0)
				: producer.getGlobalSupply().getPower();
		this.disabledPower = (producer.hasExpired() || producer.isDisabled()) ? producer.getGlobalSupply().getPower()
				: new Float(0.0);
		this.beginDate = producer.getGlobalSupply().getBeginDate();
		this.endDate = producer.getGlobalSupply().getEndDate();
		this.duration = producer.getGlobalSupply().getDuration();
		this.delayToleranceRatio = new Float(0.0);
		this.delayToleranceMinutes = new Float(0);
		Object[] objs = producer.getLinkedAgents().toArray();
		// String sobjs = objs.toString();
		this.linkedAgents = new String[objs.length];
		for (int idx = 0; idx < objs.length; idx++) {
			linkedAgents[idx] = "" + objs[idx];
		}
		this.contractsTotal = producer.getContractsPower(null);
		this.contractsTotalLocal = producer.getContractsPower(location);
		this.hasExpired = producer.hasExpired();
		this.offersTotal = producer.getOffersTotal();
		this.contractsRepartition = producer.getContractsRepartition();
		this.offersRepartition = producer.getOffersRepartition();
		this.availablePower = Math.max(0,this.power - this.contractsTotal);
		this.missingPower = new Float(0.0);
		this.priorityLevel = "";
		// Waiting contracts
		this.waitingContractsPower = producer.computePowerOfWaitingContrats();
		this.waitingContractsConsumers = producer.getConsumersOfWaitingContrats();
		this.isDisabled = producer.isDisabled();
		this.isSatisfied = false;
		this.warningDurationSec = 0;
		this.contractAgentName = "";
	}

	public AgentForm(ConsumerAgent consumer, boolean _isInSpace, boolean _isContractAgentInSpace) {
		super();
		init(consumer, _isInSpace);
		this.id = consumer.getId();
		if (consumer.getNeed() != null) {
			this.deviceName = consumer.getNeed().getDeviceName();
			this.deviceCategoryCode = consumer.getNeed().getDeviceCategory().name();
			this.deviceCategoryLabel = consumer.getNeed().getDeviceCategory().getLabel();
			this.warningDurationSec = consumer.getNeed().getWarningDurationSec();
		}
		//this.url = consumer.getAgentName();
		this.power = (consumer.hasExpired() || consumer.isDisabled()) ? new Float(0.0) : consumer.getNeed().getPower();
		this.disabledPower = (consumer.hasExpired() || consumer.isDisabled()) ? consumer.getNeed().getPower()
				: new Float(0.0);
		this.beginDate = consumer.getNeed().getBeginDate();
		this.endDate = consumer.getNeed().getEndDate();
		this.duration = consumer.getNeed().getDuration();
		this.delayToleranceMinutes = consumer.getNeed().getDelayToleranceMinutes();
		delayToleranceRatio = new Float(0.0);
		if(duration>0) {
			this.delayToleranceRatio =  (float) Math.min(1.0, delayToleranceMinutes / duration);
		}
		Object[] objs = consumer.getLinkedAgents().toArray();
		// String repartition =
		// SapereUtil.formaMapValues(consumer.getContractsRepartition());
		this.linkedAgents = new String[objs.length];
		for (int idx = 0; idx < objs.length; idx++) {
			linkedAgents[idx] = "" + objs[idx];
		}
		if ("_Consumer_2".equals(agentName)) {
			String test = SapereUtil.formaMapValues(consumer.getOffersRepartition());
			SapereLogger.getInstance().info(test);
		}
		this.contractsTotal = consumer.getContractedPower(null);
		this.contractsTotalLocal = consumer.getContractedPower(location);
		this.hasExpired = consumer.hasExpired();
		this.contractsRepartition = consumer.getContractsRepartition();
		this.offersRepartition = consumer.getOffersRepartition();
		this.offersTotal = consumer.getOffersTotal();
		this.availablePower = new Float(0.0);
		this.missingPower = Math.max(0, this.power - this.contractsTotal);
		this.priorityLevel = consumer.getNeed().getPriorityLevel().getLabel();
		this.isSatisfied = consumer.isSatisfied();
		this.isDisabled = consumer.isDisabled();
		this.contractAgentName = consumer.getContractAgentName();
		this.isContractAgentInSpace = _isContractAgentInSpace;
	}

	/**
	 * AgentForm comparator
	 *
	 * @param other
	 * @return
	 */
	public int compareTo(AgentForm other) {
		int compareType = this.agentType.compareTo(other.getAgentType());
		if (compareType == 0) {
			return this.id - other.getId();
		} else {
			return compareType;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof AgentForm) {
			boolean result;
			try {
				AgentForm agentForm = (AgentForm) obj;
				result = ((this.power==null)? (agentForm.getPower()==null) : this.power.floatValue()==agentForm.getPower().floatValue());
				result = result && ((this.agentName==null)? (agentForm.getAgentName()==null) : this.agentName.equals(agentForm.getAgentName()));
				result = result && (this.id == agentForm.getId());
				result = result && ((this.beginDate==null)? (agentForm.getBeginDate()==null) : this.beginDate.equals(agentForm.getBeginDate()));
				result = result && ((this.endDate==null)? (agentForm.getEndDate()==null) : this.endDate.equals(agentForm.getEndDate()));
				result = result && ((this.duration==null)? (agentForm.getDuration()==null) : this.duration.floatValue() == agentForm.getDuration().floatValue());
				result = result && ((this.url==null)? (agentForm.getUrl()==null) : this.url.equals(agentForm.getUrl()));
				result = result && (this.hasExpired == agentForm.getHasExpired());
				result = result && (this.isDisabled == agentForm.getIsDisabled());
				result = result && (this.isSatisfied == agentForm.getIsSatisfied());
				result = result && (this.isInSpace == agentForm.getIsInSpace());
				result = result && ((this.agentType==null)? (agentForm.getAgentType()==null) : this.agentType.equals(agentForm.getAgentType()));
				result = result && ((this.deviceName==null)? (agentForm.getDeviceName()==null) :this.deviceName.equals(agentForm.getDeviceName()));
				result = result && ((this.deviceCategoryCode==null)? (agentForm.getDeviceCategoryCode()==null) :this.deviceCategoryCode.equals(agentForm.getDeviceCategoryCode()));
				result = result && ((this.deviceCategoryLabel==null)? (agentForm.getDeviceCategoryLabel()==null) :this.deviceCategoryLabel.equals(agentForm.getDeviceCategoryLabel()));
				result = result	&& ((this.availablePower==null)? (agentForm.getAvailablePower()==null) : this.availablePower.floatValue() == agentForm.getAvailablePower().floatValue());
				result = result	&& (this.disabledPower.floatValue() == agentForm.getDisabledPower().floatValue());
				result = result	&& (this.delayToleranceMinutes.floatValue() == agentForm.getDelayToleranceMinutes().floatValue());
				result = result	&& ((this.priorityLevel==null)? (agentForm.getPriorityLevel()==null) : this.priorityLevel.equals(agentForm.getPriorityLevel()));
				result = result	&& (this.delayToleranceMinutes.floatValue() == agentForm.getDelayToleranceMinutes().floatValue());
				result = result	&& (this.contractsTotal.floatValue() == agentForm.getContractsTotal().floatValue());
				result = result && ((this.offersRepartition==null)? (agentForm.getOffersRepartition()==null) : this.offersRepartition.equals(agentForm.getOffersRepartition()));
				result = result	&& ((this.contractsRepartition==null)? (agentForm.getContractsRepartition()==null) : this.contractsRepartition.equals(agentForm.getContractsRepartition()));
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			return result;
		}
		return false;
	}
}
