package com.sapereapi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.energy.markov.HomeMarkovTransitions;
import com.energy.markov.MarkovState;
import com.sapereapi.api.PredictionStep;

public class PredictionData implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<String> variables;
	private Date initialDate;
	private SortedSet<Date> targetDates = new TreeSet<Date>();
	private List<PredictionStep> listTimeSlot;
	private Map<String, MarkovState> initialStates;
	private Map<String, Float> initialValues;
	private Map<String, Map<Date, PredictionResult>> mapResults;
	private Map<String, PredictionResult> mapLastResults;
	private List<String> errors;
	private int learningWindow;
	private String location;
	private String scenario;

	public PredictionData() {
		super();
		listTimeSlot = new ArrayList<PredictionStep>();
		targetDates = new TreeSet<Date>();
		mapResults = new HashMap<String, Map<Date, PredictionResult>>();
		mapLastResults = new HashMap<String, PredictionResult>();
		errors = new ArrayList<String>();
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(String[] _variables) {
		this.variables = Arrays.asList(_variables);
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Map<String, MarkovState> getInitialStates() {
		return initialStates;
	}

	public void setInitialStates(Map<String, MarkovState> initialStates) {
		this.initialStates = initialStates;
	}

	public void setInitialContent(HomeMarkovTransitions transitions) {
		this.initialStates = transitions.getMapStates();
		this.initialValues = transitions.getMapValues();
	}

	public boolean hasInitialContent() {
		return initialStates != null && initialValues != null && initialStates.size() > 0 && initialValues.size() > 0;
	}

	public void addTargetDate(Date aDate) {
		targetDates.add(aDate);
	}

	public Date getLastTargetDate() {
		if (targetDates.size() > 0) {
			return targetDates.last();
		}
		return null;// targetDate;
	}

	public Map<String, PredictionResult> getMapLastResults() {
		return mapLastResults;
	}


	public void addError(String error) {
		if (!errors.contains(error)) {
			errors.add(error);
		}
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<PredictionStep> getListTimeSlot() {
		return listTimeSlot;
	}

	public void setListTimeSlot(List<PredictionStep> listTimeSlot) {
		this.listTimeSlot = listTimeSlot;
	}

	public Map<String, Float> getInitialValues() {
		return initialValues;
	}

	public void setInitialValues(Map<String, Float> initialValues) {
		this.initialValues = initialValues;
	}

	public Map<String, Map<Date, PredictionResult>> getMapResults() {
		return mapResults;
	}

	public void setMapResults(Map<String, Map<Date, PredictionResult>> mapResults) {
		this.mapResults = mapResults;
	}

	public void setResult(String variable, Date targetDate, PredictionResult result) {
		if (targetDates.contains(targetDate) && variables.contains(variable)) {
			if (!mapResults.containsKey(variable)) {
				mapResults.put(variable, new HashMap<Date, PredictionResult>());
			}
			(this.mapResults.get(variable)).put(targetDate, result);
			Date lastTargetDate = getLastTargetDate();
			if (lastTargetDate.equals(targetDate)) {
				mapLastResults.put(variable, result);
			}
		}
	}

	public boolean hasResult(String variable, Date targetDate) {
		return mapResults.containsKey(variable) && (mapResults.get(variable)).containsKey(targetDate);
	}

	public boolean hasLastResult(String variable) {
		return mapLastResults.containsKey(variable);
	}

	public PredictionResult getResult(String variable, Date targetDate) {
		if (hasResult(variable, targetDate)) {
			return (mapResults.get(variable)).get(targetDate);
		}
		return null;
	}

	public PredictionResult getLastResult(String variable) {
		return mapLastResults.get(variable);
	}

	public MarkovState getLastRandomTargetState(String variable) {
		PredictionResult result = getLastResult(variable);
		if (result != null) {
			return result.getRadomTargetState();
		}
		return null;
	}

	public void generateRandomState() {
		for (String variable : mapResults.keySet()) {
			Map<Date, PredictionResult> varResults = mapResults.get(variable);
			for (PredictionResult nextResult : varResults.values()) {
				nextResult.generateRandomState();
			}
		}
	}

	public int getLearningWindow() {
		return learningWindow;
	}

	public void setLearningWindow(int learningWindow) {
		this.learningWindow = learningWindow;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public SortedSet<Date> getTargetDates() {
		return targetDates;
	}

	public SortedSet<Date> getTargetDatesWithoutResult(String variable) {
		SortedSet<Date> result = new TreeSet<Date>();
		for (Date targetDate : this.targetDates) {
			if (!hasResult(variable, targetDate)) {
				result.add(targetDate);
			}
		}
		return result;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}

	public void setTargetDates(SortedSet<Date> targetDates) {
		this.targetDates = targetDates;
	}

	public void setMapLastResults(Map<String, PredictionResult> mapLastResults) {
		this.mapLastResults = mapLastResults;
	}

	@Override
	public String toString() {
		return "PredictionData [scenario = " + scenario + ", initialDate=" + SapereUtil.formatTimeOrDate(initialDate)
				+ ", targetDate=" + SapereUtil.formatTimeOrDate(getLastTargetDate()) + ", initialStates="
				+ initialStates + ", mapResults=" + mapResults + "]";
	}
}
