package com.sapereapi.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ContractAgent;
import com.energy.agent.IEnergyAgent;
import com.energy.agent.LearningAgent;
import com.energy.agent.ProducerAgent;
import com.energy.agent.RegulatorAgent;
import com.energy.markov.HomeTransitionMatrices;
import com.energy.markov.MarkovState;
import com.energy.markov.MarkovTimeWindow;
import com.energy.model.AgentType;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.EnergyRequest;
import com.energy.model.EnergySupply;
import com.energy.model.Event;
import com.energy.model.EventType;
import com.energy.model.ExtendedEvent;
import com.energy.model.OptionItem;
import com.energy.model.PriorityLevel;
import com.energy.model.RegulationWarning;
import com.energy.util.EnergyDbHelper;
import com.energy.util.SapereLogger;
import com.sapereapi.api.ConfigRepository;
import com.sapereapi.sapere.AgentBloodSearch;
import com.sapereapi.sapere.AgentTransport;
import com.sapereapi.sapere.QueryAgent;
import com.sapereapi.sapere.ServiceAgent;
import com.sapereapi.sapere.ServiceAgentWeb;

import eu.sapere.middleware.agent.AgentAuthentication;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.node.NodeManager;

public class Sapere {

	private List<QueryAgent> querys;
	public static List<SapereAgent> ServiceAgents;
	private static Sapere instance = null;
	public NodeManager nodeManager;

	private Map<String, AgentAuthentication> mapAgentAuthentication = null;
	private Map<String, Integer> mapDistance = null;
	private Set<AgentAuthentication> authentifiedAgentsCash = null;

	protected final static double devicePowerCoeffProducer = 2.0;
	protected final static double devicePowerCoeffConsumer = 0.25;
	protected final static double statisticPowerCoeffConsumer = 3.0;
	protected final static double statisticPowerCoeffProducer = 12.0;//6.0;
	public static String learningAgentName = generateAgentName(AgentType.LEARNING_AGENT);
	public static String regulatorAgentName = generateAgentName(AgentType.REGULATOR);
	private String salt = null;
	private static int nextConsumerId = 1;
	private static int nextProducerId = 1;
	private boolean useSecuresPasswords = false;
	private String scenario = "";
	private Integer forcedHourOfDay = null;
	private static SapereLogger logger = SapereLogger.getInstance();
	int debugLevel = 0;
	private static boolean disableRegulation;

	public static Sapere getInstance() {
		if (instance == null) {
			instance = new Sapere();
		}
		return instance;
	}

	public Sapere() {
		nodeManager = NodeManager.instance();
		querys = new ArrayList<QueryAgent>();
		ServiceAgents = new ArrayList<SapereAgent>();
		authentifiedAgentsCash = new HashSet<>();
		// Generate Salt. The generated value can be stored.
        salt = PasswordUtils.getSalt(2);
		mapAgentAuthentication = new HashMap<String, AgentAuthentication>();
		mapDistance = new HashMap<String, Integer>();
	}

	public void updateDistance(String location, int lastDistance) {
		if(lastDistance > 0 && lastDistance < 999) {
			int newDistance = 1+lastDistance;
			if(mapDistance.containsKey(location)) {
				if(mapDistance.get(location)>newDistance) {
					mapDistance.put(location, newDistance);
				}
			} else {
				mapDistance.put(location,newDistance);
			}
		}
	}

	public int getDistance(String location) {
		if(mapDistance.containsKey(location)) {
			return mapDistance.get(location);
		}
		return 999;
	}

	public int getDistance(String location, int lastDistance) {
		updateDistance(location, lastDistance);
		return getDistance(location);
	}

	public String getInfo() {
		return NodeManager.getNodeName() + " - " + NodeManager.getLocation() + " -: "
				+ Arrays.toString(NodeManager.instance().networkDeliveryManager.getNeighbours());
	}

	public List<OptionItem> getLocations() {
		List<OptionItem> result = new ArrayList<>();
		result.add(new OptionItem(NodeManager.getLocation(), "home : " + NodeManager.getLocation()));
		int neighborIdx = 1;
		for(String neighbor : NodeManager.instance().networkDeliveryManager.getNeighbours()) {
			result.add(new OptionItem(neighbor, "neighbor " + neighborIdx + " : " + neighbor));
			neighborIdx++;
		}
		return result;
	}

	public void diffuseLsa(String lsaName, int hops) {
		for (SapereAgent service : ServiceAgents) {
			if (lsaName.equals(service.getAgentName())) {
				service.addGradient(hops);
				break;
			}
		}
	}

	public List<String> getLsa() {
		List<String> lsaList = new ArrayList<String>();

		for (Lsa lsa : NodeManager.instance().getSpace().getAllLsa().values()) {
			lsaList.add(lsa.toVisualString());
		}
		return lsaList;
	}

	public List<LsaForm> getLsasObj() {
		List<LsaForm> result = new ArrayList<LsaForm>();
		for (Lsa lsa : NodeManager.instance().getSpace().getAllLsa().values()) {
			LsaForm lsaForm = new LsaForm(lsa);
			result.add(lsaForm);
		}
		return result;
	}

	public List<Service> getLsas() {
		List<Service> serviceList = new ArrayList<Service>();
		for (SapereAgent service : ServiceAgents) {
			serviceList.add(new Service(service));
		}
		return serviceList;
	}

	public Map<String, Double[]> getQtable(String name) {
		for (SapereAgent serviceAgent : ServiceAgents) {
			if (serviceAgent.getAgentName().equals(name)) {
				return serviceAgent.getQ();
			}
		}
		return null;
	}

	public String getLsa(String name) {
		String visualLsa = "";
		for (Lsa lsa : NodeManager.instance().getSpace().getAllLsa().values()) {
			if (name.equals(lsa.getAgentName())) {
				visualLsa = lsa.toVisualString();
				break;
			}
		}
		return visualLsa;
	}

	// Add and start a service
	public void addServiceGeneric(Service service) {
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.GENERIC_SERVICE);
		authentication.setAgentName(service.getName());
		ServiceAgents.add(new ServiceAgent(authentication, service.getInput(), service.getOutput(), LsaType.Service));
		startService(service.getName());
	}

	public void addServiceRest(Service service) {
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.WEB_SERVICE);
		authentication.setAgentName(service.getName());
		ServiceAgents.add(new ServiceAgentWeb(service.getUrl(), authentication, service.getInput(),
				service.getOutput(), service.getAppid(), LsaType.Service));
		startService(service.getName());
	}

	public void addServiceBlood(Service service) {
		addServiceBlood(service.getName(), null);
	}

	public void addServiceBlood(String name, String type) {
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.BLOOD_SEARCH);
		authentication.setAgentName(name);
		ServiceAgents.add(
				new AgentBloodSearch(authentication, new String[] { "Blood" }, new String[] { "Position" }, LsaType.Service));
		startService(name);
	}

	public void addServiceTransport(Service service) {
		addServiceTransport(service.getName(), null);
	}

	public void addServiceTransport(String name, String type) {
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.TRANSPORT);
		authentication.setAgentName(name);
		ServiceAgents.add(new AgentTransport(authentication, new String[] { "Position", "Destination" },
				new String[] { "Transport" }, LsaType.Service));
		startService(name);
	}

	public List<Service> getServices() {
		List<Service> services = new ArrayList<Service>();
		for (SapereAgent service : ServiceAgents) {
			services.add(new Service(service));
		}
		return services;
	}

	public List<String> getNodes() {
		HashSet<String> nodeSet = new HashSet<>();
		for (SapereAgent service : ServiceAgents) {
			nodeSet.add(service.getInput()[0]);
			nodeSet.add(service.getOutput()[0]);
		}
		List<String> nodes = new ArrayList<String>(nodeSet);
		return nodes;
	}

	public static void startService(String name) {
		for (SapereAgent serviceAgent : ServiceAgents) {
			if (serviceAgent.getAgentName().equals(name)) {
				serviceAgent.setInitialLSA();
			}
		}
	}

	public void addQuery(Query request) {
		querys.add(new QueryAgent(request.getName(), null, request.getWaiting(), request.getProp(), request.getValues(),
				LsaType.Query));
	}

	public QueryAgent getQueryByName(String name) {
		for (QueryAgent query : querys) {
			if (query.getAgentName().equals(name)) {
				return query;
			}
		}
		return null;
	}

	public List<QueryAgent> getQuerys() {
		return querys;
	}

	public void setQuerys(List<QueryAgent> querys) {
		this.querys = querys;
	}

	// Energy
	private SapereAgent getAgent(String agentName) {
		for (SapereAgent agent : ServiceAgents) {
			if (agent.getAgentName().equals(agentName)) {
				return agent;
			}
		}
		// Agent not found : it can be a query agent
		for (QueryAgent agent : querys) {
			if (agent.getAgentName().equals(agentName)) {
				return agent;
			}
		}
		return null;
	}

	public static void setLocation(ConfigRepository repository) {
		List<Config> listConfig = repository.findAll();
		if(listConfig.size()>0) {
			Config config = listConfig.get(0);
			int localPost = new Integer(config.getLocalport());
			NodeManager.setConfiguration(config.getName(), config.getLocalip(), localPost);
		}
	}

	public void initNodeManager(ConfigRepository repository) {
		setLocation(repository);
		List<Config> listConfig = repository.findAll();
		if(listConfig.size()>0) {
			Config config = listConfig.get(0);
			String[] neighs = new String[config.getNeighbours().size()];
			int k = 0;
			for (String s : config.getNeighbours()) {
				neighs[k++] = s;
			}
			nodeManager = NodeManager.instance();
			nodeManager.getNetworkDeliveryManager().setNeighbours(neighs);
		}
		mapDistance = new HashMap<String, Integer>();
		mapDistance.put(NodeManager.getLocation(), 0);
		for(String nextNeighbour : NodeManager.instance().getNetworkDeliveryManager().getNeighbours()) {
			mapDistance.put(nextNeighbour, 1);
		}
	}

	public void initEnergyService(ConfigRepository repository, String _scenario, Integer _forcedHourOfDay) {
		initNodeManager(repository);
		this.scenario = _scenario;
		this.forcedHourOfDay = _forcedHourOfDay;
		learningAgentName = generateAgentName(AgentType.LEARNING_AGENT);
		regulatorAgentName = generateAgentName(AgentType.REGULATOR);
		SapereAgent toRemove = getAgent(learningAgentName);
		if (toRemove!= null) {
			ServiceAgents.remove(toRemove);
		}
		addServiceLearningAgent(learningAgentName, scenario, forcedHourOfDay);
	}

	public void checkInitialisation() {
		if (getAgent(learningAgentName) == null) {
			addServiceLearningAgent(learningAgentName, scenario, forcedHourOfDay);
		}
		if (getAgent(regulatorAgentName) == null) {
			addServiceRegulatorAgent(regulatorAgentName);
		}
	}

	public EnergySupply generateSupply(Float power, Date beginDate, Float durationMinutes, String deviceName, DeviceCategory deviceCategory) {
		Date endDate = SapereUtil.shiftDateMinutes(beginDate, durationMinutes);
		return generateSupply(power, beginDate, endDate, deviceName, deviceCategory );
	}

	public EnergySupply generateSupply(Float power, Date beginDate, Date endDate, String deviceName, DeviceCategory deviceCategory) {
		String issuer = generateAgentName(AgentType.PRODUCER);
		return new EnergySupply(issuer, NodeManager.getLocation(), power, beginDate, endDate, deviceName, deviceCategory);
	}

	public EnergyRequest generateRequest(Float power, Date beginDate, Float durationMinutes,
			Float _delayToleranceMinutes, PriorityLevel _priority, String deviceName, DeviceCategory deviceCategory) {
		Date endDate = SapereUtil.shiftDateMinutes(beginDate, durationMinutes);
		if(!SapereUtil.checkRound(power, 2)) {
			logger.warning("generateRequest : power with more than 2 dec : " + power);
		}
		return generateRequest(power, beginDate, endDate, _delayToleranceMinutes, _priority, deviceName, deviceCategory);
	}

	public EnergyRequest generateRequest(Float power, Date beginDate, Date endDate, Float _delayToleranceMinutes,
			PriorityLevel _priority, String deviceName, DeviceCategory deviceCategory) {
		if(!SapereUtil.checkRound(power, 2)) {
			logger.warning("generateRequest : power with more than 2 dec : " + power);
		}
		String issuer = generateAgentName(AgentType.CONSUMER);
		return new EnergyRequest(issuer, NodeManager.getLocation(), power, beginDate, endDate, _delayToleranceMinutes, _priority, deviceName, deviceCategory);
	}

	public static String generateAgentName(AgentType agentType) {
		String radical = agentType.getPreffix() + "_" + NodeManager.getNodeName();
		if(AgentType.PRODUCER.equals(agentType)) {
			return radical + "_" + nextProducerId;
		} else if(AgentType.CONSUMER.equals(agentType) || AgentType.CONTRACT.equals(agentType)) {
			return radical + "_" + nextConsumerId;
		}
		return radical;
	}

	private AgentAuthentication generateAgentAuthentication(AgentType agentType) {
		String agentName = generateAgentName(agentType);
		String authenticationKey = PasswordUtils.generateAuthenticationKey();
		String securedKey = useSecuresPasswords ? PasswordUtils.generateSecurePassword(authenticationKey, salt) : authenticationKey;
		AgentAuthentication authentication = new AgentAuthentication(agentName, agentType.getLabel(), securedKey, NodeManager.getNodeName(), NodeManager.getLocation());
		this.mapAgentAuthentication.put(authentication.getAgentName(), authentication);
		return authentication;
	}

	public List<Service> test1(ConfigRepository repository) {
		// Add producer agents
		initEnergyService(repository, "test1", null);
		Date current = new Date(); // getCurrentMinute();
		addServiceProducer(generateSupply(new Float(30.0), current, new Float(24 * 60 * 365), "EDF", DeviceCategory.EXTERNAL_ENG));
		addServiceProducer(generateSupply(new Float(15.0), current, new Float(40), "wind turbine1", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(30.0), current, new Float(30), "wind turbine2", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(100), current, new Float(6), "wind turbine3", DeviceCategory.WIND_ENG));

		// Add query
		addQueryConsumer(generateRequest(new Float(43.1), current, new Float(24 * 60 * 365),new Float(24 * 60 * 365) , PriorityLevel.LOW, "Refrigerator", DeviceCategory.COLD_APPLIANCES));
		/*
		addQueryConsumer(generateRequest(new Float(27.7), current, new Float(0.1), new Float(150), PriorityLevel.LOW, "Laptop Compute", DeviceCategory.ICT));
		addQueryConsumer(generateRequest(new Float(72.7), current, new Float(0.1), new Float(80), PriorityLevel.LOW, " MacBook Pro ", DeviceCategory.ICT));
		addQueryConsumer(generateRequest(new Float(10.0), current, new Float(0.1), new Float(10), PriorityLevel.LOW, "Led1", DeviceCategory.LIGHTING));
		 */
		// restartConsumer("Consumer_3", new Float("11"), current,
		// SapereUtil.shiftDateMinutes(current, 10), new Float(01));
		return getLsas();
	}

	public List<Service> test1bis(ConfigRepository repository) {
		// Add producer agents
		initEnergyService(repository, "test1bis", null);
		Date current = new Date(); // getCurrentMinute();
		// addServiceProducer(generateSupply(new Float(30.0), current, new
		// Float(24 * 60 * 365)));
		addServiceProducer(generateSupply(new Float(150.0), current, new Float(200), "EDF", DeviceCategory.EXTERNAL_ENG));

		// Add query
		addQueryConsumer(generateRequest(new Float(43.1), current, new Float(24 * 60 * 365),
				new Float(24 * 60 * 365), PriorityLevel.LOW, "Refrigerator", DeviceCategory.COLD_APPLIANCES));

		return getLsas();
	}

	public List<Service> test1ter(ConfigRepository repository) {
		// Add producer agents
		initEnergyService(repository, "test1ter", null);
		Date current = new Date(); // getCurrentMinute();
		addServiceProducer(generateSupply(new Float(2000.0), current, new Float(24 * 60 * 365), "EDF", DeviceCategory.EXTERNAL_ENG));
		addServiceProducer(generateSupply(new Float(150.0), current, new Float(120), "wind turbine1", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(300.0), current, new Float(120), "wind turbine2", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(200), current, new Float(120), "wind turbine3", DeviceCategory.WIND_ENG));
		/**/

		// Add query
		addQueryConsumer(generateRequest(new Float(43.1), current, new Float(24 * 60 * 365),
				new Float(24 * 60 * 365), PriorityLevel.HIGH, "Refrigerator", DeviceCategory.COLD_APPLIANCES));
		addQueryConsumer(generateRequest(new Float(270.7), current, new Float(150), new Float(150), PriorityLevel.LOW,  "Household Fan ", DeviceCategory.OTHER));
		addQueryConsumer(generateRequest(new Float(720.7), current, new Float(80), new Float(80), PriorityLevel.LOW, " Toaster", DeviceCategory.COOKING));
		addQueryConsumer(generateRequest(new Float(100.0), current, new Float(50), new Float(50), PriorityLevel.LOW, "iPad / Tablet", DeviceCategory.ICT));
		/**/

		// restartConsumer("Consumer_3", new Float("11"), current,
		// SapereUtil.shiftDateMinutes(current, 10), new Float(01));
		return getLsas();
	}

	public List<Service> test2(ConfigRepository repository) {
		initEnergyService(repository, "test2", null);
		Date current = SapereUtil.getCurrentSeconde(); // getCurrentMinute();
		int nbAgents = 5;
		for(int i = 0; i < nbAgents; i++) {
			addServiceProducer(generateSupply(new Float(25.0), current, new Float(60), "wind turbine " + i, DeviceCategory.WIND_ENG));
		}
		for(int i = 0; i < nbAgents; i++) {
			addQueryConsumer(generateRequest(new Float(30+0.1*i), current, new Float(120), new Float(120), PriorityLevel.LOW,  "Laptop "+i, DeviceCategory.ICT));
		}
		return getLsas();
	}

	public List<Service> test3(ConfigRepository repository) {
		initEnergyService(repository, "test3", null);
		Date current = SapereUtil.getCurrentSeconde(); // getCurrentMinute();
		addServiceProducer(generateSupply(new Float(30.0), current, new Float(24 * 60 * 365), "EDF", DeviceCategory.EXTERNAL_ENG));
		addServiceProducer(generateSupply(new Float(25.0), current, new Float(60), "wind turbine1", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(25.0), current, new Float(60), "wind turbine2", DeviceCategory.WIND_ENG));
		addServiceProducer(generateSupply(new Float(25.0), current, new Float(60), "wind turbine3", DeviceCategory.WIND_ENG));
		addQueryConsumer(generateRequest(new Float(24.7), current, new Float(150), new Float(150), PriorityLevel.LOW, "solar panel", DeviceCategory.SOLOR_ENG));
		return getLsas();
	}

	public List<Service> test4(ConfigRepository repository) {
		initEnergyService(repository, "test4", null);
		Date current = SapereUtil.getCurrentSeconde(); // getCurrentMinute();
		//addServiceProducer(generateSupply(new Float(30.0), current, new Float(24 * 60 * 365), "EDF", DeviceCategory.EXTERNAL_ENG));
		addServiceProducer(generateSupply(new Float(50.0), current, new Float(10), "solar panel1", DeviceCategory.SOLOR_ENG));
		addQueryConsumer(generateRequest(new Float(97.7), current, new Float(150), new Float(150), PriorityLevel.LOW, "TV 32 LED/LCD ", DeviceCategory.AUDIOVISUAL));
		int nbAgents = 5;
		for(int i = 0; i < nbAgents; i++) {
			addQueryConsumer(generateRequest(new Float(30+0.1*i), current, new Float(120), new Float(120), PriorityLevel.LOW,  "Laptop "+i, DeviceCategory.ICT));
		}
		return getLsas();
	}

	public List<Service> test5(ConfigRepository repository) {
		initEnergyService(repository, "test5", null);
		// Add producer agents
		Date current = new Date();
		addServiceProducer(generateSupply(new Float(2700.0), current, new Float(24 * 60 * 365), "EDF", DeviceCategory.EXTERNAL_ENG));
		// Add query
		/*
		addQueryConsumer(
				generateRequest(new Float(10.0), current, new Float(0.5), new Float(0.5), PriorityLevel.LOW, "Led1", DeviceCategory.LIGHTING));
		*/
		return getLsas();
	}


	public List<Service> initState(ConfigRepository repository, String variable, MarkovState targetState) {
		//initEnergyService(repository, "test5");
		// Add producer agents
		List<Device> devices = retrieveHomeDevices();
		Collections.shuffle(devices);
		Date current = new Date();
		float minPower = targetState.getMinValue();
		float maxPower = targetState.getMaxValue()==null? (float) 1.2*minPower:targetState.getMaxValue();
		double powerRandom = Math.random();
		float targetPower = minPower +  (float) ( powerRandom * (maxPower - minPower));
		int nbAgents = 5;
		disableRegulation = true;
		if("produced".equals(variable) || "available".equals(variable) || "consumed".equals(variable) || "provided".equals(variable)) {
			List<String> devicesProd = new ArrayList<>();
			for(Device device : devices) {
				if(DeviceCategory.WIND_ENG.getLabel().equals(device.getCategory())
					|| DeviceCategory.SOLOR_ENG.getLabel().equals(device.getCategory())
					|| DeviceCategory.BIOMASS_ENG.getLabel().equals(device.getCategory())
							) {
					devicesProd.add(device.getName());
				}
			}
			for(int i = 0; i < nbAgents; i++) {
				float agentPower = SapereUtil.round(targetPower/nbAgents, 2);
				addServiceProducer(generateSupply(agentPower, current, new Float(60), "wind turbine " + i, DeviceCategory.WIND_ENG));
			}
		}
		if ("requested".equals(variable) || "missing".equals(variable) || "consumed".equals(variable) || "provided".equals(variable)) {
			List<Device> consumerDevices = new ArrayList<>();
			for(Device device : devices) {
				//if(DeviceCategory.ICT.getLabel().equals(device.getCategory())) {
				if(!device.isProducer()) {
					consumerDevices.add(device);
				}
			}
			for(int i = 0; i < nbAgents; i++) {
				float agentPower = SapereUtil.round(targetPower/nbAgents, 2);
				Device nextDevice = consumerDevices.get(i);
				DeviceCategory category = DeviceCategory.getByName(nextDevice.getCategory());
				addQueryConsumer(generateRequest(agentPower, current, new Float(120), new Float(120), PriorityLevel.LOW,  nextDevice.getName(),  category));
			}
		}
		try {
			Thread.sleep(10*1000);
			LearningAgent learningAgent  = getLearningAgent();
			learningAgent.refreshHistory(null);
			learningAgent.refreshMarkovChains(false);
		} catch (Exception e) {
			logger.error(e);
		}
		disableRegulation = false;

		// Add query
		return getLsas();
	}

	public boolean isAuthenticated(AgentAuthentication agentAuthentication) {
		String agentName = agentAuthentication.getAgentName();
		if(authentifiedAgentsCash.contains(agentAuthentication)) {
			return true;
		}
		String authenticationKey = agentAuthentication.getAuthenticationKey();
		boolean result = false;
		if(this.mapAgentAuthentication.containsKey(agentName)) {
			String key = (mapAgentAuthentication.get(agentName)).getAuthenticationKey();
			if(useSecuresPasswords) {
				String securedKey =  PasswordUtils.generateSecurePassword(key, salt);
				result = securedKey.equals(authenticationKey);
			} else {
				result = key.equals(authenticationKey);
			}
		}
		if(result) {
			authentifiedAgentsCash.add(agentAuthentication);
		}
		return result;
	}

	private void addServiceLearningAgent(String agentName, String scenario, Integer forcedHourOfDay) {
		if(scenario==null || scenario.length()==0) {
			throw new RuntimeException("addServiceLearningAgent : undefined scenario");
		}
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.LEARNING_AGENT);
		LearningAgent learningAgent = new LearningAgent(agentName, authentication, scenario, forcedHourOfDay);
		ServiceAgents.add(learningAgent);
		startService(agentName);
	}

	private void addServiceRegulatorAgent(String agentName) {
		AgentAuthentication authentication = generateAgentAuthentication(AgentType.REGULATOR);
		RegulatorAgent regulatorAgent = new RegulatorAgent(agentName, authentication);
		ServiceAgents.add(regulatorAgent);
		startService(agentName);
	}

	private LearningAgent getLearningAgent() {
		for(SapereAgent agent : ServiceAgents) {
			if(agent instanceof LearningAgent) {
				return (LearningAgent) agent;
			}
		}
		return null;
	}

	private RegulatorAgent getRegulatorAgent() {
		for(SapereAgent agent : ServiceAgents) {
			if(agent instanceof RegulatorAgent) {
				return (RegulatorAgent) agent;
			}
		}
		return null;
	}

	public ConsumerAgent addQueryConsumer(Float power, Date beginDate, Date endDate, Float _delayToleranceMinutes,
			PriorityLevel _priority, String deviceName, DeviceCategory deviceCategory) {
		EnergyRequest request = generateRequest(power, beginDate, endDate, _delayToleranceMinutes, _priority, deviceName, deviceCategory);
		return addQueryConsumer(request);
	}

	private ConsumerAgent addQueryConsumer(EnergyRequest need) {
		checkInitialisation();
		try {
			need.checkBeginNotPassed();
			synchronized (mapAgentAuthentication) {
				AgentAuthentication authentication = generateAgentAuthentication(AgentType.CONSUMER);
				ConsumerAgent consumerAgent = new ConsumerAgent(nextConsumerId, authentication, need);
				querys.add(consumerAgent);
				addServiceContract(consumerAgent);
				logger.info("Add new consumer " + consumerAgent.getAgentName());
				nextConsumerId++;
				return consumerAgent;
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	private ProducerAgent addServiceProducer(EnergySupply supply) {
		checkInitialisation();
		supply.checkBeginNotPassed();
		synchronized (mapAgentAuthentication) {
			AgentAuthentication authentication = generateAgentAuthentication(AgentType.PRODUCER);
			ProducerAgent producerAgent = new ProducerAgent(nextProducerId, authentication, supply);
			nextProducerId++;
			ServiceAgents.add(producerAgent);
			synchronized(producerAgent) {
				producerAgent.setInitialLSA();
			}
			SapereLogger.getInstance().info("Add new producer " + producerAgent.getAgentName());
			return producerAgent;
		}
	}

	public ProducerAgent addServiceProducer(Float power, Date beginDate, Date endDate, String deviceName, DeviceCategory deviceCategory) {
		EnergySupply supply = generateSupply(power, beginDate, endDate, deviceName, deviceCategory);
		return this.addServiceProducer(supply);
	}

	public ContractAgent addServiceContract(ConsumerAgent consumerAgent) {
		synchronized (mapAgentAuthentication) {
			AgentAuthentication authentication = generateAgentAuthentication(AgentType.CONTRACT);
			ContractAgent contractAgent = new ContractAgent(consumerAgent, authentication);
			consumerAgent.setContractAgentName(contractAgent.getAgentName());
			ServiceAgents.add(contractAgent);
			synchronized(contractAgent) {
				contractAgent.setInitialLSA();
			}
			return contractAgent;
		}
	}

	public String getContractAgentName(String consumerAgentName) {
		QueryAgent queryAgent = this.getQueryByName(consumerAgentName);
		if(queryAgent instanceof ConsumerAgent) {
			ConsumerAgent consumerAgent = (ConsumerAgent) queryAgent;
			return consumerAgent.getContractAgentName();
		}
		return null;
	}

	public String getConsumerAgentName(String contractAgentName) {
		SapereAgent agent = this.getAgent(contractAgentName);
		if(agent instanceof ContractAgent) {
			ContractAgent contractAgent = (ContractAgent) agent;
			return contractAgent.getConsumer();
		}
		return null;
	}

	public ConsumerAgent restartConsumer(String consumerAgentName, EnergyRequest need) {
		if(!(this.getQueryByName(consumerAgentName) instanceof ConsumerAgent)) {
			return null;
		}
		if(need.getIssuerLocation()==null) {
			need.setIssuerLocation(NodeManager.getLocation());
		}
		String contractAgentName = getContractAgentName(consumerAgentName);
		if (!isInSpace(consumerAgentName)) {
			logger.info("restartConsumer : restart agent " + consumerAgentName);
			// Restart consuer agent
			QueryAgent queryAgent = this.getQueryByName(consumerAgentName);
			NodeManager.instance().getNotifier().unsubscribe(consumerAgentName);
			AgentAuthentication consumerAuthentication = queryAgent.getAuthentication();
			need.checkBeginNotPassed();
			ConsumerAgent consumerAgent = (ConsumerAgent) queryAgent;
			synchronized (consumerAgent) {
				try {
					// Re-initialize consumer agent
					Integer id = consumerAgent.getId();
					consumerAgent.reinitialize(id, consumerAuthentication, need);
					if(!querys.contains(consumerAgent)) {
						querys.add(consumerAgent);
					}
					// Wait untill contract agent is in space
				} catch (Exception e) {
					logger.error(e);
				}
			}
		} else {
			logger.info("restartConsumer : " + consumerAgentName + " is already in tupple splace");
		}

		if (contractAgentName!=null) {
			if(!isInSpace(contractAgentName)) {
				// Restart contract agent
				logger.info("restartConsumer : restart agent " + contractAgentName);
				NodeManager.instance().getNotifier().unsubscribe(contractAgentName);
				ContractAgent contractAgent = (ContractAgent) this.getAgent(contractAgentName);
				synchronized (contractAgent) {
					if(debugLevel>0) {
						logger.info("ContractAgent = " + contractAgent + " memory address =" + contractAgent.hashCode());
					}
					ConsumerAgent consumerAgent = (ConsumerAgent) (getQueryByName(consumerAgentName));
					contractAgent.reinitialize(consumerAgent, contractAgent.getAuthentication());
					if(debugLevel>0) {
						logger.info("ContractAgent(2) = " + contractAgent + " memory address =" + contractAgent.hashCode());
					}
					if(!ServiceAgents.contains(contractAgent)) {
						ServiceAgents.add(contractAgent);
					}
					contractAgent.setInitialLSA();
					//Thread.sleep(5000);
				}
			} else {
				logger.info("restartConsumer : " + contractAgentName + " is already in tupple splace");
			}
		}
		// Wait untill consumer agent is in sapce
		int nbWait = 0;
		while(!isInSpace(consumerAgentName) && nbWait<30) {
			logger.info("restartConsumer : agent " + consumerAgentName + " not in sapce : Waiting ");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				logger.error(e);
			}
			nbWait++;
		}
		nbWait = 0;
		// Wait untill contract agent is in sapce
		if(contractAgentName!=null) {
			while(!isInSpace(contractAgentName) && nbWait<30) {
				logger.info("restartConsumer : agent " + contractAgentName + " not in sapce : Waiting ");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.error(e);
				}
				nbWait++;
			}
		}
		logger.info("restartConsumer : end : " + consumerAgentName + " in space = " + isInSpace(consumerAgentName)
				+ " " + contractAgentName + " in space = "  + isInSpace(contractAgentName));
		// Return consumer agent
		QueryAgent queryAgent1 = getQueryByName(consumerAgentName);
		if(queryAgent1 instanceof ConsumerAgent) {
			return (ConsumerAgent) (getQueryByName(consumerAgentName));
		}
		return null;
	}


	private void setAgentExpired(String agentName, RegulationWarning warning) {
		SapereAgent agent = this.getAgent(agentName);
		Event stopEvent = this.getStopEvent(agentName);
		if (agent instanceof ContractAgent) {
			ContractAgent contractAgent = (ContractAgent) agent;
			if(!contractAgent.hasExpired()) {
				contractAgent.setConsumerEndDate(new Date());
			}
			// Stop the consumer agent if not expired
			String consumerName = contractAgent.getConsumer();
			SapereAgent consumerAgent1 = getAgent(consumerName);
			if(consumerAgent1 instanceof ConsumerAgent) {
				setAgentExpired(consumerName, warning);
			}
		}
		if (agent instanceof ConsumerAgent) {
			ConsumerAgent consumerAgent = (ConsumerAgent) agent;
			if(!consumerAgent.hasExpired()) {
				consumerAgent.getNeed().setEndDate(new Date());
			}
		}
		if (agent instanceof ProducerAgent) {
			ProducerAgent producerAgent = (ProducerAgent) agent;
			if(!producerAgent.hasExpired()) {
				producerAgent.getGlobalSupply().setEndDate(new Date());
			}
		}
		if(stopEvent == null) {
			generateStopEvent(agentName, warning);
		}
	}

	public boolean isAgentStopped(String agentName) {
		SapereAgent agent = this.getAgent(agentName);
		boolean result = false;
		if(agent instanceof IEnergyAgent) {
			result = ((IEnergyAgent) agent).hasExpired();
		}
		return result;
	}

	public Event getStopEvent(String agentName) {
		SapereAgent agent = this.getAgent(agentName);
		Event result = null;
		if(agent instanceof IEnergyAgent) {
			result = ((IEnergyAgent) agent).getStopEvent();
		}
		return result;
	}

	public Event generateStopEvent(String agentName, RegulationWarning warning) {
		SapereAgent agent = this.getAgent(agentName);
		Event result = null;
		try {
			if(agent instanceof IEnergyAgent) {
				result = ((IEnergyAgent) agent).generateStopEvent(warning);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return result;
	}

	public SapereAgent stopAgent(String agentName, RegulationWarning warning) {
		if(!isInSpace(agentName)) {
			// Agent already out of tuple space
			setAgentExpired(agentName, warning);
			return this.getAgent(agentName);
		}
		// Use the regulator agent to send a user interruption
		SapereAgent regAgent = getAgent(regulatorAgentName);
		if(regAgent instanceof RegulatorAgent) {
			RegulatorAgent regulatorAgent = (RegulatorAgent) regAgent;
			regulatorAgent.interruptAgent(agentName);
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			logger.error(e1);
		}
		int nbWaiting=0;
		// Wait untill the agent is stopped
		while(isInSpace(agentName) &&  nbWaiting<20) {
			try {
				Thread.sleep(100);
				nbWaiting++;
			} catch (InterruptedException e) {
				logger.error(e);
			}
		}
		logger.info("stopAgent " + agentName + ": isInSpace = " + isInSpace(agentName) );
		// Wait untill the agent is stopped
		while( !isAgentStopped(agentName) &&  nbWaiting<20) {
			try {
				Thread.sleep(100);
				if(!isAgentStopped(agentName) && !isInSpace(agentName)) {
					setAgentExpired(agentName, warning);
				}
				nbWaiting++;
			} catch (InterruptedException e) {
				logger.error(e);
			}
		}
		return this.getAgent(agentName);
	}

	public EnergySupply getAgentSupply(String agentName) {
		EnergySupply result = null;
		SapereAgent agent = this.getAgent(agentName);
		if(agent instanceof ConsumerAgent) {
			result = ((ConsumerAgent) agent).getNeed();
		} else if (agent instanceof ProducerAgent) {
			result = ((ProducerAgent) agent).getGlobalSupply();
		}
		if(result!=null) {
			try {
				return result.clone();
			} catch (CloneNotSupportedException e) {
				logger.error(e);
			}
		}
		return result;
	}

	public SapereAgent modifyAgent(String agentName, EnergySupply energySupply) {
		// Use the regulator agent to send a user interruption
		if(energySupply.getIssuerLocation()==null) {
			energySupply.setIssuerLocation(NodeManager.getLocation());
		}
		SapereAgent regAgent = getAgent(regulatorAgentName);
		if(regAgent instanceof RegulatorAgent) {
			RegulatorAgent regulatorAgent = (RegulatorAgent) regAgent;
			regulatorAgent.modifyAgent(agentName, energySupply);
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		int nbWait = 0;
		EnergySupply newAgentSpply = this.getAgentSupply(agentName);
		boolean updateDone = newAgentSpply!=null && (Math.abs(newAgentSpply.getPower() - energySupply.getPower()) < 0.001);
		while(!updateDone && nbWait < 10) {
			try {
				Thread.sleep(200);
				// Refresh agent supply
				newAgentSpply = this.getAgentSupply(agentName);
				// Refresh updateD
				updateDone = newAgentSpply!=null && (Math.abs(newAgentSpply.getPower() - energySupply.getPower()) < 0.001);
				nbWait++;
			} catch (InterruptedException e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		return this.getAgent(agentName);
	}

	public PredictionData getPrediction(Date initDate, Date targetDate, String location) {
		checkInitialisation();
		LearningAgent learningAgent = this.getLearningAgent();
		//Date current = new Date();
		List<Date> targetDates = new ArrayList<Date>();
		targetDates.add(targetDate);
		PredictionData result = learningAgent.computePrediction(initDate, targetDates, location);
		//.getPrediction(targetDate);
		return result;
	}

	public ProducerAgent restartProducer(String agentName, EnergySupply supply) {
		logger.info("restartProducer " + agentName);
		if(supply.getIssuerLocation()==null) {
			supply.setIssuerLocation(NodeManager.getLocation());
		}
		if(!isInSpace(agentName)) {
			supply.checkBeginNotPassed();
			NodeManager.instance().getNotifier().unsubscribe(agentName);
			SapereAgent agent = this.getAgent(agentName);
			if (agent instanceof ProducerAgent) {
				ProducerAgent producerAgent = (ProducerAgent) agent;
				synchronized (producerAgent) {
					int id = producerAgent.getId();
					//ServiceAgents.remove(producerAgent);
					AgentAuthentication authentication = producerAgent.getAuthentication();
					producerAgent.reinitialize(id, authentication, supply);
					if(!ServiceAgents.contains(producerAgent)) {
						ServiceAgents.add(producerAgent);
					}
					producerAgent.setInitialLSA();
				}
			}
		}
		// Wait untill contract agent is in sapce
		int nbWait = 0;
		while(!isInSpace(agentName) && nbWait<30) {
			logger.info("restartProducer : agent " + agentName + " not in sapce : Waiting ");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				logger.error(e);
			}
			nbWait++;
		}
		logger.info("restartProducer : end : " + agentName + " in space = " + isInSpace(agentName));
		SapereAgent agent = this.getAgent(agentName);
		if(agent instanceof ProducerAgent) {
			return (ProducerAgent) agent;
		}
		return null;
	}

	public boolean isLocalAgent(String agentName) {
		boolean isLocal = false;
		if(mapAgentAuthentication.containsKey(agentName)) {
			AgentAuthentication authentication = this.mapAgentAuthentication.get(agentName);
			isLocal = (NodeManager.getLocation().equals(authentication.getAgentLocation()));
		}
		return isLocal;
	}

	public boolean isInSpace(String agentName) {
		boolean isLocal = isLocalAgent(agentName);
		String agentName2 = agentName + (isLocal?"":"*");
		boolean result = NodeManager.instance().getSpace().getAllLsa().containsKey(agentName2);
		if(!result) {
			//logger.info("isInSpace : for debug : " + agentName + " not in space");
		}
		return result;
	}

	public HomeContent retrieveHomeContent() {
		AgentFilter filter = new AgentFilter();
		return retrieveHomeContent(filter);
	}

	public HomeContent retrieveHomeContent(AgentFilter filter) {
		HomeContent content = new HomeContent(filter);
		Map<String, Lsa> lsaInSpace = NodeManager.instance().getSpace().getAllLsa();
		content.setNoFilter(true);
		for (SapereAgent agent : ServiceAgents) {
			if(filter.applyFilter(agent)) {
				boolean inSpace = lsaInSpace.containsKey(agent.getAgentName());
				if (agent instanceof ProducerAgent) {
					ProducerAgent producer = (ProducerAgent) agent;
					//producer.setInSpace(inSpace);
					content.addProducer(producer, inSpace);
				}
			} else {
				content.setNoFilter(false);
			}
		}
		for (QueryAgent queryAgent : querys) {
			if(filter.applyFilter(queryAgent)) {
				boolean inSpace = lsaInSpace.containsKey(queryAgent.getAgentName());
				if (queryAgent instanceof ConsumerAgent) {
					ConsumerAgent consumer = (ConsumerAgent) queryAgent;
					// consumer.getLinkedAgents();
					boolean isContractAgentInSpace = lsaInSpace.containsKey(consumer.getContractAgentName());
					//consumer.setInSpace(inSpace);
					content.addConsumer(consumer, inSpace, isContractAgentInSpace);
				}
			} else {
				content.setNoFilter(false);
			}
		}
		content.sortAgents();
		content.computeTotal();
		return content;
	}

	public HomeContent restartLastHomeContent() {
		List<ExtendedEvent> events = EnergyDbHelper.retrieveLastSessionEvents();
		Date current = new Date();
		this.checkInitialisation();
		for(Event event : events) {
			if(EventType.PRODUCTION.equals(event.getType())) {
				EnergySupply supply = generateSupply(event.getPower(), new Date(), event.getEndDate(), event.getDeviceName(), event.getDeviceCategory());
				this.addServiceProducer(supply);
			} else if (EventType.REQUEST.equals(event.getType())) {
				current = new Date();
				float _delayToleranceMinutes = SapereUtil.computeDurationMinutes(current, event.getEndDate());
				EnergyRequest request = generateRequest(event.getPower(), new Date(), event.getEndDate(), _delayToleranceMinutes
						, PriorityLevel.LOW, event.getDeviceName(), event.getDeviceCategory());
				this.addQueryConsumer(request);
			}
		}
		return this.retrieveHomeContent();
	}

	public ConsumerAgent getConsumerAgent(String agentName) {
		SapereAgent agent = getAgent(agentName);
		if(agent instanceof ConsumerAgent ) {
			ConsumerAgent consumer = (ConsumerAgent) agent;
			return consumer;
		}
		return null;
	}
	public boolean isConsumerSatified(String agentName) {
		ConsumerAgent agent = getConsumerAgent(agentName);
		if(agent!=null) {
			return agent.isSatisfied();
		}
		return false;
	}

	public String getConsumerConfirmTag(String agentName) {
		ConsumerAgent agent = getConsumerAgent(agentName);
		if(agent!=null) {
			return agent.getConfirmTag();
		}
		return null;
	}

	public boolean consumerHasConfirmed(String agentName) {
		ConsumerAgent agent = getConsumerAgent(agentName);
		if(agent!=null) {
			return agent.hasConfirmed();
		}
		return false;
	}

	public HomeTransitionMatrices getCurrentHomeTransitionMatrices() {
		LearningAgent learningAgent = getLearningAgent();
		if(learningAgent!=null) {
			return learningAgent.getHomeTransitionMatrices();
		}
		return null;
	}

	private static final Comparator<HomeTransitionMatrices> timeWindowComparator = new Comparator<HomeTransitionMatrices>() {
		public int compare(HomeTransitionMatrices trMatrix1, HomeTransitionMatrices trMatrix2) {
			return trMatrix1.getTimeWindowId() - trMatrix2.getTimeWindowId();
		}
	};

	public List<HomeTransitionMatrices> getAllHomeTransitionMatrices(MatrixFilter matrixFilter) {
		LearningAgent learningAgent = getLearningAgent();
		if(learningAgent!=null) {
			List<HomeTransitionMatrices> result = new ArrayList<HomeTransitionMatrices>();
			List<MarkovTimeWindow> listTimeWindows = new ArrayList<MarkovTimeWindow>();
			for(MarkovTimeWindow nextTimeWindow : LearningAgent.ALL_TIME_WINDOWS) {
				if(matrixFilter.applyFilter(nextTimeWindow)) {
					listTimeWindows.add(nextTimeWindow);
				}
			}
			Map<Integer,HomeTransitionMatrices> mapResult =
				EnergyDbHelper.loadListHomeTransitionMatrice(learningAgent.getAgentName(), learningAgent.getVariables()
						,matrixFilter.getLocation()
						,learningAgent.getScenario(), listTimeWindows, new Date());
			for(HomeTransitionMatrices next : mapResult.values()) {
				result.add(next);
			}
			Collections.sort(result, timeWindowComparator);
			return result;
		}
		return new ArrayList<HomeTransitionMatrices>();
	}

	public List<Device> retrieveHomeDevices() {
		List<Device> devices = EnergyDbHelper.retrieveHomeDevices((float) devicePowerCoeffProducer,
				(float) devicePowerCoeffConsumer);
		return devices;
	}

	public Map<String, Double> retrieveDeviceStatistics(DeviceFilter deviceFilter, TimeFilter timeFilter) {
		List<DeviceCategory> categories = new ArrayList<DeviceCategory>();
		if(deviceFilter.getDeviceCategories() !=null) {
			for(String sCateogry : deviceFilter.getDeviceCategories()) {
				categories.add(DeviceCategory.getByName(sCateogry));
			}
		}
		Calendar calendar = Calendar.getInstance();
		int hourOfDay = (timeFilter.getHourOfDay()==null) ? calendar.get(Calendar.HOUR_OF_DAY) : timeFilter.getHourOfDay();
		Map<String, Double>  deviceStatistics = EnergyDbHelper.retrieveDeviceStatistics(statisticPowerCoeffConsumer, statisticPowerCoeffProducer, categories, hourOfDay);
		 return deviceStatistics;
	}

	public void logProdAgents() {
		for (SapereAgent agent : ServiceAgents) {
			if(agent instanceof ProducerAgent) {
				ProducerAgent prodAgent = (ProducerAgent) agent;
				if(prodAgent.isActive()) {
					float available1 = prodAgent.computeAvailablePower(true, false);
					if(available1 > 0) {
						logger.info("Agent " + agent.getAgentName() + " has " + SapereUtil.df.format(available1) + " W");
						logger.info("  waiting request " + prodAgent.getTableWaitingRequest().keySet() + " warning request " + prodAgent.generateTableRequestWarnings().keySet());
					}
				}
			}
		}
	}

	public List<String> checkupNoContractAgent() {
		List<String>  result = new ArrayList<>();
		for (QueryAgent agent : querys) {
			if(isInSpace(agent.getAgentName()) && agent instanceof ConsumerAgent) {
				ConsumerAgent consumerAgent = (ConsumerAgent) agent;
				if(!consumerAgent.hasExpired()) {
					String contractAgentName = consumerAgent.getContractAgentName();
					if(!isInSpace(contractAgentName)) {
						result.add(agent.getAgentName());
					}
				}
			}
		}
		return result;
	}

	public void callSetInitialLSA(String agentName) {
		if(!isInSpace(agentName)) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error(e);
			}
			if(!isInSpace(agentName)) {
				SapereAgent agent = getAgent(agentName);
				synchronized (agent) {
					agent.setInitialLSA();
				}
			}
		}
	}

	public List<String> checkupNotInSpace() {
		List<String>  result = new ArrayList<>();
		for (SapereAgent agent : ServiceAgents) {
			if(!isInSpace(agent.getAgentName())) {
				boolean hasExpired = true;
				if(agent instanceof ProducerAgent) {
					hasExpired =  ((ProducerAgent) agent).hasExpired();
				} else if (agent instanceof ContractAgent) {
					hasExpired =  ((ContractAgent) agent).hasExpired();
				}
				if(!hasExpired) {
					result.add(agent.getAgentName());
				}
			}
		}
		for (QueryAgent agent : querys) {
			if(!isInSpace(agent.getAgentName())) {
				boolean hasExpired = true;
				if(agent instanceof ConsumerAgent) {
					hasExpired =  ((ConsumerAgent) agent).hasExpired();
				}
				if(!hasExpired) {
					result.add(agent.getAgentName());
				}
			}
		}
		return result;
	}

	/**
	 * Cautious : do not call this method from an agent (causes a ConcurrentModificationException)
	 * THis mtho
	 */
	public void cleanSubscriptions() {
		Map<String, Integer> nbSubscriptions = NodeManager.instance().getNotifier().getNbSubscriptionsByAgent();
		for (SapereAgent agent : ServiceAgents) {
			if(!isInSpace(agent.getAgentName()) && isAgentStopped(agent.getAgentName())) {
				if(nbSubscriptions.containsKey(agent.getAgentName())) {
					NodeManager.instance().getNotifier().unsubscribe(agent.getAgentName());
				}
			}
		}
		for (QueryAgent agent : querys) {
			if(!isInSpace(agent.getAgentName()) && isAgentStopped(agent.getAgentName())) {
				if(nbSubscriptions.containsKey(agent.getAgentName())) {
					NodeManager.instance().getNotifier().unsubscribe(agent.getAgentName());
				}
			}
		}
		int totalSubscriptions =  NodeManager.instance().getNotifier().getNbSubscriptions();
		logger.info("after cleanSubscriptions : nbSubscriptions = " + totalSubscriptions);
	}
	public void checkupProdAgents() {
		for (SapereAgent agent : ServiceAgents) {
			if(agent instanceof ProducerAgent) {
				ProducerAgent prodAgent = (ProducerAgent) agent;
				prodAgent.checkup();
			}
		}
	}

	public static boolean isDisableRegulation() {
		return disableRegulation;
	}

	/*
	 * public void movePropertiesOnLSA(String agentName) { for (Lsa lsa:
	 * NodeManager.instance().getSpace().getAllLsa().values()) {
	 * if(lsa.getAgentName().equals(agentName)) { lsa.removeAllProperties(); } } }
	 */
}
