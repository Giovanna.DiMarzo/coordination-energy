package com.sapereapi.api;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.energy.agent.ConsumerAgent;
import com.energy.agent.ProducerAgent;
import com.energy.markov.HomeMarkovStates;
import com.energy.markov.HomeTransitionMatrices;
import com.energy.markov.MarkovState;
import com.energy.model.Device;
import com.energy.model.DeviceCategory;
import com.energy.model.ExtendedHomeTotal;
import com.energy.model.OptionItem;
import com.energy.model.PriorityLevel;
import com.energy.model.RegulationWarning;
import com.energy.model.SimulatorLog;
import com.energy.model.SingleOffer;
import com.energy.model.WarningType;
import com.energy.util.DBConnection;
import com.energy.util.EnergyDbHelper;
import com.energy.util.SapereLogger;
import com.sapereapi.model.AgentFilter;
import com.sapereapi.model.AgentForm;
import com.sapereapi.model.DeviceFilter;
import com.sapereapi.model.HomeContent;
import com.sapereapi.model.InitializationForm;
import com.sapereapi.model.MatrixFilter;
import com.sapereapi.model.PredictionData;
import com.sapereapi.model.PredictionRequest;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.SapereUtil;
import com.sapereapi.model.Service;
import com.sapereapi.model.TimeFilter;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.node.NodeManager;

@RestController
@RequestMapping("/energy")
public class EnergyController {
	 @Autowired
	 private Environment environment;
	 @Autowired
	 private ConfigRepository repository;

	 @PostConstruct
	 public void init() {
	    System.out.println("environment = " + environment);
	    DBConnection.init(environment);
	    Sapere.setLocation(repository);
	 }

	private void resolveAgentsNotInsapce() {
		for(String agentName : Sapere.getInstance().checkupNotInSpace()) {
			Sapere.getInstance().callSetInitialLSA(agentName);
		}
	}

	private AgentForm generateAgentForm(SapereAgent agent) {
		boolean isInSpace = true;
		boolean isContractagentInSpace = true;
		AgentForm result = null;
		if(agent instanceof ConsumerAgent) {
			ConsumerAgent consumer = (ConsumerAgent) agent;
			isInSpace = Sapere.getInstance().isInSpace(consumer.getAgentName());
			isContractagentInSpace = Sapere.getInstance().isInSpace(consumer.getContractAgentName());
			result = new AgentForm(consumer,isInSpace,isContractagentInSpace);
		} else if(agent instanceof ProducerAgent) {
			ProducerAgent producer = (ProducerAgent) agent;
			isInSpace = Sapere.getInstance().isInSpace(producer.getAgentName());
			result = new AgentForm(producer, isInSpace);
		}
		return result;
	}


	@GetMapping(value = "/restartLastHomeContent")
	public HomeContent restartLastHomeContent() {
		return Sapere.getInstance().restartLastHomeContent();
	}

	@GetMapping(value = "/homeContent")
	public HomeContent retrieveHomeContent(AgentFilter filter) {
		return Sapere.getInstance().retrieveHomeContent(filter);
	}

	@GetMapping(value = "/homeTotalHistory")
	public List<ExtendedHomeTotal> retrieveHomeTotalHistory() {
		return EnergyDbHelper.getInstance().retrieveHomeTotalHistory();
	}

	@GetMapping(value = "/retrieveHomeDevices")
	public static List<Device> retrieveHomeDevices() {
		return Sapere.getInstance().retrieveHomeDevices();
	}

	@GetMapping(value = "/retrieveDeviceStatistics")
	public Map<String, Double> retrieveDeviceStatistics(TimeFilter timeFilter) {
		DeviceFilter deviceFilter = new DeviceFilter();
		return Sapere.getInstance().retrieveDeviceStatistics(deviceFilter, timeFilter);
	}

	@GetMapping(value = "/currentHomeTransitionMatrices")
	public HomeTransitionMatrices retrieveCurrentHomeTransitionMatrices() {
		return Sapere.getInstance().getCurrentHomeTransitionMatrices();
	}

	@GetMapping(value = "/getLocations")
	public List<OptionItem> getLocations() {
		return Sapere.getInstance().getLocations();
	}

	@GetMapping(value = "/allHomeTransitionMatrices")
	public List<HomeTransitionMatrices> retrieveAllHomeTransitionMatrices(MatrixFilter matrixFilter) {
		if("".equals(matrixFilter.getLocation())) {
			// Default location : localhost
			matrixFilter.setLocation(NodeManager.getLocation());
		}
		return Sapere.getInstance().getAllHomeTransitionMatrices(matrixFilter);
	}

	@PostMapping(value = "/initEnergyService")
	public InitializationForm initEnergyService(@RequestBody InitializationForm initForm) {
		SapereLogger.getInstance().info("initEnergyService : scenario = " +  initForm.getScenario());
		Sapere.getInstance().initEnergyService(repository, initForm.getScenario(), initForm.getForcedHourOfDay());
		String stateVariable = initForm.getInitialStateVariable();
		Integer stateId = initForm.getInitialStateId();
		if(stateId!=null && stateVariable!=null && !"".equals(stateVariable)) {
			MarkovState targetState = HomeMarkovStates.getById(stateId);
			if(targetState!=null) {
				Sapere.getInstance().initState(repository, stateVariable, targetState);
			}
		}
		return initForm;
	}

	@PostMapping(value = "/getPrediction")
	public PredictionData getPrediction(@RequestBody PredictionRequest predictionRequest) {
		Date initDate = predictionRequest.getInitDate();
		Date targetDate = predictionRequest.getTargetDate();
		String location = predictionRequest.getLocation();
		SapereLogger.getInstance().info("targetDate = " + targetDate);
		PredictionData prediction = Sapere.getInstance().getPrediction(initDate, targetDate, location);
		return prediction;
	}

	@PostMapping(value = "/retrieveOffers")
	public List<SingleOffer> retrieveOffers(@RequestBody ExtendedHomeTotal homeTotalHistory) {
		SapereLogger.getInstance().info("retrieveOffers : homeTotalHistory = " + homeTotalHistory);
		return EnergyDbHelper.retrieveOffers(homeTotalHistory.getDate(), homeTotalHistory.getDateNext(), null, null, null);
	}

	@PostMapping(value = "/addAgent")
	public AgentForm addAgent(@RequestBody AgentForm agentForm) {
		resolveAgentsNotInsapce();
		AgentForm result = null;
		Date current = new Date();
		if(agentForm.getEndDate()!=null &&  agentForm.getEndDate().before(current)) {
			SapereLogger.getInstance().warning("#### addAgent : enddate = " + agentForm.getEndDate());
		}
		if(!SapereUtil.checkRound(agentForm.getPower(), 2)) {
			SapereLogger.getInstance().warning("#### addAgent : power = " + agentForm.getPower());
			agentForm.setPower(SapereUtil.round(agentForm.getPower(), 2));
		}
		DeviceCategory deviceCategory = DeviceCategory.getByName(agentForm.getDeviceCategoryCode());
		try {
			SapereAgent newAgent = null;
			if(agentForm.isConsumer()) {
				PriorityLevel priority = PriorityLevel.getByLabel(agentForm.getPriorityLevel());
				float delayToleranceMinutes = agentForm.getDelayToleranceMinutes();
				if(delayToleranceMinutes == 0  && agentForm.getDelayToleranceRatio() != null) {
					delayToleranceMinutes = agentForm.getDelayToleranceRatio() * SapereUtil.computeDurationMinutes(agentForm.getBeginDate(), agentForm.getEndDate());
				}
				newAgent = Sapere.getInstance().addQueryConsumer(agentForm.getPower(), agentForm.getBeginDate(),
						agentForm.getEndDate(), delayToleranceMinutes, priority, agentForm.getDeviceName(), deviceCategory);
			} else if (agentForm.isProducer()) {
				newAgent = Sapere.getInstance().addServiceProducer(agentForm.getPower(), agentForm.getBeginDate(),
						agentForm.getEndDate(), agentForm.getDeviceName(), deviceCategory);
			}
			if(newAgent!=null) {
				Thread.sleep(1*1000);
				result = generateAgentForm(newAgent);
			}
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		/*
		if(false && result!=null &&!result.checkInSpace()) {
			resolveAgentsNotInsapce();
		}*/
		return result;
	}

	@PostMapping(value = "/stopAgent")
	public AgentForm stopAgent(@RequestBody AgentForm agentForm) {
		resolveAgentsNotInsapce();
		AgentForm result = null;
		RegulationWarning warning = new RegulationWarning(WarningType.USER_INTERRUPTION, new Date());
		warning.addAgent(agentForm.getAgentName());
		try {
			SapereAgent agent = Sapere.getInstance().stopAgent(agentForm.getAgentName(), warning);
			result = generateAgentForm(agent);
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		Sapere.getInstance().cleanSubscriptions();
		return result;
	}


	@PostMapping(value = "/modifyAgent")
	public AgentForm modifyAgent(@RequestBody AgentForm agentForm) {
		resolveAgentsNotInsapce();
		AgentForm result =  null;
		try {
			if(!SapereUtil.checkRound(agentForm.getPower(), 2)) {
				SapereLogger.getInstance().warning("#### modifyAgent : power = " + agentForm.getPower());
				agentForm.setPower(SapereUtil.round(agentForm.getPower(),2));
			}
			SapereAgent agent = Sapere.getInstance().modifyAgent(agentForm.getAgentName(), agentForm.getEnergyRequest());
			result = generateAgentForm(agent);
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		/*
		if(false && result!=null &&!result.checkInSpace()) {
			resolveAgentsNotInsapce();
		}*/
		return result;
	}

	@PostMapping(value = "/restartAgent")
	public AgentForm restartAgent(@RequestBody AgentForm agentForm) {
		resolveAgentsNotInsapce();
		AgentForm result = null;
		Date current = new Date();
		if( agentForm.getEndDate()!=null &&  agentForm.getEndDate().before(current)) {
			SapereLogger.getInstance().warning("#### restartAgent : endadate = " + agentForm.getEndDate());
		}
		if(!SapereUtil.checkRound(agentForm.getPower(), 2)) {
			SapereLogger.getInstance().warning("#### restartAgent : power = " + agentForm.getPower());
			agentForm.setPower(SapereUtil.round(agentForm.getPower(),2));
		}
		try {
			SapereAgent agent = null;
			if (agentForm.isConsumer()) {
				// Restart consumer agent
				agent = Sapere.getInstance().restartConsumer(agentForm.getAgentName(), agentForm.getEnergyRequest());
			} else if (agentForm.isProducer()) {
				// Restart producer agent
				agent = Sapere.getInstance().restartProducer(agentForm.getAgentName(), agentForm.getEnergySupply());
			}
			if(agent!=null) {
				result = generateAgentForm(agent);
			}
		} catch (Exception e) {
			SapereLogger.getInstance().error(e);
		}
		/*
		if(false && result!=null &&!result.checkInSpace()) {
			resolveAgentsNotInsapce();
		}*/
		return result;
	}

	@PostMapping(value = "/resetSimulatorLogs")
	public void resetSimulatorLogs() {
		EnergyDbHelper.resetSimulatorLogs();
	}

	@PostMapping(value = "/addSimulatorLog")
	public void addSimulatorLog(@RequestBody SimulatorLog simulatorLog) {
		EnergyDbHelper.registerSimulatorLog(simulatorLog);
	}

	@GetMapping(path = "/test1")
	public @ResponseBody Iterable<Service> test1() {
		return Sapere.getInstance().test1bis(repository);
	}

	@GetMapping(path = "/test2")
	public @ResponseBody Iterable<Service> test2() {
		return Sapere.getInstance().test2(repository);
	}

	@GetMapping(path = "/test3")
	public @ResponseBody Iterable<Service> test3() {
		return Sapere.getInstance().test3(repository);
	}

	@GetMapping(path = "/test4")
	public @ResponseBody Iterable<Service> test4() {
		return Sapere.getInstance().test4(repository);
	}

	@GetMapping(path = "/test5")
	public @ResponseBody Iterable<Service> test5() {
		return Sapere.getInstance().test5(repository);
	}
}
