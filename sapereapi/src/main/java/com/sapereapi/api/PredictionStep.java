package com.sapereapi.api;

import java.util.Date;

import com.energy.markov.MarkovTimeWindow;
import com.sapereapi.model.SapereUtil;

public class PredictionStep {
	private MarkovTimeWindow markovTimeWindow;
	private Date startDate;
	private Date endDate;

	public PredictionStep(MarkovTimeWindow _markovTimeWindow, Date startDate, Date endDate) {
		super();
		this.markovTimeWindow = _markovTimeWindow;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public MarkovTimeWindow getMarkovTimeWindow() {
		return markovTimeWindow;
	}

	public void setMarkovTimeWindow(MarkovTimeWindow _markovTimeWindow) {
		this.markovTimeWindow = _markovTimeWindow;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getMarovTimeWindowId() {
		if (markovTimeWindow != null) {
			return markovTimeWindow.getId();
		}
		return -1;
	}

	public boolean isInSlot(Date aDate) {
		return (!aDate.before(startDate)) && aDate.before(this.endDate);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer result = new StringBuffer();
		result.append(SapereUtil.format_time.format(startDate))
			.append("-")
			.append(SapereUtil.format_time.format(endDate))
		;
		if(this.markovTimeWindow!=null) {
			result.append("(MTW:").append(this.markovTimeWindow.getId()).append(")");
		}
		return result.toString();
	}
}
